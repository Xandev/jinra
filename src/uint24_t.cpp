#include "Jinra.h"
#include "uint24_t.h"

namespace Jinra
{

uint24_t::uint24_t(const uint24_t& a)
	: val(a.val)
{ }

uint24_t::uint24_t(const u32& a) 
	: val(a)
{
	val &= 0x00FFFFFF;
}

uint24_t uint24_t::operator++() 
{
	++val;
	val &= 0x00FFFFFF;
	return *this;
}

uint24_t uint24_t::operator--() 
{
	--val;
	val &= 0x00FFFFFF;
	return *this;
}

uint24_t uint24_t::operator++(s32) 
{
	uint24_t temp(val);
	++val;
	val &= 0x00FFFFFF;
	return temp;
}

uint24_t uint24_t::operator--(s32) 
{
	uint24_t temp(val);
	--val;
	val &= 0x00FFFFFF;
	return temp;
}

uint24_t uint24_t::operator&(const uint24_t& a) {
	return uint24_t(val & a.val);
}

uint24_t& uint24_t::operator=(const uint24_t& a) 
{
	val = a.val; 
	return *this;
}

uint24_t& uint24_t::operator+=(const uint24_t& a) 
{
	val += a.val; 
	val &= 0x00FFFFFF; 
	return *this;
}

uint24_t& uint24_t::operator-=(const uint24_t& a) 
{
	val -= a.val; 
	val &= 0x00FFFFFF; 
	return *this;
}

bool uint24_t::operator==(const uint24_t& right) const {
	return val == right.val;
}

bool uint24_t::operator!=(const uint24_t& right) const {
	return val != right.val;
}

bool uint24_t::operator>(const uint24_t& right) const {
	return val > right.val;
}

bool uint24_t::operator<(const uint24_t& right) const {
	return val < right.val;
}

const uint24_t uint24_t::operator+(const uint24_t& other) const {
	return uint24_t(val + other.val);
}

const uint24_t uint24_t::operator-(const uint24_t& other) const {
	return uint24_t(val - other.val);
}

const uint24_t uint24_t::operator/(const uint24_t& other) const {
	return uint24_t(val / other.val);
}

const uint24_t uint24_t::operator*(const uint24_t& other) const {
	return uint24_t(val * other.val);
}

uint24_t uint24_t::operator&(const u32& a) {
	return uint24_t(val & a);
}

uint24_t& uint24_t::operator=(const u32& a) 
{
	val = a;
	val &= 0x00FFFFFF;
	return *this;
}

uint24_t& uint24_t::operator+=(const u32& a) 
{
	val += a;
	val &= 0x00FFFFFF;
	return *this;
}

uint24_t& uint24_t::operator-=(const u32& a) 
{
	val -= a;
	val &= 0x00FFFFFF;
	return *this;
}

bool uint24_t::operator==(const u32& right) const {
	return val == (right & 0x00FFFFFF);
}

bool uint24_t::operator!=(const u32& right) const {
	return val != (right & 0x00FFFFFF);
}

bool uint24_t::operator>(const u32& right) const {
	return val > (right & 0x00FFFFFF);
}

bool uint24_t::operator<(const u32& right) const {
	return val < (right & 0x00FFFFFF);
}

uint24_t uint24_t::operator+(const u32& other) const { 
	return uint24_t(val + other); 
}

uint24_t uint24_t::operator-(const u32& other) const { 
	return uint24_t(val - other); 
}

uint24_t uint24_t::operator/(const u32& other) const { 
	return uint24_t(val / other); 
}

uint24_t uint24_t::operator*(const u32& other) const { 
	return uint24_t(val * other);
}

uint24_t uint24_t::operator+(const ulong &other) const {
	return uint24_t(val + other);
}

} // namespace Jinra