#include "Jinra.h"
#include "SplitPacketChannel.h"

namespace Jinra
{

s32 splitPacketChannelComp(SplitPacketIdType const& key, SplitPacketChannel* const& data)
{
	if (key < data->splitPacketList.getPacketID())
		return -1;

	if (key == data->splitPacketList.getPacketID())
		return 0;

	return 1;
}


} // namespace Jinra