#include "Jinra.h"
#include "NetTask.h"

namespace Jinra
{

const auto SYSTEM_TIME_ZERO = std::chrono::system_clock::time_point(std::chrono::milliseconds(0));

NetTask::NetTask(std::function<void()> f)
    : _func(move(f)), _expiration(SYSTEM_TIME_ZERO)
{

}

NetTask::NetTask(std::function<void()> f, u32 delay)
    : _func(move(f)), 
      _expiration(std::chrono::system_clock::now() + std::chrono::milliseconds(delay))
{}

void NetTask::operator()() const
{
    _func();
}

void NetTask::setDontExpire()
{
    _expiration = SYSTEM_TIME_ZERO;
}

bool NetTask::hasExpired() const
{
    return _expiration == SYSTEM_TIME_ZERO ? 
           false : _expiration < std::chrono::system_clock::now();
}

}