#include "Jinra.h"

#if !defined(_WIN32)

#include "LinuxSocket2.h"
#include "SocketDefines.h"

namespace Jinra
{

NetSocket2::BindResult LinuxSocket2::bind(BerkleySocket2::BindParameters* bindParameters) { 
	return bindShared(bindParameters); 
}

RNS2SendResult LinuxSocket2::send(NetSocket2::SendParameters* sendParameters) {
	s32 len = 0;

	do {
		s32 oldTTL = -1;
		if (sendParameters->ttl > 0) {
			socklen_t opLen = sizeof(oldTTL);
			// Get the current TTL
			if (::getsockopt(_rns2Socket, sendParameters->systemAddress.getIPPROTO(), IP_TTL, 
				(char*)&oldTTL, &opLen) != -1) {
				s32 newTTL = sendParameters->ttl;
				::setsockopt(_rns2Socket, sendParameters->systemAddress.getIPPROTO(), IP_TTL, 
							 (char*)& newTTL, sizeof(newTTL));
			}
		}

		if (sendParameters->systemAddress.address.addr4.sin_family == AF_INET) {
			len = ::sendto(_rns2Socket, sendParameters->data, sendParameters->length, 0,
						   (const sockaddr*)&sendParameters->systemAddress.address.addr4,
						   sizeof(sockaddr_in));
		} else {
#if defined(JINRA_SUPPORT_IPV6)
			len = ::sendto(_rns2Socket, sendParameters->data, sendParameters->length, 0,
						   (const sockaddr*)&sendParameters->systemAddress.address.addr6,
						   sizeof(sockaddr_in6));
#endif
		}

		if (len < 0) {
            printf("sendto failed with code %i for char %i and length %i.\n", len,
							   sendParameters->data[0], sendParameters->length);
		}

		if (oldTTL != -1) {
			::setsockopt(_rns2Socket, sendParameters->systemAddress.getIPPROTO(), IP_TTL,
						 (char*)&oldTTL, sizeof(oldTTL));
		}
	} while (len == 0);

	return len;
}

void LinuxSocket2::getMyIP(NetAddress addresses[MAXIMUM_NUMBER_OF_INTERNAL_IDS])
{ 
#if defined(JINRA_SUPPORT_IPV6)
	getMyIPIPV4And6(addresses);
#else // defined(JINRA_SUPPORT_IPV6)
	getMyIPIPV4(addresses);
#endif // defined(JINRA_SUPPORT_IPV6)
}

void LinuxSocket2::getMyIPIPV4(NetAddress addresses[MAXIMUM_NUMBER_OF_INTERNAL_IDS])
{
	char ac[80];
	s32 err = gethostname(ac, sizeof(ac));
	ASSERT(err != -1);

	struct hostent* phe = gethostbyname(ac);
	if (phe == nullptr) {
		ASSERT(phe != nullptr);
		return;
	}

	s32 idx;
	for (idx = 0; idx < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++idx) {
		if (phe->h_addr_list[idx] == nullptr)
			break;

		memcpy(&addresses[idx].address.addr4.sin_addr, phe->h_addr_list[idx],
			   sizeof(struct in_addr));
	}

	while (idx < MAXIMUM_NUMBER_OF_INTERNAL_IDS) {
		addresses[idx] = UNASSIGNED_SYSTEM_ADDRESS;
		++idx;
	}

}

#if defined(JINRA_SUPPORT_IPV6)

void PrepareAddrInfoHints2(addrinfo* hints)
{
	memset(hints, 0, sizeof(addrinfo)); // make sure the struct is empty
	hints->ai_socktype = SOCK_DGRAM; // UDP sockets
	hints->ai_flags = AI_PASSIVE;     // fill in my IP for me
}

void LinuxSocket2::getMyIPIPV4And6(NetAddress addresses[MAXIMUM_NUMBER_OF_INTERNAL_IDS])
{
	s32 idx = 0;
	char ac[80];
	s32 err = gethostname(ac, sizeof(ac));
	ASSERT(err != -1);

	struct addrinfo hints;
	struct addrinfo* servinfo = 0, *aip;  // will point to the results
	PrepareAddrInfoHints2(&hints);
	getaddrinfo(ac, "", &hints, &servinfo);

	for (idx = 0, aip = servinfo; aip != nullptr && idx < MAXIMUM_NUMBER_OF_INTERNAL_IDS; aip = aip->ai_next, idx++) {
		if (aip->ai_family == AF_INET) {
			struct sockaddr_in *ipv4 = (struct sockaddr_in *)aip->ai_addr;
			memcpy(&addresses[idx].address.addr4, ipv4, sizeof(sockaddr_in));
		} else {
			struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)aip->ai_addr;
			memcpy(&addresses[idx].address.addr4, ipv6, sizeof(sockaddr_in6));
		}

	}

	freeaddrinfo(servinfo); // free the linked-list

	while (idx < MAXIMUM_NUMBER_OF_INTERNAL_IDS) {
		addresses[idx] = UNASSIGNED_SYSTEM_ADDRESS;
		idx++;
	}
}

#endif // defined(JINRA_SUPPORT_IPV6)

} // namespace Jinra

#endif // !defined(_WIN32)