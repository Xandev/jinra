#include "Jinra.h"
#include "WSAStartupSingleton.h"

bool WSAStartupSingleton::_initialized = false;

void WSAStartupSingleton::initialize()
{
	if (_initialized)
		return;

#if defined(_WIN32)
	WSADATA winsockInfo;
	if (WSAStartup(MAKEWORD(2, 2), &winsockInfo) != 0) {
#if defined(JINRA_DEBUG)
		auto error = GetLastError();
		LPVOID messageBuffer;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | 
					  FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, error,
					  MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT), 
					  reinterpret_cast<LPTSTR>(&messageBuffer), 0, nullptr);
        printf("WSAStartup failed\nError code: %d\n%s", error, static_cast<cchar*>(messageBuffer));
		
		//Free the buffer.
		LocalFree(messageBuffer);
#endif // defined(JINRA_DEBUG)
	}
	_initialized = true;
#else // defined(_WIN32)
	_initialized = false;
#endif // defined(_WIN32)
}

void WSAStartupSingleton::release()
{
#if defined(_WIN32)
	if (!_initialized)
		return;
	WSACleanup();
	_initialized = false;
#endif // defined(_WIN32)
}
