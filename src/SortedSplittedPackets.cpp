#include "Jinra.h"
#include "SortedSplittedPackets.h"

namespace Jinra
{

SortedSplittedPackets::SortedSplittedPackets()
    : _data(nullptr), _allocationSize(0u), _addedPacketsCount(0u), _packetId(0u)
{
}

SortedSplittedPackets::~SortedSplittedPackets()
{
	if (_allocationSize > 0u)
		delete[] _data;
}

void SortedSplittedPackets::preallocate(InternalPacket* internalPacket)
{
	ASSERT(_data == nullptr);

	_allocationSize = internalPacket->splitPacketCount;
	_data = new InternalPacket*[_allocationSize];
	_packetId = internalPacket->splitPacketId;

	for (u32 i = 0u; i < _allocationSize; ++i)
		_data[i] = nullptr;
}

bool SortedSplittedPackets::add(InternalPacket* internalPacket)
{
	ASSERT(_data != nullptr);
	ASSERT(internalPacket->splitPacketIndex < _allocationSize);
	ASSERT(_packetId == internalPacket->splitPacketId);
	ASSERT(_data[internalPacket->splitPacketIndex] == nullptr);

	if (_data[internalPacket->splitPacketIndex] == nullptr) {
		_data[internalPacket->splitPacketIndex] = internalPacket;
		++_addedPacketsCount;
		return true;
	}

	return false;
}

InternalPacket* SortedSplittedPackets::get(u32 index) const
{
	ASSERT(_data != nullptr);
	ASSERT(index < _allocationSize);
	return _data[index];
}

SplitPacketIdType SortedSplittedPackets::getPacketID() const
{
	ASSERT(_data != nullptr);
	return _packetId;
}

} // namespace Jinra