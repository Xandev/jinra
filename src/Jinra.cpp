#include "Jinra.h"

namespace Jinra
{

void defaultOutOfMemoryHandler(cchar* file, const long line) {
	(void)file;
	(void)line;
	ASSERT(0);
}

void(*notifyOutOfMemory)(cchar *file, const long line) = defaultOutOfMemoryHandler;

void setNotifyOutOfMemory(void(*userFunction)(cchar *file, const long line)) {
	notifyOutOfMemory = userFunction;
}

bool ipAddressMatch(const String& string, cchar* IP)
{
	if (IP == nullptr || IP[0] == 0 || strlen(IP) > 15)
		return false;

	auto characterIndex = 0u;

	while (true) {
		if (string[characterIndex] == IP[characterIndex]) {
			// Equal characters
			if (IP[characterIndex] == 0) {
				// End of the string and the strings match
				return true;
			}
			++characterIndex;
		} else {
			if (string[characterIndex] == 0 || IP[characterIndex] == 0) {
				// End of one of the strings
				break;
			}

			// Characters do not match
			if (string[characterIndex] == '*') {
				// Domain is banned.
				return true;
			}

			// Characters do not match and it is not a *
			break;
		}
	}

	// No match found.
	return false;
}

} // namespace Jinra