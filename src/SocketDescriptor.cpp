#include "Jinra.h"
#include "SocketDescriptor.h"

namespace Jinra
{

SocketDescriptor::SocketDescriptor() 
	: port(0u), socketFamily(AF_INET), blockingSocket(true)
{
	hostAddress[0] = 0;
}

SocketDescriptor::SocketDescriptor(u16 port, cchar* hostAddress)
	: port(port), socketFamily(AF_INET), blockingSocket(true)
{
	if (hostAddress != nullptr) {
		strcpy(this->hostAddress, hostAddress);
	} else {
		this->hostAddress[0] = 0;
	}
}

} // namespace Jinra