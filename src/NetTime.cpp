#include "Jinra.h"
#include <thread>

using namespace std;
using namespace chrono;

namespace Jinra
{

TimeMS Time::nowMS()
{
    return static_cast<TimeMS>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
}

TimeNS Time::nowNS()
{
    return static_cast<TimeNS>(duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());
}

TimeUS Time::nowUS()
{
    return static_cast<TimeUS>(duration_cast<microseconds>(
        system_clock::now().time_since_epoch()).count());
}

TimeMS Time::nsToMS(TimeNS time)
{
    return static_cast<TimeMS>(duration_cast<milliseconds>(nanoseconds(time)).count());
}

double Time::nsToMS(TimeNS time, byte precise)
{
    double value = time / 1000000.0;
    return ceil(value * pow(10, precise)) / pow(10, precise);
}

TimeNS Time::msToNS(TimeMS time)
{
    return static_cast<TimeNS>(duration_cast<nanoseconds>(milliseconds(time)).count());
}

void Time::sleepMS(TimeMS time)
{
    this_thread::sleep_for(milliseconds(time));
}

} // namespace Jinra