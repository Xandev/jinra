#include "Jinra.h"
#include "Itoa.h"

namespace Jinra
{

extern "C"
{

char* itoa(s32 value, char* result, s32 base)
{
	// Check that the base if valid
	if (base < 2 || base > 16) {
		*result = 0;
		return result;
	}

	auto out = result;
	s32 quotient = value;

	do {
		s32 absQModB = quotient % base;
		if (absQModB < 0)
			absQModB = -absQModB;
		*out = "0123456789abcdef"[absQModB];
		++out;
		quotient /= base;
	} while (quotient);

	// Only apply negative sign for base 10
	if (value < 0 && base == 10)
		*out++ = '-';

	*out = 0;

	auto start = result;
	--out;
	while (start < out) {
        char temp = *start;
		*start = *out;
		*out = temp;
		++start;
		--out;
	}
	return result;
}

} // extern "C"

} // namespace Jinra