﻿#include "Jinra.h"
#include "NetGuidGenerator.h"

namespace Jinra
{

u64 NetGuidGenerator::get64BitUniqueRandomNumber()
{
#if defined(_WIN32)
    auto id = Time::nowUS();

    // Sleep a small random time, then use the last 4 bits as a source of randomness.
    for (s32 i = 0; i < 8; ++i) {
        auto lastTime = Time::nowUS();
        Time::sleepMS(1);
        Time::sleepMS(0);
        auto diff4Bits = static_cast<u32>(Time::nowUS() - lastTime & 15);
        diff4Bits <<= 32 - 4;
        diff4Bits >>= i * 4;
        reinterpret_cast<char*>(&id)[i] ^= diff4Bits;
    }

    return id;
#else // defined(_WIN32)
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return tv.tv_usec + tv.tv_sec * 1000000;
#endif // defined(_WIN32)
}

} // namespace Jinra