#include "Jinra.h"

#include "BerkleySocket2.h"
#include "SocketDefines.h"
#include "Itoa.h"
#include "NetTime.h"

namespace Jinra
{

#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif

BerkleySocket2::BerkleySocket2()
    : _rns2Socket(static_cast<Socket2>(INVALID_SOCKET)), _endThreads(false)
{ }

BerkleySocket2::~BerkleySocket2()
{
	if (_rns2Socket != INVALID_SOCKET)
		closesocket__(_rns2Socket);
}

s32 BerkleySocket2::createRecvPollingThread(s32 threadPriority)
{
	_endThreads = false;
	return NetThread::create(recvFromLoop, this, threadPriority);
}

void BerkleySocket2::signalStopRecvPollingThread() {
	_endThreads = true;
}

void BerkleySocket2::blockOnStopRecvPollingThread()
{
	_endThreads = true;

	// Get recvfrom to unblock
	SendParameters bsp;
	ulong zero = 0;
	bsp.data = reinterpret_cast<char*>(&zero);
	bsp.length = 4;
	bsp.systemAddress = _boundAddress;
	bsp.ttl = 0;
	send(&bsp);

	auto timeout = Time::nowMS() + 1000u;
	while (_isRecvFromLoopThreadActive > 0u && Time::nowMS() < timeout) {
		// Get recvfrom to unblock
		send(&bsp);
        Time::sleepMS(30);
	}
}

bool BerkleySocket2::isPortInUse(u16 port, cchar* hostAddress, u16 addressFamily, s32 type)
{
	BindParameters bbp;
	bbp.port = port;
	bbp.hostAddress = const_cast<char*>(hostAddress);
	bbp.addressFamily = addressFamily;
	bbp.type = type;
	bbp.protocol = 0;
	bbp.nonBlockingSocket = false;
	bbp.setBroadcast = false;
	bbp.doNotFragment = false;
	bbp.protocol = 0;
	bbp.setIPHdrIncl = false;

	auto rns2 = static_cast<BerkleySocket2*>(create());
	auto bindResult = rns2->bind(&bbp);
	delete rns2;
	return bindResult == BindResult::FAILED_TO_BIND_SOCKET;
}

void BerkleySocket2::setDoNotFragment(s32 opt)
{
#if defined(IP_DONTFRAGMENT)
#if defined(_WIN32) && !defined(JINRA_DEBUG)
	// If this assert hit you improperly linked against WSock32.h
	ASSERT(IP_DONTFRAGMENT == 14);
#endif // defined(_WIN32) && !defined(JINRA_DEBUG)
	setsockopt(_rns2Socket, _boundAddress.getIPPROTO(), IP_DONTFRAGMENT, 
				 reinterpret_cast<char*>(&opt), sizeof opt);
#endif // defined(IP_DONTFRAGMENT)
}

NetSocket2::BindResult BerkleySocket2::bindShared(BindParameters* bindParameters)
{
#if !defined(JINRA_SUPPORT_IPV6)
	auto bindResult = bindSharedIPV4(bindParameters);
#else // !defined(JINRA_SUPPORT_IPV6)
	auto bindResult = bindSharedIPV4And6(bindParameters);
#endif // !defined(JINRA_SUPPORT_IPV6)

	if (bindResult != BindResult::SUCCESS)
		return bindResult;

	ulong zero = 0;
    SendParameters bsp;
	bsp.data = reinterpret_cast<char*>(&zero);
	bsp.length = 4;
	bsp.systemAddress = _boundAddress;
	bsp.ttl = 0;
	if (send(&bsp) < 0)
		return BindResult::FAILED_SEND_TEST;

	memcpy(&_binding, bindParameters, sizeof(BindParameters));

	return bindResult;
}

void BerkleySocket2::recvFromBlocking(RecvStruct* recvFromStruct)
{
#if defined(JINRA_SUPPORT_IPV6)
	return recvFromBlockingIPV4And6(recvFromStruct);
#else // defined(JINRA_SUPPORT_IPV6)
	return recvFromBlockingIPV4(recvFromStruct);
#endif // defined(JINRA_SUPPORT_IPV6)
}

JINRA_THREAD_DECLARATION(BerkleySocket2::recvFromLoop)
{
	static_cast<BerkleySocket2*>(arguments)->recvFromLoopInt();
	return 0;
}

u32 BerkleySocket2::recvFromLoopInt()
{
	++_isRecvFromLoopThreadActive;

	while (!_endThreads) {
		auto recvFromStruct = _binding.eventHandler->allocRNS2RecvStruct();
		if (recvFromStruct != nullptr) {
			recvFromStruct->socket = this;
			recvFromBlocking(recvFromStruct);

			if (recvFromStruct->bytesRead > 0) {
				ASSERT(recvFromStruct->systemAddress.getPort());
				_binding.eventHandler->onRNS2Recv(recvFromStruct);
			} else {
                Time::sleepMS(0);
				_binding.eventHandler->deallocRNS2RecvStruct(recvFromStruct);
			}
		}
	}
	--_isRecvFromLoopThreadActive;

	return 0;

}

void BerkleySocket2::setSocketOptions()
{
	// This doubles the max throughput rate.
	auto sock_opt = 1024 * 256;
	s32 result = setsockopt(_rns2Socket, SOL_SOCKET, SO_RCVBUF, 
							   reinterpret_cast<char*>(&sock_opt), sizeof sock_opt);
	ASSERT(result == 0);

	// Immediate hard close. Don't linger the socket, or recreating the socket quickly on 
	// Vista fails.
	sock_opt = 0;
	setsockopt(_rns2Socket, SOL_SOCKET, SO_LINGER, reinterpret_cast<char*>(&sock_opt), 
                 sizeof sock_opt);
	// Do not assert, ignore failure.

	// This doesn't make much difference: 10% maybe. Not supported on console 2.
	sock_opt = 1024 * 16;
	result = setsockopt(_rns2Socket, SOL_SOCKET, SO_SNDBUF, reinterpret_cast<char*>(&sock_opt), 
						  sizeof sock_opt);
	ASSERT(result == 0);

}

void BerkleySocket2::setNonBlockingSocket(ulong nonBlocking)
{
#if defined(_WIN32)
	auto result = ioctlsocket(_rns2Socket, FIONBIO, &nonBlocking);
	ASSERT(result == 0);
#else // defined(_WIN32)
	if (nonBlocking)
		fcntl(_rns2Socket, F_SETFL, O_NONBLOCK);
#endif // defined(_WIN32)
}

void BerkleySocket2::setBroadcastSocket(s32 broadcast) {
	setsockopt(_rns2Socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char*>(&broadcast), 
				 sizeof broadcast);
}

void BerkleySocket2::setIPHdrIncl(s32 ipHdrIncl) {
	setsockopt(_rns2Socket, IPPROTO_IP, IP_HDRINCL, reinterpret_cast<char*>(&ipHdrIncl), 
				 sizeof ipHdrIncl);
}

NetSocket2::BindResult BerkleySocket2::bindSharedIPV4(BindParameters* bindParameters) 
{
	memset(&_boundAddress.address.addr4, 0, sizeof(sockaddr_in));
	_boundAddress.address.addr4.sin_port = htons(bindParameters->port);
	_rns2Socket = static_cast<s32>(socket(bindParameters->addressFamily, bindParameters->type, 
		bindParameters->protocol));
	if (_rns2Socket == -1)
		return BindResult::FAILED_TO_BIND_SOCKET;

	setSocketOptions();
	setNonBlockingSocket(bindParameters->nonBlockingSocket);
	setBroadcastSocket(bindParameters->setBroadcast);
	setIPHdrIncl(bindParameters->setIPHdrIncl);

	// Fill in the rest of the address structure.
	_boundAddress.address.addr4.sin_family = AF_INET;

	if (bindParameters->hostAddress && bindParameters->hostAddress[0]) {
		_boundAddress.address.addr4.sin_addr.s_addr = inet_addr(bindParameters->hostAddress);
	} else {
		_boundAddress.address.addr4.sin_addr.s_addr = INADDR_ANY;
	}

	// bind our name to the socket
	s32 ret = ::bind(_rns2Socket, reinterpret_cast<struct sockaddr*>(&_boundAddress.address.addr4), 
				 sizeof _boundAddress.address.addr4);
	if (ret <= -1) {
#if defined(_WIN32)
		closesocket__(_rns2Socket);
#elif (defined(__GNUC__) || defined(__GCCXML__) ) && !defined(_WIN32)
		closesocket__(_rns2Socket);
		switch (ret) {
			case EBADF:
                printf("bind__(): sockfd is not a valid descriptor.\n");
				break;

			case ENOTSOCK:
                printf("bind__(): Argument is a descriptor for a file, not a socket.\n");
				break;

			case EINVAL:
                printf("bind__(): The addrlen is wrong, or the socket was not in the AF_UNIX family.\n");
				break;

			case EROFS:
                printf("bind__(): The socket inode would reside on a read-only file system.\n");
				break;

			case EFAULT:
                printf("bind__(): my_addr points outside the user's accessible address space.\n");
				break;

			case ENAMETOOLONG:
                printf("bind__(): my_addr is too long.\n");
				break;

			case ENOENT:
                printf("bind__(): The file does not exist.\n");
				break;

			case ENOMEM:
                printf("bind__(): Insufficient kernel memory was available.\n");
				break;

			case ENOTDIR:
                printf("bind__(): A component of the path prefix is not a directory.\n");
				break;

			case EACCES:
				// Port reserved on PS4
                printf("bind__(): Search permission is denied on a component of the path prefix.\n");
				break;

			case ELOOP:
                printf("bind__(): Too many symbolic links were encountered in resolving my_addr.\n");
				break;

			default:
                printf("Unknown bind__() error %i.\n", ret);
				break;
}
#endif
		return BindResult::FAILED_TO_BIND_SOCKET;
	}

	getSystemAddressIPV4(_rns2Socket, &_boundAddress);

	return BindResult::SUCCESS;
}

NetSocket2::BindResult BerkleySocket2::bindSharedIPV4And6(BindParameters* bindParameters)
{
#if defined(JINRA_SUPPORT_IPV6)
	int ret = 0;
	struct addrinfo hints;
	struct addrinfo *servinfo = 0, *aip;  // will point to the results
	PrepareAddrInfoHints2(&hints);
	hints.ai_family = bindParameters->addressFamily;
	char portStr[32];
	itoa(bindParameters->port, portStr, 10);

	// On Ubuntu, "" returns "No address associated with hostname" while 0 works.
	if (bindParameters->hostAddress &&
		(_stricmp(bindParameters->hostAddress, "UNASSIGNED_SYSTEM_ADDRESS") == 0 || bindParameters->hostAddress[0] == 0)) {
		getaddrinfo(0, portStr, &hints, &servinfo);
	} else {
		getaddrinfo(bindParameters->hostAddress, portStr, &hints, &servinfo);
	}

	// Try all returned addresses until one works
	for (aip = servinfo; aip != nullptr; aip = aip->ai_next) {
		// Open socket. The address type depends on what
		// getaddrinfo() gave us.
		_rns2Socket = socket__(aip->ai_family, aip->ai_socktype, aip->ai_protocol);

		if (_rns2Socket == -1)
			return BR_FAILED_TO_BIND_SOCKET;

		ret = bind__(_rns2Socket, aip->ai_addr, (int)aip->ai_addrlen);
		if (ret >= 0) {
			// Is this valid?
			memcpy(&boundAddress.address.addr6, aip->ai_addr, sizeof(boundAddress.address.addr6));

			freeaddrinfo(servinfo); // free the linked-list

			setSocketOptions();
			setNonBlockingSocket(bindParameters->nonBlockingSocket);
			setBroadcastSocket(bindParameters->setBroadcast);
			setIPHdrIncl(bindParameters->setIPHdrIncl);

			getSystemAddressIPV4And6(_rns2Socket, &boundAddress);

			return BR_SUCCESS;
		} else {
			closesocket__(_rns2Socket);
		}
	}

	return NetSocket2::BindResult::FAILED_TO_BIND_SOCKET;

#else // defined(JINRA_SUPPORT_IPV6)
	(void)bindParameters;
	return BindResult::REQUIRES_JINRA_SUPPORT_IPV6_DEFINE;
#endif // defined(JINRA_SUPPORT_IPV6)
}

void BerkleySocket2::recvFromBlockingIPV4(RecvStruct* recvFromStruct)
{
	sockaddr_in sa;
	memset(&sa, 0, sizeof(sockaddr_in));
	sa.sin_family = AF_INET;
	sa.sin_port = 0;

	socklen_t sockLen = sizeof sa;
	recvFromStruct->bytesRead = recvfrom(getSocket(), recvFromStruct->data,
										   sizeof recvFromStruct->data, 0,
										   reinterpret_cast<sockaddr*>(&sa), &sockLen);
	if (recvFromStruct->bytesRead <= 0)
		return;

	recvFromStruct->timeRead = Time::nowUS();
	recvFromStruct->systemAddress.setPortNetworkOrder(sa.sin_port);
	recvFromStruct->systemAddress.address.addr4.sin_addr.s_addr = sa.sin_addr.s_addr;
}


void BerkleySocket2::recvFromBlockingIPV4And6(RecvStruct* recvFromStruct)
{
#if defined(JINRA_SUPPORT_IPV6)
	sockaddr_storage their_addr;
	sockaddr* sockAddrPtr;
	socklen_t sockLen;
	socklen_t* socketlenPtr = (socklen_t*)&sockLen;
	memset(&their_addr, 0, sizeof(their_addr));
	int dataOutSize;
	const int flag = 0;

	{
		sockLen = sizeof(their_addr);
		sockAddrPtr = (sockaddr*)&their_addr;
	}

	dataOutSize = MAXIMUM_MTU_SIZE;


	recvFromStruct->bytesRead = recvfrom__(_rns2Socket, recvFromStruct->data, dataOutSize, flag, sockAddrPtr, socketlenPtr);

#if defined(_WIN32) && defined(JINRA_DEBUG)
	if (recvFromStruct->bytesRead == -1) {
		DWORD dwIOError = GetLastError();
		if (dwIoError != 10035) {
			LPVOID messageBuffer;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
						  nullptr, dwIOError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),  // Default language
						  (LPTSTR)& messageBuffer, 0, nullptr);
			// I see this hit on XP with IPV6 for some reason
            printf("Warning: recvfrom failed:Error code - %d\n%s", dwIOError, messageBuffer);
			LocalFree(messageBuffer);
		}
	}
#endif

	if (recvFromStruct->bytesRead <= 0)
		return;
	recvFromStruct->timeRead = Time::nowUS();

	{
		if (their_addr.ss_family == AF_INET) {
			memcpy(&recvFromStruct->systemAddress.address.addr4, (sockaddr_in *)&their_addr, sizeof(sockaddr_in));
			recvFromStruct->systemAddress.debugPort = ntohs(recvFromStruct->systemAddress.address.addr4.sin_port);
			//	systemAddressOut->address.addr4.sin_port=ntohs( systemAddressOut->address.addr4.sin_port );
		} else {
			memcpy(&recvFromStruct->systemAddress.address.addr6, (sockaddr_in6 *)&their_addr, sizeof(sockaddr_in6));
			recvFromStruct->systemAddress.debugPort = ntohs(recvFromStruct->systemAddress.address.addr6.sin6_port);
			//	systemAddressOut->address.addr6.sin6_port=ntohs( systemAddressOut->address.addr6.sin6_port );
		}
	}


#else // defined(JINRA_SUPPORT_IPV6)
	(void)recvFromStruct;
#endif // defined(JINRA_SUPPORT_IPV6)
}

void domainNameToIP_IPV4(cchar* domainName, char ip[65])
{
	// Use inet_addr instead? What is the difference?
	auto phe = gethostbyname(domainName);

	if (phe == nullptr || phe->h_addr_list[0] == nullptr) {
		memset(ip, 0, 65 * sizeof(char));
		return;
	}

	if (phe->h_addr_list[0] == nullptr) {
		memset(ip, 0, 65 * sizeof(char));
		return;
	}

	static struct in_addr addr;
	memset(&addr, 0, sizeof(in_addr));
	memcpy(&addr, phe->h_addr_list[0], sizeof(struct in_addr));
	strcpy(ip, inet_ntoa(addr));
}

void domainNameToIP_IPV4And6(cchar* domainName, char ip[65])
{
#if defined(JINRA_SUPPORT_IPV6)
	struct addrinfo hints, *res, *p;
	int status;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
	hints.ai_socktype = SOCK_DGRAM;

	if ((status = getaddrinfo(domainName, nullptr, &hints, &res)) != 0) {
		memset(ip, 0, sizeof(ip));
		return;
	}

	p = res;
	void *addr;

	// get the pointer to the address itself,
	// different fields in IPv4 and IPv6:
	if (p->ai_family == AF_INET) {
		struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
		addr = &(ipv4->sin_addr);
		strcpy(ip, inet_ntoa(ipv4->sin_addr));
	} else {
		// TODO - test
		struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
		addr = &(ipv6->sin6_addr);
		// inet_ntop function does not exist on windows
		// http://www.mail-archive.com/users@ipv6.org/msg02107.html
		getnameinfo((struct sockaddr *)ipv6, sizeof(struct sockaddr_in6), ip, 1, nullptr, 0, NI_NUMERICHOST);
	}
	freeaddrinfo(res); // free the linked list
#else // defined(JINRA_SUPPORT_IPV6)
	(void)domainName;
	(void)ip;
#endif // defined(JINRA_SUPPORT_IPV6)
}

void BerkleySocket2::domainNameToIP(cchar *domainName, char ip[65])
{
#if defined(JINRA_SUPPORT_IPV6)
	return domainNameToIP_IPV4And6(domainName, ip);
#else // defined(JINRA_SUPPORT_IPV6)
	return domainNameToIP_IPV4(domainName, ip);
#endif // defined(JINRA_SUPPORT_IPV6)
}

void BerkleySocket2::getSystemAddressIPV4(Socket2 rns2Socket, NetAddress* systemAddressOut)
{
	sockaddr_in sa;
	memset(&sa, 0, sizeof(sockaddr_in));
	socklen_t len = sizeof sa;

	getsockname(rns2Socket, (sockaddr*)&sa, &len);
	systemAddressOut->setPortNetworkOrder(sa.sin_port);
	systemAddressOut->address.addr4.sin_addr.s_addr = sa.sin_addr.s_addr;

	if (systemAddressOut->address.addr4.sin_addr.s_addr == INADDR_ANY)
		systemAddressOut->address.addr4.sin_addr.s_addr = inet_addr("127.0.0.1");
}

void BerkleySocket2::getSystemAddressIPV4And6(Socket2 rns2Socket, NetAddress* systemAddressOut)
{
#if defined(JINRA_SUPPORT_IPV6)

	socklen_t slen;
	sockaddr_storage ss;
	slen = sizeof(ss);

	if (getsockname__(rns2Socket, (struct sockaddr *)&ss, &slen) != 0) {
#if defined(_WIN32) && defined(JINRA_DEBUG)
		DWORD dwIOError = GetLastError();
		LPVOID messageBuffer;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					  nullptr, dwIOError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),  // Default language
					  (LPTSTR)& messageBuffer, 0, nullptr);
		// something has gone wrong here...
        printf("getsockname failed:Error code - %d\n%s", dwIOError, messageBuffer);

		//Free the buffer.
		LocalFree(messageBuffer);
#endif
		systemAddressOut->FromString(0);
		return;
	}

	if (ss.ss_family == AF_INET) {
		memcpy(&systemAddressOut->address.addr4, (sockaddr_in *)&ss, sizeof(sockaddr_in));
		systemAddressOut->debugPort = ntohs(systemAddressOut->address.addr4.sin_port);

		u32 zero = 0;
		if (memcmp(&systemAddressOut->address.addr4.sin_addr.s_addr, &zero, sizeof(zero)) == 0)
			systemAddressOut->SetToLoopback(4);
		//	systemAddressOut->address.addr4.sin_port=ntohs(systemAddressOut->address.addr4.sin_port);
	} else {
		memcpy(&systemAddressOut->address.addr6, (sockaddr_in6 *)&ss, sizeof(sockaddr_in6));
		systemAddressOut->debugPort = ntohs(systemAddressOut->address.addr6.sin6_port);

		char zero[16];
		memset(zero, 0, sizeof(zero));
		if (memcmp(&systemAddressOut->address.addr4.sin_addr.s_addr, &zero, sizeof(zero)) == 0)
			systemAddressOut->SetToLoopback(6);

		//	systemAddressOut->address.addr6.sin6_port=ntohs(systemAddressOut->address.addr6.sin6_port);
	}

#else // defined(JINRA_SUPPORT_IPV6)
	(void)rns2Socket;
	(void)systemAddressOut;
#endif // defined(JINRA_SUPPORT_IPV6)
}

} // namespace Jinra