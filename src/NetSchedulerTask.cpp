#include "Jinra.h"
#include "NetSchedulerTask.h"

namespace Jinra
{

NetSchedulerTask::NetSchedulerTask(std::function<void()>&& f, u32 delay)
    : NetTask(move(f), delay)
{

}

} // namespace Jinra