#include "Jinra.h"
#include "NetStatistics.h"
#include "NetTime.h"

namespace Jinra
{

NetStatistics& NetStatistics::operator+=(const NetStatistics& netStatistics)
{
	for (u32 i = 0u; i < Packet::NUMBER_OF_PRIORITIES; ++i) {
		messageInSendBuffer[i] += netStatistics.messageInSendBuffer[i];
		bytesInSendBuffer[i] += netStatistics.bytesInSendBuffer[i];
	}

	for (u32 i = 0u; i < RNS_PER_SECOND_METRICS_COUNT; ++i) {
		valueOverLastSecond[i] += netStatistics.valueOverLastSecond[i];
		runningTotal[i] += netStatistics.runningTotal[i];
	}

	return *this;
}

void verbosityLevel0(NetStatistics* netStatistics, char* buffer)
{
	sprintf(buffer,
			"Bytes per second sent     %" PRINTF_64_BIT_MODIFIER "u\n"
			"Bytes per second received %" PRINTF_64_BIT_MODIFIER "u\n"
			"Current packetloss        %.1f%%\n",
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_RECEIVED]),
				netStatistics->packetlossLastSecond * 100.0f);
}

void verbosityLevel1(NetStatistics* netStatistics, char* buffer)
{
	sprintf(buffer,
			"Actual bytes per second sent       %" PRINTF_64_BIT_MODIFIER "u\n"
			"Actual bytes per second received   %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second pushed    %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total actual bytes sent            %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total actual bytes received        %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes pushed         %" PRINTF_64_BIT_MODIFIER "u\n"
			"Current packetloss                 %.1f%%\n"
			"Average packetloss                 %.1f%%\n"
			"Elapsed connection time in seconds %" PRINTF_64_BIT_MODIFIER "u\n",
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_RECEIVED]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_PUSHED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[ACTUAL_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[ACTUAL_BYTES_RECEIVED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_PUSHED]),
			netStatistics->packetlossLastSecond * 100.0f,
			netStatistics->packetlossTotal * 100.0f,
			static_cast<long long unsigned int>(static_cast<u64>(
				(Time::nowUS() - netStatistics->connectionStartTime) / 1000000)));

	if (netStatistics->BPSLimitByCongestionControl != 0) {
		char buffer2[128];
		sprintf(buffer2,
				"Send capacity                    %" PRINTF_64_BIT_MODIFIER "u bytes per second (%.0f%%)\n",
				static_cast<long long unsigned int>(
					netStatistics->BPSLimitByCongestionControl),
				100.0f * netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT] /
				netStatistics->BPSLimitByCongestionControl);
		strcat(buffer, buffer2);
	}

	if (netStatistics->BPSLimitByOutgoingBandwidthLimit != 0) {
		char buffer2[128];
		sprintf(buffer2,
				"Send limit                       %" PRINTF_64_BIT_MODIFIER "u (%.0f%%)\n",
				static_cast<long long unsigned int>(
					netStatistics->BPSLimitByOutgoingBandwidthLimit),
				100.0f * netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT] /
				netStatistics->BPSLimitByOutgoingBandwidthLimit);
		strcat(buffer, buffer2);
	}
}

void verbosityLevel2(NetStatistics* netStatistics, char* buffer)
{
	sprintf(buffer,
			"Actual bytes per second sent         %" PRINTF_64_BIT_MODIFIER "u\n"
			"Actual bytes per second received     %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second sent        %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second resent      %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second pushed      %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second returned	  %" PRINTF_64_BIT_MODIFIER "u\n"
			"Message bytes per second ignored     %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total bytes sent                     %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total bytes received                 %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes sent             %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes resent           %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes pushed           %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes returned		  %" PRINTF_64_BIT_MODIFIER "u\n"
			"Total message bytes ignored          %" PRINTF_64_BIT_MODIFIER "u\n"
			"Messages in send buffer, by priority %i,%i,%i,%i\n"
			"Bytes in send buffer, by priority    %i,%i,%i,%i\n"
			"Messages in resend buffer            %i\n"
			"Bytes in resend buffer               %" PRINTF_64_BIT_MODIFIER "u\n"
			"Current packetloss                   %.1f%%\n"
			"Average packetloss                   %.1f%%\n"
			"Elapsed connection time in seconds   %" PRINTF_64_BIT_MODIFIER "u\n",
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[ACTUAL_BYTES_RECEIVED]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_RESENT]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_PUSHED]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_RECEIVED_PROCESSED]),
			static_cast<long long unsigned int>(
				netStatistics->valueOverLastSecond[USER_MESSAGE_BYTES_RECEIVED_IGNORED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[ACTUAL_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[ACTUAL_BYTES_RECEIVED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_SENT]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_RESENT]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_PUSHED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_RECEIVED_PROCESSED]),
			static_cast<long long unsigned int>(
				netStatistics->runningTotal[USER_MESSAGE_BYTES_RECEIVED_IGNORED]),
			netStatistics->messageInSendBuffer[Packet::IMMEDIATE_PRIORITY], 
			netStatistics->messageInSendBuffer[Packet::HIGH_PRIORITY], 
			netStatistics->messageInSendBuffer[Packet::MEDIUM_PRIORITY],
			netStatistics->messageInSendBuffer[Packet::LOW_PRIORITY],
			static_cast<u32>(netStatistics->bytesInSendBuffer[Packet::IMMEDIATE_PRIORITY]),
			static_cast<u32>(netStatistics->bytesInSendBuffer[Packet::HIGH_PRIORITY]),
			static_cast<u32>(netStatistics->bytesInSendBuffer[Packet::MEDIUM_PRIORITY]),
			static_cast<u32>(netStatistics->bytesInSendBuffer[Packet::LOW_PRIORITY]),
				netStatistics->messagesInResendBuffer,
			static_cast<long long unsigned int>(netStatistics->bytesInResendBuffer),
				netStatistics->packetlossLastSecond*100.0f,
				netStatistics->packetlossTotal*100.0f,
			static_cast<long long unsigned int>(static_cast<u64>(
				(Time::nowUS() - netStatistics->connectionStartTime) / 1000000)));

	if (netStatistics->BPSLimitByCongestionControl != 0) {
		char buffer2[128];
		sprintf(buffer2,
				"Send capacity                    %" PRINTF_64_BIT_MODIFIER "u bytes per second (%.0f%%)\n",
				static_cast<long long unsigned int>(
					netStatistics->BPSLimitByCongestionControl),
				100.0f * netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT] /
				netStatistics->BPSLimitByCongestionControl);
		strcat(buffer, buffer2);
	}

	if (netStatistics->BPSLimitByOutgoingBandwidthLimit != 0) {
		char buffer2[128];
		sprintf(buffer2,
				"Send limit                       %" PRINTF_64_BIT_MODIFIER "u (%.0f%%)\n",
				static_cast<long long unsigned int>(
					netStatistics->BPSLimitByOutgoingBandwidthLimit),
				100.0f * netStatistics->valueOverLastSecond[ACTUAL_BYTES_SENT] /
				netStatistics->BPSLimitByOutgoingBandwidthLimit);
		strcat(buffer, buffer2);
	}
}

void JINRA_DLL_EXPORT statisticsToString(NetStatistics* netStatistics, char* buffer, 
										 s32 verbosityLevel)
{
	if (netStatistics == nullptr) {
		sprintf(buffer, "netStatistics is a nullptr pointer in statisticsToString");
		return;
	}

	switch (verbosityLevel) {
		case 0:
			verbosityLevel0(netStatistics, buffer);
			break;

		case 1:
			verbosityLevel1(netStatistics, buffer);
			break;

		default:
			verbosityLevel2(netStatistics, buffer);
			break;
	}
}

} // namespace Jinra