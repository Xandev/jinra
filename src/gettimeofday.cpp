#include "Jinra.h"

#if defined(_WIN32) && !defined(__GNUC__)  &&!defined(__GCCXML__)

#include "gettimeofday.h"

// From http://www.openasthra.com/c-tidbits/gettimeofday-function-for-windows/

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else // defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif // defined(_MSC_VER) || defined(_MSC_EXTENSIONS)

s32 gettimeofday(struct timeval* tv, struct timezone* tz)
{
	FILETIME ft;
	unsigned __int64 tmpres = 0;
	static s32 tzflag;

	if (tv != nullptr) {
		GetSystemTimeAsFileTime(&ft);

		tmpres |= ft.dwHighDateTime;
		tmpres <<= 32;
		tmpres |= ft.dwLowDateTime;

		// Converting file time to unix epoch.
		tmpres /= 10; // Convert into microseconds.
		tmpres -= DELTA_EPOCH_IN_MICROSECS;
		tv->tv_sec = static_cast<long>(tmpres / 1000000UL);
		tv->tv_usec = static_cast<long>(tmpres % 1000000UL);
	}

	if (tz != nullptr) {
		if (!tzflag) {
			_tzset();
			++tzflag;
		}
		tz->tz_minuteswest = _timezone / 60;
		tz->tz_dsttime = _daylight;
	}

	return 0;
}

#endif

