#include "Jinra.h"

#include "WindowsSocket2.h"
#include "LinuxSocket2.h"
#include "NetTime.h"

#include "NetDefines.h"
#include "NetPeer.h"

#include "MessageIdentifiers.h"

#include "gettimeofday.h"
#include "SignaledEvent.h"
#include "SuperFastHash.h"

#include "WSAStartupSingleton.h"
#include "NetGuidGenerator.h"

namespace Jinra
{
JINRA_THREAD_DECLARATION(UpdateNetworkLoop);
JINRA_THREAD_DECLARATION(RecvFromLoop);
}

#define REMOTE_SYSTEM_LOOKUP_HASH_MULTIPLE 8

static const s32 NUM_MTU_SIZES = 3;

static const s32 mtuSizes[NUM_MTU_SIZES] = {MAXIMUM_MTU_SIZE, 1200, 576};

// Note to self - if I change this it might affect RECIPIENT_OFFLINE_MESSAGE_INTERVAL in Natpunchthrough.cpp
//static const int MAX_OPEN_CONNECTION_REQUESTS=8;
//static const int TIME_BETWEEN_OPEN_CONNECTION_REQUESTS=500;

using namespace Jinra;

static const u32 MAX_OFFLINE_DATA_LENGTH = 400; // I set this because I limit ID_CONNECTION_REQUEST to 512 bytes, and the password is appended to that packet.

// Used to distinguish between offline messages with data, and messages from the reliability layer
// Should be different than any message that could result from messages from the reliability layer
// Make sure highest bit is 0, so isValid in DatagramHeaderFormat is false
static const u8 OFFLINE_MESSAGE_DATA_ID[16] = {0x00, 0xFF, 0xFF, 0x00, 0xFE, 0xFE, 0xFE, 0xFE, 0xFD, 0xFD, 0xFD, 0xFD, 0x12, 0x34, 0x56, 0x78};

NetPeer::NetPeer()
{
	WSAStartupSingleton::initialize();

	defaultMTUSize = mtuSizes[NUM_MTU_SIZES - 1];
	trackFrequencyTable = false;
	maximumIncomingConnections = 0;
	maximumNumberOfPeers = 0;
	remoteSystemList = nullptr;
	activeSystemList = nullptr;
	activeSystemListSize = 0;
	remoteSystemLookup = nullptr;
	bytesSentPerSecond = bytesReceivedPerSecond = 0;
	endThreads = true;
	isMainLoopThreadActive = false;
	incomingDatagramEventHandler = nullptr;

#if defined(GET_TIME_SPIKE_LIMIT) && GET_TIME_SPIKE_LIMIT>0
	occasionalPing = true;
#else
	occasionalPing = false;
#endif
	allowInternalRouting = false;
	for (u32 i = 0u; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i)
		ipList[i] = UNASSIGNED_SYSTEM_ADDRESS;
	allowConnectionResponseIPMigration = false;
	splitMessageProgressInterval = 0;
	unreliableTimeout = 1000;
	maxOutgoingBPS = 0;
	firstExternalID = UNASSIGNED_SYSTEM_ADDRESS;
	myGuid = UNASSIGNED_NET_GUID;
	userUpdateThreadPtr = nullptr;
	userUpdateThreadData = nullptr;

#ifdef JINRA_DEBUG
	// Wait longer to disconnect in debug so I don't get disconnected while tracing
	defaultTimeoutTime = 30000u;
#else
	defaultTimeoutTime = 10000u;
#endif

	bufferedCommands.SetPageSize(sizeof(BufferedCommandStruct) * 16);
	socketQueryOutput.SetPageSize(sizeof(SocketQueryOutput) * 8);

	packetAllocationPoolMutex.lock();
	packetAllocationPool.SetPageSize(sizeof(DataStructures::MemoryPool<Packet>::MemoryWithPage) * 32);
	packetAllocationPoolMutex.unlock();

	remoteSystemIndexPool.SetPageSize(sizeof(DataStructures::MemoryPool<RemoteSystemIndex>::MemoryWithPage) * 32);

    myGuid.id = NetGuidGenerator::get64BitUniqueRandomNumber();

	quitAndDataEvents.initEvent();
	limitConnectionFrequencyFromTheSameIP = false;
	ResetSendReceipt();
}

NetPeer::~NetPeer()
{
	shutdown(0u, 0u);

	// Free the ban list.
	clearBanList();

	WSAStartupSingleton::release();

	quitAndDataEvents.closeEvent();
}

NetPeer::StartupResult NetPeer::startup(u32 maxConnections, 
										SocketDescriptor* socketDescriptors, 
										u32 socketDescriptorCount, s32 threadPriority)
{
	if (isActive())
		return StartupResult::ALREADY_STARTED;

	// If getting the guid failed in the constructor, try again
	if (myGuid.id == 0) {
        myGuid.id = NetGuidGenerator::get64BitUniqueRandomNumber();
		if (myGuid.id == 0)
			return StartupResult::COULD_NOT_GENERATE_GUID;
	}

	if (threadPriority == -99999) {
#if defined(_WIN32)
		threadPriority = 0;
#else // defined(_WIN32)
		threadPriority = 1000;
#endif // defined(_WIN32)
	}

	fillIPList();

	ASSERT(socketDescriptors && socketDescriptorCount >= 1);

	if (socketDescriptors == nullptr || socketDescriptorCount < 1)
		return StartupResult::INVALID_SOCKET_DESCRIPTORS;

	if (maxConnections <= 0u)
		return StartupResult::INVALID_MAX_CONNECTIONS;

	derefAllSockets();

	// Go through all socket descriptors and precreate sockets on the specified addresses
	for (u32 i = 0u; i < socketDescriptorCount; ++i) {
		auto socket = NetSocket2::create();
		socket->setUserConnectionSocketIndex(i);

		if (socket->isBerkleySocket()) {
			BerkleySocket2::BindParameters bbp;
			bbp.port = socketDescriptors[i].port;
			bbp.hostAddress = (char*)socketDescriptors[i].hostAddress;
			bbp.addressFamily = socketDescriptors[i].socketFamily;
			bbp.type = SOCK_DGRAM;
			bbp.protocol = 0;
			bbp.nonBlockingSocket = false;
			bbp.setBroadcast = true;
			bbp.setIPHdrIncl = false;
			bbp.doNotFragment = false;
			bbp.pollingThreadPriority = threadPriority;
			bbp.eventHandler = this;
			NetSocket2::BindResult br = static_cast<BerkleySocket2*>(socket)->bind(&bbp);

			if (
#if !defined(JINRA_SUPPORT_IPV6)
				socketDescriptors[i].socketFamily != AF_INET ||
#endif
				br == NetSocket2::BindResult::REQUIRES_JINRA_SUPPORT_IPV6_DEFINE) 
			{
				delete socket;
				derefAllSockets();
				return StartupResult::SOCKET_FAMILY_NOT_SUPPORTED;
			}
		    if (br == NetSocket2::BindResult::FAILED_TO_BIND_SOCKET) {
		        delete socket;
		        derefAllSockets();
		        return StartupResult::SOCKET_PORT_ALREADY_IN_USE;
		    }
		    if (br == NetSocket2::BindResult::FAILED_SEND_TEST) {
		        delete socket;
		        derefAllSockets();
		        return StartupResult::SOCKET_FAILED_TEST_SEND;
		    }
		    ASSERT(br == NetSocket2::BindResult::SUCCESS);
		} else {
		        ASSERT("TODO" && false);
		    }
		    socketList.push_back(socket);
		    }

		    for (u32 i = 0u; i < socketDescriptorCount; ++i) {
		        if (socketList[i]->isBerkleySocket()) {
		            static_cast<BerkleySocket2*>(socketList[i])->createRecvPollingThread(threadPriority);
		        }
		    }

		    for (u32 i = 0u; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i) {
		        if (ipList[i] == UNASSIGNED_SYSTEM_ADDRESS)
		            break;

		        if (socketList[0]->isBerkleySocket()) {
		            u16 port = static_cast<BerkleySocket2*>(socketList[0])->getBoundAddress().getPort();
		            ipList[i].setPortHostOrder(port);

		        }
		    }


		    if (maximumNumberOfPeers == 0) {
		        // Don't allow more incoming connections than we have peers.
		        if (maximumIncomingConnections > maxConnections)
		            maximumIncomingConnections = maxConnections;

		        maximumNumberOfPeers = maxConnections;
		        // 04/19/2006 - Don't overallocate because I'm no longer allowing connected pings.
		        // The disconnects are not consistently processed and the process was sloppy and complicated.
		        // Allocate 10% extra to handle new connections from players trying to connect when the server is full
		        //remoteSystemListSize = maxConnections;// * 11 / 10 + 1;

		        // remoteSystemList in Single thread
		        remoteSystemList = new RemoteSystemStruct[maximumNumberOfPeers];

		        remoteSystemLookup = new RemoteSystemIndex*[
		            maximumNumberOfPeers * REMOTE_SYSTEM_LOOKUP_HASH_MULTIPLE];

		        activeSystemList = new RemoteSystemStruct*[maximumNumberOfPeers];

		        for (u32 i = 0u; i < maximumNumberOfPeers; ++i) {
		            // remoteSystemList in Single thread
		            remoteSystemList[i].isActive = false;
		            remoteSystemList[i].systemAddress = UNASSIGNED_SYSTEM_ADDRESS;
		            remoteSystemList[i].guid = UNASSIGNED_NET_GUID;
		            remoteSystemList[i].myExternalSystemAddress = UNASSIGNED_SYSTEM_ADDRESS;
		            remoteSystemList[i].connectMode = RemoteSystemStruct::NO_ACTION;
		            remoteSystemList[i].MTUSize = defaultMTUSize;
		            remoteSystemList[i].remoteSystemIndex = static_cast<SystemIndex>(i);

		            // All entries in activeSystemList have valid pointers all the time.
		            activeSystemList[i] = &remoteSystemList[i];
		        }

		        for (u32 i = 0u; i < maximumNumberOfPeers * REMOTE_SYSTEM_LOOKUP_HASH_MULTIPLE; ++i)
		            remoteSystemLookup[i] = nullptr;
		    }

		    if (endThreads) {
		        updateCycleIsRunning = false;
		        endThreads = false;
		        firstExternalID = UNASSIGNED_SYSTEM_ADDRESS;

		        clearBufferedCommands();
		        clearBufferedPackets();
		        clearSocketQueryOutput();

		        if (!isMainLoopThreadActive) {
		            s32 errorCode = NetThread::create(UpdateNetworkLoop, this, threadPriority);
		            if (errorCode != 0) {
		                shutdown(0u, 0u);
		                return StartupResult::FAILED_TO_CREATE_NETWORK_THREAD;
		            }
		        }

		        // Wait for the threads to activate.  When they are active they will set these variables to true
		        while (!isMainLoopThreadActive)
		            Time::sleepMS(10u);
		    }

return StartupResult::STARTED;
}

u32 NetPeer::numberOfConnections() const
{
	std::vector<NetAddress> addresses;
	std::vector<NetGUID> guids;
	getSystemList(addresses, guids);
	return addresses.size();
}

bool NetPeer::setIncomingPassword(const String& password)
{
	if (password.length() > 255)
		return false;

	incomingPassword = password;
	return true;
}

String NetPeer::getIncomingPassword() const
{
	return incomingPassword;
}

NetPeer::ConnectionAttemptResult NetPeer::connect(const String& host, u16 remotePort,
		                                            const String& password,
		                                            u32 connectionSocketIndex,
		                                            u32 sendConnectionAttemptCount,
		                                            u32 timeBetweenSendConnectionAttemptsMS,
		                                            TimeMS timeoutTime)
{
	// If endThreads is true here you didn't call startup() first.
	if (host.empty() || endThreads || connectionSocketIndex >= socketList.size())
		return ConnectionAttemptResult::INVALID_PARAMETER;

	ASSERT(remotePort != 0);

	connectionSocketIndex = getJinraSocketFromUserConnectionSocketIndex(connectionSocketIndex);

	return sendConnectionRequest(host, remotePort, password,
		                            connectionSocketIndex, 0, sendConnectionAttemptCount,
		                            timeBetweenSendConnectionAttemptsMS, timeoutTime);
}

NetPeer::ConnectionAttemptResult NetPeer::connectWithSocket(const String& host, u16 remotePort, const String& password,
		                                                    NetSocket2* socket, 
		                                                    u32 sendConnectionAttemptCount, 
		                                                    u32 timeBetweenSendConnectionAttemptsMS,
		                                                    TimeMS timeoutTime)
{
	if (host.empty() || endThreads || socket == nullptr)
		return ConnectionAttemptResult::INVALID_PARAMETER;

	return sendConnectionRequest(host, remotePort, password, 0, 0,
		                            sendConnectionAttemptCount, 
		                            timeBetweenSendConnectionAttemptsMS, timeoutTime, socket);

}

void NetPeer::shutdown(u32 blockDuration, u8 orderingChannel,
		                Packet::Priority disconnectionNotificationPriority)
{
	u32 systemListSize = maximumNumberOfPeers;

	if (blockDuration > 0) {
		for (u32 i = 0u; i < systemListSize; ++i) {
		    // remoteSystemList in user thread
		    if (remoteSystemList[i].isActive) {
		        notifyAndFlagForShutdown(remoteSystemList[i].systemAddress, false,
		                                    orderingChannel, disconnectionNotificationPriority);
		    }
		}

		auto time = Time::nowMS();
		auto startWaitingTime = time;
		while (time - startWaitingTime < blockDuration) {
		    bool anyActive = false;
		    for (u32 i = 0u; i < systemListSize; ++i) {
		        // remoteSystemList in user thread
		        if (remoteSystemList[i].isActive) {
		            anyActive = true;
		            break;
		        }
		    }

		    // If this system is out of packets to send, then stop waiting
		    if (!anyActive)
		        break;

		    // This will probably cause the update thread to run which will probably
		    // send the disconnection notification

		    Time::sleepMS(15);
		    time = Time::nowMS();
		}
	}

	activeSystemListSize = 0;

	quitAndDataEvents.setEvent();

	endThreads = true;

	for (u32 i = 0u; i < socketList.size(); ++i) {
		if (socketList[i]->isBerkleySocket()) {
		    static_cast<BerkleySocket2*>(socketList[i])->signalStopRecvPollingThread();
		}
	}

	while (isMainLoopThreadActive) {
		endThreads = true;
		Time::sleepMS(15);
	}

	for (u32 i = 0u; i < socketList.size(); ++i) {
		if (socketList[i]->isBerkleySocket()) {
		    static_cast<BerkleySocket2*>(socketList[i])->blockOnStopRecvPollingThread();
		}
	}

	// remoteSystemList in Single thread
	for (u32 i = 0u; i < systemListSize; ++i) {
		// Reserve this reliability layer for ourselves
		remoteSystemList[i].isActive = false;

		// Remove any remaining packets
		ASSERT(remoteSystemList[i].MTUSize <= MAXIMUM_MTU_SIZE);
		remoteSystemList[i].reliabilityLayer.reset(false, remoteSystemList[i].MTUSize);
		remoteSystemList[i].netSocket = nullptr;
	}


	// Setting maximumNumberOfPeers to 0 allows remoteSystemList to be reallocated in Initialize.
	// Setting remoteSystemListSize prevents threads from accessing the reliability layer
	maximumNumberOfPeers = 0;

	// Free any packets the user didn't deallocate
	packetReturnMutex.lock();
	for (u32 i = 0u; i < packetReturnQueue.size(); ++i)
		deallocate(packetReturnQueue[i]);
	packetReturnQueue.clear();
	packetReturnMutex.unlock();
	packetAllocationPoolMutex.lock();
	packetAllocationPool.clear();
	packetAllocationPoolMutex.unlock();

	derefAllSockets();

	clearBufferedCommands();
	clearBufferedPackets();
	clearSocketQueryOutput();
	bytesSentPerSecond = bytesReceivedPerSecond = 0;

	clearRequestedConnectionList();


	// Clear out the reliability layer list in case we want to reallocate it in a successive call to Init.
	auto temp = remoteSystemList;
	remoteSystemList = nullptr;
	delete[] temp;
	delete[] activeSystemList;
	activeSystemList = nullptr;

	clearRemoteSystemLookup();

	ResetSendReceipt();
}

bool NetPeer::getConnectionList(NetAddress* remoteSystems, u16* numberOfSystems) const
{
	if (numberOfSystems == nullptr)
		return false;

	if (remoteSystemList == nullptr || endThreads) {
		if (numberOfSystems)
		    *numberOfSystems = 0;
		return false;
	}

	std::vector<NetAddress> addresses;
	std::vector<NetGUID> guids;
	getSystemList(addresses, guids);
	if (remoteSystems) {
		u16 i = 0u;
		for (; i < *numberOfSystems && i < addresses.size(); ++i)
		    remoteSystems[i] = addresses[i];
		*numberOfSystems = i;
	} else {
		*numberOfSystems = static_cast<u16>(addresses.size());
	}
	return true;
}

u32 NetPeer::GetNextSendReceipt()
{
	sendReceiptSerialMutex.lock();
	auto retVal = sendReceiptSerial;
	sendReceiptSerialMutex.unlock();
	return retVal;
}

u32 NetPeer::IncrementNextSendReceipt()
{
	sendReceiptSerialMutex.lock();
	auto returned = sendReceiptSerial;
	++sendReceiptSerial;
	if (sendReceiptSerial == 0)
		sendReceiptSerial = 1;
	sendReceiptSerialMutex.unlock();
	return returned;
}

u32 NetPeer::send(cchar* data, const u32 length, Packet::Priority priority,
		            Packet::Reliability reliability, char orderingChannel,
		            const AddressOrGUID systemIdentifier, bool broadcast,
		            u32 forceReceiptNumber)
{
#ifdef JINRA_DEBUG
	ASSERT(data && length > 0);
#endif
	ASSERT(!(reliability >= Packet::NUMBER_OF_RELIABILITIES || reliability < 0));
	ASSERT(!(priority > Packet::NUMBER_OF_PRIORITIES || priority < 0));
	ASSERT(!(orderingChannel >= NUMBER_OF_ORDERED_STREAMS));

	if (data == nullptr || length == 0)
		return 0u;

	if (remoteSystemList == nullptr || endThreads)
		return 0u;

	if (!broadcast && systemIdentifier.isUndefined())
		return 0u;

	u32 usedSendReceipt;
	if (forceReceiptNumber != 0) {
		usedSendReceipt = forceReceiptNumber;
	} else {
		usedSendReceipt = IncrementNextSendReceipt();
	}

	if (!broadcast && IsLoopbackAddress(systemIdentifier, true)) {
		sendLoopback(data, length);

		if (reliability >= Packet::UNRELIABLE_WITH_ACK_RECEIPT) {
		    char buff[5];
		    buff[0] = ID_SND_RECEIPT_ACKED;
		    sendReceiptSerialMutex.lock();
		    memcpy(buff + 1, &sendReceiptSerial, 4);
		    sendReceiptSerialMutex.unlock();
		    sendLoopback(buff, 5);
		}

		return usedSendReceipt;
	}

	SendBuffered(data, length * 8, priority, reliability, orderingChannel, systemIdentifier,
		            broadcast, RemoteSystemStruct::NO_ACTION, usedSendReceipt);

	return usedSendReceipt;
}

void NetPeer::sendLoopback(cchar* data, const u32 length)
{
	if (data == nullptr || length == 0)
		return;

	auto packet = allocatePacket(length);
	memcpy(packet->data, data, length);
	packet->systemAddress = getLoopbackAddress();
	packet->guid = myGuid;
	PushBackPacket(packet, false);
}

u32 NetPeer::send(const BitStream* bitStream, Packet::Priority priority, Packet::Reliability reliability, char orderingChannel, const AddressOrGUID systemIdentifier, bool broadcast, u32 forceReceiptNumber)
{
#ifdef JINRA_DEBUG
	ASSERT(bitStream->getNumberOfBytesUsed() > 0);
#endif

	ASSERT(!(reliability >= Packet::NUMBER_OF_RELIABILITIES || reliability < 0));
	ASSERT(!(priority > Packet::NUMBER_OF_PRIORITIES || priority < 0));
	ASSERT(!(orderingChannel >= NUMBER_OF_ORDERED_STREAMS));

	if (bitStream->getNumberOfBytesUsed() == 0 || remoteSystemList == nullptr || endThreads)
		return 0;

	if (!broadcast && systemIdentifier.isUndefined())
		return 0;

	u32 usedSendReceipt;
	if (forceReceiptNumber != 0) {
		usedSendReceipt = forceReceiptNumber;
	} else {
		usedSendReceipt = IncrementNextSendReceipt();
	}

	if (!broadcast && IsLoopbackAddress(systemIdentifier, true)) {
		sendLoopback(reinterpret_cast<cchar*>(bitStream->getData()), bitStream->getNumberOfBytesUsed());
		if (reliability >= Packet::UNRELIABLE_WITH_ACK_RECEIPT) {
		    char buff[5];
		    buff[0] = ID_SND_RECEIPT_ACKED;
		    sendReceiptSerialMutex.lock();
		    memcpy(buff + 1, &sendReceiptSerial, 4);
		    sendReceiptSerialMutex.unlock();
		    sendLoopback(buff, 5);
		}
		return usedSendReceipt;
	}

	// Sends need to be buffered and processed in the update thread because the systemAddress 
	// associated with the reliability layer can change, from that thread, resulting in a send
	// to the wrong player! While I could mutex the systemAddress, that is much slower than 
	// doing this.
	SendBuffered((cchar*)bitStream->getData(), bitStream->getNumberOfBitsUsed(), priority,
		            reliability, orderingChannel, systemIdentifier, broadcast,
		            RemoteSystemStruct::NO_ACTION, usedSendReceipt);

	return usedSendReceipt;
}

u32 NetPeer::SendList(cchar** data, const s32* lengths, const s32 numParameters,
		                Packet::Priority priority, Packet::Reliability reliability,
		                char orderingChannel, const AddressOrGUID systemIdentifier,
		                bool broadcast, u32 forceReceiptNumber)
{
#ifdef JINRA_DEBUG
	ASSERT(data);
#endif

	if (data == nullptr || lengths == nullptr)
		return 0u;

	if (remoteSystemList == nullptr || endThreads)
		return 0;

	if (numParameters == 0)
		return 0u;

	if (!broadcast && systemIdentifier.isUndefined())
		return 0u;

	u32 usedSendReceipt;
	if (forceReceiptNumber != 0)
		usedSendReceipt = forceReceiptNumber;
	else
		usedSendReceipt = IncrementNextSendReceipt();

	SendBufferedList(data, lengths, numParameters, priority, reliability, orderingChannel,
		                systemIdentifier, broadcast, RemoteSystemStruct::NO_ACTION,
		                usedSendReceipt);

	return usedSendReceipt;
}

Packet* NetPeer::readMessage()
{
	if (!isActive())
		return nullptr;

	Packet* packet;
	do {
		packetReturnMutex.lock();
		if (packetReturnQueue.empty()) {
		    packet = nullptr;
		} else {
		    packet = packetReturnQueue.front();
		    packetReturnQueue.pop_front();
		}
		packetReturnMutex.unlock();

		if (packet == nullptr)
		    return nullptr;
	} while (packet == nullptr);

#if defined(JINRA_DEBUG)
	ASSERT(packet->data);
#endif // defined(JINRA_DEBUG)

	return packet;
}

void NetPeer::deallocate(Packet* packet)
{
	if (packet == nullptr)
		return;

	if (!packet->deleteData) {
		free(packet);
		return;
	}

	free(packet->data);
	packet->~Packet();
	packetAllocationPoolMutex.lock();
	packetAllocationPool.release(packet);
	packetAllocationPoolMutex.unlock();
}

void NetPeer::closeConnection(const AddressOrGUID target, bool sendDisconnectionNotification,
		                        u8 orderingChannel,
		                        Packet::Priority disconnectionNotificationPriority)
{
	closeConnectionInternal(target, sendDisconnectionNotification, false, orderingChannel,
		                    disconnectionNotificationPriority);

	if (!sendDisconnectionNotification && getConnectionState(target) ==
		ConnectionState::IS_CONNECTED) {
		auto packet = allocatePacket(sizeof(char));
		packet->data[0] = ID_CONNECTION_LOST; // DeadConnection
		packet->guid = target.guid == UNASSIGNED_NET_GUID ? GetGuidFromSystemAddress(target.address) : target.guid;
		packet->systemAddress = target.address == UNASSIGNED_SYSTEM_ADDRESS ? GetSystemAddressFromGuid(target.guid) : target.address;
		packet->systemAddress.systemIndex = static_cast<SystemIndex>(GetIndexFromSystemAddress(packet->systemAddress));
		packet->guid.systemIndex = packet->systemAddress.systemIndex;
		AddPacketToProducer(packet);
	}
}

void NetPeer::cancelConnectionAttempt(const NetAddress target)
{
	// Cancel pending connection attempt, if there is one
	requestedConnectionQueueMutex.lock();
	for (u32 i = 0u; i < requestedConnectionQueue.size(); ++i) {
		if (requestedConnectionQueue[i]->systemAddress == target) {
		    delete requestedConnectionQueue[i];
		    requestedConnectionQueue.erase(requestedConnectionQueue.begin() + i);
		    break;
		}
	}
	requestedConnectionQueueMutex.unlock();

}

NetPeer::ConnectionState NetPeer::getConnectionState(const AddressOrGUID systemIdentifier)
{
	if (systemIdentifier.address != UNASSIGNED_SYSTEM_ADDRESS) {
		requestedConnectionQueueMutex.lock();
		for (u32 i = 0u; i < requestedConnectionQueue.size(); ++i) {
		    if (requestedConnectionQueue[i]->systemAddress == systemIdentifier.address) {
		        requestedConnectionQueueMutex.unlock();
		        return ConnectionState::IS_PENDING;
		    }
		}
		requestedConnectionQueueMutex.unlock();
	}

	s32 index;
	if (systemIdentifier.address != UNASSIGNED_SYSTEM_ADDRESS) {
		index = GetIndexFromSystemAddress(systemIdentifier.address, false);
	} else {
		index = GetIndexFromGuid(systemIdentifier.guid);
	}

	if (index == -1)
		return ConnectionState::IS_NOT_CONNECTED;

	if (!remoteSystemList[index].isActive)
		return ConnectionState::IS_DISCONNECTED;

	switch (remoteSystemList[index].connectMode) {
		case RemoteSystemStruct::DISCONNECT_ASAP:
		    return ConnectionState::IS_DISCONNECTING;
		case RemoteSystemStruct::DISCONNECT_ASAP_SILENTLY:
		    return ConnectionState::IS_SILENTLY_DISCONNECTING;
		case RemoteSystemStruct::DISCONNECT_ON_NO_ACK:
		    return ConnectionState::IS_DISCONNECTING;
		case RemoteSystemStruct::REQUESTED_CONNECTION:
		    return ConnectionState::IS_CONNECTING;
		case RemoteSystemStruct::HANDLING_CONNECTION_REQUEST:
		    return ConnectionState::IS_CONNECTING;
		case RemoteSystemStruct::UNVERIFIED_SENDER:
		    return ConnectionState::IS_CONNECTING;
		case RemoteSystemStruct::CONNECTED:
		    return ConnectionState::IS_CONNECTED;
		default:
		    return ConnectionState::IS_NOT_CONNECTED;
	}
}


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Given a systemAddress, returns an index from 0 to the maximum number of players allowed - 1.
//
// Parameters
// systemAddress - The systemAddress to search for
//
// Returns
// An integer from 0 to the maximum number of peers -1, or -1 if that player is not found
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int NetPeer::GetIndexFromSystemAddress(const NetAddress systemAddress) const
{
	return GetIndexFromSystemAddress(systemAddress, false);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// This function is only useful for looping through all players.
//
// Parameters
// index - an integer between 0 and the maximum number of players allowed - 1.
//
// Returns
// A valid systemAddress or UNASSIGNED_SYSTEM_ADDRESS if no such player at that index
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
NetAddress NetPeer::GetSystemAddressFromIndex(u32 index) const
{
	// remoteSystemList in user thread
	//if ( index >= 0 && index < remoteSystemListSize )
	if (index < maximumNumberOfPeers)
		if (remoteSystemList[index].isActive && remoteSystemList[index].connectMode == RemoteSystemStruct::CONNECTED) // Don't give the user players that aren't fully connected, since sends will fail
		    return remoteSystemList[index].systemAddress;

	return UNASSIGNED_SYSTEM_ADDRESS;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Same as GetSystemAddressFromIndex but returns JinraGUID
// \param[in] index Index should range between 0 and the maximum number of players allowed - 1.
// \return The JinraGUID
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
NetGUID NetPeer::GetGUIDFromIndex(u32 index) const
{
	// remoteSystemList in user thread
	//if ( index >= 0 && index < remoteSystemListSize )
	if (index < maximumNumberOfPeers)
		if (remoteSystemList[index].isActive && remoteSystemList[index].connectMode == RemoteSystemStruct::CONNECTED) // Don't give the user players that aren't fully connected, since sends will fail
		    return remoteSystemList[index].guid;

	return UNASSIGNED_NET_GUID;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Same as calling GetSystemAddressFromIndex and GetGUIDFromIndex for all systems, but more efficient
// Indices match each other, so \a addresses[0] and \a guids[0] refer to the same system
// \param[out] addresses All system addresses. Size of the list is the number of connections. Size of the list will match the size of the \a guids list.
// \param[out] guids All guids. Size of the list is the number of connections. Size of the list will match the size of the \a addresses list.
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::getSystemList(std::vector<NetAddress>& addresses,
		                    std::vector<NetGUID>& guids) const
{
	addresses.clear();
	guids.clear();

	if (remoteSystemList == nullptr || endThreads)
		return;

	for (u32 i = 0u; i < activeSystemListSize; ++i) {
		if (activeSystemList[i]->isActive &&
		    activeSystemList[i]->connectMode == RemoteSystemStruct::CONNECTED) {
		    addresses.push_back(activeSystemList[i]->systemAddress);
		    guids.push_back(activeSystemList[i]->guid);
		}
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Bans an IP from connecting. Banned IPs persist between connections.
//
// Parameters
// IP - Dotted IP address.  Can use * as a wildcard, such as 128.0.0.* will ban
// All IP addresses starting with 128.0.0
// milliseconds - how many ms for a temporary ban.  Use 0 for a permanent ban
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::AddToBanList(cchar *IP, TimeMS milliseconds)
{
	TimeMS time = Time::nowMS();

	if (IP == nullptr || IP[0] == 0 || strlen(IP) > 15)
		return;

	// If this guy is already in the ban list, do nothing
	u32 index = 0;

	banListMutex.lock();

	for (; index < banList.size(); ++index) {
		if (strcmp(IP, banList[index]->IP) == 0) {
		    // Already in the ban list.  Just update the time
		    if (milliseconds == 0)
		        banList[index]->timeout = 0; // Infinite
		    else
		        banList[index]->timeout = time + milliseconds;
		    banListMutex.unlock();
		    return;
		}
	}

	banListMutex.unlock();

	auto banStruct = new BanStruct();
	banStruct->IP = (char*)malloc(16);
	if (milliseconds == 0)
		banStruct->timeout = 0; // Infinite
	else
		banStruct->timeout = time + milliseconds;
	strcpy(banStruct->IP, IP);
	banListMutex.lock();
	banList.push_back(banStruct);
	banListMutex.unlock();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Allows a previously banned IP to connect.
//
// Parameters
// IP - Dotted IP address.  Can use * as a wildcard, such as 128.0.0.* will ban
// All IP addresses starting with 128.0.0
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::RemoveFromBanList(cchar *IP)
{
	if (IP == nullptr || IP[0] == 0 || strlen(IP) > 15)
		return;

	u32 index = 0;
	BanStruct *temp = nullptr;

	banListMutex.lock();

	for (; index < banList.size(); ++index) {
		if (strcmp(IP, banList[index]->IP) == 0) {
		    temp = banList[index];
		    banList[index] = banList[banList.size() - 1];
		    banList.erase(banList.begin() + banList.size() - 1);
		    break;
		}
	}

	banListMutex.unlock();

	if (temp) {
		free(temp->IP);
		delete temp;
	}

}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Allows all previously banned IPs to connect.
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::clearBanList()
{
	u32 index = 0;
	banListMutex.lock();

	for (; index < banList.size(); ++index) {
		free(banList[index]->IP);
		delete banList[index];
	}

	banList.clear();

	banListMutex.unlock();
}
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::SetLimitIPConnectionFrequency(bool b)
{
	limitConnectionFrequencyFromTheSameIP = b;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Determines if a particular IP is banned.
//
// Parameters
// IP - Complete dotted IP address
//
// Returns
// True if IP matches any IPs in the ban list, accounting for any wildcards.
// False otherwise.
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool NetPeer::IsBanned(cchar *IP)
{
	if (IP == nullptr || IP[0] == 0 || strlen(IP) > 15)
		return false;

	u32 banListIndex = 0u;

	if (banList.empty())
		return false; // Skip the mutex if possible

	auto time = Time::nowMS();

	banListMutex.lock();

	while (banListIndex < banList.size()) {
		if (banList[banListIndex]->timeout>0 && banList[banListIndex]->timeout < time) {
		    // Delete expired ban
		    auto temp = banList[banListIndex];
		    banList[banListIndex] = banList[banList.size() - 1];
		    banList.erase(banList.begin() + banList.size() - 1);
		    free(temp->IP);
		    delete temp;
		} else {
		    u32 characterIndex = 0u;

		    while (true) {
		        if (banList[banListIndex]->IP[characterIndex] == IP[characterIndex]) {
		            // Equal characters

		            if (IP[characterIndex] == 0) {
		                banListMutex.unlock();
		                // End of the string and the strings match

		                return true;
		            }

		            ++characterIndex;
		        }

		        else {
		            if (banList[banListIndex]->IP[characterIndex] == 0 || IP[characterIndex] == 0) {
		                // End of one of the strings
		                break;
		            }

		            // Characters do not match
		            if (banList[banListIndex]->IP[characterIndex] == '*') {
		                banListMutex.unlock();

		                // Domain is banned.
		                return true;
		            }

		            // Characters do not match and it is not a *
		            break;
		        }
		    }

		    ++banListIndex;
		}
	}

	banListMutex.unlock();

	// No match found.
	return false;
}

void NetPeer::ping(const NetAddress target) {
	pingInternal(target, false, Packet::UNRELIABLE);
}

bool NetPeer::ping(cchar* host, u16 remotePort, bool onlyReplyOnAcceptingConnections,
		            u32 connectionSocketIndex)
{
	if (host == nullptr)
		return false;

	// If this assert hits then Startup wasn't called or the call failed.
	ASSERT(connectionSocketIndex < socketList.size());

	BitStream bitStream(sizeof(u8) + sizeof(TimeMS));
	if (onlyReplyOnAcceptingConnections) {
		bitStream.write(static_cast<u8>(ID_UNCONNECTED_PING_OPEN_CONNECTIONS));
	} else {
		bitStream.write(static_cast<u8>(ID_UNCONNECTED_PING));
	}

	bitStream.write(Time::nowMS());

	bitStream.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID),
		                        sizeof OFFLINE_MESSAGE_DATA_ID);

	bitStream.write(getMyGUID());

	// No timestamp for 255.255.255.255
	u32 realIndex = getJinraSocketFromUserConnectionSocketIndex(connectionSocketIndex);

	NetSocket2::SendParameters bsp;
	bsp.data = (char*)bitStream.getData();
	bsp.length = bitStream.getNumberOfBytesUsed();
	bsp.systemAddress.fromStringExplicitPort(host, remotePort,
		                                        socketList[realIndex]->getBoundAddress().getIPVersion());
	if (bsp.systemAddress == UNASSIGNED_SYSTEM_ADDRESS)
		return false;

	bsp.systemAddress.fixForIPVersion(socketList[realIndex]->getBoundAddress());
	socketList[realIndex]->send(&bsp);

	return true;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Returns the average of all ping times read for a specified target
//
// Parameters:
// target - whose time to read
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int NetPeer::GetAveragePing(const AddressOrGUID systemIdentifier) const
{
	int sum, quantity;
	auto remoteSystem = getRemoteSystem(systemIdentifier, false, false);

	if (remoteSystem == nullptr)
		return -1;

	for (sum = 0, quantity = 0; quantity < PING_TIMES_ARRAY_SIZE; quantity++) {
		if (remoteSystem->pingAndClockDifferential[quantity].pingTime == 65535)
		    break;
		sum += remoteSystem->pingAndClockDifferential[quantity].pingTime;
	}

	if (quantity > 0)
		return sum / quantity;
	return -1;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Returns the last ping time read for the specific player or -1 if none read yet
//
// Parameters:
// target - whose time to read
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int NetPeer::getLastPing(const AddressOrGUID systemIdentifier) const
{
	auto remoteSystem = getRemoteSystem(systemIdentifier, false, false);

	if (remoteSystem == 0)
		return -1;

	//	return (int)(remoteSystem->reliabilityLayer.GetAckPing()/(TimeUS)1000);

	if (remoteSystem->pingAndClockDifferentialWriteIndex == 0)
		return remoteSystem->pingAndClockDifferential[PING_TIMES_ARRAY_SIZE - 1].pingTime;
	return remoteSystem->pingAndClockDifferential[remoteSystem->pingAndClockDifferentialWriteIndex - 1].pingTime;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
// Returns the lowest ping time read or -1 if none read yet
//
// Parameters:
// target - whose time to read
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int NetPeer::GetLowestPing(const AddressOrGUID systemIdentifier) const
{
	auto remoteSystem = getRemoteSystem(systemIdentifier, false, false);

	if (remoteSystem == nullptr)
		return -1;

	return remoteSystem->lowestPing;
}

void NetPeer::SetOccasionalPing(bool doPing)
{
	occasionalPing = doPing;
}

TimeMS NetPeer::getClockDifferential(const AddressOrGUID systemIdentifier) const
{
	auto remoteSystem = getRemoteSystem(systemIdentifier, false, false);
	if (remoteSystem == nullptr)
		return 0;
	return getClockDifferentialInt(remoteSystem);
}

TimeMS NetPeer::getClockDifferentialInt(RemoteSystemStruct *remoteSystem)
{
	s32 lowestPingSoFar = 65535;

	TimeMS clockDifferential = 0;

	for (s32 counter = 0; counter < PING_TIMES_ARRAY_SIZE; counter++) {
		if (remoteSystem->pingAndClockDifferential[counter].pingTime == 65535)
		    break;

		if (remoteSystem->pingAndClockDifferential[counter].pingTime < lowestPingSoFar) {
		    clockDifferential = remoteSystem->pingAndClockDifferential[counter].clockDifferential;
		    lowestPingSoFar = remoteSystem->pingAndClockDifferential[counter].pingTime;
		}
	}

	return clockDifferential;
}

void NetPeer::SetOfflinePingResponse(cchar *data, const u32 length)
{
	ASSERT(length < 400);

	netPeerMutexes[offlinePingResponse_Mutex].lock();
	offlinePingResponse.reset();

	if (data && length > 0)
		offlinePingResponse.write(data, length);

	netPeerMutexes[offlinePingResponse_Mutex].unlock();
}

void NetPeer::GetOfflinePingResponse(char **data, u32 *length)
{
	netPeerMutexes[offlinePingResponse_Mutex].lock();
	*data = reinterpret_cast<char*>(offlinePingResponse.getData());
	*length = static_cast<int>(offlinePingResponse.getNumberOfBytesUsed());
	netPeerMutexes[offlinePingResponse_Mutex].unlock();
}

NetAddress NetPeer::GetInternalID(const NetAddress systemAddress, const int index) const
{
	if (systemAddress == UNASSIGNED_SYSTEM_ADDRESS) {
		return ipList[index];
	}
	//		NetAddress returnValue;
	auto remoteSystem = getRemoteSystemFromSystemAddress(systemAddress, false, true);
	if (remoteSystem == nullptr)
		return UNASSIGNED_SYSTEM_ADDRESS;

	return remoteSystem->theirInternalSystemAddress[index];
}

void NetPeer::SetInternalID(NetAddress systemAddress, int index)
{
	ASSERT(index >= 0 && index < MAXIMUM_NUMBER_OF_INTERNAL_IDS);
	ipList[index] = systemAddress;
}

NetAddress NetPeer::GetExternalID(const NetAddress target) const
{
	auto inactiveExternalId = UNASSIGNED_SYSTEM_ADDRESS;

	if (target == UNASSIGNED_SYSTEM_ADDRESS)
		return firstExternalID;

	// First check for active connection with this systemAddress
	for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		if (remoteSystemList[i].systemAddress == target) {
		    if (remoteSystemList[i].isActive)
		        return remoteSystemList[i].myExternalSystemAddress;
		    if (remoteSystemList[i].myExternalSystemAddress != UNASSIGNED_SYSTEM_ADDRESS)
		        inactiveExternalId = remoteSystemList[i].myExternalSystemAddress;
		}
	}

	return inactiveExternalId;
}

NetAddress NetPeer::getMyBoundAddress(const s32 socketIndex)
{
	std::vector<NetSocket2*> sockets;
	getSockets(sockets);
	if (sockets.size() > 0)
		return sockets[socketIndex]->getBoundAddress();
	return UNASSIGNED_SYSTEM_ADDRESS;
}

const NetGUID& NetPeer::GetGuidFromSystemAddress(const NetAddress input) const
{
	if (input == UNASSIGNED_SYSTEM_ADDRESS)
		return myGuid;

	if (input.systemIndex != static_cast<SystemIndex>(-1) && input.systemIndex < maximumNumberOfPeers && remoteSystemList[input.systemIndex].systemAddress == input)
		return remoteSystemList[input.systemIndex].guid;

	for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		if (remoteSystemList[i].systemAddress == input) {
		    // Set the systemIndex so future lookups will be fast
		    remoteSystemList[i].guid.systemIndex = static_cast<SystemIndex>(i);

		    return remoteSystemList[i].guid;
		}
	}

	return UNASSIGNED_NET_GUID;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

u32 NetPeer::GetSystemIndexFromGuid(const NetGUID input) const
{
	if (input == UNASSIGNED_NET_GUID)
		return static_cast<u32>(-1);

	if (input == myGuid)
		return static_cast<u32>(-1);

	if (input.systemIndex != static_cast<SystemIndex>(-1) && input.systemIndex < maximumNumberOfPeers && remoteSystemList[input.systemIndex].guid == input)
		return input.systemIndex;

	for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		if (remoteSystemList[i].guid == input) {
		    // Set the systemIndex so future lookups will be fast
		    remoteSystemList[i].guid.systemIndex = static_cast<SystemIndex>(i);

		    return i;
		}
	}

	return static_cast<u32>(-1);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

NetAddress NetPeer::GetSystemAddressFromGuid(const NetGUID input) const
{
	if (input == UNASSIGNED_NET_GUID)
		return UNASSIGNED_SYSTEM_ADDRESS;

	if (input == myGuid)
		return GetInternalID(UNASSIGNED_SYSTEM_ADDRESS);

	if (input.systemIndex != static_cast<SystemIndex>(-1) && input.systemIndex < maximumNumberOfPeers && remoteSystemList[input.systemIndex].guid == input)
		return remoteSystemList[input.systemIndex].systemAddress;

	for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		if (remoteSystemList[i].guid == input) {
		    // Set the systemIndex so future lookups will be fast
		    remoteSystemList[i].guid.systemIndex = static_cast<SystemIndex>(i);

		    return remoteSystemList[i].systemAddress;
		}
	}

	return UNASSIGNED_SYSTEM_ADDRESS;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Set the time, in MS, to use before considering ourselves disconnected after not being able to deliver a reliable packet
// \param[in] time Time, in MS
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void NetPeer::setTimeoutTime(TimeMS timeMS, const NetAddress target)
{
	if (target == UNASSIGNED_SYSTEM_ADDRESS) {
		defaultTimeoutTime = timeMS;

		for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		    if (remoteSystemList[i].isActive) {
		        if (remoteSystemList[i].isActive)
		            remoteSystemList[i].reliabilityLayer.setTimeoutTime(timeMS);
		    }
		}
	} else {
		auto remoteSystem = getRemoteSystemFromSystemAddress(target, false, true);

		if (remoteSystem != nullptr)
		    remoteSystem->reliabilityLayer.setTimeoutTime(timeMS);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

TimeMS NetPeer::GetTimeoutTime(const NetAddress target) const
{
	if (target == UNASSIGNED_SYSTEM_ADDRESS) {
		return defaultTimeoutTime;
	}
	auto remoteSystem = getRemoteSystemFromSystemAddress(target, false, true);

	if (remoteSystem != nullptr)
		return remoteSystem->reliabilityLayer.getTimeoutTime();
	return defaultTimeoutTime;
}

s32 NetPeer::GetMTUSize(const NetAddress target) const
{
	if (target != UNASSIGNED_SYSTEM_ADDRESS) {
		auto rss = getRemoteSystemFromSystemAddress(target, false, true);
		if (rss)
		    return rss->MTUSize;
	}
	return defaultMTUSize;
}

u32 NetPeer::GetNumberOfAddresses()
{
	if (!isActive())
		fillIPList();

	u32 i = 0u;
	while (ipList[i] != UNASSIGNED_SYSTEM_ADDRESS)
		i++;
	return i;
}

cchar* NetPeer::GetLocalIP(u32 index)
{
	if (!isActive())
		fillIPList();

	static char str[128];
	ipList[index].toString(false, str);
	return str;
}

bool NetPeer::isLocalIP(cchar* ip)
{
	if (ip == nullptr || ip[0] == 0)
		return false;


	if (strcmp(ip, "127.0.0.1") == 0 || strcmp(ip, "localhost") == 0)
		return true;

	auto num = GetNumberOfAddresses();
	for (u32 i = 0u; i < num; ++i) {
		if (strcmp(ip, GetLocalIP(i)) == 0)
		    return true;
	}
	return false;
}

void NetPeer::AllowConnectionResponseIPMigration(bool allow) {
	allowConnectionResponseIPMigration = allow;
}

bool NetPeer::AdvertiseSystem(cchar *host, u16 remotePort, cchar *data, int dataLength, unsigned connectionSocketIndex)
{
	BitStream bs;
	bs.write(static_cast<MessageID>(ID_ADVERTISE_SYSTEM));
	bs.writeAlignedBytes(reinterpret_cast<const u8*>(data), dataLength);
	return SendOutOfBand(host, remotePort, reinterpret_cast<cchar*>(bs.getData()), bs.getNumberOfBytesUsed(), connectionSocketIndex);
}

void NetPeer::SetSplitMessageProgressInterval(int interval)
{
	ASSERT(interval >= 0);
	splitMessageProgressInterval = interval;
	for (u16 i = 0u; i < maximumNumberOfPeers; ++i)
		remoteSystemList[i].reliabilityLayer.setSplitMessageProgressInterval(splitMessageProgressInterval);
}

void NetPeer::SetUnreliableTimeout(TimeMS timeoutMS)
{
	unreliableTimeout = timeoutMS;
	for (u16 i = 0u; i < maximumNumberOfPeers; ++i)
		remoteSystemList[i].reliabilityLayer.setUnreliableTimeout(unreliableTimeout);
}

void NetPeer::PushBackPacket(Packet* packet, bool pushAtHead)
{
	if (packet == nullptr)
		return;

	packetReturnMutex.lock();
	if (pushAtHead) {
		packetReturnQueue.insert(packetReturnQueue.begin(), packet);
	} else {
		packetReturnQueue.push_back(packet);
	}
	packetReturnMutex.unlock();
}

void NetPeer::ChangeSystemAddress(NetGUID guid, const NetAddress &systemAddress)
{
	auto bcs = bufferedCommands.Allocate();
	bcs->data = nullptr;
	bcs->systemIdentifier.address = systemAddress;
	bcs->systemIdentifier.guid = guid;
	bcs->command = BufferedCommandStruct::BCS_CHANGE_SYSTEM_ADDRESS;
	bufferedCommands.Push(bcs);
}

Packet* NetPeer::allocatePacket(u32 dataSize) {
	return allocatePacket(dataSize, static_cast<u8*>(malloc(dataSize)));
}

Packet* NetPeer::allocatePacket(u32 dataSize, u8* packetData)
{
	packetAllocationPoolMutex.lock();
	auto packet = packetAllocationPool.allocate();
	packetAllocationPoolMutex.unlock();
	packet = new (static_cast<void*>(packet)) Packet;
	ASSERT(packet);
	packet->data = packetData;
	packet->length = dataSize;
	packet->bitSize = BYTES_TO_BITS(dataSize);
	packet->deleteData = true;
	packet->guid = UNASSIGNED_NET_GUID;
	return packet;
}

NetSocket2* NetPeer::getSocket(const NetAddress target)
{
	// Send a query to the thread to get the socket, and return when we got it
	auto bcs = bufferedCommands.Allocate();
	bcs->command = BufferedCommandStruct::BCS_GET_SOCKET;
	bcs->systemIdentifier = target;
	bcs->data = nullptr;
	bufferedCommands.Push(bcs);

	// Block up to one second to get the socket, although it should actually take virtually no time
	TimeMS stopWaiting = Time::nowMS() + 1000u;
	while (Time::nowMS() < stopWaiting) {
		if (!isMainLoopThreadActive)
		    return nullptr;

		Time::sleepMS(0u);

		auto sqo = socketQueryOutput.Pop();
		if (sqo != nullptr) {
		    auto output = sqo->sockets;
		    sqo->sockets.clear();
		    socketQueryOutput.Deallocate(sqo);
		    if (output.size())
		        return output[0];
		    break;
		}
	}
	return nullptr;
}

void NetPeer::getSockets(std::vector<NetSocket2*>& sockets)
{
	sockets.clear();

	// Send a query to the thread to get the socket, and return when we got it
	auto bcs = bufferedCommands.Allocate();
	bcs->command = BufferedCommandStruct::BCS_GET_SOCKET;
	bcs->systemIdentifier = UNASSIGNED_SYSTEM_ADDRESS;
	bcs->data = nullptr;
	bufferedCommands.Push(bcs);

	// Block up to one second to get the socket, although it should actually take virtually no time
	while (true) {
		if (!isMainLoopThreadActive)
		    return;

		Time::sleepMS(0u);

		auto sqo = socketQueryOutput.Pop();
		if (sqo != nullptr) {
		    sockets = sqo->sockets;
		    sqo->sockets.clear();
		    socketQueryOutput.Deallocate(sqo);
		    return;
		}
	}
}

void NetPeer::ReleaseSockets(std::vector<NetSocket2*>& sockets) {
	sockets.clear();
}

void NetPeer::WriteOutOfBandHeader(BitStream *bitStream)
{
	bitStream->write(static_cast<MessageID>(ID_OUT_OF_BAND_INTERNAL));
	bitStream->write(myGuid);
	bitStream->writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), 
		                            sizeof OFFLINE_MESSAGE_DATA_ID);
}

void NetPeer::SetUserUpdateThread(void(*_userUpdateThreadPtr)(NetPeer *, void *), void *_userUpdateThreadData)
{
	userUpdateThreadPtr = _userUpdateThreadPtr;
	userUpdateThreadData = _userUpdateThreadData;
}

bool NetPeer::SendOutOfBand(cchar *host, u16 remotePort, cchar *data, BitSize_t dataLength, unsigned connectionSocketIndex)
{
	if (!isActive())
		return false;

	if (host == nullptr || host[0] == 0)
		return false;

	// If this assert hits then Startup wasn't called or the call failed.
	ASSERT(connectionSocketIndex < socketList.size());

	// This is a security measure.  Don't send data longer than this value
	ASSERT(dataLength <= (MAX_OFFLINE_DATA_LENGTH + sizeof(u8) + sizeof(TimeMS) +
		NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID));

	// 34 bytes
	BitStream bitStream;
	WriteOutOfBandHeader(&bitStream);

	if (dataLength > 0)
		bitStream.write(data, dataLength);

	u32 realIndex = getJinraSocketFromUserConnectionSocketIndex(connectionSocketIndex);

	NetSocket2::SendParameters bsp;
	bsp.data = reinterpret_cast<char*>(bitStream.getData());
	bsp.length = bitStream.getNumberOfBytesUsed();
	bsp.systemAddress.fromStringExplicitPort(host, remotePort, socketList[realIndex]->getBoundAddress().getIPVersion());
	bsp.systemAddress.fixForIPVersion(socketList[realIndex]->getBoundAddress());

	socketList[realIndex]->send(&bsp);

	return true;
}

NetStatistics * NetPeer::GetStatistics(const NetAddress systemAddress, NetStatistics *rns) const
{
	static NetStatistics staticStatistics;
	auto systemStats = !rns ? &staticStatistics : rns;

	if (systemAddress == UNASSIGNED_SYSTEM_ADDRESS) {
		bool firstWrite = false;
		// Return a crude sum
		for (u16 i = 0; i < maximumNumberOfPeers; ++i) {
		    if (remoteSystemList[i].isActive) {
		        NetStatistics rnsTemp;
		        remoteSystemList[i].reliabilityLayer.getStatistics(&rnsTemp);

		        if (firstWrite == false) {
		            memcpy(systemStats, &rnsTemp, sizeof(NetStatistics));
		            firstWrite = true;
		        } else
		            *systemStats += rnsTemp;
		    }
		}
		return systemStats;
	}

	auto rss = getRemoteSystemFromSystemAddress(systemAddress, false, false);
	if (rss && endThreads == false) {
		rss->reliabilityLayer.getStatistics(systemStats);
		return systemStats;
	}

	return nullptr;
}

void NetPeer::getStatisticsList(std::vector<NetAddress>& addresses,
		                        std::vector<NetGUID>& guids, 
		                        std::vector<NetStatistics> &statistics)
{
	addresses.clear();
	guids.clear();
	statistics.clear();

	if (remoteSystemList == nullptr || endThreads == true)
		return;

	for (u32 i = 0; i < activeSystemListSize; ++i) {
		if (activeSystemList[i]->isActive &&
		    activeSystemList[i]->connectMode == RemoteSystemStruct::CONNECTED) {
		    addresses.push_back(activeSystemList[i]->systemAddress);
		    guids.push_back(activeSystemList[i]->guid);
		    NetStatistics rns;
		    activeSystemList[i]->reliabilityLayer.getStatistics(&rns);
		    statistics.push_back(rns);
		}
	}
}

bool NetPeer::GetStatistics(const u32 index, NetStatistics* rns) const
{
	if (index < maximumNumberOfPeers && remoteSystemList[index].isActive) {
		remoteSystemList[index].reliabilityLayer.getStatistics(rns);
		return true;
	}
	return false;
}

u32 NetPeer::GetReceiveBufferSize()
{
	packetReturnMutex.lock();
	u32 size = packetReturnQueue.size();
	packetReturnMutex.unlock();
	return size;
}

s32 NetPeer::GetIndexFromSystemAddress(const NetAddress systemAddress, bool calledFromNetworkThread) const
{
	if (systemAddress == UNASSIGNED_SYSTEM_ADDRESS)
		return -1;

	if (systemAddress.systemIndex != (SystemIndex)-1 && systemAddress.systemIndex < maximumNumberOfPeers && remoteSystemList[systemAddress.systemIndex].systemAddress == systemAddress && remoteSystemList[systemAddress.systemIndex].isActive)
		return systemAddress.systemIndex;

	if (calledFromNetworkThread) {
		return getRemoteSystemIndex(systemAddress);
	}

	// remoteSystemList in user and network thread
	for (u32 i = 0; i < maximumNumberOfPeers; ++i)
		if (remoteSystemList[i].isActive && remoteSystemList[i].systemAddress == systemAddress)
		    return i;

	// If no active results found, try previously active results.
	for (u32 i = 0; i < maximumNumberOfPeers; ++i)
		if (remoteSystemList[i].systemAddress == systemAddress)
		    return i;

	return -1;
}

s32 NetPeer::GetIndexFromGuid(const NetGUID guid) const
{
	if (guid == UNASSIGNED_NET_GUID)
		return -1;

	if (guid.systemIndex != static_cast<SystemIndex>(-1) && guid.systemIndex < maximumNumberOfPeers && remoteSystemList[guid.systemIndex].guid == guid && remoteSystemList[guid.systemIndex].isActive)
		return guid.systemIndex;

	// remoteSystemList in user and network thread
	for (u32 i = 0u; i < maximumNumberOfPeers; ++i)
		if (remoteSystemList[i].isActive && remoteSystemList[i].guid == guid)
		    return i;

	// If no active results found, try previously active results.
	for (u32 i = 0u; i < maximumNumberOfPeers; ++i)
		if (remoteSystemList[i].guid == guid)
		    return i;

	return -1;
}

NetPeer::ConnectionAttemptResult NetPeer::sendConnectionRequest(const String& host,
		                                                        u16 remotePort, const String& connectionPassword,
		                                                        u32 connectionSocketIndex, u32 extraData, 
		                                                        u32 sendConnectionAttemptCount, 
		                                                        u32 timeBetweenSendConnectionAttemptsMS, 
		                                                        TimeMS timeoutTime)
{
	ASSERT(remotePort != 0);
	NetAddress systemAddress;
	if (!systemAddress.fromStringExplicitPort(host, remotePort, socketList[connectionSocketIndex]->getBoundAddress().getIPVersion()))
		return ConnectionAttemptResult::CANNOT_RESOLVE_DOMAIN_NAME;

	// Already connected?
	if (getRemoteSystemFromSystemAddress(systemAddress, false, true))
		return ConnectionAttemptResult::ALREADY_CONNECTED_TO_ENDPOINT;

	auto rcs = new RequestedConnectionStruct();
	rcs->systemAddress = systemAddress;
	rcs->nextRequestTime = Time::nowMS();
	rcs->requestsMade = 0;
	rcs->data = nullptr;
	rcs->socket = nullptr;
	rcs->extraData = extraData;
	rcs->socketIndex = connectionSocketIndex;
	rcs->actionToTake = RequestedConnectionStruct::CONNECT;
	rcs->sendConnectionAttemptCount = sendConnectionAttemptCount;
	rcs->timeBetweenSendConnectionAttemptsMS = timeBetweenSendConnectionAttemptsMS;
	rcs->connectionPassword = connectionPassword;
	rcs->timeoutTime = timeoutTime;

	// Return false if already pending, else push on queue
	requestedConnectionQueueMutex.lock();
	for (u32 i = 0u; i < requestedConnectionQueue.size(); ++i) {
		if (requestedConnectionQueue[i]->systemAddress == systemAddress) {
		    requestedConnectionQueueMutex.unlock();
		    delete rcs;
		    return ConnectionAttemptResult::ALREADY_IN_PROGRESS;
		}
	}
	requestedConnectionQueue.push_back(rcs);
	requestedConnectionQueueMutex.unlock();

	return ConnectionAttemptResult::STARTED;
}

NetPeer::ConnectionAttemptResult NetPeer::sendConnectionRequest(const String& host,
		                                                        u16 remotePort, const String& password, u32 connectionSocketIndex,
		                                                        u32 extraData, u32 sendConnectionAttemptCount, 
		                                                        u32 timeBetweenSendConnectionAttemptsMS, 
		                                                        TimeMS timeoutTime, NetSocket2* socket)
{
	NetAddress systemAddress;
	systemAddress.fromStringExplicitPort(host, remotePort);

	// Already connected?
	if (getRemoteSystemFromSystemAddress(systemAddress, false, true))
		return ConnectionAttemptResult::ALREADY_CONNECTED_TO_ENDPOINT;

	auto rcs = new RequestedConnectionStruct();
	rcs->systemAddress = systemAddress;
	rcs->nextRequestTime = Time::nowMS();
	rcs->requestsMade = 0u;
	rcs->data = nullptr;
	rcs->socket = nullptr;
	rcs->extraData = extraData;
	rcs->socketIndex = connectionSocketIndex;
	rcs->actionToTake = RequestedConnectionStruct::CONNECT;
	rcs->sendConnectionAttemptCount = sendConnectionAttemptCount;
	rcs->timeBetweenSendConnectionAttemptsMS = timeBetweenSendConnectionAttemptsMS;
	rcs->connectionPassword = password;
	rcs->timeoutTime = timeoutTime;
	rcs->socket = socket;

	// Return false if already pending, else push on queue
	requestedConnectionQueueMutex.lock();
	for (u32 i = 0u; i < requestedConnectionQueue.size(); ++i) {
		if (requestedConnectionQueue[i]->systemAddress == systemAddress) {
		    requestedConnectionQueueMutex.unlock();
		    // Not necessary
		    delete rcs;
		    return ConnectionAttemptResult::ALREADY_IN_PROGRESS;
		}
	}
	requestedConnectionQueue.push_back(rcs);
	requestedConnectionQueueMutex.unlock();

	return ConnectionAttemptResult::STARTED;
}

NetPeer::RemoteSystemStruct* NetPeer::getRemoteSystem(const AddressOrGUID& systemIdentifier, 
		                                                bool calledFromNetworkThread, 
		                                                bool onlyActive) const
{
	if (systemIdentifier.guid != UNASSIGNED_NET_GUID) {
		return GetRemoteSystemFromGUID(systemIdentifier.guid, onlyActive);
	}
	return getRemoteSystemFromSystemAddress(systemIdentifier.address, calledFromNetworkThread, onlyActive);
}

NetPeer::RemoteSystemStruct* NetPeer::getRemoteSystemFromSystemAddress(const NetAddress& systemAddress, bool calledFromNetworkThread, bool onlyActive) const
{
	if (systemAddress == UNASSIGNED_SYSTEM_ADDRESS)
		return nullptr;

	if (calledFromNetworkThread) {
		u32 index = getRemoteSystemIndex(systemAddress);
		if (index != static_cast<u32>(-1)) {
		    if (!onlyActive || remoteSystemList[index].isActive) {
		        ASSERT(remoteSystemList[index].systemAddress == systemAddress);
		        return remoteSystemList + index;
		    }
		}
	} else {
		s32 deadConnectionIndex = -1;

		// Active connections take priority.  But if there are no active connections, return the first systemAddress match found
		for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		    if (remoteSystemList[i].systemAddress == systemAddress) {
		        if (remoteSystemList[i].isActive)
		            return remoteSystemList + i;
		        if (deadConnectionIndex == -1)
		            deadConnectionIndex = i;
		    }
		}

		if (deadConnectionIndex != -1 && !onlyActive)
		    return remoteSystemList + deadConnectionIndex;
	}

	return nullptr;
}

NetPeer::RemoteSystemStruct* NetPeer::GetRemoteSystemFromGUID(const NetGUID& guid, bool onlyActive) const
{
	if (guid == UNASSIGNED_NET_GUID)
		return nullptr;

	for (u32 i = 0u; i < maximumNumberOfPeers; ++i) {
		if (remoteSystemList[i].guid == guid && (!onlyActive || remoteSystemList[i].isActive))
		    return remoteSystemList + i;
	}
	return nullptr;
}

void NetPeer::ParseConnectionRequestPacket(RemoteSystemStruct *remoteSystem, const NetAddress &systemAddress, cchar *data, int byteSize) const
{
	BitStream bs((u8*)data, byteSize, false);
	bs.ignoreBytes(sizeof(MessageID));
	NetGUID guid;
	bs.read(guid);
	TimeMS incomingTimestamp;
	bs.read(incomingTimestamp);

	String password;
	bs.read(password);

	if (incomingPassword != password) {
		// This one we only send once since we don't care if it arrives.
		BitStream bitStream;
		bitStream.write(static_cast<MessageID>(ID_INVALID_PASSWORD));
		bitStream.write(GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));
		SendImmediate(reinterpret_cast<char*>(bitStream.getData()), bitStream.getNumberOfBytesUsed(),
		                Packet::IMMEDIATE_PRIORITY, Packet::RELIABLE, 0, systemAddress, false, 
		                false, Time::nowUS(), 0);
		remoteSystem->connectMode = RemoteSystemStruct::DISCONNECT_ASAP_SILENTLY;
		return;
	}

	// OK
	remoteSystem->connectMode = RemoteSystemStruct::HANDLING_CONNECTION_REQUEST;

	OnConnectionRequest(remoteSystem, incomingTimestamp);
}

void NetPeer::OnConnectionRequest(RemoteSystemStruct *remoteSystem, TimeMS incomingTimestamp) const
{
	BitStream bitStream;
	bitStream.write(static_cast<MessageID>(ID_CONNECTION_REQUEST_ACCEPTED));
	bitStream.write(remoteSystem->systemAddress);
	auto systemIndex = static_cast<SystemIndex>(GetIndexFromSystemAddress(remoteSystem->systemAddress, true));

	ASSERT(systemIndex != 65535);
	bitStream.write(systemIndex);
	for (u32 i = 0u; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i)
		bitStream.write(ipList[i]);
	bitStream.write(incomingTimestamp);
	bitStream.write(Time::nowMS());

	SendImmediate(reinterpret_cast<char*>(bitStream.getData()), bitStream.getNumberOfBitsUsed(), 
		            Packet::IMMEDIATE_PRIORITY, Packet::RELIABLE_ORDERED, 0, 
		            remoteSystem->systemAddress, false, false, Time::nowUS(), 0);
}

void NetPeer::notifyAndFlagForShutdown(const NetAddress systemAddress, bool performImmediate, u8 orderingChannel, Packet::Priority disconnectionNotificationPriority)
{
	BitStream temp(sizeof(u8));
	temp.write(static_cast<MessageID>(ID_DISCONNECTION_NOTIFICATION));
	if (performImmediate) {
		SendImmediate(reinterpret_cast<char*>(temp.getData()), temp.getNumberOfBitsUsed(), disconnectionNotificationPriority, Packet::RELIABLE_ORDERED, orderingChannel, systemAddress, false, false, Time::nowUS(), 0);
	    auto rss = getRemoteSystemFromSystemAddress(systemAddress, true, true);
		rss->connectMode = RemoteSystemStruct::DISCONNECT_ASAP;
	} else {
		SendBuffered(reinterpret_cast<cchar*>(temp.getData()), temp.getNumberOfBitsUsed(), disconnectionNotificationPriority, Packet::RELIABLE_ORDERED, orderingChannel, systemAddress, false, RemoteSystemStruct::DISCONNECT_ASAP, 0);
	}
}

u32 NetPeer::getNumberOfRemoteInitiatedConnections() const
{
	if (remoteSystemList == nullptr || endThreads)
		return 0u;

	auto numberOfIncomingConnections = 0u;
	for (u32 i = 0u; i < activeSystemListSize; ++i) {
		if (activeSystemList[i]->isActive &&
		    activeSystemList[i]->connectMode == RemoteSystemStruct::CONNECTED &&
		    !activeSystemList[i]->weInitiatedTheConnection) {
		    numberOfIncomingConnections++;
		}
	}

	return numberOfIncomingConnections;
}

NetPeer::RemoteSystemStruct* NetPeer::AssignSystemAddressToRemoteSystemList(const NetAddress systemAddress, 
		                                                                    RemoteSystemStruct::ConnectMode connectionMode, NetSocket2* incomingJinraSocket, bool* thisIPConnectedRecently, 
		                                                                    NetAddress bindingAddress, s32 incomingMTU, NetGUID guid)
{
	RemoteSystemStruct * remoteSystem;
	u32 assignedIndex;
	TimeMS time = Time::nowMS();
#ifdef JINRA_DEBUG
	ASSERT(systemAddress != UNASSIGNED_SYSTEM_ADDRESS);
#endif

	if (limitConnectionFrequencyFromTheSameIP) {
		if (IsLoopbackAddress(systemAddress, false) == false) {
		    for (u32 i = 0; i < maximumNumberOfPeers; ++i) {
		        if (remoteSystemList[i].isActive == true &&
		            remoteSystemList[i].systemAddress.equalsExcludingPort(systemAddress) &&
		            time >= remoteSystemList[i].connectionTime &&
		            time - remoteSystemList[i].connectionTime < 100
		        ) {
		            // 4/13/09 Attackers can flood ID_OPEN_CONNECTION_REQUEST and use up all available connection slots
		            // Ignore connection attempts if this IP address connected within the last 100 milliseconds
		            *thisIPConnectedRecently = true;
		            return nullptr;
		        }
		    }
		}
	}

	// Don't use a different port than what we received on
	bindingAddress.copyPort(incomingJinraSocket->getBoundAddress());

	*thisIPConnectedRecently = false;
	for (assignedIndex = 0; assignedIndex < maximumNumberOfPeers; assignedIndex++) {
		if (remoteSystemList[assignedIndex].isActive == false) {
		    // printf("--- Address %s has become active\n", systemAddress.ToString());

		    remoteSystem = remoteSystemList + assignedIndex;
		    ReferenceRemoteSystem(systemAddress, assignedIndex);
		    remoteSystem->MTUSize = defaultMTUSize;
		    remoteSystem->guid = guid;
		    remoteSystem->isActive = true; // This one line causes future incoming packets to go through the reliability layer
		    // Reserve this reliability layer for ourselves.
		    if (incomingMTU > remoteSystem->MTUSize)
		        remoteSystem->MTUSize = incomingMTU;
		    ASSERT(remoteSystem->MTUSize <= MAXIMUM_MTU_SIZE);
		    remoteSystem->reliabilityLayer.reset(true, remoteSystem->MTUSize);
		    remoteSystem->reliabilityLayer.setSplitMessageProgressInterval(splitMessageProgressInterval);
		    remoteSystem->reliabilityLayer.setUnreliableTimeout(unreliableTimeout);
		    remoteSystem->reliabilityLayer.setTimeoutTime(defaultTimeoutTime);
		    AddToActiveSystemList(assignedIndex);
		    if (incomingJinraSocket->getBoundAddress() == bindingAddress) {
		        remoteSystem->netSocket = incomingJinraSocket;
		    } else {
		        char str[256];
		        bindingAddress.toString(true, str);
		        // See if this is an internal IP address.
		        // If so, force binding on it so we reply on the same IP address as they sent to.
		        u32 foundIndex = static_cast<u32>(-1);

		        for (u32 ipListIndex = 0; ipListIndex < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ipListIndex++) {
		            if (ipList[ipListIndex] == UNASSIGNED_SYSTEM_ADDRESS)
		                break;

		            if (bindingAddress.equalsExcludingPort(ipList[ipListIndex])) {
		                foundIndex = ipListIndex;
		                break;
		            }
		        }

		        // 06/26/09 Unconfirmed report that Vista firewall blocks the reply if we force a binding
		        // For now use the incoming socket only
		        // Originally this code was to force a machine with multiple IP addresses to reply back on the IP
		        // that the datagram came in on
		        if (foundIndex == static_cast<u32>(-1)) {
		            // Must not be an internal LAN address. Just use whatever socket it came in on
		            remoteSystem->netSocket = incomingJinraSocket;
		        }
		    }

		    for (u32 j = 0; j < static_cast<u32>(PING_TIMES_ARRAY_SIZE); j++) {
		        remoteSystem->pingAndClockDifferential[j].pingTime = 65535;
		        remoteSystem->pingAndClockDifferential[j].clockDifferential = 0;
		    }

		    remoteSystem->connectMode = connectionMode;
		    remoteSystem->pingAndClockDifferentialWriteIndex = 0;
		    remoteSystem->lowestPing = 65535;
		    remoteSystem->nextPingTime = 0; // Ping immediately
		    remoteSystem->weInitiatedTheConnection = false;
		    remoteSystem->connectionTime = time;
		    remoteSystem->myExternalSystemAddress = UNASSIGNED_SYSTEM_ADDRESS;
		    remoteSystem->lastReliableSend = time;

#ifdef JINRA_DEBUG
		    s32 indexLoopupCheck = GetIndexFromSystemAddress(systemAddress, true);
		    if (static_cast<s32>(indexLoopupCheck) != static_cast<s32>(assignedIndex)) {
		        ASSERT(static_cast<s32>(indexLoopupCheck) == static_cast<s32>(assignedIndex));
		    }
#endif

		    return remoteSystem;
		}
	}

	return nullptr;
}

TimeMS NetPeer::getBestClockDifferential(const NetAddress systemAddress) const
{
	auto remoteSystem = getRemoteSystemFromSystemAddress(systemAddress, true, true);
	if (remoteSystem == nullptr)
		return 0u;

	return getClockDifferentialInt(remoteSystem);
}

u32 NetPeer::remoteSystemLookupHashIndex(const NetAddress& sa) const {
	return NetAddress::toInteger(sa) % (static_cast<u32>(maximumNumberOfPeers) * REMOTE_SYSTEM_LOOKUP_HASH_MULTIPLE);
}

void NetPeer::ReferenceRemoteSystem(const NetAddress& sa, u32 remoteSystemListIndex)
{
	auto oldAddress = remoteSystemList[remoteSystemListIndex].systemAddress;
	if (oldAddress != UNASSIGNED_SYSTEM_ADDRESS) {
		// Remove the reference if the reference is pointing to this inactive system
		if (getRemoteSystem(oldAddress) == &remoteSystemList[remoteSystemListIndex])
		    DereferenceRemoteSystem(oldAddress);
	}
	DereferenceRemoteSystem(sa);

	remoteSystemList[remoteSystemListIndex].systemAddress = sa;

	u32 hashIndex = remoteSystemLookupHashIndex(sa);
	auto rsi = remoteSystemIndexPool.allocate();
	if (remoteSystemLookup[hashIndex] == 0) {
		rsi->next = nullptr;
		rsi->index = remoteSystemListIndex;
		remoteSystemLookup[hashIndex] = rsi;
	} else {
		auto cur = remoteSystemLookup[hashIndex];
		while (cur->next != nullptr) {
		    cur = cur->next;
		}

		rsi = remoteSystemIndexPool.allocate();
		rsi->next = nullptr;
		rsi->index = remoteSystemListIndex;
		cur->next = rsi;
	}

	ASSERT(getRemoteSystemIndex(sa) == remoteSystemListIndex);
}

void NetPeer::DereferenceRemoteSystem(const NetAddress &sa)
{
	u32 hashIndex = remoteSystemLookupHashIndex(sa);
	auto cur = remoteSystemLookup[hashIndex];
	RemoteSystemIndex *last = nullptr;
	while (cur != nullptr) {
		if (remoteSystemList[cur->index].systemAddress == sa) {
		    if (last == nullptr) {
		        remoteSystemLookup[hashIndex] = cur->next;
		    } else {
		        last->next = cur->next;
		    }
		    remoteSystemIndexPool.release(cur);
		    break;
		}
		last = cur;
		cur = cur->next;
	}
}

u32 NetPeer::getRemoteSystemIndex(const NetAddress& sa) const
{
	auto hashIndex = remoteSystemLookupHashIndex(sa);
	auto cur = remoteSystemLookup[hashIndex];
	while (cur != nullptr) {
		if (remoteSystemList[cur->index].systemAddress == sa)
		    return cur->index;
		cur = cur->next;
	}
	return static_cast<u32>(-1);
}

NetPeer::RemoteSystemStruct* NetPeer::getRemoteSystem(const NetAddress& sa) const
{
	auto remoteSystemIndex = getRemoteSystemIndex(sa);
	if (remoteSystemIndex == static_cast<u32>(-1))
		return nullptr;
	return remoteSystemList + remoteSystemIndex;
}

void NetPeer::clearRemoteSystemLookup()
{
	remoteSystemIndexPool.clear();
	delete[] remoteSystemLookup;
	remoteSystemLookup = nullptr;
}

void NetPeer::AddToActiveSystemList(u32 remoteSystemListIndex) {
	activeSystemList[activeSystemListSize++] = remoteSystemList + remoteSystemListIndex;
}

void NetPeer::RemoveFromActiveSystemList(const NetAddress &sa)
{
	for (u32 i = 0u; i < activeSystemListSize; ++i) {
		auto rss = activeSystemList[i];
		if (rss->systemAddress == sa) {
		    activeSystemList[i] = activeSystemList[activeSystemListSize - 1];
		    --activeSystemListSize;
		    return;
		}
	}
	ASSERT("activeSystemList invalid, entry not found in RemoveFromActiveSystemList. Ensure that AddToActiveSystemList and RemoveFromActiveSystemList are called by the same thread." && false);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool NetPeer::IsLoopbackAddress(const AddressOrGUID &systemIdentifier, bool matchPort) const
{
	if (systemIdentifier.guid != UNASSIGNED_NET_GUID)
		return systemIdentifier.guid == myGuid;

	for (s32 i = 0; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS && ipList[i] != UNASSIGNED_SYSTEM_ADDRESS; ++i) {
		if (matchPort) {
		    if (ipList[i] == systemIdentifier.address)
		        return true;
		} else {
		    if (ipList[i].equalsExcludingPort(systemIdentifier.address))
		        return true;
		}
	}

	return matchPort && systemIdentifier.address == firstExternalID || 
		    !matchPort && systemIdentifier.address.equalsExcludingPort(firstExternalID);
}

void NetPeer::deallocRNS2RecvStruct(NetSocket2::RecvStruct *s)
{
	bufferedPacketsFreePoolMutex.lock();
	bufferedPacketsFreePool.push_back(s);
	bufferedPacketsFreePoolMutex.unlock();
}

NetSocket2::RecvStruct* NetPeer::allocRNS2RecvStruct()
{
	bufferedPacketsFreePoolMutex.lock();
	if (!bufferedPacketsFreePool.empty()) {
		auto s = bufferedPacketsFreePool.front();
		bufferedPacketsFreePool.pop_front();
		bufferedPacketsFreePoolMutex.unlock();
		return s;
	}
	bufferedPacketsFreePoolMutex.unlock();
	return new NetSocket2::RecvStruct();
}

void NetPeer::clearBufferedPackets()
{
	bufferedPacketsFreePoolMutex.lock();
	while (!bufferedPacketsFreePool.empty()) {
		auto s = bufferedPacketsFreePool.front();
		bufferedPacketsFreePool.pop_front();
		delete s;
	}
	bufferedPacketsFreePoolMutex.unlock();

	bufferedPacketsQueueMutex.lock();
	while (!bufferedPacketsQueue.empty()) {
		auto s = bufferedPacketsQueue.front();
		bufferedPacketsQueue.pop_front();
		delete s;
	}
	bufferedPacketsQueueMutex.unlock();
}

void NetPeer::PushBufferedPacket(NetSocket2::RecvStruct * p)
{
	bufferedPacketsQueueMutex.lock();
	bufferedPacketsQueue.push_back(p);
	bufferedPacketsQueueMutex.unlock();
}

NetSocket2::RecvStruct* NetPeer::PopBufferedPacket()
{
	bufferedPacketsQueueMutex.lock();
	if (bufferedPacketsQueue.size() > 0) {
		auto s = bufferedPacketsQueue.front();
		bufferedPacketsQueue.pop_front();
		bufferedPacketsQueueMutex.unlock();
		return s;
	}
	bufferedPacketsQueueMutex.unlock();
	return nullptr;
}

void NetPeer::pingInternal(const NetAddress target, bool performImmediate,
		                    Packet::Reliability reliability)
{
	if (!isActive())
		return;

	BitStream bitStream(sizeof(u8) + sizeof(TimeMS));
	bitStream.write(static_cast<u8>(ID_CONNECTED_PING));
	bitStream.write(Time::nowMS());
	if (performImmediate) {
		SendImmediate(reinterpret_cast<char*>(bitStream.getData()), bitStream.getNumberOfBitsUsed(),
		                Packet::IMMEDIATE_PRIORITY, reliability, 0, target, false, false,
		                Time::nowUS(), 0);
	} else {
		send(&bitStream, Packet::IMMEDIATE_PRIORITY, reliability, 0, target, false);
	}
}

void NetPeer::closeConnectionInternal(const AddressOrGUID& systemIdentifier, bool sendDisconnectionNotification, bool performImmediate, u8 orderingChannel, Packet::Priority disconnectionNotificationPriority)
{
#ifdef JINRA_DEBUG
	ASSERT(orderingChannel < 32);
#endif

	if (systemIdentifier.isUndefined())
		return;

	if (remoteSystemList == nullptr || endThreads)
		return;

	NetAddress target;
	if (systemIdentifier.address != UNASSIGNED_SYSTEM_ADDRESS) {
		target = systemIdentifier.address;
	} else {
		target = GetSystemAddressFromGuid(systemIdentifier.guid);
	}

	if (target != UNASSIGNED_SYSTEM_ADDRESS && performImmediate)
		target.fixForIPVersion(socketList[0]->getBoundAddress());

	if (sendDisconnectionNotification) {
		notifyAndFlagForShutdown(target, performImmediate, orderingChannel,
		                            disconnectionNotificationPriority);
	} else {
		if (performImmediate) {
		    auto index = getRemoteSystemIndex(target);
		    if (index != static_cast<u32>(-1)) {
		        if (remoteSystemList[index].isActive) {
		            RemoveFromActiveSystemList(target);

		            // Found the index to stop
		            // printf("--- Address %s has become inactive\n", remoteSystemList[index].systemAddress.ToString());
		            remoteSystemList[index].isActive = false;

		            remoteSystemList[index].guid = UNASSIGNED_NET_GUID;

		            // Clear any remaining messages
		            ASSERT(remoteSystemList[index].MTUSize <= MAXIMUM_MTU_SIZE);
		            remoteSystemList[index].reliabilityLayer.reset(false, remoteSystemList[index].MTUSize);

		            // Not using this socket
		            remoteSystemList[index].netSocket = nullptr;
		        }
		    }
		} else {
		    auto bcs = bufferedCommands.Allocate();
		    bcs->command = BufferedCommandStruct::BCS_CLOSE_CONNECTION;
		    bcs->systemIdentifier = target;
		    bcs->data = nullptr;
		    bcs->orderingChannel = orderingChannel;
		    bcs->priority = disconnectionNotificationPriority;
		    bufferedCommands.Push(bcs);
		}
	}
}

void NetPeer::SendBuffered(cchar *data, BitSize_t numberOfBitsToSend, Packet::Priority priority,
		                    Packet::Reliability reliability, char orderingChannel,
		                    const AddressOrGUID systemIdentifier, bool broadcast,
		                    RemoteSystemStruct::ConnectMode connectionMode, u32 receipt)
{
	auto bcs = bufferedCommands.Allocate();

	// Making a copy doesn't lose efficiency because I tell the reliability layer to use this
	// allocation for its own copy.
	bcs->data = static_cast<char*>(malloc(static_cast<size_t>(BITS_TO_BYTES(numberOfBitsToSend))));
	if (bcs->data == nullptr) {
		notifyOutOfMemory(__FILE__, __LINE__);
		bufferedCommands.Deallocate(bcs);
		return;
	}

	ASSERT(!(reliability >= Packet::NUMBER_OF_RELIABILITIES || reliability < 0));
	ASSERT(!(priority > Packet::NUMBER_OF_PRIORITIES || priority < 0));
	ASSERT(!(orderingChannel >= NUMBER_OF_ORDERED_STREAMS));

	memcpy(bcs->data, data, static_cast<size_t>(BITS_TO_BYTES(numberOfBitsToSend)));
	bcs->numberOfBitsToSend = numberOfBitsToSend;
	bcs->priority = priority;
	bcs->reliability = reliability;
	bcs->orderingChannel = orderingChannel;
	bcs->systemIdentifier = systemIdentifier;
	bcs->broadcast = broadcast;
	bcs->connectionMode = connectionMode;
	bcs->receipt = receipt;
	bcs->command = BufferedCommandStruct::BCS_SEND;
	bufferedCommands.Push(bcs);

	if (priority == Packet::IMMEDIATE_PRIORITY) {
		// Forces pending sends to go out now, rather than waiting to the next update interval
		quitAndDataEvents.setEvent();
	}
}

void NetPeer::SendBufferedList(cchar **data, const int *lengths, const int numParameters,
		                        Packet::Priority priority, Packet::Reliability reliability, char orderingChannel, const AddressOrGUID systemIdentifier, bool broadcast, RemoteSystemStruct::ConnectMode connectionMode, u32 receipt)
{
	u32 totalLength = 0;

	for (s32 i = 0; i < numParameters; ++i) {
		if (lengths[i] > 0)
		    totalLength += lengths[i];
	}

	if (totalLength == 0u)
		return;

	// Making a copy doesn't lose efficiency because I tell the reliability layer to use this 
	// allocation for its own copy.
    auto dataAggregate = static_cast<char*>(malloc(static_cast<size_t>(totalLength))); 
	if (dataAggregate == nullptr) {
		notifyOutOfMemory(__FILE__, __LINE__);
		return;
	}

	u32 lengthOffset = 0u;
	for (s32 i = 0; i < numParameters; ++i) {
		if (lengths[i] > 0) {
		    memcpy(dataAggregate + lengthOffset, data[i], lengths[i]);
		    lengthOffset += lengths[i];
		}
	}

	if (!broadcast  && IsLoopbackAddress(systemIdentifier, true)) {
		sendLoopback(dataAggregate, totalLength);
		free(dataAggregate);
		return;
	}

	ASSERT(!(reliability >= Packet::NUMBER_OF_RELIABILITIES || reliability < 0));
	ASSERT(!(priority > Packet::NUMBER_OF_PRIORITIES || priority < 0));
	ASSERT(!(orderingChannel >= NUMBER_OF_ORDERED_STREAMS));

	auto bcs = bufferedCommands.Allocate();
	bcs->data = dataAggregate;
	bcs->numberOfBitsToSend = BYTES_TO_BITS(totalLength);
	bcs->priority = priority;
	bcs->reliability = reliability;
	bcs->orderingChannel = orderingChannel;
	bcs->systemIdentifier = systemIdentifier;
	bcs->broadcast = broadcast;
	bcs->connectionMode = connectionMode;
	bcs->receipt = receipt;
	bcs->command = BufferedCommandStruct::BCS_SEND;
	bufferedCommands.Push(bcs);

	if (priority == Packet::IMMEDIATE_PRIORITY) {
		// Forces pending sends to go out now, rather than waiting to the next update interval
		quitAndDataEvents.setEvent();
	}
}

bool NetPeer::SendImmediate(char *data, BitSize_t numberOfBitsToSend, Packet::Priority priority, Packet::Reliability reliability, char orderingChannel, const AddressOrGUID systemIdentifier, bool broadcast, bool useCallerDataAllocation, TimeUS currentTime, u32 receipt) const
{
	u32 remoteSystemIndex; // Iterates into the list of remote systems
	bool callerDataAllocationUsed = false;

	u32* sendList;
	u32 sendListSize = 0u;

	if (systemIdentifier.address != UNASSIGNED_SYSTEM_ADDRESS)
		remoteSystemIndex = GetIndexFromSystemAddress(systemIdentifier.address, true);
	else if (systemIdentifier.guid != UNASSIGNED_NET_GUID)
		remoteSystemIndex = GetSystemIndexFromGuid(systemIdentifier.guid);
	else
		remoteSystemIndex = static_cast<u32>(-1);

	// 03/06/06 - If broadcast is false, use the optimized version of GetIndexFromSystemAddress
	if (broadcast == false) {
		if (remoteSystemIndex == static_cast<u32>(-1))
		    return false;

#if defined(USE_ALLOCA)
		sendList = static_cast<u32*>(alloca(sizeof(unsigned)));
#else // defined(USE_ALLOCA)
sendList = (unsigned *) malloc(sizeof(unsigned), _FILE_AND_LINE_);
#endif // defined(USE_ALLOCA)

		if (remoteSystemList[remoteSystemIndex].isActive &&
		    remoteSystemList[remoteSystemIndex].connectMode != RemoteSystemStruct::DISCONNECT_ASAP &&
		    remoteSystemList[remoteSystemIndex].connectMode != RemoteSystemStruct::DISCONNECT_ASAP_SILENTLY &&
		    remoteSystemList[remoteSystemIndex].connectMode != RemoteSystemStruct::DISCONNECT_ON_NO_ACK) {
		    sendList[0] = remoteSystemIndex;
		    sendListSize = 1;
		}
	} else {
#if defined(USE_ALLOCA)
		sendList = static_cast<u32*>(alloca(sizeof(unsigned) * maximumNumberOfPeers));
#else // defined(USE_ALLOCA)
sendList = (unsigned *) malloc(sizeof(unsigned)*maximumNumberOfPeers, _FILE_AND_LINE_);
#endif // defined(USE_ALLOCA)

		for (u32 idx = 0; idx < maximumNumberOfPeers; idx++) {
		    if (remoteSystemIndex != static_cast<u32>(-1) && idx == remoteSystemIndex)
		        continue;

		    if (remoteSystemList[idx].isActive && remoteSystemList[idx].systemAddress != UNASSIGNED_SYSTEM_ADDRESS)
		        sendList[sendListSize++] = idx;
		}
	}

	if (sendListSize == 0u) {
#if !defined(USE_ALLOCA)
free(sendList, _FILE_AND_LINE_ );
#endif

		return false;
	}

	for (u32 sendListIndex = 0; sendListIndex < sendListSize; sendListIndex++) {
		// Send may split the packet and thus deallocate data.  Don't assume data is valid if we use the callerAllocationData
		bool useData = useCallerDataAllocation && callerDataAllocationUsed == false && sendListIndex + 1 == sendListSize;
		remoteSystemList[sendList[sendListIndex]].reliabilityLayer.send(data, numberOfBitsToSend, priority, reliability, orderingChannel, !useData, currentTime, receipt);
		if (useData)
		    callerDataAllocationUsed = true;

		if (reliability == Packet::RELIABLE || reliability == Packet::RELIABLE_ORDERED ||
		    reliability == Packet::RELIABLE_SEQUENCED || reliability == Packet::RELIABLE_WITH_ACK_RECEIPT ||
		    reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT)
		    remoteSystemList[sendList[sendListIndex]].lastReliableSend =
		        static_cast<TimeMS>(currentTime / static_cast<TimeUS>(1000));
	}

#if !defined(USE_ALLOCA)
free(sendList, _FILE_AND_LINE_ );
#endif

	// Return value only meaningful if true was passed for useCallerDataAllocation.  Means the reliability layer used that data copy, so the caller should not deallocate it
	return callerDataAllocationUsed;
}

void NetPeer::ResetSendReceipt()
{
	sendReceiptSerialMutex.lock();
	sendReceiptSerial = 1u;
	sendReceiptSerialMutex.unlock();
}

void NetPeer::OnConnectedPong(TimeMS sendPingTime, TimeMS sendPongTime, 
		                        RemoteSystemStruct* remoteSystem)
{
	TimeMS ping;
	TimeMS time = Time::nowMS(); // Update the time value to be accurate
	time > sendPingTime ? ping = time - sendPingTime : ping = 0u;

	remoteSystem->pingAndClockDifferential[remoteSystem->pingAndClockDifferentialWriteIndex].pingTime = static_cast<u16>(ping);
	// Thanks to Chris Taylor (cat02e@fsu.edu) for the improved timestamping algorithm
	// Divide each integer by 2, rather than the sum by 2, to prevent overflow
	remoteSystem->pingAndClockDifferential[remoteSystem->pingAndClockDifferentialWriteIndex].clockDifferential = sendPongTime - (time / 2 + sendPingTime / 2);

	if (remoteSystem->lowestPing == static_cast<u16>(-1) || remoteSystem->lowestPing > static_cast<s32>(ping))
		remoteSystem->lowestPing = static_cast<u16>(ping);

	if (++remoteSystem->pingAndClockDifferentialWriteIndex == static_cast<TimeMS>(PING_TIMES_ARRAY_SIZE))
		remoteSystem->pingAndClockDifferentialWriteIndex = 0u;
}

void NetPeer::clearBufferedCommands()
{
	BufferedCommandStruct* bcs;
	while ((bcs = bufferedCommands.Pop()) != nullptr) {
		if (bcs->data != nullptr)
		    free(bcs->data);

		bufferedCommands.Deallocate(bcs);
	}
	bufferedCommands.Clear();
}

void NetPeer::clearSocketQueryOutput() {
	socketQueryOutput.Clear();
}

void NetPeer::clearRequestedConnectionList()
{
	std::deque<RequestedConnectionStruct*> freeQueue;

	requestedConnectionQueueMutex.lock();

	while (!requestedConnectionQueue.empty()) {
		freeQueue.push_back(requestedConnectionQueue.front());
		requestedConnectionQueue.pop_front();
	}

	requestedConnectionQueueMutex.unlock();

	for (u32 i = 0u; i < freeQueue.size(); ++i)
		delete freeQueue[i];
}

void NetPeer::AddPacketToProducer(Packet* packet)
{
	packetReturnMutex.lock();
	packetReturnQueue.push_back(packet);
	packetReturnMutex.unlock();
}

namespace Jinra
{
bool processOfflineNetworkPacket(NetAddress systemAddress, cchar *data, const int length, NetPeer *netPeer, NetSocket2* netSocket, bool *isOfflineMessage, TimeUS timeRead)
{
	Packet *packet;


	char str1[64];
	systemAddress.toString(false, str1);
	if (netPeer->IsBanned(str1)) {
		BitStream bs;
		bs.write(static_cast<MessageID>(ID_CONNECTION_BANNED));
		bs.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		bs.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));


		NetSocket2::SendParameters bsp;
		bsp.data = reinterpret_cast<char*>(bs.getData());
		bsp.length = bs.getNumberOfBytesUsed();
		bsp.systemAddress = systemAddress;
		netSocket->send(&bsp);

		return true;
	}



	// The reason for all this is that the reliability layer has no way to tell between offline messages that arrived late for a player that is now connected,
	// and a regular encoding. So I insert OFFLINE_MESSAGE_DATA_ID into the stream, the encoding of which is essentially impossible to hit by chance
	if (length <= 2) {
		*isOfflineMessage = true;
	} else if ((static_cast<u8>(data[0]) == ID_UNCONNECTED_PING || static_cast<u8>(data[0]) == ID_UNCONNECTED_PING_OPEN_CONNECTIONS) &&
		        (u32)length >= sizeof(u8) + sizeof(TimeMS) + sizeof OFFLINE_MESSAGE_DATA_ID) {
		*isOfflineMessage = memcmp(data + sizeof(u8) + sizeof(TimeMS), OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID) == 0;
	} else if ((u8)data[0] == ID_UNCONNECTED_PONG && (size_t)length >= sizeof(u8) + sizeof(TimeMS) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID) {
		*isOfflineMessage = memcmp(data + sizeof(u8) + sizeof(TimeMS) + NetGUID::getSize(), OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID) == 0;
	} else if (
		static_cast<u8>(data[0]) == ID_OUT_OF_BAND_INTERNAL &&
		static_cast<size_t>(length) >= sizeof(MessageID) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID) {
		*isOfflineMessage = memcmp(data + sizeof(MessageID) + NetGUID::getSize(), OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID) == 0;
	} else if (
		(
		    (u8)data[0] == ID_OPEN_CONNECTION_REPLY_1 ||
		    (u8)data[0] == ID_OPEN_CONNECTION_REPLY_2 ||
		    static_cast<u8>(data[0]) == ID_OPEN_CONNECTION_REQUEST_1 ||
		    static_cast<u8>(data[0]) == ID_OPEN_CONNECTION_REQUEST_2 ||
		    (u8)data[0] == ID_CONNECTION_ATTEMPT_FAILED ||
		    (u8)data[0] == ID_NO_FREE_INCOMING_CONNECTIONS ||
		    (u8)data[0] == ID_CONNECTION_BANNED ||
		    static_cast<u8>(data[0]) == ID_ALREADY_CONNECTED ||
		    (u8)data[0] == ID_IP_RECENTLY_CONNECTED) &&
		(size_t)length >= sizeof(MessageID) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID) {
		*isOfflineMessage = memcmp(data + sizeof(MessageID), OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID) == 0;
	} else if ((u8)data[0] == ID_INCOMPATIBLE_PROTOCOL_VERSION &&
		        (size_t)length == sizeof(MessageID) * 2 + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID) {
		*isOfflineMessage = memcmp(data + sizeof(MessageID) * 2, OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID) == 0;
	} else {
		*isOfflineMessage = false;
	}

	if (*isOfflineMessage) {
		// These are all messages from unconnected systems.  Messages here can be any size, but are never processed from connected systems.
		if (((u8)data[0] == ID_UNCONNECTED_PING_OPEN_CONNECTIONS
		        || (u8)data[0] == ID_UNCONNECTED_PING) && (u32)length >= sizeof(u8) + sizeof(TimeMS) + sizeof OFFLINE_MESSAGE_DATA_ID) {
		    if (static_cast<u8>(data[0]) == ID_UNCONNECTED_PING ||
		        netPeer->getAllowedIncomingConnections()) // Open connections with players
		    {
		        BitStream inBitStream((u8*)data, length, false);
		        inBitStream.ignoreBits(8);
		        TimeMS sendPingTime;
		        inBitStream.read(sendPingTime);
		        inBitStream.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);
		        auto remoteGuid = UNASSIGNED_NET_GUID;
		        inBitStream.read(remoteGuid);

		        BitStream outBitStream;
		        outBitStream.write(static_cast<MessageID>(ID_UNCONNECTED_PONG)); // Should be named ID_UNCONNECTED_PONG eventually
		        outBitStream.write(sendPingTime);
		        outBitStream.write(netPeer->myGuid);
		        outBitStream.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);

		        netPeer->netPeerMutexes[NetPeer::offlinePingResponse_Mutex].lock();
		        // They are connected, so append offline ping data
		        outBitStream.write((char*)netPeer->offlinePingResponse.getData(), netPeer->offlinePingResponse.getNumberOfBytesUsed());
		        netPeer->netPeerMutexes[NetPeer::offlinePingResponse_Mutex].unlock();

		        NetSocket2::SendParameters bsp;
		        bsp.data = reinterpret_cast<char*>(outBitStream.getData());
		        bsp.length = outBitStream.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;
		        netSocket->send(&bsp);

		        packet = netPeer->allocatePacket(sizeof(MessageID));
		        packet->data[0] = data[0];
		        packet->systemAddress = systemAddress;
		        packet->guid = remoteGuid;
		        packet->systemAddress.systemIndex = static_cast<SystemIndex>(netPeer->GetIndexFromSystemAddress(systemAddress, true));
		        packet->guid.systemIndex = packet->systemAddress.systemIndex;
		        netPeer->AddPacketToProducer(packet);
		    }
		}
		// UNCONNECTED MESSAGE Pong with no data.
		else if (static_cast<u8>(data[0]) == ID_UNCONNECTED_PONG && static_cast<size_t>(length) >= sizeof(u8) + sizeof(TimeMS) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID && (size_t)length < sizeof(u8) + sizeof(TimeMS) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID + MAX_OFFLINE_DATA_LENGTH) {
		    packet = netPeer->allocatePacket(static_cast<u32>(length - sizeof OFFLINE_MESSAGE_DATA_ID - NetGUID::getSize() - sizeof(TimeMS) + sizeof(TimeMS)));
		    BitStream bsIn((u8*)data, length, false);
		    bsIn.ignoreBytes(sizeof(u8));
		    TimeMS ping;
		    bsIn.read(ping);
		    bsIn.read(packet->guid);

		    BitStream bsOut(static_cast<u8*>(packet->data), packet->length, false);
		    bsOut.resetWritePointer();
		    bsOut.write(static_cast<u8>(ID_UNCONNECTED_PONG));
		    TimeMS pingMS = static_cast<TimeMS>(ping);
		    bsOut.write(pingMS);
		    bsOut.writeAlignedBytes(
		                            reinterpret_cast<const u8*>(data) + sizeof(u8) + sizeof(TimeMS) + NetGUID::getSize() + sizeof OFFLINE_MESSAGE_DATA_ID,
		                            length - sizeof(u8) - sizeof(TimeMS) - NetGUID::getSize() - sizeof OFFLINE_MESSAGE_DATA_ID
		                            );

		    packet->systemAddress = systemAddress;
		    packet->systemAddress.systemIndex = static_cast<SystemIndex>(netPeer->GetIndexFromSystemAddress(systemAddress, true));
		    packet->guid.systemIndex = packet->systemAddress.systemIndex;
		    netPeer->AddPacketToProducer(packet);
		} else if (static_cast<u8>(data[0]) == ID_OUT_OF_BAND_INTERNAL &&
		            static_cast<size_t>(length) > sizeof OFFLINE_MESSAGE_DATA_ID + sizeof(MessageID) + NetGUID::getSize() &&
		            static_cast<size_t>(length) < MAX_OFFLINE_DATA_LENGTH + sizeof OFFLINE_MESSAGE_DATA_ID + sizeof(MessageID) + NetGUID::getSize()) {
		    u32 dataLength = static_cast<u32>(length - sizeof OFFLINE_MESSAGE_DATA_ID - NetGUID::getSize() - sizeof(MessageID));
		    ASSERT(dataLength < 1024);
		    packet = netPeer->allocatePacket(dataLength + 1);
		    ASSERT(packet->length < 1024);

		    BitStream bs2((u8*)data, length, false);
		    bs2.ignoreBytes(sizeof(MessageID));
		    bs2.read(packet->guid);

		    if (data[sizeof OFFLINE_MESSAGE_DATA_ID + sizeof(MessageID) + NetGUID::getSize()] == ID_ADVERTISE_SYSTEM) {
		        packet->length--;
		        packet->bitSize = BYTES_TO_BITS(packet->length);
		        packet->data[0] = ID_ADVERTISE_SYSTEM;
		        memcpy(packet->data + 1, data + sizeof OFFLINE_MESSAGE_DATA_ID + sizeof(MessageID) * 2 + NetGUID::getSize(), dataLength - 1);
		    } else {
		        packet->data[0] = ID_OUT_OF_BAND_INTERNAL;
		        memcpy(packet->data + 1, data + sizeof OFFLINE_MESSAGE_DATA_ID + sizeof(MessageID) + NetGUID::getSize(), dataLength);
		    }

		    packet->systemAddress = systemAddress;
		    packet->systemAddress.systemIndex = static_cast<SystemIndex>(netPeer->GetIndexFromSystemAddress(systemAddress, true));
		    packet->guid.systemIndex = packet->systemAddress.systemIndex;
		    netPeer->AddPacketToProducer(packet);
		} else if (static_cast<u8>(data[0]) == static_cast<MessageID>(ID_OPEN_CONNECTION_REPLY_1)) {
		    BitStream bsIn((u8*)data, length, false);
		    bsIn.ignoreBytes(sizeof(MessageID));
		    bsIn.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);
		    NetGUID serverGuid;
		    bsIn.read(serverGuid);
		    u8 serverHasSecurity;
		    u32 cookie;
		    bsIn.read(serverHasSecurity);
		    // Even if the server has security, it may not be required of us if we are in the security exception list
		    if (serverHasSecurity)
		        bsIn.read(cookie);

		    BitStream bsOut;
		    bsOut.write(static_cast<MessageID>(ID_OPEN_CONNECTION_REQUEST_2));
		    bsOut.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		    if (serverHasSecurity)
		        bsOut.write(cookie);

		    netPeer->requestedConnectionQueueMutex.lock();
		    for (u32 i = 0u; i < netPeer->requestedConnectionQueue.size(); ++i) {
		        auto rcs = netPeer->requestedConnectionQueue[i];
		        if (rcs->systemAddress == systemAddress) {
		            if (serverHasSecurity) {
		                // Message does not contain a challenge
		                bsOut.write((u8)0);
		            }

		            u16 mtu;
		            bsIn.read(mtu);

		            // Binding address
		            bsOut.write(rcs->systemAddress);
		            netPeer->requestedConnectionQueueMutex.unlock();
		            // MTU
		            bsOut.write(mtu);
		            // Our guid
		            bsOut.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));

		            NetSocket2::SendParameters bsp;
		            bsp.data = (char*)bsOut.getData();
		            bsp.length = bsOut.getNumberOfBytesUsed();
		            bsp.systemAddress = systemAddress;
		            netSocket->send(&bsp);

		            return true;
		        }
		    }
		    netPeer->requestedConnectionQueueMutex.unlock();
		} else if ((u8)data[0] == (MessageID)ID_OPEN_CONNECTION_REPLY_2) {
		    BitStream bs((u8*)data, length, false);
		    bs.ignoreBytes(sizeof(MessageID));
		    bs.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);
		    NetGUID guid;
		    bs.read(guid);
		    NetAddress bindingAddress;
		    auto b = bs.read(bindingAddress);
		    ASSERT(b);
		    uint16_t mtu;
		    b = bs.read(mtu);
		    ASSERT(b);
		    bool doSecurity = false;
		    b = bs.read(doSecurity);
		    ASSERT(b);

		    NetPeer::RequestedConnectionStruct *rcs;
		    bool unlock = true;
		    netPeer->requestedConnectionQueueMutex.lock();
		    for (u32 i = 0u; i < netPeer->requestedConnectionQueue.size(); ++i) {
		        rcs = netPeer->requestedConnectionQueue[i];


		        if (rcs->systemAddress == systemAddress) {
		            netPeer->requestedConnectionQueueMutex.unlock();
		            unlock = false;

		            ASSERT(rcs->actionToTake == NetPeer::RequestedConnectionStruct::CONNECT);
		            // You might get this when already connected because of cross-connections
		            bool thisIPConnectedRecently = false;
		            auto remoteSystem = netPeer->getRemoteSystemFromSystemAddress(systemAddress, true, true);
		            if (remoteSystem == nullptr) {
		                if (rcs->socket == nullptr) {
		                    remoteSystem = netPeer->AssignSystemAddressToRemoteSystemList(systemAddress, NetPeer::RemoteSystemStruct::UNVERIFIED_SENDER, netSocket, &thisIPConnectedRecently, bindingAddress, mtu, guid);
		                } else {
		                    remoteSystem = netPeer->AssignSystemAddressToRemoteSystemList(systemAddress, NetPeer::RemoteSystemStruct::UNVERIFIED_SENDER, rcs->socket, &thisIPConnectedRecently, bindingAddress, mtu, guid);
		                }
		            }

		            // 4/13/09 Attackers can flood ID_OPEN_CONNECTION_REQUEST and use up all available connection slots
		            // Ignore connection attempts if this IP address connected within the last 100 milliseconds
		            if (thisIPConnectedRecently == false) {
		                // Don't check GetRemoteSystemFromGUID, server will verify
		                if (remoteSystem) {
		                    // Move pointer from RequestedConnectionStruct to RemoteSystemStruct

		                    remoteSystem->weInitiatedTheConnection = true;
		                    remoteSystem->connectMode = NetPeer::RemoteSystemStruct::REQUESTED_CONNECTION;
		                    if (rcs->timeoutTime != 0)
		                        remoteSystem->reliabilityLayer.setTimeoutTime(rcs->timeoutTime);

		                    BitStream temp;
		                    temp.write((MessageID)ID_CONNECTION_REQUEST);
		                    temp.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));
		                    temp.write(Time::nowMS());
		                    temp.write(rcs->connectionPassword);

		                    netPeer->SendImmediate((char*)temp.getData(), temp.getNumberOfBitsUsed(), Packet::IMMEDIATE_PRIORITY, Packet::RELIABLE, 0, systemAddress, false, false, timeRead, 0);
		                } else {
		                    // Failed, no connections available anymore
		                    packet = netPeer->allocatePacket(sizeof(char));
		                    packet->data[0] = ID_CONNECTION_ATTEMPT_FAILED; // Attempted a connection and couldn't
		                    packet->bitSize = sizeof(char) * 8;
		                    packet->systemAddress = rcs->systemAddress;
		                    packet->guid = guid;
		                    netPeer->AddPacketToProducer(packet);
		                }
		            }

		            netPeer->requestedConnectionQueueMutex.lock();
		            for (u32 k = 0; k < netPeer->requestedConnectionQueue.size(); k++) {
		                if (netPeer->requestedConnectionQueue[k]->systemAddress == systemAddress) {
		                    netPeer->requestedConnectionQueue.erase(
		                                                            netPeer->requestedConnectionQueue.begin() + k);
		                    break;
		                }
		            }
		            netPeer->requestedConnectionQueueMutex.unlock();

		            delete rcs;

		            break;
		        }
		    }

		    if (unlock)
		        netPeer->requestedConnectionQueueMutex.unlock();

		    return true;

		} else if (static_cast<u8>(data[0]) == static_cast<MessageID>(ID_CONNECTION_ATTEMPT_FAILED) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_NO_FREE_INCOMING_CONNECTIONS) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_CONNECTION_BANNED) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_ALREADY_CONNECTED) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_INVALID_PASSWORD) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_IP_RECENTLY_CONNECTED) ||
		            static_cast<u8>(data[0]) == static_cast<MessageID>(ID_INCOMPATIBLE_PROTOCOL_VERSION)) {

		    BitStream bs((u8*)data, length, false);
		    bs.ignoreBytes(sizeof(MessageID));
		    bs.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);
		    if (static_cast<u8>(data[0]) == static_cast<MessageID>(ID_INCOMPATIBLE_PROTOCOL_VERSION))
		        bs.ignoreBytes(sizeof(u8));

		    NetGUID guid;
		    bs.read(guid);

		    bool connectionAttemptCancelled = false;
		    netPeer->requestedConnectionQueueMutex.lock();
		    for (u32 i = 0u; i < netPeer->requestedConnectionQueue.size(); ++i) {
		        auto rcs = netPeer->requestedConnectionQueue[i];
		        if (rcs->actionToTake == NetPeer::RequestedConnectionStruct::CONNECT && rcs->systemAddress == systemAddress) {
		            connectionAttemptCancelled = true;
		            netPeer->requestedConnectionQueue.erase(
		                                                    netPeer->requestedConnectionQueue.begin() + i);
		            delete rcs;
		            break;
		        }
		    }

		    netPeer->requestedConnectionQueueMutex.unlock();

		    if (connectionAttemptCancelled) {
		        // Tell user of connection attempt failed
		        packet = netPeer->allocatePacket(sizeof(char));
		        packet->data[0] = data[0]; // Attempted a connection and couldn't
		        packet->bitSize = sizeof(char) * 8;
		        packet->systemAddress = systemAddress;
		        packet->guid = guid;
		        netPeer->AddPacketToProducer(packet);
		    }
		} else if (static_cast<u8>(data[0]) == ID_OPEN_CONNECTION_REQUEST_1 && length > static_cast<s32>(1 + sizeof OFFLINE_MESSAGE_DATA_ID)) {
		    char remoteProtocol = data[1 + sizeof OFFLINE_MESSAGE_DATA_ID];
		    if (remoteProtocol != JINRA_PROTOCOL_VERSION) {
		        BitStream bs;
		        bs.write((MessageID)ID_INCOMPATIBLE_PROTOCOL_VERSION);
		        bs.write(static_cast<u8>(JINRA_PROTOCOL_VERSION));
		        bs.writeAlignedBytes((const u8*)OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID);
		        bs.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));

		        NetSocket2::SendParameters bsp;
		        bsp.data = (char*)bs.getData();
		        bsp.length = bs.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;

		        netSocket->send(&bsp);
		        return true;
		    }

		    BitStream bsOut;
		    bsOut.write(static_cast<MessageID>(ID_OPEN_CONNECTION_REPLY_1));
		    bsOut.writeAlignedBytes((const u8*)OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID);
		    bsOut.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));
		    bsOut.write(static_cast<u8>(0));  // HasCookie oN

		    // MTU. Lower MTU if it is exceeds our own limit
		    if (length + UDP_HEADER_SIZE > MAXIMUM_MTU_SIZE)
		        bsOut.write(static_cast<u16>(MAXIMUM_MTU_SIZE));
		    else
		        bsOut.write(static_cast<u16>(length + UDP_HEADER_SIZE));

		    NetSocket2::SendParameters bsp;
		    bsp.data = reinterpret_cast<char*>(bsOut.getData());
		    bsp.length = bsOut.getNumberOfBytesUsed();
		    bsp.systemAddress = systemAddress;
		    netSocket->send(&bsp);
		} else if (static_cast<u8>(data[0]) == ID_OPEN_CONNECTION_REQUEST_2) {
		    NetAddress bindingAddress;
		    NetGUID guid;
		    BitStream bsOut;
		    BitStream bs((u8*)data, length, false);
		    bs.ignoreBytes(sizeof(MessageID));
		    bs.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);

		    bool requiresSecurityOfThisClient = false;

		    bs.read(bindingAddress);
		    u16 mtu;
		    bs.read(mtu);
		    bs.read(guid);

		    auto rssFromSA = netPeer->getRemoteSystemFromSystemAddress(systemAddress, true, true);
		    bool IPAddrInUse = rssFromSA != nullptr && rssFromSA->isActive;
		    auto rssFromGuid = netPeer->GetRemoteSystemFromGUID(guid, true);
		    bool GUIDInUse = rssFromGuid != nullptr && rssFromGuid->isActive;

		    // IPAddrInUse, GuidInUse, outcome
		    // TRUE,	  , TRUE	 , ID_OPEN_CONNECTION_REPLY if they are the same, else ID_ALREADY_CONNECTED
		    // FALSE,     , TRUE     , ID_ALREADY_CONNECTED (someone else took this guid)
		    // TRUE,	  , FALSE	 , ID_ALREADY_CONNECTED (silently disconnected, restarted)
		    // FALSE	  , FALSE	 , Allow connection

		    int outcome;
		    if (IPAddrInUse & GUIDInUse) {
		        if (rssFromSA == rssFromGuid && rssFromSA->connectMode == NetPeer::RemoteSystemStruct::UNVERIFIED_SENDER) {
		            // ID_OPEN_CONNECTION_REPLY if they are the same
		            outcome = 1;

		            // Note to self: If REQUESTED_CONNECTION, this means two systems attempted to connect to each other at the same time, and one finished first.
		            // Returns ID)_CONNECTION_REQUEST_ACCEPTED to one system, and ID_ALREADY_CONNECTED followed by ID_NEW_INCOMING_CONNECTION to another
		        } else {
		            // ID_ALREADY_CONNECTED (restarted, connected again from same ip, plus someone else took this guid)
		            outcome = 2;
		        }
		    } else if (IPAddrInUse == false && GUIDInUse == true) {
		        // ID_ALREADY_CONNECTED (someone else took this guid)
		        outcome = 3;
		    } else if (IPAddrInUse == true && GUIDInUse == false) {
		        // ID_ALREADY_CONNECTED (silently disconnected, restarted)
		        outcome = 4;
		    } else {
		        // Allow connection
		        outcome = 0;
		    }

		    BitStream bsAnswer;
		    bsAnswer.write(static_cast<MessageID>(ID_OPEN_CONNECTION_REPLY_2));
		    bsAnswer.writeAlignedBytes((const u8*)OFFLINE_MESSAGE_DATA_ID, sizeof OFFLINE_MESSAGE_DATA_ID);
		    bsAnswer.write(netPeer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));
		    bsAnswer.write(systemAddress);
		    bsAnswer.write(mtu);
		    bsAnswer.write(requiresSecurityOfThisClient);

		    if (outcome == 1) {
		        NetSocket2::SendParameters bsp;
		        bsp.data = (char*)bsAnswer.getData();
		        bsp.length = bsAnswer.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;
		        netSocket->send(&bsp);

		        return true;
		    }
		    if (outcome != 0) {
		        bsOut.write(static_cast<MessageID>(ID_ALREADY_CONNECTED));
		        bsOut.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		        bsOut.write(netPeer->myGuid);

		        NetSocket2::SendParameters bsp;
		        bsp.data = reinterpret_cast<char*>(bsOut.getData());
		        bsp.length = bsOut.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;
		        netSocket->send(&bsp);

		        return true;
		    }

		    if (netPeer->getAllowedIncomingConnections() == false) {
		        bsOut.write(static_cast<MessageID>(ID_NO_FREE_INCOMING_CONNECTIONS));
		        bsOut.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		        bsOut.write(netPeer->myGuid);

		        NetSocket2::SendParameters bsp;
		        bsp.data = reinterpret_cast<char*>(bsOut.getData());
		        bsp.length = bsOut.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;
		        netSocket->send(&bsp);

		        return true;
		    }

		    bool thisIPConnectedRecently = false;
		    rssFromSA = netPeer->AssignSystemAddressToRemoteSystemList(systemAddress, NetPeer::RemoteSystemStruct::UNVERIFIED_SENDER, netSocket, &thisIPConnectedRecently, bindingAddress, mtu, guid);

		    if (thisIPConnectedRecently == true) {
		        bsOut.write(static_cast<MessageID>(ID_IP_RECENTLY_CONNECTED));
		        bsOut.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		        bsOut.write(netPeer->myGuid);

		        NetSocket2::SendParameters bsp;
		        bsp.data = reinterpret_cast<char*>(bsOut.getData());
		        bsp.length = bsOut.getNumberOfBytesUsed();
		        bsp.systemAddress = systemAddress;
		        netSocket->send(&bsp);

		        return true;
		    }

		    NetSocket2::SendParameters bsp;
		    bsp.data = reinterpret_cast<char*>(bsAnswer.getData());
		    bsp.length = bsAnswer.getNumberOfBytesUsed();
		    bsp.systemAddress = systemAddress;
		    netSocket->send(&bsp);
		}
		return true;
	}

	return false;
}

void processNetworkPacket(NetAddress systemAddress, cchar* data, const s32 length,
		                    NetPeer* netPeer, TimeUS timeRead, BitStream &updateBitStream) {
	processNetworkPacket(systemAddress, data, length, netPeer, netPeer->socketList[0], timeRead, updateBitStream);
}

void processNetworkPacket(NetAddress systemAddress, cchar* data, const s32 length,
		                    NetPeer* netPeer, NetSocket2* netSocket, TimeUS timeRead,
		                    BitStream &updateBitStream)
{
	ASSERT(systemAddress.getPort());
	bool isOfflineMessage;
	if (processOfflineNetworkPacket(systemAddress, data, length, netPeer, netSocket,
		                            &isOfflineMessage, timeRead))
		return;

	// See if this datagram came from a connected system
	auto remoteSystem = netPeer->getRemoteSystemFromSystemAddress(systemAddress, true, true);
	if (remoteSystem != nullptr) {
		// Handle regular incoming data
		// HandleSocketReceiveFromConnectedPlayer is only safe to be called from the same thread as Update, which is this thread
		if (!isOfflineMessage) {
		    remoteSystem->reliabilityLayer.handleSocketReceiveFromConnectedPlayer(data, length,
		                                                                            systemAddress, netSocket, timeRead, updateBitStream);
		}
	}
}

}

u32 NetPeer::generateSeedFromGuid() const
{
	return static_cast<u32>(myGuid.id >> 32 ^ myGuid.id);
}

void NetPeer::derefAllSockets()
{
	for (u32 i = 0u; i < socketList.size(); ++i)
		delete socketList[i];
	socketList.clear();
}

u32 NetPeer::getJinraSocketFromUserConnectionSocketIndex(u32 userIndex) const
{
	for (u32 i = 0u; i < socketList.size(); ++i) {
		if (socketList[i]->getUserConnectionSocketIndex() == userIndex)
		    return i;
	}
	ASSERT("getJinraSocketFromUserConnectionSocketIndex failed");
	return static_cast<u32>(-1);
}

bool NetPeer::RunUpdateCycle(BitStream &updateBitStream)
{
	RemoteSystemStruct * remoteSystem;
	Packet *packet;
	BitSize_t bitSize;
	u8* data;
	NetAddress systemAddress;
	BufferedCommandStruct* bcs;
	TimeUS timeNS = 0u;
	TimeMS timeMS = 0u;

	NetSocket2::RecvStruct* recvFromStruct;
	while ((recvFromStruct = PopBufferedPacket()) != nullptr) {
		processNetworkPacket(recvFromStruct->systemAddress, recvFromStruct->data, recvFromStruct->bytesRead, this, recvFromStruct->socket, recvFromStruct->timeRead, updateBitStream);
		deallocRNS2RecvStruct(recvFromStruct);
	}

	while ((bcs = bufferedCommands.PopInaccurate()) != 0) {
		if (bcs->command == BufferedCommandStruct::BCS_SEND) {
		    // GetTime is a very slow call so do it once and as late as possible
		    if (timeNS == 0u) {
		        timeNS = Time::nowUS();
		        timeMS = static_cast<TimeMS>(timeNS / static_cast<TimeUS>(1000u));
		    }

		    auto callerDataAllocationUsed = SendImmediate(static_cast<char*>(bcs->data), bcs->numberOfBitsToSend, bcs->priority, bcs->reliability, bcs->orderingChannel, bcs->systemIdentifier, bcs->broadcast, true, timeNS, bcs->receipt);
		    if (!callerDataAllocationUsed)
		        free(bcs->data);

		    // Set the new connection state AFTER we call sendImmediate in case we are setting it to a disconnection state, which does not allow further sends
		    if (bcs->connectionMode != RemoteSystemStruct::NO_ACTION) {
		        remoteSystem = getRemoteSystem(bcs->systemIdentifier, true, true);
		        if (remoteSystem)
		            remoteSystem->connectMode = bcs->connectionMode;
		    }
		} else if (bcs->command == BufferedCommandStruct::BCS_CLOSE_CONNECTION) {
		    closeConnectionInternal(bcs->systemIdentifier, false, true, bcs->orderingChannel, bcs->priority);
		} else if (bcs->command == BufferedCommandStruct::BCS_CHANGE_SYSTEM_ADDRESS) {
		    // Reroute
		    auto rssFromGuid = getRemoteSystem(bcs->systemIdentifier.guid, true, true);
		    if (rssFromGuid != nullptr) {
		        u32 existingSystemIndex = getRemoteSystemIndex(rssFromGuid->systemAddress);
		        ReferenceRemoteSystem(bcs->systemIdentifier.address, existingSystemIndex);
		    }
		} else if (bcs->command == BufferedCommandStruct::BCS_GET_SOCKET) {
		    SocketQueryOutput *sqo;
		    if (bcs->systemIdentifier.isUndefined()) {
		        sqo = socketQueryOutput.Allocate();
		        sqo->sockets = socketList;
		        socketQueryOutput.Push(sqo);
		    } else {
		        remoteSystem = getRemoteSystem(bcs->systemIdentifier, true, true);
		        sqo = socketQueryOutput.Allocate();

		        sqo->sockets.clear();
		        if (remoteSystem) {
		            sqo->sockets.push_back(remoteSystem->netSocket);
		        } else {
		            // Leave empty smart pointer
		        }
		        socketQueryOutput.Push(sqo);
		    }

		}

#ifdef JINRA_DEBUG
		bcs->data = nullptr;
#endif

		bufferedCommands.Deallocate(bcs);
	}

	if (!requestedConnectionQueue.empty()) {
		if (timeNS == 0u) {
		    timeNS = Time::nowUS();
		    timeMS = static_cast<TimeMS>(timeNS / 1000u);
		}

		auto requestedConnectionQueueIndex = 0u;
		requestedConnectionQueueMutex.lock();
		while (requestedConnectionQueueIndex < requestedConnectionQueue.size()) {
		    auto rcs = requestedConnectionQueue[requestedConnectionQueueIndex];
		    requestedConnectionQueueMutex.unlock();

		    if (rcs->nextRequestTime < timeMS) {
		        auto condition1 = rcs->requestsMade == rcs->sendConnectionAttemptCount + 1;
		        auto condition2 = static_cast<bool>(rcs->systemAddress == UNASSIGNED_SYSTEM_ADDRESS == 1);
		        // If too many requests made or a hole then remove this if possible, otherwise invalidate it
		        if (condition1 || condition2) {
		            if (rcs->data != nullptr) {
		                free(rcs->data);
		                rcs->data = nullptr;
		            }

		            if (condition1 && !condition2 &&
		                rcs->actionToTake == RequestedConnectionStruct::CONNECT) {
		                // Tell user of connection attempt failed
		                packet = allocatePacket(sizeof(char));
		                packet->data[0] = ID_CONNECTION_ATTEMPT_FAILED; // Attempted a connection and couldn't
		                packet->bitSize = sizeof(char) * 8;
		                packet->systemAddress = rcs->systemAddress;
		                AddPacketToProducer(packet);
		            }

		            requestedConnectionQueueMutex.lock();
		            for (auto k = 0u; k < requestedConnectionQueue.size(); ++k) {
		                if (requestedConnectionQueue[k] == rcs) {
		                    requestedConnectionQueue.erase(requestedConnectionQueue.begin() + k);
		                    break;
		                }
		            }
		            requestedConnectionQueueMutex.unlock();
		            delete rcs;
		        } else {

		            int MTUSizeIndex = rcs->requestsMade / (rcs->sendConnectionAttemptCount / NUM_MTU_SIZES);
		            if (MTUSizeIndex >= NUM_MTU_SIZES)
		                MTUSizeIndex = NUM_MTU_SIZES - 1;
		            ++rcs->requestsMade;
		            rcs->nextRequestTime = timeMS + rcs->timeBetweenSendConnectionAttemptsMS;

		            BitStream bitStream;
		            bitStream.write(static_cast<MessageID>(ID_OPEN_CONNECTION_REQUEST_1));
		            bitStream.writeAlignedBytes(static_cast<const u8*>(OFFLINE_MESSAGE_DATA_ID), sizeof OFFLINE_MESSAGE_DATA_ID);
		            bitStream.write(static_cast<MessageID>(JINRA_PROTOCOL_VERSION));
		            bitStream.PadWithZeroToByteLength(mtuSizes[MTUSizeIndex] - UDP_HEADER_SIZE);

		            char str[256];
		            rcs->systemAddress.toString(true, str);

		            NetSocket2* socketToUse;
		            if (rcs->socket == nullptr) {
		                socketToUse = socketList[rcs->socketIndex];
		            } else {
		                socketToUse = rcs->socket;
		            }

		            rcs->systemAddress.fixForIPVersion(socketToUse->getBoundAddress());
		            if (socketToUse->isBerkleySocket())
		                static_cast<BerkleySocket2*>(socketToUse)->setDoNotFragment(1);

		            TimeMS sendToStart = Time::nowMS();

		            NetSocket2::SendParameters bsp;
		            bsp.data = reinterpret_cast<char*>(bitStream.getData());
		            bsp.length = bitStream.getNumberOfBytesUsed();
		            bsp.systemAddress = rcs->systemAddress;
		            if (socketToUse->send(&bsp) == 10040) {
		                // Don't use this MTU size again
		                rcs->requestsMade = static_cast<u8>((MTUSizeIndex + 1) * (rcs->sendConnectionAttemptCount / NUM_MTU_SIZES));
		                rcs->nextRequestTime = timeMS;
		            } else {
		                TimeMS sendToEnd = Time::nowMS();
		                if (sendToEnd - sendToStart > 100) {
		                    // Drop to lowest MTU
		                    int lowestMtuIndex = rcs->sendConnectionAttemptCount / NUM_MTU_SIZES * (NUM_MTU_SIZES - 1);
		                    if (lowestMtuIndex > rcs->requestsMade) {
		                        rcs->requestsMade = static_cast<u8>(lowestMtuIndex);
		                        rcs->nextRequestTime = timeMS;
		                    } else
		                        rcs->requestsMade = static_cast<u8>(rcs->sendConnectionAttemptCount + 1);
		                }
		            }

		            if (socketToUse->isBerkleySocket())
		                static_cast<BerkleySocket2*>(socketToUse)->setDoNotFragment(0);
		            ++requestedConnectionQueueIndex;
		        }
		    } else {
		        ++requestedConnectionQueueIndex;
		    }

		    requestedConnectionQueueMutex.lock();
		}

		requestedConnectionQueueMutex.unlock();
	}

	// remoteSystemList in network thread
	for (u32 activeSystemListIndex = 0; activeSystemListIndex < activeSystemListSize; ++activeSystemListIndex) {
		// I'm using systemAddress from remoteSystemList but am not locking it because this loop is called very frequently and it doesn't
		// matter if we miss or do an extra update.  The reliability layers themselves never care which player they are associated with
		//systemAddress = remoteSystemList[ remoteSystemIndex ].systemAddress;
		// Allow the systemAddress for this remote system list to change.  We don't care if it changes now.
		//	remoteSystemList[ remoteSystemIndex ].allowSystemAddressAssigment=true;


		// Found an active remote system
		remoteSystem = activeSystemList[activeSystemListIndex];
		systemAddress = remoteSystem->systemAddress;
		ASSERT(systemAddress != UNASSIGNED_SYSTEM_ADDRESS);
		// Update is only safe to call from the same thread that calls HandleSocketReceiveFromConnectedPlayer,
		// which is this thread

		if (timeNS == 0) {
		    timeNS = Time::nowUS();
		    timeMS = static_cast<TimeMS>(timeNS / static_cast<TimeUS>(1000u));
		}


		if (timeMS > remoteSystem->lastReliableSend && timeMS - remoteSystem->lastReliableSend > remoteSystem->reliabilityLayer.getTimeoutTime() / 2 && remoteSystem->connectMode == RemoteSystemStruct::CONNECTED) {
		    // If no reliable packets are waiting for an ack, do a one byte reliable send so that disconnections are noticed
		    NetStatistics netStatistics;
		    auto rnss = remoteSystem->reliabilityLayer.getStatistics(&netStatistics);
		    if (rnss->messagesInResendBuffer == 0) {
		        pingInternal(systemAddress, true, Packet::RELIABLE);

		        remoteSystem->lastReliableSend = timeMS;
		    }
		}

		remoteSystem->reliabilityLayer.update(remoteSystem->netSocket, systemAddress, timeNS, maxOutgoingBPS, updateBitStream); // systemAddress only used for the internet simulator test

		// Check for failure conditions
		if (remoteSystem->reliabilityLayer.isDeadConnection() ||
		    (remoteSystem->connectMode == RemoteSystemStruct::DISCONNECT_ASAP || remoteSystem->connectMode == RemoteSystemStruct::DISCONNECT_ASAP_SILENTLY) && remoteSystem->reliabilityLayer.isOutgoingDataWaiting() == false ||
		    remoteSystem->connectMode == RemoteSystemStruct::DISCONNECT_ON_NO_ACK && (remoteSystem->reliabilityLayer.areAcksWaiting() == false || remoteSystem->reliabilityLayer.getAckTimeout(timeMS) == true) ||
		    (remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION ||
		        remoteSystem->connectMode == RemoteSystemStruct::HANDLING_CONNECTION_REQUEST ||
		        remoteSystem->connectMode == RemoteSystemStruct::UNVERIFIED_SENDER)
		    && timeMS > remoteSystem->connectionTime && timeMS - remoteSystem->connectionTime > 10000
		) {

		    // Failed.  Inform the user?
		    // TODO - Jinra 4.0 - Return a different message identifier for DISCONNECT_ASAP_SILENTLY and DISCONNECT_ASAP than for DISCONNECT_ON_NO_ACK
		    // The first two mean we called CloseConnection(), the last means the other system sent us ID_DISCONNECTION_NOTIFICATION
		    if (remoteSystem->connectMode == RemoteSystemStruct::CONNECTED || remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION
		        || remoteSystem->connectMode == RemoteSystemStruct::DISCONNECT_ASAP || remoteSystem->connectMode == RemoteSystemStruct::DISCONNECT_ON_NO_ACK) {

		        packet = allocatePacket(sizeof(char));
		        if (remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION) {
		            packet->data[0] = ID_CONNECTION_ATTEMPT_FAILED; // Attempted a connection and couldn't
		        } else if (remoteSystem->connectMode == RemoteSystemStruct::CONNECTED)
		            packet->data[0] = ID_CONNECTION_LOST; // DeadConnection
		        else
		            packet->data[0] = ID_DISCONNECTION_NOTIFICATION; // DeadConnection


		        packet->guid = remoteSystem->guid;
		        packet->systemAddress = systemAddress;
		        packet->systemAddress.systemIndex = remoteSystem->remoteSystemIndex;
		        packet->guid.systemIndex = packet->systemAddress.systemIndex;

		        AddPacketToProducer(packet);
		    }
		    // else connection shutting down, don't bother telling the user

		    closeConnectionInternal(systemAddress, false, true, 0, Packet::LOW_PRIORITY);
		    continue;
		}

		// Ping this guy if it is time to do so
		if (remoteSystem->connectMode == RemoteSystemStruct::CONNECTED && timeMS > remoteSystem->nextPingTime && (occasionalPing || remoteSystem->lowestPing == (u16)-1)) {
		    remoteSystem->nextPingTime = timeMS + 5000;
		    pingInternal(systemAddress, true, Packet::UNRELIABLE);

		    // Update again immediately after this tick so the ping goes out right away
		    quitAndDataEvents.setEvent();
		}

		// Find whoever has the lowest player ID
		//if (systemAddress < authoritativeClientSystemAddress)
		// authoritativeClientSystemAddress=systemAddress;

		// Does the reliability layer have any packets waiting for us?
		// To be thread safe, this has to be called in the same thread as HandleSocketReceiveFromConnectedPlayer
		bitSize = remoteSystem->reliabilityLayer.receive(&data);

		while (bitSize > 0) {
		    // These types are for internal use and should never arrive from a network packet
		    if (data[0] == ID_CONNECTION_ATTEMPT_FAILED) {
		        ASSERT(0);
		        bitSize = 0;
		        continue;
		    }

		    // Fast and easy - just use the data that was returned
		    u32 byteSize = static_cast<u32>(BITS_TO_BYTES(bitSize));

		    // For unknown senders we only accept a few specific packets
		    if (remoteSystem->connectMode == RemoteSystemStruct::UNVERIFIED_SENDER) {
		        if (static_cast<u8>(data[0]) == ID_CONNECTION_REQUEST) {
		            ParseConnectionRequestPacket(remoteSystem, systemAddress, reinterpret_cast<cchar*>(data), byteSize);
		            free(data);
		        } else {
		            closeConnectionInternal(systemAddress, false, true, 0, Packet::LOW_PRIORITY);

		            char str1[64];
		            systemAddress.toString(false, str1);
		            AddToBanList(str1, remoteSystem->reliabilityLayer.getTimeoutTime());


		            free(data);
		        }
		    } else {
		        // However, if we are connected we still take a connection request in case both systems are trying to connect to each other
		        // at the same time
		        if (static_cast<u8>(data[0]) == ID_CONNECTION_REQUEST) {
		            // 04/27/06 This is wrong.  With cross connections, we can both have initiated the connection are in state REQUESTED_CONNECTION
		            // 04/28/06 Downgrading connections from connected will close the connection due to security at ((remoteSystem->connectMode!=RemoteSystemStruct::CONNECTED && time > remoteSystem->connectionTime && time - remoteSystem->connectionTime > 10000))
		            if (remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION) {
		                ParseConnectionRequestPacket(remoteSystem, systemAddress, reinterpret_cast<cchar*>(data), byteSize);
		            } else {

		                BitStream bs(static_cast<u8*>(data), byteSize, false);
		                bs.ignoreBytes(sizeof(MessageID));
		                bs.ignoreBytes(sizeof OFFLINE_MESSAGE_DATA_ID);
		                bs.ignoreBytes(NetGUID::getSize());
		                TimeMS incomingTimestamp;
		                bs.read(incomingTimestamp);

		                // Got a connection request message from someone we are already connected to. Just reply normally.
		                // This can happen due to race conditions with the fully connected mesh
		                OnConnectionRequest(remoteSystem, incomingTimestamp);
		            }
		            free(data);
		        } else if (static_cast<u8>(data[0]) == ID_NEW_INCOMING_CONNECTION && byteSize > sizeof(u8) + sizeof(u32) + sizeof(u16) + sizeof(TimeMS) * 2) {
		            if (remoteSystem->connectMode == RemoteSystemStruct::HANDLING_CONNECTION_REQUEST) {
		                remoteSystem->connectMode = RemoteSystemStruct::CONNECTED;
		                pingInternal(systemAddress, true, Packet::UNRELIABLE);

		                // Update again immediately after this tick so the ping goes out right away
		                quitAndDataEvents.setEvent();

		                BitStream inBitStream(static_cast<u8 *>(data), byteSize, false);
		                NetAddress bsSystemAddress;

		                inBitStream.ignoreBits(8);
		                inBitStream.read(bsSystemAddress);
		                for (u32 i = 0; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i)
		                    inBitStream.read(remoteSystem->theirInternalSystemAddress[i]);

		                TimeUS sendPingTime, sendPongTime;
		                inBitStream.read(sendPingTime);
		                inBitStream.read(sendPongTime);
		                OnConnectedPong(sendPingTime, sendPongTime, remoteSystem);

		                // Overwrite the data in the packet
		                //					NewIncomingConnectionStruct newIncomingConnectionStruct;
		                //					Jinra::BitStream nICS_BS( data, NewIncomingConnectionStruct_Size, false );
		                //					newIncomingConnectionStruct.Deserialize( nICS_BS );

		                remoteSystem->myExternalSystemAddress = bsSystemAddress;

		                // Bug: If A connects to B through R, A's firstExternalID is set to R. If A tries to send to R, sends to loopback because R==firstExternalID
		                // Correct fix is to specify in Connect() if target is through a proxy.
		                // However, in practice you have to connect to something else first anyway to know about the proxy. So setting once only is good enough
		                if (firstExternalID == UNASSIGNED_SYSTEM_ADDRESS) {
		                    firstExternalID = bsSystemAddress;
		                    firstExternalID.debugPort = ntohs(firstExternalID.address.addr4.sin_port);
		                }

		                // Send this info down to the game
		                packet = allocatePacket(byteSize, data);
		                packet->bitSize = bitSize;
		                packet->systemAddress = systemAddress;
		                packet->systemAddress.systemIndex = remoteSystem->remoteSystemIndex;
		                packet->guid = remoteSystem->guid;
		                packet->guid.systemIndex = packet->systemAddress.systemIndex;
		                AddPacketToProducer(packet);
		            } else {
		                // Send to game even if already connected. This could happen when connecting to 127.0.0.1
		                // Ignore, already connected
		                //	free(data, _FILE_AND_LINE_ );
		            }
		        } else if (static_cast<u8>(data[0]) == ID_CONNECTED_PONG && byteSize == sizeof(u8) + sizeof(TimeMS) * 2) {
		            TimeMS sendPingTime, sendPongTime;

		            // Copy into the ping times array the current time - the value returned
		            // First extract the sent ping
		            BitStream inBitStream(static_cast<u8*>(data), byteSize, false);
		            inBitStream.ignoreBits(8);
		            inBitStream.read(sendPingTime);
		            inBitStream.read(sendPongTime);

		            OnConnectedPong(sendPingTime, sendPongTime, remoteSystem);

		            free(data);
		        } else if (static_cast<u8>(data[0]) == ID_CONNECTED_PING && byteSize == sizeof(u8) + sizeof(TimeMS)) {
		            BitStream inBitStream(static_cast<u8*>(data), byteSize, false);
		            inBitStream.ignoreBits(8);
		            TimeMS sendPingTime;
		            inBitStream.read(sendPingTime);

		            BitStream outBitStream;
		            outBitStream.write(static_cast<MessageID>(ID_CONNECTED_PONG));
		            outBitStream.write(sendPingTime);
		            outBitStream.write(Time::nowMS());
		            SendImmediate(reinterpret_cast<char*>(outBitStream.getData()), outBitStream.getNumberOfBitsUsed(), Packet::IMMEDIATE_PRIORITY, Packet::UNRELIABLE, 0, systemAddress, false, false, Time::nowUS(), 0);

		            // Update again immediately after this tick so the ping goes out right away
		            quitAndDataEvents.setEvent();

		            free(data);
		        } else if (static_cast<u8>(data[0]) == ID_DISCONNECTION_NOTIFICATION) {
		            // We shouldn't close the connection immediately because we need to ack the ID_DISCONNECTION_NOTIFICATION
		            remoteSystem->connectMode = RemoteSystemStruct::DISCONNECT_ON_NO_ACK;
		            free(data);

		            //	AddPacketToProducer(packet);
		        } else if (static_cast<u8>(data[0]) == ID_DETECT_LOST_CONNECTIONS && byteSize == sizeof(u8)) {
		            // Do nothing
		            free(data);
		        } else if (static_cast<u8>(data[0]) == ID_INVALID_PASSWORD) {
		            if (remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION) {
		                packet = allocatePacket(byteSize, data);
		                packet->bitSize = bitSize;
		                packet->systemAddress = systemAddress;
		                packet->systemAddress.systemIndex = remoteSystem->remoteSystemIndex;
		                packet->guid = remoteSystem->guid;
		                packet->guid.systemIndex = packet->systemAddress.systemIndex;
		                AddPacketToProducer(packet);

		                remoteSystem->connectMode = RemoteSystemStruct::DISCONNECT_ASAP_SILENTLY;
		            } else {
		                free(data);
		            }
		        } else if (static_cast<u8>(data[0]) == ID_CONNECTION_REQUEST_ACCEPTED) {
		            if (byteSize > sizeof(MessageID) + sizeof(u32) + sizeof(u16) + sizeof(SystemIndex) + sizeof(TimeMS) * 2) {
		                // Make sure this connection accept is from someone we wanted to connect to
		                bool allowConnection, alreadyConnected;

		                if (remoteSystem->connectMode == RemoteSystemStruct::HANDLING_CONNECTION_REQUEST ||
		                    remoteSystem->connectMode == RemoteSystemStruct::REQUESTED_CONNECTION ||
		                    allowConnectionResponseIPMigration)
		                    allowConnection = true;
		                else
		                    allowConnection = false;

		                if (remoteSystem->connectMode == RemoteSystemStruct::HANDLING_CONNECTION_REQUEST)
		                    alreadyConnected = true;
		                else
		                    alreadyConnected = false;

		                if (allowConnection) {
		                    NetAddress externalID;
		                    SystemIndex systemIndex;
		                    //								NetAddress internalID;

		                    BitStream inBitStream(static_cast<u8*>(data), byteSize, false);
		                    inBitStream.ignoreBits(8);
		                    //	inBitStream.Read(remotePort);
		                    inBitStream.read(externalID);
		                    inBitStream.read(systemIndex);
		                    for (u32 i = 0; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i)
		                        inBitStream.read(remoteSystem->theirInternalSystemAddress[i]);

		                    TimeMS sendPingTime, sendPongTime;
		                    inBitStream.read(sendPingTime);
		                    inBitStream.read(sendPongTime);
		                    OnConnectedPong(sendPingTime, sendPongTime, remoteSystem);

		                    // Find a free remote system struct to use
		                    //						BitStream casBitS(data, byteSize, false);
		                    //						ConnectionAcceptStruct cas;
		                    //						cas.Deserialize(casBitS);
		                    //	systemAddress.GetPort() = remotePort;

		                    // The remote system told us our external IP, so save it
		                    remoteSystem->myExternalSystemAddress = externalID;
		                    remoteSystem->connectMode = RemoteSystemStruct::CONNECTED;

		                    // Bug: If A connects to B through R, A's firstExternalID is set to R. If A tries to send to R, sends to loopback because R==firstExternalID
		                    // Correct fix is to specify in Connect() if target is through a proxy.
		                    // However, in practice you have to connect to something else first anyway to know about the proxy. So setting once only is good enough
		                    if (firstExternalID == UNASSIGNED_SYSTEM_ADDRESS) {
		                        firstExternalID = externalID;
		                        firstExternalID.debugPort = ntohs(firstExternalID.address.addr4.sin_port);
		                    }

		                    // Send the connection request complete to the game
		                    packet = allocatePacket(byteSize, data);
		                    packet->bitSize = byteSize * 8;
		                    packet->systemAddress = systemAddress;
		                    packet->systemAddress.systemIndex = static_cast<SystemIndex>(GetIndexFromSystemAddress(systemAddress, true));
		                    packet->guid = remoteSystem->guid;
		                    packet->guid.systemIndex = packet->systemAddress.systemIndex;
		                    AddPacketToProducer(packet);

		                    BitStream outBitStream;
		                    outBitStream.write(static_cast<MessageID>(ID_NEW_INCOMING_CONNECTION));
		                    outBitStream.write(systemAddress);
		                    for (u32 i = 0; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS; ++i)
		                        outBitStream.write(ipList[i]);
		                    outBitStream.write(sendPongTime);
		                    outBitStream.write(Time::nowMS());

		                    SendImmediate(reinterpret_cast<char*>(outBitStream.getData()), outBitStream.getNumberOfBitsUsed(), Packet::IMMEDIATE_PRIORITY, Packet::RELIABLE_ORDERED, 0, systemAddress, false, false, Time::nowUS(), 0);

		                    if (!alreadyConnected) {
		                        pingInternal(systemAddress, true, Packet::UNRELIABLE);
		                    }
		                } else {
		                    // Ignore, already connected
		                    free(data);
		                }
		            } else {
		                // Version mismatch error?
		                ASSERT(0);
		                free(data);
		            }
		        } else {
		            // What do I do if I get a message from a system, before I am fully connected?
		            // I can either ignore it or give it to the user
		            // It seems like giving it to the user is a better option
		            if ((data[0] > static_cast<MessageID>(ID_IP_RECENTLY_CONNECTED) || data[0] == ID_SND_RECEIPT_ACKED || data[0] == ID_SND_RECEIPT_LOSS) && remoteSystem->isActive) {
		                packet = allocatePacket(byteSize, data);
		                packet->bitSize = bitSize;
		                packet->systemAddress = systemAddress;
		                packet->systemAddress.systemIndex = remoteSystem->remoteSystemIndex;
		                packet->guid = remoteSystem->guid;
		                packet->guid.systemIndex = packet->systemAddress.systemIndex;
		                AddPacketToProducer(packet);
		            } else {
		                free(data);
		            }
		        }
		    }

		    // Does the reliability layer have any more packets waiting for us?
		    // To be thread safe, this has to be called in the same thread as HandleSocketReceiveFromConnectedPlayer
		    bitSize = remoteSystem->reliabilityLayer.receive(&data);
		}

	}

	return true;
}

void NetPeer::onRNS2Recv(NetSocket2::RecvStruct *recvStruct)
{
	if (incomingDatagramEventHandler != nullptr && !incomingDatagramEventHandler(recvStruct))
		return;

	PushBufferedPacket(recvStruct);
	quitAndDataEvents.setEvent();
}

JINRA_THREAD_DECLARATION(Jinra::UpdateNetworkLoop)
{
	auto netPeer = static_cast<NetPeer*>(arguments);

	BitStream updateBitStream(MAXIMUM_MTU_SIZE);

	netPeer->isMainLoopThreadActive = true;

	while (!netPeer->endThreads) {
		if (netPeer->userUpdateThreadPtr)
		    netPeer->userUpdateThreadPtr(netPeer, netPeer->userUpdateThreadData);

		netPeer->RunUpdateCycle(updateBitStream);

		// Pending sends go out this often, unless quitAndDataEvents is set
		netPeer->quitAndDataEvents.waitOnEvent(10);
	}

	netPeer->isMainLoopThreadActive = false;
	return 0;
}

void NetPeer::fillIPList()
{
	if (ipList[0] != UNASSIGNED_SYSTEM_ADDRESS)
		return;

	// Fill out ipList structure
	NetSocket2::getMyIP(ipList);

	// Sort the addresses from lowest to highest
	s32 startingIdx = 0;
	while (startingIdx < MAXIMUM_NUMBER_OF_INTERNAL_IDS - 1 && ipList[startingIdx] != UNASSIGNED_SYSTEM_ADDRESS) {
		s32 lowestIdx = startingIdx;
		for (s32 curIdx = startingIdx + 1; curIdx < MAXIMUM_NUMBER_OF_INTERNAL_IDS - 1 && ipList[curIdx] != UNASSIGNED_SYSTEM_ADDRESS; ++curIdx) {
		    if (ipList[curIdx] < ipList[startingIdx])
		        lowestIdx = curIdx;
		}
		if (startingIdx != lowestIdx) {
		    auto temp = ipList[startingIdx];
		    ipList[startingIdx] = ipList[lowestIdx];
		    ipList[lowestIdx] = temp;
		}
		++startingIdx;
	}
}
