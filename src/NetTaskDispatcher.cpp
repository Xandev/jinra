#include "Jinra.h"
#include "NetTaskDispatcher.h"
#include "NetTask.h"

namespace Jinra
{

void NetTaskDispatcher::main() {
    // NOTE: second argument defer_lock is to prevent from immediate locking.
    std::unique_lock<std::mutex> taskLockUnique(_taskLock, std::defer_lock);

    while (getState() != ThreadState::TERMINATED) {
        // Check if there are tasks waiting.
        taskLockUnique.lock();

        if (_taskList.empty()) {
            // If the list is empty wait for signal.
            _taskSignal.wait(taskLockUnique);
        }

        if (!_taskList.empty()) {
            // Take the first task.
            auto task = _taskList.front();
            _taskList.pop_front();
            taskLockUnique.unlock();

            if (!task->hasExpired()) {
                ++_dispatcherCycle;

                // Execute it.
                (*task)();
            }
            delete task;
        } else {
            taskLockUnique.unlock();
        }
    }
}

void NetTaskDispatcher::addTask(NetTask *task, bool pushFront) {
    bool doSignal = false;

    _taskLock.lock();

    if (getState() == ThreadState::RUNNING) {
        doSignal = _taskList.empty();
        pushFront ? _taskList.push_front(task) : _taskList.push_back(task);
    } else {
        delete task;
    }

    _taskLock.unlock();

    // Send a signal if the list was empty.
    if (doSignal) {
        _taskSignal.notify_one();
    }
}

void NetTaskDispatcher::shutdown() {
    auto task = new NetTask([this]() {
        setState(ThreadState::TERMINATED);
        _taskSignal.notify_one();
    });

    std::lock_guard<std::mutex> lockClass(_taskLock);
    _taskList.push_back(task);

    _taskSignal.notify_one();
}

NetTaskDispatcher::NetTaskDispatcher()
    : _dispatcherCycle(0u)
{

}

} // namespace Jinra