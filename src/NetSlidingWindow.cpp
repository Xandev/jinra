#include "Jinra.h"
#include "NetSlidingWindow.h"

namespace Jinra
{

static const TimeUS SYN = 10000u;

const double NetSlidingWindow::_UNSET_TIME_US = -1.0;

void NetSlidingWindow::init(u32 maxDatagramPayload)
{
	_lastRtt = _estimatedRTT = _deviationRtt = _UNSET_TIME_US;
	ASSERT(maxDatagramPayload <= MAXIMUM_MTU_SIZE);
	_maxMTUIncludingUPDHeader = maxDatagramPayload;
	_cwnd = maxDatagramPayload;
	_ssThresh = 0.0;
	_oldestUnsentAck = 0u;
	_nextDatagramSequenceNumber = 0;
	_nextCongestionControlBlock = 0;
	_backoffThisBlock = _speedUpThisBlock = false;
	_expectedNextSequenceNumber = 0;
	_isContinuousSend = false;
}

bool NetSlidingWindow::shouldSendACKs(TimeUS curTime) const
{
	auto rto = getSenderRTOForACK();

	// iphone crashes on comparison between double and int64 
	// http://www.jenkinssoftware.com/forum/index.php?topic=2717.0
	if (rto == static_cast<TimeUS>(_UNSET_TIME_US)) {
		// Unknown how long until the remote system will retransmit, so better send right away.
		return true;
	}

	return curTime >= _oldestUnsentAck + SYN;
}

DatagramSequenceNumberType NetSlidingWindow::getAndIncrementNextDatagramSequenceNumber()
{
	auto dsnt = _nextDatagramSequenceNumber;
	++_nextDatagramSequenceNumber;
	return dsnt;
}

bool NetSlidingWindow::onGotPacket(DatagramSequenceNumberType datagramSequenceNumber, 
								   TimeUS curTime, u32* skippedMessageCount)
{
	if (_oldestUnsentAck == 0u)
		_oldestUnsentAck = curTime;

	if (datagramSequenceNumber == _expectedNextSequenceNumber) {
		*skippedMessageCount = 0u;
		_expectedNextSequenceNumber = datagramSequenceNumber + 
			static_cast<DatagramSequenceNumberType>(1);
	} else if (greaterThan(datagramSequenceNumber, _expectedNextSequenceNumber)) {
		*skippedMessageCount = datagramSequenceNumber - _expectedNextSequenceNumber;
		
		// Sanity check, just use timeout resend if this was really valid.
		if (*skippedMessageCount > 1000u) {
			// During testing, the nat punchthrough server got 51200 on the first packet. 
			// I have no idea where this comes from, but has happened twice
			if (*skippedMessageCount > 50000u)
				return false;
			*skippedMessageCount = 1000u;
		}

		_expectedNextSequenceNumber = datagramSequenceNumber + 
			static_cast<DatagramSequenceNumberType>(1);
	} else {
		*skippedMessageCount = 0u;
	}

	return true;
}

void NetSlidingWindow::onResend()
{
	if (_isContinuousSend && !_backoffThisBlock && _cwnd > _maxMTUIncludingUPDHeader * 2.0) {
		_ssThresh = _cwnd / 2.0;
		if (_ssThresh < _maxMTUIncludingUPDHeader)
			_ssThresh = _maxMTUIncludingUPDHeader;
		_cwnd = _maxMTUIncludingUPDHeader;

		// Only backoff once per period
		_nextCongestionControlBlock = _nextDatagramSequenceNumber;
		_backoffThisBlock = true;
	}
}

void NetSlidingWindow::onNAK()
{
	if (_isContinuousSend && !_backoffThisBlock) {
		// Start congestion avoidance.
		_ssThresh = _cwnd / 2.0;
	}
}

void NetSlidingWindow::onAck(TimeUS rtt, bool isContinuousSend, 
							 DatagramSequenceNumberType sequenceNumber)
{
	_lastRtt = static_cast<double>(rtt);
	if (_estimatedRTT == _UNSET_TIME_US) {
		_estimatedRTT = _deviationRtt = _lastRtt;
	} else {
		double difference = rtt - _estimatedRTT;
		_estimatedRTT = _estimatedRTT + 0.05 * difference;
		_deviationRtt = _deviationRtt + 0.05 * (abs(difference) - _deviationRtt);
	}

	_isContinuousSend = isContinuousSend;
	if (!isContinuousSend)
		return;

	auto isNewCongestionControlPeriod = greaterThan(sequenceNumber, _nextCongestionControlBlock);
	if (isNewCongestionControlPeriod) {
		_backoffThisBlock = false;
		_speedUpThisBlock = false;
		_nextCongestionControlBlock = _nextDatagramSequenceNumber;
	}

	if (isInSlowStart()) {
		_cwnd += _maxMTUIncludingUPDHeader;
		if (_cwnd > _ssThresh && _ssThresh != 0.0)
			_cwnd = _ssThresh + _maxMTUIncludingUPDHeader * _maxMTUIncludingUPDHeader / _cwnd;
	} else if (isNewCongestionControlPeriod) {
		_cwnd += _maxMTUIncludingUPDHeader * _maxMTUIncludingUPDHeader / _cwnd;
	}
}

void NetSlidingWindow::onSendAck() {
	_oldestUnsentAck = 0;
}

TimeUS NetSlidingWindow::getRTOForRetransmission() const
{
	const TimeUS maxThreshold = 2000000u;
	const TimeUS additionalVariance = 30000u;

	if (_estimatedRTT == _UNSET_TIME_US)
		return maxThreshold;

	auto threshhold = static_cast<TimeUS>(2.0 * _estimatedRTT + 4.0 * _deviationRtt) + 
		additionalVariance;

	return threshhold > maxThreshold ? maxThreshold : threshhold;
}

s32 NetSlidingWindow::getTransmissionBandwidth(u32 unacknowledgedBytes, bool isContinuousSend)
{
	_isContinuousSend = isContinuousSend;
	return unacknowledgedBytes <= _cwnd ? static_cast<s32>(_cwnd - unacknowledgedBytes) : 0;
}

void NetSlidingWindow::setMTU(u32 bytes)
{
	ASSERT(bytes < MAXIMUM_MTU_SIZE);
	_maxMTUIncludingUPDHeader = bytes;
}

bool NetSlidingWindow::greaterThan(DatagramSequenceNumberType a, DatagramSequenceNumberType b)
{
	// a > b?
	const DatagramSequenceNumberType halfSpan = static_cast<DatagramSequenceNumberType>(
		static_cast<const u32>(-1) / static_cast<DatagramSequenceNumberType>(2));
	return b != a && b - a > halfSpan;
}

bool NetSlidingWindow::lessThan(DatagramSequenceNumberType a, DatagramSequenceNumberType b)
{
	// a < b?
	const DatagramSequenceNumberType halfSpan = static_cast<DatagramSequenceNumberType>(
		static_cast<const u32>(-1) / static_cast<DatagramSequenceNumberType>(2));
	return b != a && b - a < halfSpan;
}

TimeUS NetSlidingWindow::getSenderRTOForACK() const
{
	if (_lastRtt == _UNSET_TIME_US)
		return static_cast<TimeUS>(_UNSET_TIME_US);
	return static_cast<TimeUS>(_lastRtt + SYN);
}

}