#include "Jinra.h"
#include "SuperFastHash.h"

#undef get16bits

#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
|| defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*(reinterpret_cast<const u16*>(d)))
#else
#define get16bits(d) ((((u32)(((const u8*)(d))[1])) << 8) + (u32)(((const u8*)(d))[0]) )
#endif

namespace Jinra
{

static const s32 INCREMENTAL_READ_BLOCK = 65536;

u32 superFastHash(cchar* data, s32 length)
{
	// All this is necessary or the hash does not match superFastHashIncremental().
	auto bytesRemaining = length;
	u32 lastHash = length;
	s32 offset = 0;
	while (bytesRemaining >= INCREMENTAL_READ_BLOCK) {
		lastHash = superFastHashIncremental(data + offset, INCREMENTAL_READ_BLOCK, lastHash);
		bytesRemaining -= INCREMENTAL_READ_BLOCK;
		offset += INCREMENTAL_READ_BLOCK;
	}

	if (bytesRemaining > 0)
		lastHash = superFastHashIncremental(data + offset, bytesRemaining, lastHash);

	return lastHash;
}

u32 superFastHashIncremental(cchar * data, s32 len, u32 lastHash)
{
	u32 hash = lastHash;

    if (len <= 0 || data == nullptr)
		return 0u;

	s32 rem = len & 3;
	len >>= 2;

	// Main loop.
	for (; len > 0; --len) {
		hash += get16bits(data);
		u32 tmp = get16bits(data + 2) << 11 ^ hash;
		hash = hash << 16 ^ tmp;
		data += 2 * sizeof(u16);
		hash += hash >> 11;
	}

	// Handle end cases.
	switch (rem) {
		case 3: hash += get16bits(data);
			hash ^= hash << 16;
			hash ^= data[sizeof(u16)] << 18;
			hash += hash >> 11;
			break;
		case 2: hash += get16bits(data);
			hash ^= hash << 11;
			hash += hash >> 17;
			break;
		case 1: hash += *data;
			hash ^= hash << 10;
			hash += hash >> 1;
	}

	// Force "avalanching" of final 127 bits.
	hash ^= hash << 3;
	hash += hash >> 5;
	hash ^= hash << 4;
	hash += hash >> 17;
	hash ^= hash << 25;
	hash += hash >> 6;

	return static_cast<u32>(hash);
}

u32 superFastHashFile(cchar* fileName)
{
	auto file = fopen(fileName, "rb");
	if (file == nullptr)
		return 0;

	auto hash = superFastHashFilePtr(file);
	fclose(file);

	return hash;
}

u32 superFastHashFilePtr(FILE* file)
{
	fseek(file, 0, SEEK_END);
	s32 length = ftell(file);
	fseek(file, 0, SEEK_SET);

	s32 bytesRemaining = length;
	u32 lastHash = length;
	char readBlock[INCREMENTAL_READ_BLOCK];

	while (bytesRemaining >= static_cast<s32>(sizeof readBlock)) {
		fread(readBlock, sizeof readBlock, 1, file);
		lastHash = superFastHashIncremental(readBlock, static_cast<s32>(sizeof readBlock), 
											lastHash);
		bytesRemaining -= static_cast<s32>(sizeof readBlock);
	}

	if (bytesRemaining > 0) {
		fread(readBlock, bytesRemaining, 1, file);
		lastHash = superFastHashIncremental(readBlock, bytesRemaining, lastHash);
	}

	return lastHash;
}

}