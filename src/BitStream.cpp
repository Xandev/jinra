#include "Jinra.h"
#include "BitStream.h"

#include "NetDefines.h"

namespace Jinra
{

BitStream::BitStream()
	: _data(_stackData), _numberOfBitsUsed(0), 
	_numberOfBitsAllocated(BITSTREAM_STACK_ALLOCATION_SIZE * 8), 
	_readOffset(0),  _copyData(true)
{ }

BitStream::BitStream(const u32 initialBytesToAllocate)
	: _numberOfBitsUsed(0), _readOffset(0), _copyData(true)
{
	if (initialBytesToAllocate <= BITSTREAM_STACK_ALLOCATION_SIZE) {
		_data = _stackData;
		_numberOfBitsAllocated = BITSTREAM_STACK_ALLOCATION_SIZE * 8;
	} else {
		_data = static_cast<u8*>(malloc(static_cast<size_t>(initialBytesToAllocate)));
		_numberOfBitsAllocated = initialBytesToAllocate << 3;
	}

#if defined(JINRA_DEBUG)
	ASSERT(_data);
#endif // defined(JINRA_DEBUG)
}

BitStream::BitStream(u8* packetData, const u32 lengthInBytes, bool copyData)
	: _readOffset(0), _copyData(copyData)
{
	_numberOfBitsUsed = lengthInBytes << 3;
	_numberOfBitsAllocated = lengthInBytes << 3;

	if (_copyData) {
		if (lengthInBytes > 0) {
			if (lengthInBytes < BITSTREAM_STACK_ALLOCATION_SIZE) {
				_data = static_cast<u8*>(_stackData);
				_numberOfBitsAllocated = BITSTREAM_STACK_ALLOCATION_SIZE << 3;
			} else {
				_data = static_cast<u8*>(malloc(static_cast<size_t>(lengthInBytes)));
			}

#if defined(JINRA_DEBUG)
			ASSERT(_data);
#endif // defined(JINRA_DEBUG)

			memcpy(_data, packetData, static_cast<size_t>(lengthInBytes));
		} else {
			_data = nullptr;
		}
	} else {
		_data = static_cast<u8*>(packetData);
	}
}

BitStream::~BitStream()
{
	if (_copyData && _numberOfBitsAllocated > BITSTREAM_STACK_ALLOCATION_SIZE << 3)
		free(_data);  // Use realloc and free so we are more efficient than delete and new for resizing
}

void BitStream::reset()
{
	resetWritePointer();
	resetReadPointer();
}

void BitStream::resetWritePointer() {
	_numberOfBitsUsed = 0;
}

void BitStream::resetReadPointer() {
	_readOffset = 0;
}

// Write an array or casted stream
void BitStream::write(cchar* inputByteArray, const u32 numberOfBytes)
{
	if (numberOfBytes == 0u)
		return;

	// Optimization:
	if ((_numberOfBitsUsed & 7) == 0) {
		addBitsAndReallocate(BYTES_TO_BITS(numberOfBytes));
		memcpy(_data + BITS_TO_BYTES(_numberOfBitsUsed), inputByteArray,
			   static_cast<size_t>(numberOfBytes));
		_numberOfBitsUsed += BYTES_TO_BITS(numberOfBytes);
	} else {
		writeBits((u8*)inputByteArray, numberOfBytes * 8, true);
	}

}
void BitStream::write(BitStream* bitStream) {
	write(bitStream, bitStream->getNumberOfBitsUsed() - bitStream->getReadOffset());
}

void BitStream::write(BitStream* bitStream, BitSize_t numberOfBits)
{
	addBitsAndReallocate(numberOfBits);

    if ((bitStream->getReadOffset() & 7) == 0 && (_numberOfBitsUsed & 7) == 0) {
		s32 readOffsetBytes = bitStream->getReadOffset() / 8;
		s32 numBytes = numberOfBits / 8;
		memcpy(_data + (_numberOfBitsUsed >> 3), bitStream->getData() + readOffsetBytes,
			   numBytes);
		numberOfBits -= BYTES_TO_BITS(numBytes);
		bitStream->setReadOffset(BYTES_TO_BITS(numBytes + readOffsetBytes));
		_numberOfBitsUsed += BYTES_TO_BITS(numBytes);
	}

	while (numberOfBits-- > 0 && bitStream->_readOffset + 1 <= bitStream->_numberOfBitsUsed) {
		BitSize_t numberOfBitsMod8 = _numberOfBitsUsed & 7;
		if (numberOfBitsMod8 == 0) {
			// New byte
			if (bitStream->_data[bitStream->_readOffset >> 3] &
				0x80 >> (bitStream->_readOffset & 7)) {
				// Write 1
				_data[_numberOfBitsUsed >> 3] = 0x80;
			} else {
				// Write 0
				_data[_numberOfBitsUsed >> 3] = 0;
			}
		} else {
			// Existing byte
			if (bitStream->_data[bitStream->_readOffset >> 3] &
				0x80 >> (bitStream->_readOffset & 7)) {
				// Set the bit to 1
				_data[_numberOfBitsUsed >> 3] |= 0x80 >> numberOfBitsMod8;
			}
			// else 0, do nothing
		}

		++bitStream->_readOffset;
		++_numberOfBitsUsed;
	}
}

void BitStream::write(BitStream& bitStream, BitSize_t numberOfBits) {
	write(&bitStream, numberOfBits);
}

void BitStream::write(BitStream& bitStream) {
	write(&bitStream);
}

bool BitStream::read(BitStream* bitStream, BitSize_t numberOfBits)
{
	if (getNumberOfUnreadBits() < numberOfBits)
		return false;

	bitStream->write(this, numberOfBits);
	return true;
}

bool BitStream::read(BitStream* bitStream)
{
	bitStream->write(this);
	return true;
}

bool BitStream::read(BitStream& bitStream, BitSize_t numberOfBits)
{
	if (getNumberOfUnreadBits() < numberOfBits)
		return false;

	bitStream.write(this, numberOfBits);
	return true;
}

bool BitStream::read(BitStream& bitStream)
{
	bitStream.write(this);
	return true;
}

bool BitStream::read(char* outByteArray, const u32 numberOfBytes)
{
	// Optimization:
	if ((_readOffset & 7) == 0) {
		if (_readOffset + (numberOfBytes << 3) > _numberOfBitsUsed)
			return false;

		// Write the data
		memcpy(outByteArray, _data + (_readOffset >> 3), static_cast<size_t>(numberOfBytes));

		_readOffset += numberOfBytes << 3;
		return true;
	}

	return ReadBits((u8*)outByteArray, numberOfBytes * 8);
}

void BitStream::write0()
{
	addBitsAndReallocate(1);

	// New bytes need to be zeroed
	if ((_numberOfBitsUsed & 7) == 0)
		_data[_numberOfBitsUsed >> 3] = 0;

	++_numberOfBitsUsed;
}

void BitStream::write1()
{
	addBitsAndReallocate(1);

	BitSize_t numberOfBitsMod8 = _numberOfBitsUsed & 7;

	if (numberOfBitsMod8 == 0) {
		_data[_numberOfBitsUsed >> 3] = 0x80;
	} else {
		_data[_numberOfBitsUsed >> 3] |= 0x80 >> numberOfBitsMod8; // Set the bit to 1
	}

	++_numberOfBitsUsed;
}

bool BitStream::readBit()
{
	bool result = (_data[_readOffset >> 3] & 0x80 >> (_readOffset & 7)) != 0;
	++_readOffset;
	return result;
}

void BitStream::setNumberOfBitsAllocated(const BitSize_t lengthInBits)
{
#if defined(JINRA_DEBUG)
	ASSERT(lengthInBits >= static_cast<BitSize_t>(_numberOfBitsAllocated));
#endif // defined(JINRA_DEBUG)
	_numberOfBitsAllocated = lengthInBits;
}

void BitStream::writeAlignedBytes(const u8* inByteArray, const u32 numberOfBytesToWrite)
{
	alignWriteToByteBoundary();
	write(reinterpret_cast<cchar*>(inByteArray), numberOfBytesToWrite);
}

bool BitStream::readAlignedBytes(u8* inOutByteArray, const u32 numberOfBytesToRead)
{
	if (numberOfBytesToRead <= 0)
		return false;

	// Byte align
	AlignReadToByteBoundary();

	if (_readOffset + (numberOfBytesToRead << 3) > _numberOfBitsUsed)
		return false;

	// Write the data
	memcpy(inOutByteArray, _data + (_readOffset >> 3), static_cast<size_t>(numberOfBytesToRead));

	_readOffset += numberOfBytesToRead << 3;

	return true;
}

void BitStream::writeBits(const u8* inByteArray, BitSize_t numberOfBitsToWrite,
						  const bool rightAlignedBits)
{
	addBitsAndReallocate(numberOfBitsToWrite);

	const BitSize_t numberOfBitsUsedMod8 = _numberOfBitsUsed & 7;

	// If currently aligned and numberOfBits is a multiple of 8, just memcpy for speed
	if (numberOfBitsUsedMod8 == 0 && (numberOfBitsToWrite & 7) == 0) {
		memcpy(_data + (_numberOfBitsUsed >> 3), inByteArray, numberOfBitsToWrite >> 3);
		_numberOfBitsUsed += numberOfBitsToWrite;
		return;
	}

    const u8* inputPtr = inByteArray;

	// Faster to put the while at the top surprisingly enough
	while (numberOfBitsToWrite > 0) {
		u8 dataByte = *inputPtr++;

		// rightAlignedBits means in the case of a partial byte, the bits are aligned from 
		// the right (bit 0) rather than the left (as in the normal internal representation)
		if (numberOfBitsToWrite < 8 && rightAlignedBits) {
			// shift left to get the bits on the left, as in our internal representation
			dataByte <<= 8 - numberOfBitsToWrite;
		}

		// Writing to a new byte each time
		if (numberOfBitsUsedMod8 == 0) {
			*(_data + (_numberOfBitsUsed >> 3)) = dataByte;
		} else {
			// Copy over the new data.
			*(_data + (_numberOfBitsUsed >> 3)) |= dataByte >> numberOfBitsUsedMod8; // First half

			// If we didn't write it all out in the first half (8 - (numberOfBitsUsed%8) is the
			// number we wrote in the first half)
			if (8 - numberOfBitsUsedMod8 < 8 &&
				8 - numberOfBitsUsedMod8 < numberOfBitsToWrite) {
				// Second half (overlaps byte boundary)
				*(_data + (_numberOfBitsUsed >> 3) + 1) =
					(u8)(dataByte << 8 - numberOfBitsUsedMod8);
			}
		}

		if (numberOfBitsToWrite >= 8) {
			_numberOfBitsUsed += 8;
			numberOfBitsToWrite -= 8;
		} else {
			_numberOfBitsUsed += numberOfBitsToWrite;
			numberOfBitsToWrite = 0;
		}
	}
}

void BitStream::setData(u8* inByteArray)
{
	_data = inByteArray;
	_copyData = false;
}

// Read numberOfBitsToRead bits to the output source
// alignBitsToRight should be set to true to convert internal bitstream data to userdata
// It should be false if you used WriteBits with rightAlignedBits false
bool BitStream::ReadBits(u8 *inOutByteArray, BitSize_t numberOfBitsToRead, const bool alignBitsToRight)
{
	if (numberOfBitsToRead <= 0)
		return false;

	if (_readOffset + numberOfBitsToRead > _numberOfBitsUsed)
		return false;


	const BitSize_t readOffsetMod8 = _readOffset & 7;

	// If currently aligned and numberOfBits is a multiple of 8, just memcpy for speed
	if (readOffsetMod8 == 0 && (numberOfBitsToRead & 7) == 0) {
		memcpy(inOutByteArray, _data + (_readOffset >> 3), numberOfBitsToRead >> 3);
		_readOffset += numberOfBitsToRead;
		return true;
	}



	BitSize_t offset = 0;

	memset(inOutByteArray, 0, (size_t)BITS_TO_BYTES(numberOfBitsToRead));

	while (numberOfBitsToRead > 0) {
		*(inOutByteArray + offset) |= *(_data + (_readOffset >> 3)) << readOffsetMod8; // First half

		if (readOffsetMod8 > 0 && numberOfBitsToRead > 8 - readOffsetMod8)   // If we have a second half, we didn't read enough bytes in the first half
			*(inOutByteArray + offset) |= *(_data + (_readOffset >> 3) + 1) >> 8 - readOffsetMod8; // Second half (overlaps byte boundary)

		if (numberOfBitsToRead >= 8) {
			numberOfBitsToRead -= 8;
			_readOffset += 8;
			++offset;
		} else {
			int neg = (int)numberOfBitsToRead - 8;

			if (neg < 0)   // Reading a partial byte for the last byte, shift right so the data is aligned on the right
			{

				if (alignBitsToRight)
					* (inOutByteArray + offset) >>= -neg;

				_readOffset += 8 + neg;
			} else
				_readOffset += 8;

			++offset;

			numberOfBitsToRead = 0;
		}
	}

	return true;
}

// Reallocates (if necessary) in preparation of writing numberOfBitsToWrite
void BitStream::addBitsAndReallocate(const BitSize_t numberOfBitsToWrite)
{
	BitSize_t newNumberOfBitsAllocated = numberOfBitsToWrite + _numberOfBitsUsed;

	if (numberOfBitsToWrite + _numberOfBitsUsed > 0 && _numberOfBitsAllocated - 1 >> 3 < newNumberOfBitsAllocated - 1 >> 3)   // If we need to allocate 1 or more new bytes
	{
#ifdef JINRA_DEBUG
		// If this assert hits then we need to specify true for the third parameter in the constructor
		// It needs to reallocate to hold all the data and can't do it unless we allocated to begin with
		// Often hits if you call Write or Serialize on a read-only bitstream
		ASSERT(_copyData);
#endif

		// Less memory efficient but saves on news and deletes
		/// Cap to 1 meg buffer to save on huge allocations
		newNumberOfBitsAllocated = (numberOfBitsToWrite + _numberOfBitsUsed) * 2;
		if (newNumberOfBitsAllocated - (numberOfBitsToWrite + _numberOfBitsUsed) > 1048576)
			newNumberOfBitsAllocated = numberOfBitsToWrite + _numberOfBitsUsed + 1048576;

		//		BitSize_t newByteOffset = BITS_TO_BYTES( numberOfBitsAllocated );
		// Use realloc and free so we are more efficient than delete and new for resizing
		BitSize_t amountToAllocate = BITS_TO_BYTES(newNumberOfBitsAllocated);
		if (_data == (u8*)_stackData) {
			if (amountToAllocate > BITSTREAM_STACK_ALLOCATION_SIZE) {
				_data = (u8*)malloc((size_t)amountToAllocate);
				ASSERT(_data);

				// need to copy the stack data over to our new memory area too
				memcpy((void *)_data, (void *)_stackData, (size_t)BITS_TO_BYTES(_numberOfBitsAllocated));
			}
		} else {
			_data = (u8*)realloc(_data, (size_t)amountToAllocate);
		}

#if defined(JINRA_DEBUG)
		ASSERT(_data); // Make sure realloc succeeded
#endif // defined(JINRA_DEBUG)
	}

	if (newNumberOfBitsAllocated > _numberOfBitsAllocated)
		_numberOfBitsAllocated = newNumberOfBitsAllocated;
}

void BitStream::PadWithZeroToByteLength(u32 bytes)
{
	if (getNumberOfBytesUsed() < bytes) {
		alignWriteToByteBoundary();

		auto numToWrite = bytes - getNumberOfBytesUsed();
		addBitsAndReallocate(BYTES_TO_BITS(numToWrite));
		memset(_data + BITS_TO_BYTES(_numberOfBitsUsed), 0, static_cast<size_t>(numToWrite));
		_numberOfBitsUsed += BYTES_TO_BITS(numToWrite);
	}
}

void BitStream::assertStreamEmpty() const
{
	ASSERT(_readOffset == _numberOfBitsUsed);
}

// Exposes the data for you to look at, like PrintBits does.
// Data will point to the stream.  Returns the length in bits of the stream.
BitSize_t BitStream::copyData(u8** _data) const
{
#ifdef JINRA_DEBUG
	ASSERT(_numberOfBitsUsed > 0);
#endif

	*_data = static_cast<u8*>(malloc((size_t)BITS_TO_BYTES(_numberOfBitsUsed)));
	memcpy(*_data, _data, sizeof(u8) * (size_t)(BITS_TO_BYTES(_numberOfBitsUsed)));
	return _numberOfBitsUsed;
}

void BitStream::ignoreBits(const BitSize_t numberOfBits) {
	_readOffset += numberOfBits;
}

void BitStream::ignoreBytes(const u32 numberOfBytes) {
	ignoreBits(BYTES_TO_BITS(numberOfBytes));
}

// If we used the constructor version with copy data off, this makes sure it is set to on and the data pointed to is copied.
void BitStream::AssertCopyData()
{
	if (!_copyData) {
		_copyData = true;

		if (_numberOfBitsAllocated > 0) {
		    auto newdata = static_cast<u8*>(malloc((size_t)BITS_TO_BYTES(_numberOfBitsAllocated)));
#if defined(JINRA_DEBUG)
			ASSERT(_data);
#endif // defined(JINRA_DEBUG)

			memcpy(newdata, _data, (size_t)BITS_TO_BYTES(_numberOfBitsAllocated));
			_data = newdata;
		} else
			_data = nullptr;
	}
}

bool BitStream::isNetworkOrder()
{
	static ulong htonlValue = htonl(12345);
	return htonlValue == 12345;
}

bool BitStream::read(char *varString)
{
	u16 l;
	auto b = read(l);
	if (b && l > 0)
		b = readAlignedBytes(reinterpret_cast<u8*>(varString), l);

	if (!b)
		varString[0] = 0;

	varString[l] = 0;
	return b;
}

bool BitStream::read(u8 *varString)
{
	u16 l;
	auto b = read(l);
	if (b && l > 0)
		b = readAlignedBytes(varString, l);

	if (!b)
		varString[0] = 0;

	varString[l] = 0;
	return b;
}

void BitStream::WriteAlignedVar8(cchar *inByteArray)
{
	ASSERT((_numberOfBitsUsed & 7) == 0);
	addBitsAndReallocate(1 * 8);
	_data[(_numberOfBitsUsed >> 3) + 0] = inByteArray[0];
	_numberOfBitsUsed += 1 * 8;
}

bool BitStream::ReadAlignedVar8(char *inOutByteArray)
{
	ASSERT((_readOffset & 7) == 0);
	if (_readOffset + 1 * 8 > _numberOfBitsUsed)
		return false;

	inOutByteArray[0] = _data[(_readOffset >> 3) + 0];
	_readOffset += 1 * 8;
	return true;
}

void BitStream::WriteAlignedVar16(cchar *inByteArray)
{
	ASSERT((_numberOfBitsUsed & 7) == 0);
	addBitsAndReallocate(2 * 8);
_data[(_numberOfBitsUsed >> 3) + 0] = inByteArray[0];
		_data[(_numberOfBitsUsed >> 3) + 1] = inByteArray[1];
	_numberOfBitsUsed += 2 * 8;
}

bool BitStream::ReadAlignedVar16(char *inOutByteArray)
{
	ASSERT((_readOffset & 7) == 0);
	if (_readOffset + 2 * 8 > _numberOfBitsUsed)
		return false;

		inOutByteArray[0] = _data[(_readOffset >> 3) + 0];
		inOutByteArray[1] = _data[(_readOffset >> 3) + 1];

	_readOffset += 2 * 8;
	return true;
}

void BitStream::WriteAlignedVar32(cchar *inByteArray)
{
	ASSERT((_numberOfBitsUsed & 7) == 0);
	addBitsAndReallocate(4 * 8);
		_data[(_numberOfBitsUsed >> 3) + 0] = inByteArray[0];
		_data[(_numberOfBitsUsed >> 3) + 1] = inByteArray[1];
		_data[(_numberOfBitsUsed >> 3) + 2] = inByteArray[2];
		_data[(_numberOfBitsUsed >> 3) + 3] = inByteArray[3];
	_numberOfBitsUsed += 4 * 8;
}

bool BitStream::ReadAlignedVar32(char *inOutByteArray)
{
	ASSERT((_readOffset & 7) == 0);
	if (_readOffset + 4 * 8 > _numberOfBitsUsed)
		return false;
		inOutByteArray[0] = _data[(_readOffset >> 3) + 0];
		inOutByteArray[1] = _data[(_readOffset >> 3) + 1];
		inOutByteArray[2] = _data[(_readOffset >> 3) + 2];
		inOutByteArray[3] = _data[(_readOffset >> 3) + 3];

	_readOffset += 4 * 8;
	return true;
}

} // namespace Jinra