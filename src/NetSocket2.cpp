#include "Jinra.h"
#include "NetSocket2.h"
#include "WindowsSocket2.h"
#include "BerkleySocket2.h"
#include "LinuxSocket2.h"

namespace Jinra
{

NetSocket2::NetSocket2()
	: _eventHandler(nullptr)
{ }

NetSocket2* NetSocket2::create()
{
#if defined(_WIN32)
	NetSocket2* socket = new WindowsSocket2();
	socket->setSocketType(Type::WINDOWS);
#else // defined(_WIN32)
	NetSocket2* socket = new LinuxSocket2();
	socket->setSocketType(Type::LINUX);
#endif // defined(_WIN32)
	return socket;
}

void NetSocket2::domainNameToIP(cchar* domainName, char ip[65]) {
	BerkleySocket2::domainNameToIP(domainName, ip);
}

void NetSocket2::getMyIP(NetAddress addresses[MAXIMUM_NUMBER_OF_INTERNAL_IDS])
{
#if defined(_WIN32)
	WindowsSocket2::getMyIP(addresses);
#else // defined(_WIN32)
	LinuxSocket2::getMyIP(addresses);
#endif // defined(_WIN32)
}

} // namespace Jinra
