#include "Jinra.h"
#include "NetServer.h"
#include "MessageIdentifiers.h"

namespace Jinra
{

NetServer::NetServer()
	: _peer(new NetPeer()), _packet(nullptr)
{ }

NetServer::~NetServer()
{
	shutdown();
}

NetPeer::StartupResult NetServer::initialize(u16 port, u32 maxConnections) const
{
    return initialize(port, maxConnections, "Jinra");
}

NetPeer::StartupResult NetServer::initialize(u16 port, u32 maxConnections, const String& connectionPassword) const
{
	// Sets password for incoming connections.
	_peer->setIncomingPassword(connectionPassword);

	// Setup socket descriptor.
	SocketDescriptor socketDescriptor;
	socketDescriptor.socketFamily = AF_INET;
	socketDescriptor.port = port;

	// Startup peer. 
	auto result = _peer->startup(maxConnections, &socketDescriptor, 1);
	if (result != NetPeer::StartupResult::STARTED)
		return result;

	_peer->setMaximumIncomingConnections(maxConnections);

	return NetPeer::StartupResult::STARTED;
}

void NetServer::receive()
{
    if (!_listener)
        return;

	// Loop through all received packets.
    for (_packet = _peer->readMessage(); _packet != nullptr; _packet = _peer->readMessage()) {
		// Create bit stream based on received packet. 
		// It is easier to get ID of packet from packet rather than 
		// bit stream, so we ignore sizeof(MessageID) bytes.
		BitStream bitStream(_packet->data + sizeof(MessageID), _packet->length - sizeof(MessageID), false);

        u8 packetId = _packet->data[0];
        switch (packetId) {
            case ID_NEW_INCOMING_CONNECTION:
                _listener->onClientConnect();
                break;

            case ID_DISCONNECTION_NOTIFICATION:
                _listener->onClientDisconnect();
                break;

            case ID_CONNECTION_LOST:
                _listener->onClientLostConnection();
                break;

            default:
                _listener->handleMessage(packetId, &bitStream);
                break;
        }

		_peer->deallocate(_packet);
	}
}

u32 NetServer::send(const BitStream* bitStream, Packet::Priority priority, 
                    const NetGUID& guid) const
{
	return _peer->send(bitStream, priority, Packet::RELIABLE_ORDERED, 0, guid, false);
}

u32 NetServer::sendToAll(const BitStream* bitStream, Packet::Priority priority) const
{
	return _peer->send(bitStream, priority, Packet::RELIABLE_ORDERED, 0,
					   UNASSIGNED_SYSTEM_ADDRESS, true);
}

u32 NetServer::sendToAllExcept(const BitStream* bitStream, Packet::Priority priority,
							   const NetGUID& guid) const
{
	return _peer->send(bitStream, priority, Packet::RELIABLE_ORDERED, 0, guid, true);
}

void NetServer::shutdown() const
{
	// Peer should be initialized and connected to someone if we want to shutdown it.
	if (!_peer->isActive())
		return;

	_peer->shutdown(2500u);

	// Wait to end of connections.
	while (_peer->isActive());
}

} // namespace Jinra