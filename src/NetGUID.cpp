#include "Jinra.h"
#include "NetGUID.h"

namespace Jinra
{

NetGUID::NetGUID() {
	*this = UNASSIGNED_NET_GUID;
}

NetGUID::NetGUID(u64 id)
	: id(id), systemIndex(static_cast<SystemIndex>(-1))
{ }

bool NetGUID::fromString(cchar* source)
{
	if (source == nullptr)
		return false;

#if defined(WIN32)
	id = _strtoui64(source, nullptr, 10);
#else // defined(WIN32)
	id = strtoull(source, nullptr, 10);
#endif // defined(WIN32)
	return true;
}

cchar* NetGUID::toString() const
{
	static u8 strIndex = 0u;
	static char str[8][64];

	auto lastStrIndex = strIndex;
	++strIndex;
	toString(str[lastStrIndex & 7]);
	return static_cast<char*>(str[lastStrIndex & 7]);
}

void NetGUID::toString(char* dest) const
{
	if (*this == UNASSIGNED_NET_GUID) {
		strcpy(dest, "UNASSIGNED_JINRA_GUID");
	} else {
		sprintf(dest, "%" PRINTF_64_BIT_MODIFIER "u",
				static_cast<long long unsigned int>(id));
	}
}

ulong NetGUID::toUint32(const NetGUID& guid) {
	return static_cast<ulong>(guid.id >> 32) ^
			static_cast<ulong>(guid.id & 0xFFFFFFFF);
}

NetGUID& NetGUID::operator=(const NetGUID& guid)
{
	id = guid.id;
	systemIndex = guid.systemIndex;
	return *this;
}

} // namespace Jinra