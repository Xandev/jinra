#include "Jinra.h"
#include "NetTaskScheduler.h"
#include "NetTaskDispatcher.h"
#include "NetSchedulerTask.h"

namespace Jinra
{

bool NetTaskScheduler::TaskComparator::operator()(const NetSchedulerTask* lhs, 
                                                  const NetSchedulerTask* rhs) const
{
    return lhs->getCycle() > rhs->getCycle();
}

void NetTaskScheduler::main()
{
    std::unique_lock<std::mutex> eventLockUnique(_eventLock, std::defer_lock);
    while (getState() != ThreadState::TERMINATED) {
        auto ret = std::cv_status::no_timeout;

        eventLockUnique.lock();
        if (_eventList.empty()) {
            _eventSignal.wait(eventLockUnique);
        } else {
            ret = _eventSignal.wait_until(eventLockUnique, _eventList.top()->getCycle());
        }

        // The mutex is locked again now...
        if (ret == std::cv_status::timeout) {
            // Ok we had a timeout, so there has to be an event we have to execute...
            auto task = _eventList.top();
            _eventList.pop();

            // Check if the event was stopped
            auto it = _eventIds.find(task->getEventId());
            if (it == _eventIds.end()) {
                eventLockUnique.unlock();
                delete task;
                continue;
            }
            _eventIds.erase(it);
            eventLockUnique.unlock();

            task->setDontExpire();
            NetTaskDispatcher::getInstance().addTask(task, true);
        } else {
            eventLockUnique.unlock();
        }
    }
}

u32 NetTaskScheduler::addEvent(NetSchedulerTask* task)
{
    bool doSignal;
    _eventLock.lock();

    if (getState() == ThreadState::RUNNING) {
        // Check if the event has a valid id.
        if (task->getEventId() == 0u) {
            // If not generate one.
            if (++_lastEventId == 0u) {
                _lastEventId = 1u;
            }

            task->setEventId(_lastEventId);
        }

        // Insert the event id in the list of active events.
        _eventIds.insert(task->getEventId());

        // Add the event to the queue.
        _eventList.push(task);

        // If the list was empty or this event is the top in the list we have to signal it.
        doSignal = task == _eventList.top();
    } else {
        _eventLock.unlock();
        delete task;
        return 0u;
    }

    _eventLock.unlock();

    if (doSignal) {
        _eventSignal.notify_one();
    }

    return task->getEventId();
}

bool NetTaskScheduler::stopEvent(u32 eventId)
{
    if (eventId == 0u) {
        return false;
    }

    std::lock_guard<std::mutex> lockClass(_eventLock);

    // Search the event id.
    auto it = _eventIds.find(eventId);
    if (it == _eventIds.end()) {
        return false;
    }

    _eventIds.erase(it);
    return true;
}

void NetTaskScheduler::shutdown()
{
    setState(ThreadState::TERMINATED);
    _eventLock.lock();

    //this list should already be empty
    while (!_eventList.empty()) {
        delete _eventList.top();
        _eventList.pop();
    }

    _eventIds.clear();
    _eventLock.unlock();
    _eventSignal.notify_one();
}

NetTaskScheduler::NetTaskScheduler()
    : _lastEventId(0u)
{

}

} // namespace Jinra