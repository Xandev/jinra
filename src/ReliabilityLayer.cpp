#include "Jinra.h"
#include "NetRandom.h"
#include "ReliabilityLayer.h"
#include "MessageIdentifiers.h"
#include "NetTime.h"

namespace Jinra
{

static const TimeUS MAX_TIME_BETWEEN_PACKETS = 350000u; // 350 milliseconds
static const s32 DEFAULT_HAS_RECEIVED_PACKET_QUEUE_SIZE = 512;
static const TimeUS STARTING_TIME_BETWEEN_PACKETS = MAX_TIME_BETWEEN_PACKETS;

typedef u32 BitstreamLengthEncoding;

struct DatagramHeaderFormat
{
	DatagramSequenceNumberType datagramNumber;

	// Use floats to save bandwidth
	//	float B; // Link capacity
	float AS; // Data arrival rate
	bool isACK;
	bool isNAK;
	bool isPacketPair;
	bool hasBAndAS;
	bool isContinuousSend;
	bool needsBAndAs;
	bool isValid; // To differentiate between what I serialized, and offline data

	static BitSize_t getDataHeaderBitLength() {
		return BYTES_TO_BITS(getDataHeaderByteLength());
	}

	static u32 getDataHeaderByteLength() {
		return 2 + 3 + sizeof(float) * 1;
	}

	void serialize(BitStream *b) const
	{
		b->write(true); // isValid
		if (isACK) {
			b->write(true);
			b->write(hasBAndAS);
			b->alignWriteToByteBoundary();
			if (hasBAndAS)
				b->write(AS);
		} else if (isNAK) {
			b->write(false);
			b->write(true);
		} else {
			b->write(false);
			b->write(false);
			b->write(isPacketPair);
			b->write(isContinuousSend);
			b->write(needsBAndAs);
			b->alignWriteToByteBoundary();
			b->write(datagramNumber);
		}
	}

	void deserialize(BitStream *b)
	{
		b->read(isValid);
		b->read(isACK);
		if (isACK) {
			isNAK = false;
			isPacketPair = false;
			b->read(hasBAndAS);
			b->AlignReadToByteBoundary();
			if (hasBAndAS)
				b->read(AS);
		} else {
			b->read(isNAK);
			if (isNAK) {
				isPacketPair = false;
			} else {
				b->read(isPacketPair);
				b->read(isContinuousSend);
				b->read(needsBAndAs);
				b->AlignReadToByteBoundary();
				b->read(datagramNumber);
			}
		}
	}
};

s32 SplitPacketIndexComp(SplitPacketIndexType const& key, InternalPacket* const& data)
{
	if (key < data->splitPacketIndex)
		return -1;

	if (key == data->splitPacketIndex)
		return 0;

	return 1;
}

ReliabilityLayer::ReliabilityLayer()
	:
#ifdef JINRA_DEBUG
	_timeoutTime(30000u)
#else
	_timeoutTime(10000u)
#endif
{
	initializeVariables();
	datagramHistoryMessagePool.SetPageSize(sizeof(MessageNumberNode) * 128);
	internalPacketPool.SetPageSize(sizeof(InternalPacket)*INTERNAL_PACKET_PAGE_SIZE);
	_refCountedDataPool.SetPageSize(sizeof(InternalPacket::RefCountedData) * 32);
}

ReliabilityLayer::~ReliabilityLayer() {
	freeThreadSafeMemory(); // Free all memory immediately
}

void ReliabilityLayer::reset(bool resetVariables, u32 MTUSize)
{
	freeThreadSafeMemory();
	if (resetVariables) {
		initializeVariables();
		_congestionManager.init(MTUSize - UDP_HEADER_SIZE);
	}
}

void ReliabilityLayer::initializeVariables()
{
	memset(orderedWriteIndex, 0, NUMBER_OF_ORDERED_STREAMS * sizeof(OrderingIndexType));
	memset(sequencedWriteIndex, 0, NUMBER_OF_ORDERED_STREAMS * sizeof(OrderingIndexType));
	memset(orderedReadIndex, 0, NUMBER_OF_ORDERED_STREAMS * sizeof(OrderingIndexType));
	memset(highestSequencedReadIndex, 0, NUMBER_OF_ORDERED_STREAMS * sizeof(OrderingIndexType));
	memset(&_statistics, 0, sizeof _statistics);
	memset(&heapIndexOffsets, 0, sizeof heapIndexOffsets);

	_statistics.connectionStartTime = Time::nowUS();
	splitPacketId = 0;
	sendReliableMessageNumberIndex = 0;
	internalOrderIndex = 0;
	_timeToNextUnreliableCull = 0u;
	unreliableLinkedListHead = nullptr;
	_lastUpdateTime = Time::nowUS();
	_bandwidthExceededStatistic = false;
	unreliableTimeout = 0u;
	_lastBpsClear = 0u;

	// Disable packet pairs.
	_countdownToNextPacketPair = 15;

	deadConnection = false;
	_timeOfLastContinualSend = 0u;

	timeLastDatagramArrived = Time::nowMS();
	_statistics.messagesInResendBuffer = 0u;
	_statistics.bytesInResendBuffer = 0u;

	_receivedPacketsBaseIndex = 0;
	_resetReceivedPackets = true;
	_receivePacketCount = 0u;

	_timeBetweenPackets = STARTING_TIME_BETWEEN_PACKETS;

	_nextSendTime = _lastUpdateTime;

	_unacknowledgedBytes = 0u;
	resendLinkedListHead = nullptr;
	_totalUserDataBytesAcked = 0.0;

	datagramHistoryPopCount = 0;

	initHeapWeights();
	for (u32 i = 0u; i < Packet::NUMBER_OF_PRIORITIES; ++i) {
		_statistics.messageInSendBuffer[i] = 0u;
		_statistics.bytesInSendBuffer[i] = 0.0;
	}

	for (u32 i = 0u; i < RNS_PER_SECOND_METRICS_COUNT; ++i)
		_bpsMetrics[i].reset();
}

void ReliabilityLayer::freeThreadSafeMemory()
{
	clearPacketsAndDatagrams();

	for (u32 i = 0u; i < splitPacketChannelList.size(); ++i) {
		for (u32 j = 0u; j < splitPacketChannelList[i]->splitPacketList.getAllocSize(); ++j) {
			auto internalPacket = splitPacketChannelList[i]->splitPacketList.get(j);
			if (internalPacket != nullptr) {
				freeInternalPacketData(splitPacketChannelList[i]->splitPacketList.get(j));
				releaseToInternalPacketPool(splitPacketChannelList[i]->splitPacketList.get(j));
			}
		}
		delete splitPacketChannelList[i];
	}
	splitPacketChannelList.clear();

	while (outputQueue.size() > 0) {
		auto internalPacket = outputQueue.front();
		outputQueue.pop_front();
		freeInternalPacketData(internalPacket);
		releaseToInternalPacketPool(internalPacket);
	}

	outputQueue.clear();
	// RakNet: outputQueue.allocate(32)

	for (s32 i = 0; i < NUMBER_OF_ORDERED_STREAMS; ++i) {
		for (u32 j = 0u; j < orderingHeaps[i].size(); ++j) {
			freeInternalPacketData(orderingHeaps[i][j]);
			releaseToInternalPacketPool(orderingHeaps[i][j]);
		}
		orderingHeaps[i].clear();
	}

	memset(resendBuffer, 0, sizeof resendBuffer);
	_statistics.messagesInResendBuffer = 0u;
	_statistics.bytesInResendBuffer = 0u;

	if (resendLinkedListHead) {
		auto iter = resendLinkedListHead;

		while (true) {
			if (iter->data)
				freeInternalPacketData(iter);

			auto prev = iter;
			iter = iter->resendNext;
			if (iter == resendLinkedListHead) {
				releaseToInternalPacketPool(prev);
				break;
			}
			releaseToInternalPacketPool(prev);
		}
		resendLinkedListHead = nullptr;
	}
	_unacknowledgedBytes = 0u;

	for (u32 i = 0u; i < outgoingPacketBuffer.size(); ++i) {
		if (outgoingPacketBuffer[i]->data)
			freeInternalPacketData(outgoingPacketBuffer[i]);
		releaseToInternalPacketPool(outgoingPacketBuffer[i]);
	}

	outgoingPacketBuffer.clear();

	unreliableWithAckReceiptHistory.clear();

	_packetsToSendThisUpdate.clear();
	//_packetsToSendThisUpdate.preallocate(512u);
	_packetsToDeallocThisUpdate.clear();
	//_packetsToDeallocThisUpdate.preallocate(512u);
	_packetsToSendThisUpdateDatagramBoundaries.clear();
	//_packetsToSendThisUpdateDatagramBoundaries.preallocate(128u);
	_datagramSizesInBytes.clear();
	//_datagramSizesInBytes.preallocate(128u);

	internalPacketPool.clear();

	_refCountedDataPool.clear();

	while (datagramHistory.size()) {
		removeFromDatagramHistory(datagramHistoryPopCount);
		datagramHistory.pop_front();
		++datagramHistoryPopCount;
	}

	datagramHistoryMessagePool.clear();
	datagramHistoryPopCount = 0;

	_acknowlegements.clear();
	_NAKs.clear();

	unreliableLinkedListHead = nullptr;
}

bool ReliabilityLayer::handleSocketReceiveFromConnectedPlayer(cchar* buffer, u32 length,
															  NetAddress& systemAddress,
															  NetSocket2* s, TimeUS timeRead,
															  BitStream& updateBitStream)
{
#ifdef JINRA_DEBUG
	ASSERT(!(buffer == nullptr));
#endif

	_bpsMetrics[(int)ACTUAL_BYTES_RECEIVED].push(timeRead, length);

	// Length of 1 is a connection request resend that we just ignore
	if (length <= 2 || buffer == nullptr)   
		return true;

	timeLastDatagramArrived = Time::nowMS();

    unsigned i;


	BitStream socketData((u8*)buffer, length, false); // Convert the incoming data to a bitstream for easy parsing

	DatagramHeaderFormat dhf;
	dhf.deserialize(&socketData);
	if (!dhf.isValid)
		return true;

	if (dhf.isACK) {
	    // datagramNumber=dhf.datagramNumber;

#ifdef JINRA_DEBUG
		if (dhf.hasBAndAS == false) {
			//			dhf.B=0;
			dhf.AS = 0;
		}
#endif


		_incomingAcks.clear();
		if (_incomingAcks.Deserialize(&socketData) == false) {

			return false;
		}
		for (i = 0; i<_incomingAcks.ranges.size(); i++) {
			if (_incomingAcks.ranges[i].minIndex>_incomingAcks.ranges[i].maxIndex || _incomingAcks.ranges[i].maxIndex == (uint24_t)0xFFFFFFFF) {
				ASSERT(_incomingAcks.ranges[i].minIndex <= _incomingAcks.ranges[i].maxIndex);


				return false;
			}
			for (DatagramSequenceNumberType datagramNumber = _incomingAcks.ranges[i].minIndex; datagramNumber >= _incomingAcks.ranges[i].minIndex && datagramNumber <= _incomingAcks.ranges[i].maxIndex; ++datagramNumber) {
				TimeUS whenSent;

				if (unreliableWithAckReceiptHistory.size() > 0) {
					u32 k = 0u;
					while (k < unreliableWithAckReceiptHistory.size()) {
						if (unreliableWithAckReceiptHistory[k].datagramNumber == datagramNumber) {
						    auto ackReceipt = allocateFromInternalPacketPool();
							allocInternalPacketData(ackReceipt, 5, false);
							ackReceipt->dataBitLength = BYTES_TO_BITS(5);
							ackReceipt->data[0] = static_cast<MessageID>(ID_SND_RECEIPT_ACKED);
							memcpy(ackReceipt->data + sizeof(MessageID), 
								   &unreliableWithAckReceiptHistory[k].sendReceiptSerial, 
								   sizeof(u32));
							outputQueue.push_back(ackReceipt);

							// Remove, swap with last
							unreliableWithAckReceiptHistory.erase(
								unreliableWithAckReceiptHistory.begin() + k);
						} else
							k++;
					}
				}

			    auto messageNumberNode = GetMessageNumberNodeByDatagramIndex(datagramNumber, &whenSent);
				if (messageNumberNode) {
					TimeUS ping;
					if (timeRead > whenSent)
						ping = timeRead - whenSent;
					else
						ping = 0u;
					_congestionManager.onAck(ping, _bandwidthExceededStatistic, datagramNumber);
					while (messageNumberNode) {

						RemovePacketFromResendListAndDeleteOlderReliableSequenced(messageNumberNode->messageNumber);
						messageNumberNode = messageNumberNode->next;
					}

					removeFromDatagramHistory(datagramNumber);
				}

			}
		}
	} else if (dhf.isNAK) {
	    DataStructures::RangeList<DatagramSequenceNumberType> incomingNAKs;
		if (incomingNAKs.Deserialize(&socketData) == false) {

			return false;
		}
		for (i = 0; i<incomingNAKs.ranges.size(); i++) {
			if (incomingNAKs.ranges[i].minIndex>incomingNAKs.ranges[i].maxIndex) {
				ASSERT(incomingNAKs.ranges[i].minIndex <= incomingNAKs.ranges[i].maxIndex);


				return false;
			}

			for (DatagramSequenceNumberType messageNumber = incomingNAKs.ranges[i].minIndex; messageNumber >= incomingNAKs.ranges[i].minIndex && messageNumber <= incomingNAKs.ranges[i].maxIndex; ++messageNumber) {
				_congestionManager.onNAK();

				TimeUS timeSent;
				MessageNumberNode *messageNumberNode = GetMessageNumberNodeByDatagramIndex(messageNumber, &timeSent);
				while (messageNumberNode) {
					// Update timers so resends occur immediately
				    auto internalPacket = resendBuffer[messageNumberNode->messageNumber & (u32)RESEND_BUFFER_ARRAY_MASK];
					if (internalPacket) {
						if (internalPacket->nextActionTime != 0) {
							internalPacket->nextActionTime = timeRead;
						}
					}

					messageNumberNode = messageNumberNode->next;
				}
			}
		}
	} else {
		u32 skippedMessageCount;
		if (!_congestionManager.onGotPacket(dhf.datagramNumber, timeRead, &skippedMessageCount)) {

			return true;
		}

		DatagramHeaderFormat dhfNAK;
		dhfNAK.isNAK = true;

		for (auto skippedMessageOffset = skippedMessageCount; skippedMessageOffset > 0u;
			 --skippedMessageOffset)
			 _NAKs.insert(dhf.datagramNumber - skippedMessageOffset);

		_remoteSystemNeedsBAndAS = dhf.needsBAndAs;

		// Ack dhf.datagramNumber
		// Ack even unreliable messages for congestion control, just don't resend them on no ack
		SendAcknowledgementPacket(dhf.datagramNumber);

		auto internalPacket = CreateInternalPacketFromBitStream(&socketData, timeRead);
		if (internalPacket == nullptr)
			return true;

		while (internalPacket != nullptr) {

			{

				// resetReceivedPackets is set from a non-threadsafe function.
				// We do the actual reset in this function so the data is not modified by multiple threads
				if (_resetReceivedPackets) {
					_hasReceivedPacketQueue.clearAndForceAllocation(DEFAULT_HAS_RECEIVED_PACKET_QUEUE_SIZE);
					_receivedPacketsBaseIndex = 0;
					_resetReceivedPackets = false;
				}

				// Check for corrupt orderingChannel
				if (
					internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
					internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
					internalPacket->reliability == Packet::RELIABLE_ORDERED
					) {
					if (internalPacket->orderingChannel >= NUMBER_OF_ORDERED_STREAMS) {

						_bpsMetrics[static_cast<int>(USER_MESSAGE_BYTES_RECEIVED_IGNORED)].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));

						freeInternalPacketData(internalPacket);
						releaseToInternalPacketPool(internalPacket);
						goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
					}
				}

				// 8/12/09 was previously not checking if the message was reliable. However, on packetloss this would mean you'd eventually exceed the
				// hole count because unreliable messages were never resent, and you'd stop getting messages
				if (internalPacket->reliability == Packet::RELIABLE || internalPacket->reliability == Packet::RELIABLE_SEQUENCED || internalPacket->reliability == Packet::RELIABLE_ORDERED) {
					// If the following conditional is true then this either a duplicate packet
					// or an older out of order packet
					// The subtraction unsigned overflow is intentional
				    auto holeCount = static_cast<DatagramSequenceNumberType>(internalPacket->reliableMessageNumber - _receivedPacketsBaseIndex);
					const DatagramSequenceNumberType typeRange = static_cast<DatagramSequenceNumberType>(static_cast<const u32>(-1));

					// TESTING1
					// 					printf("waiting on reliableMessageNumber=%i holeCount=%i datagramNumber=%i\n", receivedPacketsBaseIndex.val, holeCount.val, dhf.datagramNumber.val);

					if (holeCount == static_cast<DatagramSequenceNumberType>(0)) {
						// Got what we were expecting
						if (_hasReceivedPacketQueue.size())
							_hasReceivedPacketQueue.pop();
						++_receivedPacketsBaseIndex;
					} else if (holeCount > typeRange / static_cast<DatagramSequenceNumberType>(2)) {
						_bpsMetrics[static_cast<int>(USER_MESSAGE_BYTES_RECEIVED_IGNORED)].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));


						// Duplicate packet
						freeInternalPacketData(internalPacket);
						releaseToInternalPacketPool(internalPacket);

						goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
					} else if (static_cast<u32>(holeCount) < _hasReceivedPacketQueue.size()) {
						// Got a higher count out of order packet that was missing in the sequence or we already got
						if (_hasReceivedPacketQueue[holeCount] != false) // non-zero means this is a hole
						{
							// Fill in the hole
							_hasReceivedPacketQueue[holeCount] = false; // We got the packet at holeCount
						} else {
							_bpsMetrics[(int)USER_MESSAGE_BYTES_RECEIVED_IGNORED].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));

							// Duplicate packet
							freeInternalPacketData(internalPacket);
							releaseToInternalPacketPool(internalPacket);

							goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
						}
					} else // holeCount>=receivedPackets.Size()
					{
						if (holeCount > static_cast<DatagramSequenceNumberType>(1000000)) {
							ASSERT("Hole count too high. See ReliabilityLayer.h" && false);

							_bpsMetrics[(int)USER_MESSAGE_BYTES_RECEIVED_IGNORED].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));

							// Would crash due to out of memory!
							freeInternalPacketData(internalPacket);
							releaseToInternalPacketPool(internalPacket);

							goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
						}

						// Fix - sending on a higher priority gives us a very very high received packets base index if we formerly had pre-split a lot of messages and
						// used that as the message number.  Because of this, a lot of time is spent in this linear loop and the timeout time expires because not
						// all of the message is sent in time.
						// Fixed by late assigning message IDs on the sender

						// Add 0 times to the queue until (reliableMessageNumber - baseIndex) < queue size.
						while (static_cast<u32>(holeCount) > _hasReceivedPacketQueue.size())
							_hasReceivedPacketQueue.push(true); // time+(TimeUS)60 * (TimeUS)1000 * (TimeUS)1000); // Didn't get this packet - set the time to give up waiting
						_hasReceivedPacketQueue.push(false); // Got the packet
#ifdef JINRA_DEBUG
						// If this assert hits then DatagramSequenceNumberType has overflowed
						ASSERT(_hasReceivedPacketQueue.size() < static_cast<u32>(static_cast<DatagramSequenceNumberType>(static_cast<const u32>(-1))));
#endif
					}

					while (_hasReceivedPacketQueue.size() > 0 && _hasReceivedPacketQueue.peek() == false) {
						_hasReceivedPacketQueue.pop();
						++_receivedPacketsBaseIndex;
					}
				}

				// If the allocated buffer is > DEFAULT_HAS_RECEIVED_PACKET_QUEUE_SIZE and it is 3x greater than the number of elements actually being used
				if (_hasReceivedPacketQueue.AllocationSize() > (u32)DEFAULT_HAS_RECEIVED_PACKET_QUEUE_SIZE && _hasReceivedPacketQueue.AllocationSize() > _hasReceivedPacketQueue.size() * 3)
					_hasReceivedPacketQueue.Compress();

				// Is this a split packet? If so then reassemble
				if (internalPacket->splitPacketCount > 0) {
					// Check for a rebuilt packet
					if (internalPacket->reliability != Packet::RELIABLE_ORDERED && internalPacket->reliability != Packet::RELIABLE_SEQUENCED && internalPacket->reliability != Packet::UNRELIABLE_SEQUENCED)
						internalPacket->orderingChannel = 255; // Use 255 to designate not sequenced and not ordered

					InsertIntoSplitPacketList(internalPacket, timeRead);

					internalPacket = BuildPacketFromSplitPacketList(internalPacket->splitPacketId, timeRead,
																	s, systemAddress, updateBitStream);

					if (internalPacket == nullptr) {
						// Don't have all the parts yet
						goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
					}
				}

				if (internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
					internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
					internalPacket->reliability == Packet::RELIABLE_ORDERED) {
					if (internalPacket->orderingIndex == orderedReadIndex[internalPacket->orderingChannel]) {
						// Has current ordering index
						if (internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
							internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED) {
							// Is sequenced
							if (IsOlderOrderedPacket(internalPacket->sequencingIndex, highestSequencedReadIndex[internalPacket->orderingChannel]) == false) {
								// Expected or highest known value

								// Update highest sequence
								// 6/26/2012 - Did not have the +1 in the next statement
								// Means a duplicated RELIABLE_SEQUENCED or UNRELIABLE_SEQUENCED packet would be returned to the user
								highestSequencedReadIndex[internalPacket->orderingChannel] = internalPacket->sequencingIndex + static_cast<OrderingIndexType>(1);

								// Fallthrough, returned to user below
							} else {
								// Lower than highest known value
								freeInternalPacketData(internalPacket);
								releaseToInternalPacketPool(internalPacket);

								goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
							}
						} else {
							// Push to output buffer immediately
							_bpsMetrics[static_cast<s32>(USER_MESSAGE_BYTES_RECEIVED_PROCESSED)].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));
							outputQueue.push_back(internalPacket);

							++orderedReadIndex[internalPacket->orderingChannel];
							highestSequencedReadIndex[internalPacket->orderingChannel] = 0;

							// Return off heap until order lost
							while (orderingHeaps[internalPacket->orderingChannel].size() > 0 &&
								   orderingHeaps[internalPacket->orderingChannel].Peek()->orderingIndex == orderedReadIndex[internalPacket->orderingChannel]) {
								internalPacket = orderingHeaps[internalPacket->orderingChannel].Pop(0);

								_bpsMetrics[(int)USER_MESSAGE_BYTES_RECEIVED_PROCESSED].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));
								outputQueue.push_back(internalPacket);

								if (internalPacket->reliability == Packet::RELIABLE_ORDERED) {
									++orderedReadIndex[internalPacket->orderingChannel];
								} else {
									highestSequencedReadIndex[internalPacket->orderingChannel] = internalPacket->sequencingIndex;
								}
							}

							// Done
							goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
						}
					} else if (IsOlderOrderedPacket(internalPacket->orderingIndex, orderedReadIndex[internalPacket->orderingChannel]) == false) {
						// internalPacket->_orderingIndex is greater
						// If a message has a greater ordering index, and is sequenced or ordered, buffer it
						// Sequenced has a lower heap weight, ordered has max sequenced weight

						// Keep orderedHoleCount count small
						if (orderingHeaps[internalPacket->orderingChannel].size() == 0)
							heapIndexOffsets[internalPacket->orderingChannel] = orderedReadIndex[internalPacket->orderingChannel];

						reliabilityHeapWeightType orderedHoleCount = internalPacket->orderingIndex - heapIndexOffsets[internalPacket->orderingChannel];
						reliabilityHeapWeightType weight = orderedHoleCount * 1048576;
						if (internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
							internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED)
							weight += internalPacket->sequencingIndex;
						else
							weight += 1048576 - 1;
						orderingHeaps[internalPacket->orderingChannel].Push(weight, internalPacket);

						// Buffered, nothing to do
						goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
					} else {
						// Out of order
						freeInternalPacketData(internalPacket);
						releaseToInternalPacketPool(internalPacket);

						// Ignored, nothing to do
						goto CONTINUE_SOCKET_DATA_PARSE_LOOP;
					}

				}

				_bpsMetrics[static_cast<s32>(USER_MESSAGE_BYTES_RECEIVED_PROCESSED)].push(timeRead, BITS_TO_BYTES(internalPacket->dataBitLength));

				// Nothing special about this packet.  Add it to the output queue
				outputQueue.push_back(internalPacket);

				internalPacket = nullptr;
			}

			// Used for a goto to jump to the resendNext packet immediately

		CONTINUE_SOCKET_DATA_PARSE_LOOP:
			// Parse the bitstream to create an internal packet
			internalPacket = CreateInternalPacketFromBitStream(&socketData, timeRead);
		}

	}


	++_receivePacketCount;

	return true;
}

BitSize_t ReliabilityLayer::receive(u8** data)
{
	if (outputQueue.size() > 0u) {
		auto internalPacket = outputQueue.front();
		outputQueue.pop_front();
		*data = internalPacket->data;

		BitSize_t bitLength = internalPacket->dataBitLength;
		releaseToInternalPacketPool(internalPacket);
		return bitLength;
	}
    return 0u;
}

bool ReliabilityLayer::send(char* data, BitSize_t numberOfBitsToSend,
							Packet::Priority priority, Packet::Reliability reliability,
							u8 orderingChannel, bool makeDataCopy, TimeUS currentTime,
							u32 receipt)
{
#if defined(JINRA_DEBUG)
	ASSERT(!(reliability >= Packet::NUMBER_OF_RELIABILITIES || reliability < 0));
	ASSERT(!(priority > Packet::NUMBER_OF_PRIORITIES || priority < 0));
	ASSERT(!(orderingChannel >= NUMBER_OF_ORDERED_STREAMS));
	ASSERT(numberOfBitsToSend > 0);
#endif // defined(JINRA_DEBUG)

	// Fix any bad parameters
	if (reliability > Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT || reliability < 0)
		reliability = Packet::RELIABLE;

	if (priority > Packet::NUMBER_OF_PRIORITIES || priority < 0)
		priority = Packet::HIGH_PRIORITY;

	if (orderingChannel >= NUMBER_OF_ORDERED_STREAMS)
		orderingChannel = 0;

	auto numberOfBytesToSend = static_cast<u32>(BITS_TO_BYTES(numberOfBitsToSend));
	if (numberOfBitsToSend == 0u)
		return false;

	auto internalPacket = allocateFromInternalPacketPool();
	if (internalPacket == nullptr) {
		notifyOutOfMemory(__FILE__, __LINE__);
		return false; // Out of memory
	}

	_bpsMetrics[static_cast<s32>(USER_MESSAGE_BYTES_PUSHED)].push(currentTime, numberOfBytesToSend);

	internalPacket->creationTime = currentTime;

	if (makeDataCopy) {
		allocInternalPacketData(internalPacket, numberOfBytesToSend, true);
		memcpy(internalPacket->data, data, numberOfBytesToSend);
	} else {
		// Allocated the data elsewhere, delete it in here
		//internalPacket->data = ( u8* ) data;
		allocInternalPacketData(internalPacket, reinterpret_cast<u8*>(data));
	}

	internalPacket->dataBitLength = numberOfBitsToSend;
	internalPacket->messageInternalOrder = internalOrderIndex++;
	internalPacket->priority = priority;
	internalPacket->reliability = reliability;
	internalPacket->sendReceiptSerial = receipt;

	// Calculate if I need to split the packet
	//	int headerLength = BITS_TO_BYTES( getMessageHeaderLengthBits( internalPacket, true ) );

	u32 maxDataSizeBytes = getMaxDatagramSizeExcludingMessageHeaderBytes() - BITS_TO_BYTES(GetMaxMessageHeaderLengthBits());

	bool splitPacket = numberOfBytesToSend > maxDataSizeBytes;

	// If a split packet, we might have to upgrade the reliability
	if (splitPacket) {
		// Split packets cannot be unreliable, in case that one part doesn't arrive and the whole cannot be reassembled.
		// One part could not arrive either due to packetloss or due to unreliable discard
		if (internalPacket->reliability == Packet::UNRELIABLE)
			internalPacket->reliability = Packet::RELIABLE;
		else if (internalPacket->reliability == Packet::UNRELIABLE_WITH_ACK_RECEIPT)
			internalPacket->reliability = Packet::RELIABLE_WITH_ACK_RECEIPT;
		else if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED)
			internalPacket->reliability = Packet::RELIABLE_SEQUENCED;
	}

	if (internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED) {
		// Assign the sequence stream and index
		internalPacket->orderingChannel = orderingChannel;
		internalPacket->orderingIndex = orderedWriteIndex[orderingChannel];
		internalPacket->sequencingIndex = sequencedWriteIndex[orderingChannel]++;
	} else if (internalPacket->reliability == Packet::RELIABLE_ORDERED ||
			   internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT) {
		// Assign the ordering channel and index
		internalPacket->orderingChannel = orderingChannel;
		internalPacket->orderingIndex = orderedWriteIndex[orderingChannel] ++;
		sequencedWriteIndex[orderingChannel] = 0;
	}

	if (splitPacket)   // If it uses a secure header it will be generated here
	{
		SplitPacket(internalPacket);
		return true;
	}

	ASSERT(internalPacket->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
	AddToUnreliableLinkedList(internalPacket);

	ASSERT(internalPacket->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
	ASSERT(internalPacket->messageNumberAssigned == false);
	outgoingPacketBuffer.Push(GetNextWeight(internalPacket->priority), internalPacket);
	ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
	_statistics.messageInSendBuffer[static_cast<s32>(internalPacket->priority)]++;
	_statistics.bytesInSendBuffer[static_cast<s32>(internalPacket->priority)] += static_cast<double>(BITS_TO_BYTES(internalPacket->dataBitLength));

	//	sendPacketSet[priority].WriteUnlock();
	return true;
}

void ReliabilityLayer::update(NetSocket2* s, NetAddress& systemAddress, TimeUS time,
							  u32 bitsPerSecondLimit, BitStream &updateBitStream)
{
	// This line is necessary because the timer isn't accurate
	if (time <= _lastUpdateTime) {
		// Always set the last time in case of overflow
		_lastUpdateTime = time;
		return;
	}

	TimeUS timeSinceLastTick = time - _lastUpdateTime;
	_lastUpdateTime = time;
	if (timeSinceLastTick > 100000)
		timeSinceLastTick = 100000;

	if (unreliableTimeout > 0) {
		if (timeSinceLastTick >= _timeToNextUnreliableCull) {
			if (unreliableLinkedListHead) {
				// Cull out all unreliable messages that have exceeded the timeout
			    auto cur = unreliableLinkedListHead;
			    auto end = unreliableLinkedListHead->unreliablePrev;

				while (true) {
					if (time > cur->creationTime + static_cast<TimeUS>(unreliableTimeout)) {
						// Flag invalid, and clear the memory. Still needs to be removed from the sendPacketSet later
						// This fixes a problem where a remote system disconnects, but we don't know it yet, and memory consumption increases to a huge value
						freeInternalPacketData(cur);
						cur->data = nullptr;
					    auto next = cur->unreliableNext;
						removeFromUnreliableLinkedList(cur);

						if (cur == end)
							break;

						cur = next;
					} else {
						// 						if (cur==end)
						// 							break;
						// 
						// 						cur=cur->unreliableNext;

						// They should be inserted in-order, so no need to iterate past the first failure
						break;
					}
				}
			}

			_timeToNextUnreliableCull = unreliableTimeout / static_cast<TimeUS>(2u);
		} else {
			_timeToNextUnreliableCull -= timeSinceLastTick;
		}
	}


	// Due to thread vagarities and the way I store the time to avoid slow calls to Jinra::GetTime
	// time may be less than lastAck
	if (_statistics.messagesInResendBuffer != 0 && getAckTimeout(TimeMS(time / static_cast<TimeUS>(1000u)))) {
		// SHOW - dead connection
		// We've waited a very long time for a reliable packet to get an ack and it never has
		deadConnection = true;
		return;
	}

	if (_congestionManager.shouldSendACKs(time))
		SendACKs(s, systemAddress, time, updateBitStream);

	if (_NAKs.size() > 0) {
		updateBitStream.reset();
		DatagramHeaderFormat dhfNAK;
		dhfNAK.isNAK = true;
		dhfNAK.isACK = false;
		dhfNAK.isPacketPair = false;
		dhfNAK.serialize(&updateBitStream);
		_NAKs.Serialize(&updateBitStream, GetMaxDatagramSizeExcludingMessageHeaderBits(), true);
		SendBitStream(s, systemAddress, &updateBitStream, time);
	}

	DatagramHeaderFormat dhf;
	dhf.needsBAndAs = _congestionManager.isInSlowStart();
	dhf.isContinuousSend = _bandwidthExceededStatistic;
	_bandwidthExceededStatistic = outgoingPacketBuffer.size() > 0;

	const bool hasDataToSendOrResend = !isResendQueueEmpty() || _bandwidthExceededStatistic;
	ASSERT(Packet::NUMBER_OF_PRIORITIES == 4);

	_statistics.BPSLimitByOutgoingBandwidthLimit = BITS_TO_BYTES(bitsPerSecondLimit);
	_statistics.BPSLimitByCongestionControl = 0;

	u32 i;
	if (time > _lastBpsClear +
		100000
		) {
		for (i = 0; i < RNS_PER_SECOND_METRICS_COUNT; ++i) {
			_bpsMetrics[i].clearExpired(time);
		}

		_lastBpsClear = time;
	}

	if (unreliableWithAckReceiptHistory.size() > 0) {
		i = 0;
		while (i < unreliableWithAckReceiptHistory.size()) {
			//if (unreliableWithAckReceiptHistory[i].nextActionTime < time)
			if (time - unreliableWithAckReceiptHistory[i].nextActionTime < static_cast<TimeUS>(-1) / 2) {
			    auto ackReceipt = allocateFromInternalPacketPool();
				allocInternalPacketData(ackReceipt, 5, false);
				ackReceipt->dataBitLength = BYTES_TO_BITS(5);
				ackReceipt->data[0] = static_cast<MessageID>(ID_SND_RECEIPT_LOSS);
				memcpy(ackReceipt->data + sizeof(MessageID), &unreliableWithAckReceiptHistory[i].sendReceiptSerial, sizeof(u32));
				outputQueue.push_back(ackReceipt);

				// Remove, swap with last
				unreliableWithAckReceiptHistory.erase(
					unreliableWithAckReceiptHistory.begin() + i);
			} else
				i++;
		}
	}

	if (hasDataToSendOrResend == true) {
		InternalPacket *internalPacket;
	    BitSize_t nextPacketBitLength;
		dhf.isACK = false;
		dhf.isNAK = false;
		dhf.hasBAndAS = false;
		ResetPacketsAndDatagrams();

		auto transmissionBandwidth = _congestionManager.getTransmissionBandwidth(_unacknowledgedBytes, dhf.isContinuousSend);
		if (_unacknowledgedBytes > 0 || transmissionBandwidth > 0) {
			_statistics.isLimitedByCongestionControl = false;

			_allDatagramSizesSoFar = 0;

			// Keep filling datagrams until we exceed retransmission bandwidth
			while (static_cast<int>(BITS_TO_BYTES(_allDatagramSizesSoFar)) < _unacknowledgedBytes) {
				bool pushedAnything = false;

				// Fill one datagram, then break
				while (!isResendQueueEmpty()) {
					internalPacket = resendLinkedListHead;
					ASSERT(internalPacket->messageNumberAssigned == true);

					//if ( internalPacket->nextActionTime < time )
					if (time - internalPacket->nextActionTime < static_cast<TimeUS>(-1) / 2) {
						nextPacketBitLength = internalPacket->headerLength + internalPacket->dataBitLength;
						if (_datagramSizeSoFar + nextPacketBitLength > GetMaxDatagramSizeExcludingMessageHeaderBits()) {
							// Gathers all PushPackets()
							PushDatagram();
							break;
						}

						popListHead(false);

						_bpsMetrics[static_cast<s32>(USER_MESSAGE_BYTES_RESENT)].push(time, BITS_TO_BYTES(internalPacket->dataBitLength));

						PushPacket(internalPacket, true); // Affects GetNewTransmissionBandwidth()
						internalPacket->timesSent++;
						_congestionManager.onResend();
						internalPacket->retransmissionTime = _congestionManager.getRTOForRetransmission();
						internalPacket->nextActionTime = internalPacket->retransmissionTime + time;

						pushedAnything = true;


						// Put the packet back into the resend list at the correct spot
						// Don't make a copy since I'm reinserting an allocated struct
						insertPacketIntoResendList(internalPacket, false);

					} else {
						// Filled one datagram.
						// If the 2nd and it's time to send a datagram pair, will be marked as a pair
						PushDatagram();
						break;
					}
				}

				if (pushedAnything == false)
					break;
			}
		} else {
			_statistics.isLimitedByCongestionControl = true;
		}

		if ((int)BITS_TO_BYTES(_allDatagramSizesSoFar) < transmissionBandwidth) {
			_allDatagramSizesSoFar = 0;

			// Keep filling datagrams until we exceed transmission bandwidth
			while (
				ResendBufferOverflow() == false &&
				((int)BITS_TO_BYTES(_allDatagramSizesSoFar) < transmissionBandwidth ||
				// This condition means if we want to send a datagram pair, and only have one datagram buffered, exceed bandwidth to add another
				_countdownToNextPacketPair == 0 &&
				_datagramsToSendThisUpdateIsPair.size() == 1)
				) {

			    _statistics.isLimitedByOutgoingBandwidthLimit = bitsPerSecondLimit != 0 && BITS_TO_BYTES(bitsPerSecondLimit) < _bpsMetrics[USER_MESSAGE_BYTES_SENT].getBPS();


				while (outgoingPacketBuffer.size() &&
					   _statistics.isLimitedByOutgoingBandwidthLimit == false) {
					internalPacket = outgoingPacketBuffer.Peek();
					ASSERT(internalPacket->messageNumberAssigned == false);
					ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));

					if (!internalPacket->data) {
						outgoingPacketBuffer.Pop(0);
						ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
						_statistics.messageInSendBuffer[static_cast<s32>(internalPacket->priority)]--;
						_statistics.bytesInSendBuffer[static_cast<s32>(internalPacket->priority)] -= static_cast<double>(BITS_TO_BYTES(internalPacket->dataBitLength));
						releaseToInternalPacketPool(internalPacket);
						continue;
					}

					internalPacket->headerLength = getMessageHeaderLengthBits(internalPacket);
					nextPacketBitLength = internalPacket->headerLength + internalPacket->dataBitLength;
					if (_datagramSizeSoFar + nextPacketBitLength > GetMaxDatagramSizeExcludingMessageHeaderBits()) {
						// Hit MTU. May still push packets if smaller ones exist at a lower priority
						ASSERT(_datagramSizeSoFar != 0);
						ASSERT(internalPacket->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
						break;
					}

					bool isReliable;
					if (internalPacket->reliability == Packet::RELIABLE ||
						internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
						internalPacket->reliability == Packet::RELIABLE_ORDERED ||
						internalPacket->reliability == Packet::RELIABLE_WITH_ACK_RECEIPT ||
						internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
						)
						isReliable = true;
					else
						isReliable = false;

					outgoingPacketBuffer.Pop(0);
					ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
					ASSERT(internalPacket->messageNumberAssigned == false);
					_statistics.messageInSendBuffer[static_cast<int>(internalPacket->priority)]--;
					_statistics.bytesInSendBuffer[static_cast<int>(internalPacket->priority)] -= static_cast<double>(BITS_TO_BYTES(internalPacket->dataBitLength));
					if (isReliable
						/*
						I thought about this and agree that UNRELIABLE_SEQUENCED_WITH_ACK_RECEIPT and RELIABLE_SEQUENCED_WITH_ACK_RECEIPT is not useful unless you also know if the message was discarded.

						The problem is that internally, message numbers are only assigned to reliable messages, because message numbers are only used to discard duplicate message receipt and only reliable messages get sent more than once. However, without message numbers getting assigned and transmitted, there is no way to tell the sender about which messages were discarded. In fact, in looking this over I realized that UNRELIABLE_SEQUENCED_WITH_ACK_RECEIPT introduced a bug, because the remote system assumes all message numbers are used (no holes). With that send type, on packetloss, a permanent hole would have been created which eventually would cause the system to discard all further packets.

						So I have two options. Either do not support ack receipts when sending sequenced, or write complex and major new systems. UNRELIABLE_SEQUENCED_WITH_ACK_RECEIPT would need to send the message ID number on a special channel which allows for non-delivery. And both of them would need to have a special range list to indicate which message numbers were not delivered, so when acks are sent that can be indicated as well. A further problem is that the ack itself can be lost - it is possible that the message can arrive but be discarded, yet the ack is lost. On resend, the resent message would be ignored as duplicate, and you'd never get the discard message either (unless I made a special buffer for that case too).
						*/
						//						||
						// If needs an ack receipt, keep the internal packet around in the list
						//						internalPacket->reliability == UNRELIABLE_WITH_ACK_RECEIPT ||
						//						internalPacket->reliability == UNRELIABLE_SEQUENCED_WITH_ACK_RECEIPT
						) {
						internalPacket->messageNumberAssigned = true;
						internalPacket->reliableMessageNumber = sendReliableMessageNumberIndex;
						internalPacket->retransmissionTime = _congestionManager.getRTOForRetransmission();
						internalPacket->nextActionTime = internalPacket->retransmissionTime + time;

						const TimeUS threshhold = 10000000;
						if (internalPacket->nextActionTime - time > threshhold) {
							//								int a=5;
							ASSERT(time - internalPacket->nextActionTime < threshhold);
						}

						if (resendBuffer[internalPacket->reliableMessageNumber & static_cast<u32>(RESEND_BUFFER_ARRAY_MASK)] != nullptr) {
							//								bool overflow = ResendBufferOverflow();
							ASSERT(0);
						}
						resendBuffer[internalPacket->reliableMessageNumber & static_cast<u32>(RESEND_BUFFER_ARRAY_MASK)] = internalPacket;
						_statistics.messagesInResendBuffer++;
						_statistics.bytesInResendBuffer += BITS_TO_BYTES(internalPacket->dataBitLength);

						insertPacketIntoResendList(internalPacket, isReliable);

						++sendReliableMessageNumberIndex;
					} else if (internalPacket->reliability == Packet::UNRELIABLE_WITH_ACK_RECEIPT) {
						unreliableWithAckReceiptHistory.push_back(UnreliableWithAckReceiptNode(
							_congestionManager.getNextDatagramSequenceNumber() + _packetsToSendThisUpdateDatagramBoundaries.size(),
							internalPacket->sendReceiptSerial,
							_congestionManager.getRTOForRetransmission() + time
							));
					}

					// If isReliable is false, the packet and its contents will be added to a list to be freed in ClearPacketsAndDatagrams
					// However, the internalPacket structure will remain allocated and be in the resendBuffer list if it requires a receipt
					_bpsMetrics[(int)USER_MESSAGE_BYTES_SENT].push(time, BITS_TO_BYTES(internalPacket->dataBitLength));

					PushPacket(internalPacket, isReliable);
					internalPacket->timesSent++;


				    if (ResendBufferOverflow())
						break;
				}

				// No datagrams pushed?
				if (_datagramSizeSoFar == 0)
					break;

				// Filled one datagram.
				// If the 2nd and it's time to send a datagram pair, will be marked as a pair
				PushDatagram();
			}
		}


		for (u32 datagramIndex = 0; datagramIndex < _packetsToSendThisUpdateDatagramBoundaries.size(); ++datagramIndex) {
			if (datagramIndex > 0)
				dhf.isContinuousSend = true;
			MessageNumberNode* messageNumberNode = nullptr;
			dhf.datagramNumber = _congestionManager.getAndIncrementNextDatagramSequenceNumber();
			dhf.isPacketPair = _datagramsToSendThisUpdateIsPair[datagramIndex];

			//printf("%p pushing datagram %i\n", this, dhf.datagramNumber.val);

			bool isSecondOfPacketPair = dhf.isPacketPair && datagramIndex > 0 && _datagramsToSendThisUpdateIsPair[datagramIndex - 1];
			u32 msgIndex, msgTerm;
			if (datagramIndex == 0) {
				msgIndex = 0;
				msgTerm = _packetsToSendThisUpdateDatagramBoundaries[0];
			} else {
				msgIndex = _packetsToSendThisUpdateDatagramBoundaries[datagramIndex - 1];
				msgTerm = _packetsToSendThisUpdateDatagramBoundaries[datagramIndex];
			}

			// More accurate time to reset here
			updateBitStream.reset();
			dhf.serialize(&updateBitStream);

			while (msgIndex < msgTerm) {
				// If reliable or needs receipt
				if (_packetsToSendThisUpdate[msgIndex]->reliability != Packet::UNRELIABLE &&
					_packetsToSendThisUpdate[msgIndex]->reliability != Packet::UNRELIABLE_SEQUENCED
					) {
					if (messageNumberNode == 0) {
						messageNumberNode = addFirstToDatagramHistory(_packetsToSendThisUpdate[msgIndex]->reliableMessageNumber, time);
					} else {
						messageNumberNode = addSubsequentToDatagramHistory(messageNumberNode, _packetsToSendThisUpdate[msgIndex]->reliableMessageNumber);
					}
				}

				ASSERT(updateBitStream.getNumberOfBytesUsed() <= MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
				WriteToBitStreamFromInternalPacket(&updateBitStream, _packetsToSendThisUpdate[msgIndex], time);
				ASSERT(updateBitStream.getNumberOfBytesUsed() <= MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
				msgIndex++;
			}

			if (isSecondOfPacketPair) {
				// Pad to size of first datagram
				ASSERT(updateBitStream.getNumberOfBytesUsed() <= MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
				updateBitStream.PadWithZeroToByteLength(_datagramSizesInBytes[datagramIndex - 1]);
				ASSERT(updateBitStream.getNumberOfBytesUsed() <= MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
			}

			if (messageNumberNode == nullptr) {
				// Unreliable, add dummy node
				addFirstToDatagramHistory(dhf.datagramNumber, time);
			}

			SendBitStream(s, systemAddress, &updateBitStream, time);

			_bandwidthExceededStatistic = outgoingPacketBuffer.size() > 0;

			_timeOfLastContinualSend = _bandwidthExceededStatistic ? time : 0u;
		}

		clearPacketsAndDatagrams();

		// Any data waiting to send after attempting to send, then bandwidth is exceeded
		_bandwidthExceededStatistic = outgoingPacketBuffer.size() > 0;
	}
}

void ReliabilityLayer::SendBitStream(NetSocket2 *s, NetAddress &systemAddress, BitStream *bitStream, TimeUS currentTime)
{
	auto length = static_cast<u32>(bitStream->getNumberOfBytesUsed());

	_bpsMetrics[static_cast<s32>(ACTUAL_BYTES_SENT)].push(currentTime, length);

	ASSERT(length <= _congestionManager.getMTU());

	NetSocket2::SendParameters bsp;
	bsp.data = reinterpret_cast<char*>(bitStream->getData());
	bsp.length = length;
	bsp.systemAddress = systemAddress;
	s->send(&bsp);
}

u32 ReliabilityLayer::RemovePacketFromResendListAndDeleteOlderReliableSequenced(const MessageNumberType messageNumber)
{
	auto internalPacket = resendBuffer[messageNumber & RESEND_BUFFER_ARRAY_MASK];
	// May ask to remove twice, for example resend twice, then second ack
	if (internalPacket && internalPacket->reliableMessageNumber == messageNumber) {
		resendBuffer[messageNumber & RESEND_BUFFER_ARRAY_MASK] = nullptr;

		_statistics.messagesInResendBuffer--;
		_statistics.bytesInResendBuffer -= BITS_TO_BYTES(internalPacket->dataBitLength);

		_totalUserDataBytesAcked += static_cast<double>(BITS_TO_BYTES(internalPacket->headerLength + internalPacket->dataBitLength));

		// Return receipt if asked for
		if (internalPacket->reliability >= Packet::RELIABLE_WITH_ACK_RECEIPT &&
			(internalPacket->splitPacketCount == 0 || internalPacket->splitPacketIndex + 1 == internalPacket->splitPacketCount)
			) {
			InternalPacket *ackReceipt = allocateFromInternalPacketPool();
			allocInternalPacketData(ackReceipt, 5, false);
			ackReceipt->dataBitLength = BYTES_TO_BITS(5);
			ackReceipt->data[0] = static_cast<MessageID>(ID_SND_RECEIPT_ACKED);
			memcpy(ackReceipt->data + sizeof(MessageID), &internalPacket->sendReceiptSerial, sizeof internalPacket->sendReceiptSerial);
			outputQueue.push_back(ackReceipt);
		}

		bool isReliable;
		if (internalPacket->reliability == Packet::RELIABLE ||
			internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
			internalPacket->reliability == Packet::RELIABLE_ORDERED ||
			internalPacket->reliability == Packet::RELIABLE_WITH_ACK_RECEIPT ||
			internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
			)
			isReliable = true;
		else
			isReliable = false;

		removeFromList(internalPacket, isReliable);
		freeInternalPacketData(internalPacket);
		releaseToInternalPacketPool(internalPacket);


		return 0;
	}

    return static_cast<u32>(-1);
}

void ReliabilityLayer::SendAcknowledgementPacket(const DatagramSequenceNumberType messageNumber) 
{
	_acknowlegements.insert(messageNumber);
}

BitSize_t ReliabilityLayer::GetMaxMessageHeaderLengthBits()
{
	InternalPacket ip;
	ip.reliability = Packet::RELIABLE_SEQUENCED;
	ip.splitPacketCount = 1;
	return getMessageHeaderLengthBits(&ip);
}

BitSize_t ReliabilityLayer::getMessageHeaderLengthBits(const InternalPacket* const internalPacket)
{
	//	bitStream->AlignWriteToByteBoundary(); // Potentially unaligned
	//	tempChar=(u8)internalPacket->reliability; bitStream->WriteBits( (const u8 *)&tempChar, 3, true ); // 3 bits to write reliability.
	//	bool hasSplitPacket = internalPacket->splitPacketCount>0; bitStream->Write(hasSplitPacket); // Write 1 bit to indicate if splitPacketCount>0
    BitSize_t bitLength = 8 * 1;

	//	bitStream->AlignWriteToByteBoundary();
	//	Assert(internalPacket->dataBitLength < 65535);
	//	u16 s; s = (u16) internalPacket->dataBitLength; bitStream->WriteAlignedVar16((cchar*)& s);
	bitLength += 8 * 2;

	if (internalPacket->reliability == Packet::RELIABLE ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED ||
		internalPacket->reliability == Packet::RELIABLE_WITH_ACK_RECEIPT ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
		)
		bitLength += 8 * 3; // bitStream->Write(internalPacket->reliableMessageNumber); // Message sequence number
	// bitStream->AlignWriteToByteBoundary(); // Potentially nothing else to write



	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED
		) {
		bitLength += 8 * 3;; // bitStream->Write(internalPacket->_sequencingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED.
	}

	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
		) {
		bitLength += 8 * 3; // bitStream->Write(internalPacket->orderingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED.
		bitLength += 8 * 1; // tempChar=internalPacket->orderingChannel; bitStream->WriteAlignedVar8((cchar*)& tempChar); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED. 5 bits needed, write one byte
	}
	if (internalPacket->splitPacketCount > 0) {
		bitLength += 8 * 4; // bitStream->WriteAlignedVar32((cchar*)& internalPacket->splitPacketCount); Assert(sizeof(SplitPacketIndexType)==4); // Only needed if splitPacketCount>0. 4 bytes
		bitLength += 8 * sizeof(SplitPacketIdType); // bitStream->WriteAlignedVar16((cchar*)& internalPacket->splitPacketId); Assert(sizeof(SplitPacketIdType)==2); // Only needed if splitPacketCount>0.
		bitLength += 8 * 4; // bitStream->WriteAlignedVar32((cchar*)& internalPacket->splitPacketIndex); // Only needed if splitPacketCount>0. 4 bytes
	}

	return bitLength;
}

BitSize_t ReliabilityLayer::WriteToBitStreamFromInternalPacket(BitStream *bitStream, const InternalPacket *const internalPacket, TimeUS curTime)
{
	(void)curTime;

	BitSize_t start = bitStream->getNumberOfBitsUsed();
	u8 tempChar;

	// (Incoming data may be all zeros due to padding)
	bitStream->alignWriteToByteBoundary(); // Potentially unaligned
	if (internalPacket->reliability == Packet::UNRELIABLE_WITH_ACK_RECEIPT)
		tempChar = Packet::UNRELIABLE;
	else if (internalPacket->reliability == Packet::RELIABLE_WITH_ACK_RECEIPT)
		tempChar = Packet::RELIABLE;
	else if (internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT)
		tempChar = Packet::RELIABLE_ORDERED;
	else
		tempChar = static_cast<u8>(internalPacket->reliability);

	bitStream->writeBits(static_cast<const u8*>(&tempChar), 3, true); // 3 bits to write reliability.

	bool hasSplitPacket = internalPacket->splitPacketCount > 0; bitStream->write(hasSplitPacket); // Write 1 bit to indicate if splitPacketCount>0
	bitStream->alignWriteToByteBoundary();
	ASSERT(internalPacket->dataBitLength < 65535);
    u16 s = static_cast<u16>(internalPacket->dataBitLength); bitStream->WriteAlignedVar16(reinterpret_cast<cchar*>(&s));
	if (internalPacket->reliability == Packet::RELIABLE ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED ||
		internalPacket->reliability == Packet::RELIABLE_WITH_ACK_RECEIPT ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
		)
		bitStream->write(internalPacket->reliableMessageNumber); // Used for all reliable types
	bitStream->alignWriteToByteBoundary(); // Potentially nothing else to write

	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED
		) {
		bitStream->write(internalPacket->sequencingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED.
	}

	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
		) {
		bitStream->write(internalPacket->orderingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED.
		tempChar = internalPacket->orderingChannel; bitStream->WriteAlignedVar8(reinterpret_cast<cchar*>(& tempChar)); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED. 5 bits needed, write one byte
	}

	if (internalPacket->splitPacketCount > 0) {
		bitStream->WriteAlignedVar32(reinterpret_cast<cchar*>(& internalPacket->splitPacketCount)); ASSERT(sizeof(SplitPacketIndexType) == 4); // Only needed if splitPacketCount>0. 4 bytes
		bitStream->WriteAlignedVar16(reinterpret_cast<cchar*>(& internalPacket->splitPacketId)); ASSERT(sizeof(SplitPacketIdType) == 2); // Only needed if splitPacketCount>0.
		bitStream->WriteAlignedVar32(reinterpret_cast<cchar*>(& internalPacket->splitPacketIndex)); // Only needed if splitPacketCount>0. 4 bytes
	}

	// Write the actual data.
	bitStream->writeAlignedBytes(static_cast<u8*>(internalPacket->data), BITS_TO_BYTES(internalPacket->dataBitLength));

	return bitStream->getNumberOfBitsUsed() - start;
}

//-------------------------------------------------------------------------------------------------------
// Parse a bitstream and create an internal packet to represent this data
//-------------------------------------------------------------------------------------------------------
InternalPacket* ReliabilityLayer::CreateInternalPacketFromBitStream(BitStream *bitStream, TimeUS time)
{
	InternalPacket* internalPacket;
	u8 tempChar;
	bool hasSplitPacket = false;
	bool readSuccess;

	if (bitStream->getNumberOfUnreadBits() < static_cast<s32>(sizeof internalPacket->reliableMessageNumber) * 8)
		return nullptr; // leftover bits

	internalPacket = allocateFromInternalPacketPool();
	if (internalPacket == nullptr) {
		// Out of memory
		ASSERT(0);
		return nullptr;
	}
	internalPacket->creationTime = time;

	// (Incoming data may be all zeros due to padding)
	bitStream->AlignReadToByteBoundary(); // Potentially unaligned
	bitStream->ReadBits(static_cast<u8*>(&tempChar), 3);
	internalPacket->reliability = static_cast<const Packet::Reliability>(tempChar);
	readSuccess = bitStream->read(hasSplitPacket); // Read 1 bit to indicate if splitPacketCount>0
	bitStream->AlignReadToByteBoundary();
	u16 s; bitStream->ReadAlignedVar16(reinterpret_cast<char*>(&s)); internalPacket->dataBitLength = s; // Length of message (2 bytes)
	if (internalPacket->reliability == Packet::RELIABLE ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED)
		bitStream->read(internalPacket->reliableMessageNumber); // Message sequence number
	else {
		internalPacket->reliableMessageNumber = static_cast<MessageNumberType>(static_cast<const u32>(-1));
	}
	bitStream->AlignReadToByteBoundary(); // Potentially nothing else to Read

	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED
		) {
		bitStream->read(internalPacket->sequencingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED.
	}

	if (internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED ||
		internalPacket->reliability == Packet::RELIABLE_ORDERED_WITH_ACK_RECEIPT
		) {
		bitStream->read(internalPacket->orderingIndex); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED. 4 bytes.
		readSuccess = bitStream->ReadAlignedVar8(reinterpret_cast<char*>(&internalPacket->orderingChannel)); // Used for UNRELIABLE_SEQUENCED, RELIABLE_SEQUENCED, RELIABLE_ORDERED. 5 bits needed, Read one byte
	} else
		internalPacket->orderingChannel = 0;

	if (hasSplitPacket) {
		bitStream->ReadAlignedVar32(reinterpret_cast<char*>(&internalPacket->splitPacketCount)); // Only needed if splitPacketCount>0. 4 bytes
		bitStream->ReadAlignedVar16(reinterpret_cast<char*>(&internalPacket->splitPacketId)); // Only needed if splitPacketCount>0.
		readSuccess = bitStream->ReadAlignedVar32(reinterpret_cast<char*>(&internalPacket->splitPacketIndex)); // Only needed if splitPacketCount>0. 4 bytes
		ASSERT(readSuccess);
	} else {
		internalPacket->splitPacketCount = 0;
	}

	if (readSuccess == false ||
		internalPacket->dataBitLength == 0 ||
		internalPacket->reliability >= Packet::NUMBER_OF_RELIABILITIES ||
		internalPacket->orderingChannel >= 32 ||
		hasSplitPacket && internalPacket->splitPacketIndex >= internalPacket->splitPacketCount) {
		// If this assert hits, encoding is garbage
		ASSERT("Encoding is garbage" && false);
		releaseToInternalPacketPool(internalPacket);
		return nullptr;
	}

	// Allocate memory to hold our data
	allocInternalPacketData(internalPacket, BITS_TO_BYTES(internalPacket->dataBitLength), false);
	ASSERT(BITS_TO_BYTES(internalPacket->dataBitLength) < MAXIMUM_MTU_SIZE);

	if (internalPacket->data == nullptr) {
		ASSERT("Out of memory in ReliabilityLayer::CreateInternalPacketFromBitStream" && false);
		notifyOutOfMemory(__FILE__, __LINE__);
		releaseToInternalPacketPool(internalPacket);
		return nullptr;
	}

	// Set the last byte to 0 so if ReadBits does not read a multiple of 8 the last bits are 0'ed out
	internalPacket->data[BITS_TO_BYTES(internalPacket->dataBitLength) - 1] = 0;

	// Read the data the packet holds
    bool bitStreamSucceeded = bitStream->readAlignedBytes(static_cast<u8*>(internalPacket->data), BITS_TO_BYTES(internalPacket->dataBitLength));

	if (bitStreamSucceeded == false) {
		// If this hits, most likely the variable buff is too small in RunUpdateCycle in NetPeer.cpp
		ASSERT("Couldn't read all the data" && false);

		freeInternalPacketData(internalPacket);
		releaseToInternalPacketPool(internalPacket);
		return nullptr;
	}

	return internalPacket;
}

bool ReliabilityLayer::IsOlderOrderedPacket(OrderingIndexType newPacketOrderingIndex, OrderingIndexType waitingForPacketOrderingIndex)
{
    auto maxRange = static_cast<OrderingIndexType>(static_cast<const u32>(-1));

	if (waitingForPacketOrderingIndex > maxRange / static_cast<OrderingIndexType>(2)) {
		if (newPacketOrderingIndex >= waitingForPacketOrderingIndex - maxRange / static_cast<OrderingIndexType>(2) + static_cast<OrderingIndexType>(1) && newPacketOrderingIndex < waitingForPacketOrderingIndex) {
			return true;
		}
	}

	else
		if (newPacketOrderingIndex >= static_cast<OrderingIndexType>(waitingForPacketOrderingIndex - (static_cast<OrderingIndexType>(maxRange) / static_cast<OrderingIndexType>(2) + static_cast<OrderingIndexType>(1))) ||
			newPacketOrderingIndex < waitingForPacketOrderingIndex) {
			return true;
		}

	// Old packet
	return false;
}

void ReliabilityLayer::SplitPacket(InternalPacket *internalPacket)
{
	// Doing all sizes in bytes in this function so I don't write partial bytes with split packets
	internalPacket->splitPacketCount = 1; // This causes getMessageHeaderLengthBits to account for the split packet header
	u32 headerLength = static_cast<u32>(BITS_TO_BYTES(getMessageHeaderLengthBits(internalPacket)));
	u32 dataByteLength = static_cast<u32>(BITS_TO_BYTES(internalPacket->dataBitLength));
    InternalPacket** internalPacketArray;

	s32 maximumSendBlockBytes = getMaxDatagramSizeExcludingMessageHeaderBytes() - BITS_TO_BYTES(GetMaxMessageHeaderLengthBits());

	// Calculate how many packets we need to create
	internalPacket->splitPacketCount = (dataByteLength - 1) / maximumSendBlockBytes+1;

	bool usedAlloca = false;
#if defined(USE_ALLOCA)
	if (sizeof(InternalPacket*) * internalPacket->splitPacketCount <
		MAX_ALLOCA_STACK_ALLOCATION) {
		internalPacketArray = static_cast<InternalPacket**>(alloca(sizeof(InternalPacket*) * internalPacket->splitPacketCount));
		usedAlloca = true;
	} else
#endif // defined(USE_ALLOCA)
		internalPacketArray = static_cast<InternalPacket**>(malloc(sizeof(InternalPacket*) * internalPacket->splitPacketCount));

	for (s32 i = 0; i < static_cast<s32>(internalPacket->splitPacketCount); ++i) {
		internalPacketArray[i] = allocateFromInternalPacketPool();
		*internalPacketArray[i] = *internalPacket;
		internalPacketArray[i]->messageNumberAssigned = false;

		if (i != 0)
			internalPacket->messageInternalOrder = internalOrderIndex++;
	}

	// This identifies which packet this is in the set
	SplitPacketIndexType splitPacketIndex = 0u;

	InternalPacket::RefCountedData* refCounter = nullptr;

	// Do a loop to send out all the packets
	do {
		s32 byteOffset = splitPacketIndex * maximumSendBlockBytes;
		s32 bytesToSend = dataByteLength - byteOffset;

		if (bytesToSend > maximumSendBlockBytes)
			bytesToSend = maximumSendBlockBytes;

		// Copy over our chunk of data

		allocInternalPacketData(internalPacketArray[splitPacketIndex], &refCounter, internalPacket->data, internalPacket->data + byteOffset);

		if (bytesToSend != maximumSendBlockBytes)
			internalPacketArray[splitPacketIndex]->dataBitLength = internalPacket->dataBitLength - splitPacketIndex * (maximumSendBlockBytes << 3);
		else
			internalPacketArray[splitPacketIndex]->dataBitLength = bytesToSend << 3;

		internalPacketArray[splitPacketIndex]->splitPacketIndex = splitPacketIndex;
		internalPacketArray[splitPacketIndex]->splitPacketId = splitPacketId;
		internalPacketArray[splitPacketIndex]->splitPacketCount = internalPacket->splitPacketCount;
		ASSERT(internalPacketArray[splitPacketIndex]->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
	} while (++splitPacketIndex < internalPacket->splitPacketCount);

	splitPacketId++; // It's ok if this wraps to 0

	//	InternalPacket *workingPacket;

	// Tell the heap we are going to push a list of elements where each element in the list follows the heap order
	ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
	outgoingPacketBuffer.StartSeries();

	// Copy all the new packets into the split packet list
	for (s32 i = 0; i < static_cast<int>(internalPacket->splitPacketCount); ++i) {
		internalPacketArray[i]->headerLength = headerLength;
		ASSERT(internalPacketArray[i]->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
		AddToUnreliableLinkedList(internalPacketArray[i]);
		ASSERT(internalPacketArray[i]->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
		ASSERT(internalPacketArray[i]->messageNumberAssigned == false);
		outgoingPacketBuffer.PushSeries(GetNextWeight(internalPacketArray[i]->priority), internalPacketArray[i]);
		ASSERT(outgoingPacketBuffer.size() == 0 || outgoingPacketBuffer.Peek()->dataBitLength < BYTES_TO_BITS(MAXIMUM_MTU_SIZE));
		_statistics.messageInSendBuffer[static_cast<s32>(internalPacketArray[i]->priority)]++;
		_statistics.bytesInSendBuffer[static_cast<s32>(internalPacketArray[i]->priority)] += static_cast<double>(BITS_TO_BYTES(internalPacketArray[i]->dataBitLength));
	}

	releaseToInternalPacketPool(internalPacket);

	if (usedAlloca == false)
		free(internalPacketArray);
}

void ReliabilityLayer::InsertIntoSplitPacketList(InternalPacket * internalPacket, TimeUS time)
{
	bool objectExists;

	// Find in splitPacketChannelList if a SplitPacketChannel with this splitPacketId was already allocated. If not, allocate and insert the channel into the list.
	auto index = splitPacketChannelList.GetIndexFromKey(internalPacket->splitPacketId, &objectExists);
	if (objectExists == false) {
		auto newChannel = new SplitPacketChannel();
		newChannel->firstPacket = nullptr;
		index = splitPacketChannelList.Insert(internalPacket->splitPacketId, newChannel, true);
		// Preallocate to the final size, to avoid runtime copies
		newChannel->splitPacketList.preallocate(internalPacket);
	}

	// Insert the packet into the SplitPacketChannel
	if (!splitPacketChannelList[index]->splitPacketList.add(internalPacket)) {
		freeInternalPacketData(internalPacket);
		releaseToInternalPacketPool(internalPacket);
		return;
	}
	splitPacketChannelList[index]->lastUpdateTime = time;

	// If the index is 0, then this is the first packet. Record this so it can be returned to the user with download progress
	if (internalPacket->splitPacketIndex == 0u)
		splitPacketChannelList[index]->firstPacket = internalPacket;

	// Return download progress if we have the first packet, the list is not complete, and there are enough packets to justify it
	if (splitMessageProgressInterval &&
		splitPacketChannelList[index]->firstPacket &&
		splitPacketChannelList[index]->splitPacketList.getAddedPacketsCount() != splitPacketChannelList[index]->firstPacket->splitPacketCount &&
		splitPacketChannelList[index]->splitPacketList.getAddedPacketsCount() % splitMessageProgressInterval == 0) {
		// Return ID_DOWNLOAD_PROGRESS
		// Write splitPacketIndex (SplitPacketIndexType)
		// Write splitPacketCount (SplitPacketIndexType)
		// Write byteLength (4)
		// Write data, splitPacketChannelList[index]->splitPacketList[0]->data
		auto progressIndicator = allocateFromInternalPacketPool();
		u32 length = sizeof(MessageID) + sizeof(u32) * 2 + sizeof(u32) + static_cast<u32>(BITS_TO_BYTES(splitPacketChannelList[index]->firstPacket->dataBitLength));
		allocInternalPacketData(progressIndicator, length, false);
		progressIndicator->dataBitLength = BYTES_TO_BITS(length);
		progressIndicator->data[0] = static_cast<MessageID>(ID_DOWNLOAD_PROGRESS);
		u32 temp = splitPacketChannelList[index]->splitPacketList.getAddedPacketsCount();
		memcpy(progressIndicator->data + sizeof(MessageID), &temp, sizeof(u32));
		temp = static_cast<u32>(internalPacket->splitPacketCount);
		memcpy(progressIndicator->data + sizeof(MessageID) + sizeof(u32) * 1, &temp, sizeof(u32));
		temp = static_cast<u32>(BITS_TO_BYTES(splitPacketChannelList[index]->firstPacket->dataBitLength));
		memcpy(progressIndicator->data + sizeof(MessageID) + sizeof(u32) * 2, &temp, sizeof(u32));

		memcpy(progressIndicator->data + sizeof(MessageID) + sizeof(u32) * 3, splitPacketChannelList[index]->firstPacket->data, static_cast<size_t>(BITS_TO_BYTES(splitPacketChannelList[index]->firstPacket->dataBitLength)));
		outputQueue.push_back(progressIndicator);
	}
}

InternalPacket* ReliabilityLayer::BuildPacketFromSplitPacketList(SplitPacketChannel *splitPacketChannel, TimeUS time)
{
	// Reconstruct
	auto internalPacket = CreateInternalPacketCopy(splitPacketChannel->splitPacketList.get(0),
												   0, 0, time);
	internalPacket->dataBitLength = 0;
	for (u32 i = 0u; i < splitPacketChannel->splitPacketList.getAllocSize(); ++i) {
		internalPacket->dataBitLength +=
			splitPacketChannel->splitPacketList.get(i)->dataBitLength;
	}

	internalPacket->data = static_cast<u8*>(malloc(static_cast<size_t>(
		BITS_TO_BYTES(internalPacket->dataBitLength))));
	internalPacket->allocationScheme = InternalPacket::NORMAL;

	BitSize_t offset = 0;
	for (u32 i = 0u; i < splitPacketChannel->splitPacketList.getAllocSize(); ++i) {
		auto splitPacket = splitPacketChannel->splitPacketList.get(i);
		memcpy(internalPacket->data + BITS_TO_BYTES(offset), splitPacket->data,
			   static_cast<size_t>(BITS_TO_BYTES(splitPacket->dataBitLength)));
		offset += splitPacket->dataBitLength;
	}

	for (u32 i = 0u; i < splitPacketChannel->splitPacketList.getAllocSize(); ++i) {
		freeInternalPacketData(splitPacketChannel->splitPacketList.get(i));
		releaseToInternalPacketPool(splitPacketChannel->splitPacketList.get(i));
	}
	delete splitPacketChannel;

	return internalPacket;
}

InternalPacket* ReliabilityLayer::BuildPacketFromSplitPacketList(SplitPacketIdType splitPacketId, TimeUS time,
																 NetSocket2 *s, NetAddress &systemAddress,
																 BitStream &updateBitStream)
{
	// Find in splitPacketChannelList the SplitPacketChannel with this splitPacketId.
	bool objectExists;
	auto i = splitPacketChannelList.GetIndexFromKey(splitPacketId, &objectExists);
	auto splitPacketChannel = splitPacketChannelList[i];

	if (splitPacketChannel->splitPacketList.getAllocSize() == splitPacketChannel->splitPacketList.getAddedPacketsCount()) {
		// Ack immediately, because for large files this can take a long time
		SendACKs(s, systemAddress, time, updateBitStream);
		auto internalPacket = BuildPacketFromSplitPacketList(splitPacketChannel, time);
		splitPacketChannelList.RemoveAtIndex(i);
		return internalPacket;
	}

	return nullptr;
}

InternalPacket* ReliabilityLayer::CreateInternalPacketCopy(InternalPacket *original, int dataByteOffset, int dataByteLength, TimeUS time)
{
	auto copy = allocateFromInternalPacketPool();
#if defined(JINRA_DEBUG)
	// Remove accessing undefined memory error
	memset(copy, 255, sizeof(InternalPacket));
#endif // defined(JINRA_DEBUG)

	// Copy over our chunk of data
	if (dataByteLength > 0) {
		allocInternalPacketData(copy, BITS_TO_BYTES(dataByteLength), false);
		memcpy(copy->data, original->data + dataByteOffset, dataByteLength);
	} else {
		copy->data = nullptr;
	}

	copy->dataBitLength = dataByteLength << 3;
	copy->creationTime = time;
	copy->nextActionTime = 0u;
	copy->orderingIndex = original->orderingIndex;
	copy->sequencingIndex = original->sequencingIndex;
	copy->orderingChannel = original->orderingChannel;
	copy->reliableMessageNumber = original->reliableMessageNumber;
	copy->priority = original->priority;
	copy->reliability = original->reliability;

	return copy;
}

void ReliabilityLayer::insertPacketIntoResendList(InternalPacket* internalPacket, bool modifyUnacknowledgedBytes)
{
	addToListTail(internalPacket, modifyUnacknowledgedBytes);
	ASSERT(internalPacket->nextActionTime != 0u);
}

void ReliabilityLayer::killConnection() 
{
	deadConnection = true;
}

NetStatistics* ReliabilityLayer::getStatistics(NetStatistics* rns)
{
	for (s32 i = 0; i < RNS_PER_SECOND_METRICS_COUNT; ++i) {
		_statistics.valueOverLastSecond[i] = _bpsMetrics[i].getBPS();
		_statistics.runningTotal[i] = _bpsMetrics[i].getTotal();
	}

	memcpy(rns, &_statistics, sizeof _statistics);

	if (rns->valueOverLastSecond[USER_MESSAGE_BYTES_SENT] + rns->valueOverLastSecond[USER_MESSAGE_BYTES_RESENT]>0)
		rns->packetlossLastSecond = static_cast<float>(static_cast<double>(rns->valueOverLastSecond[USER_MESSAGE_BYTES_RESENT]) / (static_cast<double>(rns->valueOverLastSecond[USER_MESSAGE_BYTES_SENT]) + static_cast<double>(rns->valueOverLastSecond[USER_MESSAGE_BYTES_RESENT])));
	else
		rns->packetlossLastSecond = 0.0f;

	rns->packetlossTotal = 0.0f;
    u64 uint64Denominator = rns->runningTotal[USER_MESSAGE_BYTES_SENT] + rns->runningTotal[USER_MESSAGE_BYTES_RESENT];
	if (uint64Denominator != 0 && rns->runningTotal[USER_MESSAGE_BYTES_SENT] / uint64Denominator > 0) {
        double doubleDenominator = static_cast<double>(rns->runningTotal[USER_MESSAGE_BYTES_SENT]) + static_cast<double>(rns->runningTotal[USER_MESSAGE_BYTES_RESENT]);
		if (doubleDenominator != 0) {
			rns->packetlossTotal = static_cast<float>(static_cast<double>(rns->runningTotal[USER_MESSAGE_BYTES_RESENT]) / doubleDenominator);
		}
	}

	rns->isLimitedByCongestionControl = _statistics.isLimitedByCongestionControl;
	rns->BPSLimitByCongestionControl = _statistics.BPSLimitByCongestionControl;
	rns->isLimitedByOutgoingBandwidthLimit = _statistics.isLimitedByOutgoingBandwidthLimit;
	rns->BPSLimitByOutgoingBandwidthLimit = _statistics.BPSLimitByOutgoingBandwidthLimit;

	return rns;
}

bool ReliabilityLayer::getAckTimeout(TimeMS curTime) const
{
	// I check timeLastDatagramArrived-curTime because with threading it is possible that timeLastDatagramArrived is
	// slightly greater than curTime, in which case this is NOT an ack timeout
	return timeLastDatagramArrived - curTime > 10000 && curTime - timeLastDatagramArrived > _timeoutTime;
}

void ReliabilityLayer::ResetPacketsAndDatagrams()
{
	_packetsToSendThisUpdate.clear();
	_packetsToDeallocThisUpdate.clear();
	_packetsToSendThisUpdateDatagramBoundaries.clear();
	_datagramsToSendThisUpdateIsPair.clear();
	_datagramSizesInBytes.clear();
	_datagramSizeSoFar = 0;
}

void ReliabilityLayer::PushPacket(InternalPacket* internalPacket, bool isReliable)
{
	BitSize_t bitsForThisPacket = BYTES_TO_BITS(BITS_TO_BYTES(internalPacket->dataBitLength) + BITS_TO_BYTES(internalPacket->headerLength));
	_datagramSizeSoFar += bitsForThisPacket;
	ASSERT(BITS_TO_BYTES(_datagramSizeSoFar) < MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
	_allDatagramSizesSoFar += bitsForThisPacket;
	_packetsToSendThisUpdate.push_back(internalPacket);
	_packetsToDeallocThisUpdate.push_back(!isReliable);
	ASSERT(internalPacket->headerLength == getMessageHeaderLengthBits(internalPacket));
}

void ReliabilityLayer::PushDatagram()
{
	if (_datagramSizeSoFar < 1)
		return;

	_packetsToSendThisUpdateDatagramBoundaries.push_back(_packetsToSendThisUpdate.size());
	_datagramsToSendThisUpdateIsPair.push_back(false);
	ASSERT(BITS_TO_BYTES(_datagramSizeSoFar) < MAXIMUM_MTU_SIZE - UDP_HEADER_SIZE);
	_datagramSizesInBytes.push_back(BITS_TO_BYTES(_datagramSizeSoFar));
	_datagramSizeSoFar = 0;
}

void ReliabilityLayer::clearPacketsAndDatagrams()
{
	for (u32 i = 0u; i < _packetsToDeallocThisUpdate.size(); ++i) {
		// packetsToDeallocThisUpdate holds a boolean indicating if packetsToSendThisUpdate at 
		// this index should be freed
		if (_packetsToDeallocThisUpdate[i]) {
			removeFromUnreliableLinkedList(_packetsToSendThisUpdate[i]);
			freeInternalPacketData(_packetsToSendThisUpdate[i]);
			releaseToInternalPacketPool(_packetsToSendThisUpdate[i]);
		}
	}
	_packetsToDeallocThisUpdate.clear();
}

void ReliabilityLayer::removeFromList(InternalPacket* internalPacket,
									  bool modifyUnacknowledgedBytes)
{
	internalPacket->resendPrev->resendNext = internalPacket->resendNext;
	internalPacket->resendNext->resendPrev = internalPacket->resendPrev;
	auto newPosition = internalPacket->resendNext;
	if (internalPacket == resendLinkedListHead)
		resendLinkedListHead = newPosition;

	if (resendLinkedListHead == internalPacket)
		resendLinkedListHead = nullptr;

	if (modifyUnacknowledgedBytes) {
		ASSERT(_unacknowledgedBytes >=
			   BITS_TO_BYTES(internalPacket->headerLength + internalPacket->dataBitLength));
		_unacknowledgedBytes -= BITS_TO_BYTES(internalPacket->headerLength +
											  internalPacket->dataBitLength);
	}
}

void ReliabilityLayer::addToListTail(InternalPacket* internalPacket,
									 bool modifyUnacknowledgedBytes)
{
	if (modifyUnacknowledgedBytes) {
		_unacknowledgedBytes += BITS_TO_BYTES(internalPacket->headerLength +
											  internalPacket->dataBitLength);
	}

	if (resendLinkedListHead == nullptr) {
		internalPacket->resendNext = internalPacket;
		internalPacket->resendPrev = internalPacket;
		resendLinkedListHead = internalPacket;
		return;
	}

	internalPacket->resendNext = resendLinkedListHead;
	internalPacket->resendPrev = resendLinkedListHead->resendPrev;
	internalPacket->resendPrev->resendNext = internalPacket;
	resendLinkedListHead->resendPrev = internalPacket;
}

void ReliabilityLayer::popListHead(bool modifyUnacknowledgedBytes)
{
	ASSERT(resendLinkedListHead != nullptr);
	removeFromList(resendLinkedListHead, modifyUnacknowledgedBytes);
}

void ReliabilityLayer::SendACKs(NetSocket2 *s, NetAddress &systemAddress, TimeUS time, BitStream &updateBitStream)
{
	BitSize_t maxDatagramPayload = GetMaxDatagramSizeExcludingMessageHeaderBits();

	while (_acknowlegements.size() > 0) {
		// Send acks
		updateBitStream.reset();
		DatagramHeaderFormat dhf;
		dhf.isACK = true;
		dhf.isNAK = false;
		dhf.isPacketPair = false;
		if (_remoteSystemNeedsBAndAS) {
			dhf.AS = 0.0f;
			dhf.hasBAndAS = false;
        } else {
            dhf.hasBAndAS = false;
        }

		updateBitStream.reset();
		dhf.serialize(&updateBitStream);
		_acknowlegements.Serialize(&updateBitStream, maxDatagramPayload, true);
		SendBitStream(s, systemAddress, &updateBitStream, time);
		_congestionManager.onSendAck();
	}
}

InternalPacket* ReliabilityLayer::allocateFromInternalPacketPool()
{
	auto ip = internalPacketPool.allocate();
	ip->reliableMessageNumber = static_cast<MessageNumberType>(static_cast<const u32>(-1));
	ip->messageNumberAssigned = false;
	ip->nextActionTime = 0;
	ip->splitPacketCount = 0;
	ip->splitPacketIndex = 0;
	ip->splitPacketId = 0;
	ip->allocationScheme = InternalPacket::NORMAL;
	ip->data = nullptr;
	ip->timesSent = 0;
	return ip;
}

void ReliabilityLayer::releaseToInternalPacketPool(InternalPacket* ip) {
	internalPacketPool.release(ip);
}

void ReliabilityLayer::removeFromUnreliableLinkedList(InternalPacket *internalPacket)
{
	if (internalPacket->reliability == Packet::UNRELIABLE ||
		internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::UNRELIABLE_WITH_ACK_RECEIPT) {
		internalPacket->unreliablePrev->unreliableNext = internalPacket->unreliableNext;
		internalPacket->unreliableNext->unreliablePrev = internalPacket->unreliablePrev;
		auto newPosition = internalPacket->unreliableNext;

		if (internalPacket == unreliableLinkedListHead)
			unreliableLinkedListHead = newPosition;

		if (unreliableLinkedListHead == internalPacket)
			unreliableLinkedListHead = nullptr;
	}
}

void ReliabilityLayer::AddToUnreliableLinkedList(InternalPacket *internalPacket)
{
	if (internalPacket->reliability == Packet::UNRELIABLE ||
		internalPacket->reliability == Packet::UNRELIABLE_SEQUENCED ||
		internalPacket->reliability == Packet::UNRELIABLE_WITH_ACK_RECEIPT) {
		if (unreliableLinkedListHead == nullptr) {
			internalPacket->unreliableNext = internalPacket;
			internalPacket->unreliablePrev = internalPacket;
			return;
		}
		internalPacket->unreliableNext = unreliableLinkedListHead;
		internalPacket->unreliablePrev = unreliableLinkedListHead->unreliablePrev;
		internalPacket->unreliablePrev->unreliableNext = internalPacket;
		unreliableLinkedListHead->unreliablePrev = internalPacket;
	}
}

bool ReliabilityLayer::ResendBufferOverflow() const
{
	s32 index1 = sendReliableMessageNumberIndex & static_cast<u32>(RESEND_BUFFER_ARRAY_MASK);
	ASSERT(index1 < RESEND_BUFFER_ARRAY_LENGTH);
	return resendBuffer[index1] != nullptr; // || resendBuffer[index2]!=0;

}

ReliabilityLayer::MessageNumberNode* ReliabilityLayer::GetMessageNumberNodeByDatagramIndex(
    DatagramSequenceNumberType index, TimeUS* timeSent)
{
	if (datagramHistory.empty())
		return nullptr;

	if (_congestionManager.lessThan(index, datagramHistoryPopCount))
		return nullptr;

	DatagramSequenceNumberType offsetIntoList = index - datagramHistoryPopCount;
	if (offsetIntoList >= datagramHistory.size())
		return nullptr;

	*timeSent = datagramHistory[offsetIntoList].timeSent;
	return datagramHistory[offsetIntoList].head;
}

void ReliabilityLayer::removeFromDatagramHistory(DatagramSequenceNumberType index)
{
	DatagramSequenceNumberType offsetIntoList = index - datagramHistoryPopCount;
	auto mnm = datagramHistory[offsetIntoList].head;
	while (mnm) {
		auto next = mnm->next;
		datagramHistoryMessagePool.release(mnm);
		mnm = next;
	}
	datagramHistory[offsetIntoList].head = nullptr;
}

void ReliabilityLayer::addFirstToDatagramHistory(TimeUS timeSent)
{
	if (datagramHistory.size() > DATAGRAM_MESSAGE_ID_ARRAY_LENGTH) {
		removeFromDatagramHistory(datagramHistoryPopCount);
		datagramHistory.pop_front();
		++datagramHistoryPopCount;
	}

	datagramHistory.push_back(DatagramHistoryNode(nullptr, timeSent));
}

ReliabilityLayer::MessageNumberNode* ReliabilityLayer::addFirstToDatagramHistory(DatagramSequenceNumberType messageNumber, TimeUS timeSent)
{
	if (datagramHistory.size() > DATAGRAM_MESSAGE_ID_ARRAY_LENGTH) {
		removeFromDatagramHistory(datagramHistoryPopCount);
		datagramHistory.pop_front();
		++datagramHistoryPopCount;
	}

	auto mnm = datagramHistoryMessagePool.allocate();
	mnm->next = nullptr;
	mnm->messageNumber = messageNumber;
	datagramHistory.push_back(DatagramHistoryNode(mnm, timeSent));
	return mnm;
}

ReliabilityLayer::MessageNumberNode* ReliabilityLayer::addSubsequentToDatagramHistory(MessageNumberNode *messageNumberNode, DatagramSequenceNumberType messageNumber)
{
	messageNumberNode->next = datagramHistoryMessagePool.allocate();
	messageNumberNode->next->messageNumber = messageNumber;
	messageNumberNode->next->next = nullptr;
	return messageNumberNode->next;
}

void ReliabilityLayer::allocInternalPacketData(InternalPacket *internalPacket, InternalPacket::RefCountedData **refCounter, u8 *externallyAllocatedPtr, u8 *ourOffset)
{
	internalPacket->allocationScheme = InternalPacket::REF_COUNTED;
	internalPacket->data = ourOffset;
	if (*refCounter == nullptr) {
		*refCounter = _refCountedDataPool.allocate();
		(*refCounter)->refCount = 1;
		(*refCounter)->sharedDataBlock = externallyAllocatedPtr;
	} else {
		(*refCounter)->refCount++;
	}
	internalPacket->refCountedData = *refCounter;
}

void ReliabilityLayer::allocInternalPacketData(InternalPacket* internalPacket,
											   u8* externallyAllocatedPtr)
{
	internalPacket->allocationScheme = InternalPacket::NORMAL;
	internalPacket->data = externallyAllocatedPtr;
}

void ReliabilityLayer::allocInternalPacketData(InternalPacket* internalPacket, u32 numBytes,
											   bool allowStack)
{
	if (allowStack && numBytes <= sizeof internalPacket->stackData) {
		internalPacket->allocationScheme = InternalPacket::STACK;
		internalPacket->data = internalPacket->stackData;
	} else {
		internalPacket->allocationScheme = InternalPacket::NORMAL;
		internalPacket->data = static_cast<u8*>(malloc(numBytes));
	}
}

void ReliabilityLayer::freeInternalPacketData(InternalPacket* internalPacket)
{
	if (internalPacket == nullptr)
		return;

	if (internalPacket->allocationScheme == InternalPacket::REF_COUNTED) {
		if (internalPacket->refCountedData == nullptr)
			return;

		--internalPacket->refCountedData->refCount;
		if (internalPacket->refCountedData->refCount == 0) {
			free(internalPacket->refCountedData->sharedDataBlock);
			internalPacket->refCountedData->sharedDataBlock = nullptr;
			// delete(internalPacket->refCountedData,file, line);
			_refCountedDataPool.release(internalPacket->refCountedData);
			internalPacket->refCountedData = nullptr;
		}
	} else if (internalPacket->allocationScheme == InternalPacket::NORMAL) {
		if (internalPacket->data == nullptr)
			return;

		free(internalPacket->data);
		internalPacket->data = nullptr;
	} else {
		// Data was on stack
		internalPacket->data = nullptr;
	}
}

u32 ReliabilityLayer::getMaxDatagramSizeExcludingMessageHeaderBytes() const {
	return _congestionManager.getMTU() - DatagramHeaderFormat::getDataHeaderByteLength();
}

void ReliabilityLayer::initHeapWeights()
{
	for (s32 priorityLevel = 0; priorityLevel < Packet::NUMBER_OF_PRIORITIES; ++priorityLevel) {
		outgoingPacketBufferNextWeights[priorityLevel] =
			(1 << priorityLevel) * priorityLevel + priorityLevel;
	}
}

reliabilityHeapWeightType ReliabilityLayer::GetNextWeight(s32 priorityLevel)
{
	u64 next = outgoingPacketBufferNextWeights[priorityLevel];
	if (outgoingPacketBuffer.size() > 0) {
		s32 peekPL = outgoingPacketBuffer.Peek()->priority;
		reliabilityHeapWeightType weight = outgoingPacketBuffer.PeekWeight();
		reliabilityHeapWeightType min = weight - (1 << peekPL)*peekPL + peekPL;
		if (next < min)
			next = min + (1 << priorityLevel)*priorityLevel + priorityLevel;
		outgoingPacketBufferNextWeights[priorityLevel] = next + (1 << priorityLevel)*(priorityLevel + 1) + priorityLevel;
	} else {
		initHeapWeights();
	}
	return next;
}

} // namespace Jinra