#include "Jinra.h"
#include "NetThread.h"

#if defined(_WIN32) && !defined(_WIN32_WCE)
#include <process.h>
#endif

namespace Jinra
{

#if defined(_WIN32)
s32 NetThread::create(unsigned __stdcall startAddress(void*), void* argList, s32 priority)
#else // defined(_WIN32)
s32 NetThread::create(void* startAddress(void*), void* argList, s32 priority)
#endif // defined(_WIN32)
{
#ifdef _WIN32
	auto threadID = 0u;
	auto threadHandle = reinterpret_cast<HANDLE>(_beginthreadex(nullptr, 
		MAX_ALLOCA_STACK_ALLOCATION * 2, startAddress, argList, 0, &threadID));
	
	SetThreadPriority(threadHandle, priority);

	if (threadHandle == nullptr)
		return 1;
	
	CloseHandle(threadHandle);
	return 0;
#else
	pthread_t threadHandle;
	// Create thread linux
	pthread_attr_t attr;
	sched_param param;
	param.sched_priority = priority;
	pthread_attr_init(&attr);
	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setstacksize(&attr, MAX_ALLOCA_STACK_ALLOCATION * 2);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	s32 res = pthread_create(&threadHandle, &attr, startAddress, argList);
	ASSERT(res == 0 && "pthread_create in NetThread.cpp failed.")
	return res;
#endif
}

} // namespace Jinra