#include "Jinra.h"
#include "NetClient.h"
#include "MessageIdentifiers.h"

namespace Jinra
{

NetClient::NetClient()
	: _peer(new NetPeer()), _packet(nullptr)
{ }

NetClient::~NetClient()
{
	shutdown();
}

NetPeer::StartupResult NetClient::initialize(u16 port) const
{
	// Setup socket descriptor.
	SocketDescriptor socketDescriptor;
	socketDescriptor.socketFamily = AF_INET;

	// This port should be unused. If not then we need to discuss about new one.
	socketDescriptor.port = port;

	// Startup peer. 
	auto startupResult = _peer->startup(1, &socketDescriptor, 1);
	if (startupResult != NetPeer::StartupResult::STARTED)
		return startupResult;

	// Only server can communicate with us.
	_peer->setMaximumIncomingConnections(1);

	// Success
	return NetPeer::StartupResult::STARTED;
}

void NetClient::connect(const String& ip, u16 port)
{
    connect(ip, port, "Jinra");
}

void NetClient::connect(const String& ip, u16 port, const String& connectionPassword)
{
	// Only initialized peer can connect to other peer.
	if (_peer == nullptr)
		return;

	// Saves connection address, so we can use it later for reconnection purposes.
	_connectionAddress = NetAddress(ip, port);

	_peer->connect(ip, port, connectionPassword);
}

void NetClient::reconnect(const String& connectionPassword) const
{
	// Only initialized peer can connect to other peer.
	if (_peer == nullptr)
		return;

	_peer->connect(_connectionAddress.toString(false).c_str(), _connectionAddress.getPort(), connectionPassword);
}

void NetClient::receive()
{
    if (!_listener)
        return;
    
	// Loop through all received packets.
    for (_packet = _peer->readMessage(); _packet != nullptr; _packet = _peer->readMessage()) {
		// Create bit stream based on received packet. 
		// It is easier to get ID of packet from packet rather than 
		// bit stream, so we ignore sizeof(MessageID) bytes.
		BitStream bitStream(_packet->data + sizeof(MessageID), _packet->length - sizeof(MessageID), false);

        u8 packetId = _packet->data[0];
        switch (packetId) {
            case ID_CONNECTION_REQUEST_ACCEPTED:
                _listener->onConnect();
                break;
                
            case ID_ALREADY_CONNECTED:
            	_listener->onAlreadyConnected();
            	break;
            	
            case ID_NO_FREE_INCOMING_CONNECTIONS:
            	_listener->onNoFreeIncomingConnections();
            	break;

            case ID_DISCONNECTION_NOTIFICATION:
                _listener->onDisconnect();
                break;

            case ID_CONNECTION_LOST:
                _listener->onLostConnection();
                break;
                
            case ID_CONNECTION_BANNED:
            	_listener->onBanned();
            	break;
            	
            case ID_INVALID_PASSWORD:
            	_listener->onInvalidPassword();
            	break;
            	
            case ID_INCOMPATIBLE_PROTOCOL_VERSION:
            	_listener->onIncompatibleProtocolVersion();
            	break;

            default:
                _listener->handleMessage(packetId, &bitStream);
                break;
        }

		_peer->deallocate(_packet); 
	}
}

void NetClient::send(const BitStream* bitStream, Packet::Priority priority) const
{
	// Sends packet to server. We don't need to check return value, all should be ok.
	_peer->send(bitStream, priority, Packet::RELIABLE_ORDERED, 0, _connectionAddress, false);
}

void NetClient::shutdown() const
{
	// Peer should be initialized and connected to server if we want to shutdown it.
	if (_peer == nullptr || !_peer->isActive())
		return;

	_peer->shutdown(2500u);

	// Wait to end of connection. 
	// This solution is client-only.
	while (_peer->getConnectionState(_connectionAddress) == NetPeer::ConnectionState::IS_CONNECTED);
}

} // namespace Jinra