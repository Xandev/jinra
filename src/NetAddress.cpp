#include "Jinra.h"
#include "NetAddress.h"
#include "SuperFastHash.h"
#include "SocketDefines.h"
#include "NetSocket2.h"
#include "Itoa.h"

namespace Jinra
{

cchar* IPV6_LOOPBACK = "::1";
cchar* IPV4_LOOPBACK = "127.0.0.1";

bool nonNumericHostString(const String& host)
{
	// Return false if IP address. Return true if domain
	u32 i = 0u;
	while (host[i]) {
		// IPV4: natpunch.jenkinssoftware.com
		// IPV6: fe80::7c:31f7:fec4:27de%14
		if (host[i] >= 'g' && host[i] <= 'z' || host[i] >= 'A' && host[i] <= 'Z')
			return true;
		++i;
	}
	return false;
}

NetAddress::NetAddress()
	: debugPort(0), systemIndex(static_cast<SystemIndex>(-1))
{
	memset(&address, 0, sizeof address); 
	address.addr4.sin_family = AF_INET;
}

NetAddress::NetAddress(const String& str)
	: systemIndex(static_cast<SystemIndex>(-1))
{
	address.addr4.sin_family = AF_INET;
	setPortHostOrder(0);
	fromString(str);
}

NetAddress::NetAddress(const String& str, u16 port)
	: systemIndex(static_cast<SystemIndex>(-1))
{
	address.addr4.sin_family = AF_INET;
	fromStringExplicitPort(str, port);
}

void NetAddress::copyPort(const NetAddress& systemAddress)
{
	address.addr4.sin_port = systemAddress.address.addr4.sin_port;
	debugPort = systemAddress.debugPort;
}

bool NetAddress::equalsExcludingPort(const NetAddress& systemAddress) const
{
	return address.addr4.sin_family == AF_INET && 
	       address.addr4.sin_addr.s_addr == systemAddress.address.addr4.sin_addr.s_addr
#if defined(JINRA_SUPPORT_IPV6)
			|| (address.addr4.sin_family == AF_INET6 && 
			memcmp(address.addr6.sin6_addr.s6_addr, 
					systemAddress.address.addr6.sin6_addr.s6_addr, 
					sizeof(address.addr6.sin6_addr.s6_addr)) == 0)
#endif // defined(JINRA_SUPPORT_IPV6)
		;
}

void NetAddress::fixForIPVersion(const NetAddress &boundAddressToSocket)
{
	char str[128];
	toString(false, str);
	if (strcmp(str, IPV6_LOOPBACK) == 0) {
		if (boundAddressToSocket.getIPVersion() == 4)
			fromString(IPV4_LOOPBACK, 0, 4);
	} else if (strcmp(str, IPV4_LOOPBACK) == 0) {
#if defined(JINRA_SUPPORT_IPV6)
		if (boundAddressToSocket.getIPVersion() == 6) 
			fromString(IPV6_LOOPBACK, 0, 6);
#endif // defined(JINRA_SUPPORT_IPV6)
	}
}


bool NetAddress::fromString(const String& str, char portDelineator, s32 ipVersion)
{
	// TODO: This need to be check and formated.

#if defined(JINRA_SUPPORT_IPV6)
	if (str == nullptr) {
		memset(&address, 0, sizeof(address));
		address.addr4.sin_family = AF_INET;
		return true;
	}

#if defined(JINRA_SUPPORT_IPV6)
	char ipPart[INET6_ADDRSTRLEN];
#else
	char ipPart[INET_ADDRSTRLEN];
#endif
	char portPart[32];
	s32 i = 0, j;

	// TODO - what about 255.255.255.255?
	if (ipVersion == 4 && strcmp(str, IPV6_LOOPBACK) == 0) {
		strcpy(ipPart, IPV4_LOOPBACK);
	} else if (ipVersion == 6 && strcmp(str, IPV4_LOOPBACK) == 0) {
		address.addr4.sin_family = AF_INET6;
		strcpy(ipPart, IPV6_LOOPBACK);
	} else if (nonNumericHostString(str) == false) {
		for (; i < sizeof(ipPart) && str[i] != 0 && str[i] != portDelineator; ++i) {
			if ((str[i]<'0' || str[i]>'9') && (str[i]<'a' || str[i]>'f') && (str[i]<'A' || str[i]>'F') && str[i] != '.' && str[i] != ':' && str[i] != '%' && str[i] != '-' && str[i] != '/')
				break;

			ipPart[i] = str[i];
		}
		ipPart[i] = 0;
	} else {
		strncpy(ipPart, str, sizeof(ipPart));
		ipPart[sizeof(ipPart) - 1] = 0;
	}

	j = 0;
	if (str[i] == portDelineator && portDelineator != 0) {
		++i;
		for (; j < sizeof(portPart) && str[i] != 0; ++i, ++j) {
			portPart[j] = str[i];
		}
	}
	portPart[j] = 0;

	// needed for getaddrinfo
	WSAStartupSingleton::initialized();

	// This could be a domain, or a printable address such as "192.0.2.1" or "2001:db8:63b3:1::3490"
	// I want to convert it to its binary representation
	addrinfo hints, *servinfo = 0;
	memset(&hints, 0, sizeof hints);
	hints.ai_socktype = SOCK_DGRAM;
	if (ipVersion == 6)
		hints.ai_family = AF_INET6;
	else if (ipVersion == 4)
		hints.ai_family = AF_INET;
	else
		hints.ai_family = AF_UNSPEC;
	getaddrinfo(ipPart, "", &hints, &servinfo);
	if (servinfo == 0) {
		if (ipVersion == 6) {
			ipVersion = 4;
			hints.ai_family = AF_UNSPEC;
			getaddrinfo(ipPart, "", &hints, &servinfo);
			if (servinfo == 0)
				return false;
		} else
			return false;
	}
	ASSERT(servinfo);

	u16 oldPort = address.addr4.sin_port;
#if defined(JINRA_SUPPORT_IPV6)
	if (servinfo->ai_family == AF_INET) {
		address.addr4.sin_family = AF_INET;
		memcpy(&address.addr4, (struct sockaddr_in *)servinfo->ai_addr, sizeof(struct sockaddr_in));
	} else {
		address.addr4.sin_family = AF_INET6;
		memcpy(&address.addr6, (struct sockaddr_in6 *)servinfo->ai_addr, sizeof(struct sockaddr_in6));
	}
#else
	address.addr4.sin_family = AF_INET4;
	memcpy(&address.addr4, (struct sockaddr_in *)servinfo->ai_addr, sizeof(struct sockaddr_in));
#endif

	freeaddrinfo(servinfo); // free the linked list

	// needed for getaddrinfo
	WSAStartupSingleton::release();

	// PORT
	if (portPart[0]) {
		address.addr4.sin_port = htons((u16)atoi(portPart));
		debugPort = ntohs(address.addr4.sin_port);
	} else {
		address.addr4.sin_port = oldPort;
	}
	return true;
#else // defined(JINRA_SUPPORT_IPV6)
	(void) ipVersion;
	return setBinaryAddress(str.c_str(), portDelineator);
#endif // defined(JINRA_SUPPORT_IPV6)
}

bool NetAddress::fromStringExplicitPort(const String& str, u16 port, s32 ipVersion)
{
	auto result = fromString(str, static_cast<char>(0), ipVersion);
	if (!result) {
		*this = UNASSIGNED_SYSTEM_ADDRESS;
		return false;
	}
	address.addr4.sin_port = htons(port);
	debugPort = ntohs(address.addr4.sin_port);
	return true;
}

ulong NetAddress::toInteger(const NetAddress& systemAddress)
{
	auto lastHash = superFastHashIncremental(
		reinterpret_cast<cchar*>(&systemAddress.address.addr4.sin_port), 
		sizeof systemAddress.address.addr4.sin_port, 
		sizeof systemAddress.address.addr4.sin_port);

#if defined(JINRA_SUPPORT_IPV6)
	if (systemAddress.address.addr4.sin_family == AF_INET) {
		return superFastHashIncremental(
			reinterpret_cast<cchar*>(&systemAddress.address.addr4.sin_addr.s_addr), 
			sizeof(systemAddress.address.addr4.sin_addr.s_addr), lastHash);
	else {
		return superFastHashIncremental(
			reinterpret_cast<cchar*>(&systemAddress.address.addr6.sin6_addr.s6_addr), 
			sizeof(systemAddress.address.addr6.sin6_addr.s6_addr), lastHash);
	}
#else // defined(JINRA_SUPPORT_IPV6)
	return superFastHashIncremental(
		reinterpret_cast<cchar*>(&systemAddress.address.addr4.sin_addr.s_addr), 
		sizeof systemAddress.address.addr4.sin_addr.s_addr, lastHash);
#endif // defined(JINRA_SUPPORT_IPV6)
}

String NetAddress::toString(bool writePort, char portDelineator) const
{
	char buffer[25];
	memset(buffer, 0, 25);
	toString(writePort, buffer, portDelineator);
	return String(buffer);
}

void NetAddress::toString(bool writePort, char* dest, char portDelineator) const
{
#if defined(JINRA_SUPPORT_IPV6)
	toStringNew(writePort, dest, portDelineator);
#else
	toStringOld(writePort, dest, portDelineator);
#endif // defined(JINRA_SUPPORT_IPV6)
}

bool NetAddress::isLoopback() const
{
	if (getIPVersion() == 4) {
		if (htonl(address.addr4.sin_addr.s_addr) == 2130706433)
			return true;

		if (address.addr4.sin_addr.s_addr == 0)
			return true;
	}
#if defined(JINRA_SUPPORT_IPV6)
	else {
		const static char localhost[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
		if (memcmp(&address.addr6.sin6_addr, localhost, 16) == 0)
			return true;
	}
#endif
	return false;
}

void NetAddress::setToLoopback(u8 ipVersion)
{
	if (ipVersion == 4)	fromString(IPV4_LOOPBACK, 0, ipVersion);
	else				fromString(IPV6_LOOPBACK, 0, ipVersion);
}

bool NetAddress::setBinaryAddress(cchar* str, char portDelineator)
{
	if (nonNumericHostString(str)) {
#if defined(_WIN32)
		if (_strnicmp(str, "localhost", 9) == 0)
#else // defined(_WIN32)
		if (strncasecmp(str, "localhost", 9) == 0)
#endif // defined(_WIN32)
		{
			address.addr4.sin_addr.s_addr = inet_addr("127.0.0.1");

			if (str[9])
				setPortHostOrder(static_cast<u16>(atoi(str + 9)));
			return true;
		}

		char ip[65];
		ip[0] = 0;
		NetSocket2::domainNameToIP(str, ip);
		if (ip[0]) {
			address.addr4.sin_addr.s_addr = inet_addr(ip);
		} else {
			*this = UNASSIGNED_SYSTEM_ADDRESS;
			return false;
		}
	} else {
		// Split the string into the first part, and the : part
		s32 index;
		char IPPart[22];
		char portPart[10];
		for (index = 0; str[index] && str[index] != portDelineator && index < 22; ++index) {
			if (str[index] != '.' && (str[index] < '0' || str[index] > '9'))
				break;
			IPPart[index] = str[index];
		}

		IPPart[index] = 0;
		portPart[0] = 0;
		if (str[index] && str[index + 1]) {
			++index;
			s32 portIndex = 0;
			for (; portIndex < 10 && str[index] && index < 22 + 10; ++index, ++portIndex) {
				if (str[index] < '0' || str[index] > '9')
					break;
				portPart[portIndex] = str[index];
			}
			portPart[portIndex] = 0;
		}

		if (IPPart[0])
			address.addr4.sin_addr.s_addr = inet_addr(IPPart);

		if (portPart[0]) {
			address.addr4.sin_port = htons(static_cast<u16>(atoi(portPart)));
			debugPort = ntohs(address.addr4.sin_port);
		}
	}
	return true;
}

void NetAddress::setPortHostOrder(u16 port)
{
	address.addr4.sin_port = htons(port);
	debugPort = port;
}

void NetAddress::setPortNetworkOrder(u16 port)
{
	address.addr4.sin_port = port;
	debugPort = ntohs(port);
}

NetAddress& NetAddress::operator=(const NetAddress& systemAddress)
{
	memcpy(&address, &systemAddress.address, sizeof address);
	systemIndex = systemAddress.systemIndex;
	debugPort = systemAddress.debugPort;
	return *this;
}

bool NetAddress::operator>(const NetAddress& systemAddress) const
{
	if (address.addr4.sin_port == systemAddress.address.addr4.sin_port) {
#if defined(JINRA_SUPPORT_IPV6)
		if (address.addr4.sin_family == AF_INET)
			return address.addr4.sin_addr.s_addr > systemAddress.address.addr4.sin_addr.s_addr;
		return memcmp(address.addr6.sin6_addr.s6_addr, systemAddress.address.addr6.sin6_addr.s6_addr, sizeof(address.addr6.sin6_addr.s6_addr)) > 0;
#else // defined(JINRA_SUPPORT_IPV6)
		return address.addr4.sin_addr.s_addr > systemAddress.address.addr4.sin_addr.s_addr;
#endif // defined(JINRA_SUPPORT_IPV6)
	}
	return address.addr4.sin_port > systemAddress.address.addr4.sin_port;
}

bool NetAddress::operator<(const NetAddress& systemAddress) const
{
	if (address.addr4.sin_port == systemAddress.address.addr4.sin_port) {
#if defined(JINRA_SUPPORT_IPV6)
		if (address.addr4.sin_family == AF_INET) {
			return (address.addr4.sin_addr.s_addr < 
					systemAddress.address.addr4.sin_addr.s_addr);
		}
		return (memcmp(address.addr6.sin6_addr.s6_addr, 
						systemAddress.address.addr6.sin6_addr.s6_addr, 
						sizeof(address.addr6.sin6_addr.s6_addr)) > 0);
#else // defined(JINRA_SUPPORT_IPV6)
		return address.addr4.sin_addr.s_addr < systemAddress.address.addr4.sin_addr.s_addr;
#endif // defined(JINRA_SUPPORT_IPV6)
	}
	return address.addr4.sin_port < systemAddress.address.addr4.sin_port;
}

void NetAddress::toStringOld(bool writePort, char* dest, char portDelineator) const
{
	if (*this == UNASSIGNED_SYSTEM_ADDRESS) {
		strcpy(dest, "UNASSIGNED_SYSTEM_ADDRESS");
		return;
	}

	char portStr[2] = {portDelineator, 0};

	in_addr in;
	in.s_addr = address.addr4.sin_addr.s_addr;
	cchar* ntoaStr = inet_ntoa(in);
	strcpy(dest, ntoaStr);
	if (writePort) {
		strcat(dest, portStr);
		itoa(getPort(), dest + strlen(dest), 10);
	}

}

#if defined(JINRA_SUPPORT_IPV6)
void NetAddress::toStringNew(bool writePort, char* dest, char portDelineator) const
{
	s32 ret;

	if (*this == UNASSIGNED_SYSTEM_ADDRESS) {
		strcpy(dest, "UNASSIGNED_SYSTEM_ADDRESS");
		return;
	}

	if (address.addr4.sin_family == AF_INET) {
		ret = getnameinfo(static_cast<struct sockaddr*>(&address.addr4), 
						  sizeof(struct sockaddr_in), dest, 22, nullptr, 0, NI_NUMERICHOST);
	} else {
		ret = getnameinfo(static_cast<struct sockaddr*>(&address.addr6), 
						  sizeof(struct sockaddr_in6), dest, INET6_ADDRSTRLEN, nullptr, 
						  0, NI_NUMERICHOST);
	}

	if (ret != 0)
		dest[0] = 0;

	if (writePort) {
		u8 ch[2] = {portDelineator, 0};
		strcat(dest, static_cast<cchar*>(ch));
		itoa(ntohs(address.addr4.sin_port), dest + strlen(dest), 10);
	}

}
#endif // defined(JINRA_SUPPORT_IPV6)

} // namespace Jinra