#include "Jinra.h"
#include "AddressOrGUID.h"
#include "Packet.h"

namespace Jinra
{

AddressOrGUID::AddressOrGUID(const AddressOrGUID& input)
	: address(input.address), guid(input.guid)
{ }

AddressOrGUID::AddressOrGUID(const NetAddress& input)
	: address(input), guid(UNASSIGNED_NET_GUID)
{ }

AddressOrGUID::AddressOrGUID(Packet* packet)
	: address(packet->systemAddress), guid(packet->guid)
{ }

AddressOrGUID::AddressOrGUID(const NetGUID& input)
	: address(UNASSIGNED_SYSTEM_ADDRESS), guid(input)
{ }

cchar* AddressOrGUID::toString(bool writePort) const {
	return guid != UNASSIGNED_NET_GUID ? guid.toString() : address.toString(writePort).c_str();
}

void AddressOrGUID::toString(bool writePort, char* dest) const
{
	if (guid != UNASSIGNED_NET_GUID)
		return guid.toString(dest);
	return address.toString(writePort, dest);
}

ulong AddressOrGUID::toInteger(const AddressOrGUID& aog)
{
	if (aog.guid != UNASSIGNED_NET_GUID)
		return NetGUID::toUint32(aog.guid);
	return NetAddress::toInteger(aog.address);
}

void AddressOrGUID::setUndefined()
{
	address = UNASSIGNED_SYSTEM_ADDRESS;
	guid = UNASSIGNED_NET_GUID; 
}

AddressOrGUID& AddressOrGUID::operator=(const AddressOrGUID& input)
{
	guid = input.guid;
	address = input.address;
	return *this;
}

AddressOrGUID& AddressOrGUID::operator=(const NetAddress& input)
{
	guid = UNASSIGNED_NET_GUID;
	address = input;
	return *this;
}

AddressOrGUID& AddressOrGUID::operator=(const NetGUID& input)
{
	guid = input;
	address = UNASSIGNED_SYSTEM_ADDRESS;
	return *this;
}

} // namespace Jinra