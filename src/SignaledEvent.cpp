#include "Jinra.h"
#include "SignaledEvent.h"

namespace Jinra
{

SignaledEvent::SignaledEvent()
{
#if defined(_WIN32)
	_eventList = INVALID_HANDLE_VALUE;
#else // defined(_WIN32)
	_isSignaled = false;
#endif // defined(_WIN32)
}

void SignaledEvent::initEvent()
{
#if defined(_WIN32)
	_eventList = CreateEvent(nullptr, false, false, nullptr);
#else // defined(_WIN32)

#if !defined(ANDROID)
	pthread_condattr_init(&_condAttr);
	pthread_cond_init(&_eventList, &_condAttr);
#else // !defined(ANDROID)
	pthread_cond_init(&_eventList, 0);
#endif // !defined(ANDROID)

	pthread_mutexattr_init(&_mutexAttr);
	pthread_mutex_init(&_hMutex, &_mutexAttr);
#endif
}

void SignaledEvent::closeEvent()
{
#if defined(_WIN32)
	if (_eventList != INVALID_HANDLE_VALUE) {
		CloseHandle(_eventList);
		_eventList = INVALID_HANDLE_VALUE;
	}
#else // defined(_WIN32)
	pthread_cond_destroy(&_eventList);
	pthread_mutex_destroy(&_hMutex);

#if !defined(ANDROID)
	pthread_condattr_destroy(&_condAttr);
#endif // !defined(ANDROID)

	pthread_mutexattr_destroy(&_mutexAttr);
#endif // defined(_WIN32)
}

void SignaledEvent::waitOnEvent(s32 timeoutMS)
{
#if defined(_WIN32)
	WaitForSingleObjectEx(_eventList, timeoutMS, 0);
#else // defined(_WIN32)
	// If was previously set signaled, just unset and return
	_isSignaledMutex.lock();
	if (_isSignaled) {
		_isSignaled = false;
		_isSignaledMutex.unlock();
		return;
	}
	_isSignaledMutex.unlock();

	struct timespec   ts;

	struct timeval tp;
	gettimeofday(&tp, nullptr);
	ts.tv_sec = tp.tv_sec;
	ts.tv_nsec = tp.tv_usec * 1000;

	while (timeoutMS > 30) {
		// Wait 30 milliseconds for the signal, then check again.
		// This is in case we  missed the signal between the top of this 
		// function and pthread_cond_timedwait, or after the end of the 
		// loop and pthread_cond_timedwait
		ts.tv_nsec += 30 * 1000000;
		if (ts.tv_nsec >= 1000000000) {
			ts.tv_nsec -= 1000000000;
			++ts.tv_sec;
		}

		// [SBC] added mutex lock/unlock around cond_timedwait.
		// this prevents airplay from generating a whole much of errors.
		// not sure how this works on other platforms since according to
		// the docs you are suppost to hold the lock before you wait
		// on the cond.
		pthread_mutex_lock(&_hMutex);
		pthread_cond_timedwait(&_eventList, &_hMutex, &ts);
		pthread_mutex_unlock(&_hMutex);

		timeoutMS -= 30;

		_isSignaledMutex.lock();
		if (_isSignaled) {
			_isSignaled = false;
			_isSignaledMutex.unlock();
			return;
		}
		_isSignaledMutex.unlock();
	}

	// Wait the remaining time, and turn off the signal in case it was set
	ts.tv_nsec += timeoutMS * 1000000;
	if (ts.tv_nsec >= 1000000000) {
		ts.tv_nsec -= 1000000000;
		++ts.tv_sec;
	}

	pthread_mutex_lock(&_hMutex);
	pthread_cond_timedwait(&_eventList, &_hMutex, &ts);
	pthread_mutex_unlock(&_hMutex);

	_isSignaledMutex.lock();
	_isSignaled = false;
	_isSignaledMutex.unlock();
#endif // defined(_WIN32)
}

void SignaledEvent::setEvent()
{
#if defined(_WIN32)
	SetEvent(_eventList);
#else // defined(_WIN32)
	// Different from SetEvent which stays signaled.
	// We have to record manually that the event was signaled
	_isSignaledMutex.lock();
	_isSignaled = true;
	_isSignaledMutex.unlock();

	// Unblock waiting threads
	pthread_cond_broadcast(&_eventList);
#endif // defined(_WIN32)
}

} // namespace Jinra