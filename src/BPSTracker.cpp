#include "Jinra.h"
#include "BPSTracker.h"

namespace Jinra
{

BPSTracker::TimeAndValue::TimeAndValue(TimeUS time, u64 value) 
	: value(value), time(time) 
{ }

BPSTracker::BPSTracker() { 
	reset(); 
}

void BPSTracker::push(TimeUS time, u64 value) 
{
	_dataQueue.push_back(TimeAndValue(time, value));
	_total += value;
	_lastSec += value;
}

void BPSTracker::reset() 
{ 
	_total = _lastSec = 0u; 
	_dataQueue.clear(); 
}

void BPSTracker::clearExpired(TimeUS time)
{
	while (!_dataQueue.empty() && _dataQueue.front().time + 1000000 < time) {
		_lastSec -= _dataQueue.front().value;
		_dataQueue.pop_front();
	}
}

} // namespace Jinra