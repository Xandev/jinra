﻿using System;
using System.IO;
using Jinra;

namespace Sample
{
    class Client : Jinra.NetClient
    {
        protected override void handlePacket(Jinra.Packet packet, Jinra.BitStream bitStream)
        {
            Console.WriteLine("Packet ID: {0}", packet.data[0]);
        }
    }

    class Program
    {
        static Client clientPeer;
        static void Main(string[] args)
        {
            if (!File.Exists("Jinra.dll"))
            {
                Console.WriteLine("Jinra.dll not found!");
                Console.Read();
                return;
            }

            clientPeer = new Client();
            clientPeer.initialize(60000);
            clientPeer.connect("86.111.112.99", 42069);
            int i = 0;
            while (true)
            {
                clientPeer.receive();
                if (i == 0 && clientPeer.getPeer().getConnectionState(clientPeer.getPeer().GetSystemAddressFromIndex(0)) == NetPeer.ConnectionState.IS_CONNECTED)
                {
                    var b = new BitStream();
                    b.write((byte)200);
                    long n = (long) Time.nowMS();
                    b.write(n);

                    Console.WriteLine($"Czas systemowy: {n} {b.getNumberOfBytesUsed()}");
                    clientPeer.send(b, Packet.Priority.IMMEDIATE_PRIORITY);
                    Console.WriteLine($"Ostatni odebrany ping: {clientPeer.getPeer().GetLastPing(clientPeer.getPeer().GetSystemAddressFromIndex(0))}");
                    i = 1;
                }
            }

            clientPeer = null;


            Console.Read();
        }
    }
}
