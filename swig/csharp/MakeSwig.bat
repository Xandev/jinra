@echo off

echo Clearing and initializing folders
if exist "Output" rmdir /s /q "Output"
mkdir "Output"
mkdir "Output\cpp"
mkdir "Output\cs"

echo Performing Swig build
swig -c++ -csharp -namespace Jinra -I"../../include" -I"../../src" -outdir "Output\cs" -o "Output\cpp\JinraWrap.cpp" "InterfaceFiles\Jinra.i"
if errorlevel 1 goto :SWIGERROR
echo Swig build complete

echo Copying *.cs files to Sample
rmdir /s /q "Sample\Sample\Jinra\"
mkdir "Sample\Sample\Jinra\"
copy /Y "Output\cs\*" "Sample\Sample\Jinra\*"
if errorlevel 1 goto :ERROR

:ERROR
echo Swig build complete
goto END
:SWIGERROR
echo Swig had an error during build
:END