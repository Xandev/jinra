//--------------------------------Extends-----------------------------------
//This file extends classes with new C++ code, note that not all things are possible because
//while typically it acts like the code is inserted into the class, it isn't really
//The extends are generated in the wrapper
//An example of a problem is accessing private variables and functions.
//Also instead of just calling the function in the class you use a pointer called self that is a pointer to the object

%extend Jinra::BitStream
{
	inline const char * CSharpStringReader(const char * inString)
	{
		self->read((char *)inString);
		return inString;
	}

	inline bool CSharpByteReader(unsigned char* inOutByteArray,unsigned int numberOfBytes)
	{
		return self->read((char *)inOutByteArray,numberOfBytes);
	}

	void write( unsigned char* inputByteArray, const unsigned int numberOfBytes )
	{
		self->write((const char*)inputByteArray,numberOfBytes);
	}

	BitSize_t CSharpCopyDataHelper(unsigned char* inOutByteArray)
	{
		BitSize_t returnVal;
		returnVal=self->getNumberOfBitsAllocated();
		memcpy(inOutByteArray, self->getData(), sizeof(unsigned char) * (size_t) ( BITS_TO_BYTES( returnVal ) ) );
		return returnVal;
	}
}

%extend Jinra::NetPeer
{
    uint32_t send( unsigned char *inByteArray, const int length, Packet::Priority priority, Packet::Reliability reliability, char orderingChannel, const AddressOrGUID systemIdentifier, bool broadcast )
	{
		return self->send((const char*)inByteArray,length,priority,reliability,orderingChannel,systemIdentifier,broadcast);

	}
        void SendLoopback( unsigned char *inByteArray, const int length )
	{
		self->sendLoopback( (const char *)inByteArray, length );

	}
	void SetOfflinePingResponse( unsigned char *inByteArray, const unsigned int length )
	{
		self->SetOfflinePingResponse((const char *) inByteArray,length);

	}
	bool AdvertiseSystem( const char *host, unsigned short remotePort, unsigned  char *inByteArray, int dataLength, unsigned connectionSocketIndex=0 )
	{
		return self->AdvertiseSystem(host,remotePort,(const char *) inByteArray,dataLength,connectionSocketIndex);

	}

	void CSharpGetOfflinePingResponseHelper( unsigned char *inOutByteArray, unsigned int *outLength )
	{
		char * tmp=(char *)inOutByteArray;
		self->GetOfflinePingResponse(&tmp,outLength);
		memcpy(inOutByteArray,tmp,(size_t)*outLength);
	}

	bool getConnectionList( std::vector<NetAddress> * remoteSystems, unsigned short *numberOfSystems ) const
	{
		NetAddress inSys[256];
		bool returnVal = self->getConnectionList(inSys,numberOfSystems);
		if(remoteSystems!=NULL)
		{
			for (int i=0;i<*numberOfSystems;i++)
			{
				remoteSystems->push_back(inSys[i]);
			}
		}
		return returnVal;
	}

}

%define STRUCT_UNSIGNED_CHAR_ARRAY_EXTEND(IN_FUNCTION_NAME,IN_DATA_NAME,LENGTH_MEMBER_VAR)
	void IN_FUNCTION_NAME (unsigned char * inByteArray,int numBytes)
	{
		if (self->IN_DATA_NAME!=NULL)
		{
			free(self->IN_DATA_NAME);
		}
		//create new with size
		self->IN_DATA_NAME=(unsigned char*) malloc(numBytes);
		//copy
		memcpy( self->IN_DATA_NAME,inByteArray, numBytes );
		self->LENGTH_MEMBER_VAR=numBytes;
	}
%enddef

%extend Jinra::Packet
{
	Packet()
	{
		Packet * newPacket = new Packet();
		newPacket->data=NULL;
		return newPacket;
	}
	STRUCT_CUSTOM_UNSIGNED_CHAR_ARRAY_TYPEMAP(dataIsCached,dataCache,unsigned char * data,SetPacketData,Packet_data_get,Packet,length)
	STRUCT_UNSIGNED_CHAR_ARRAY_EXTEND(SetPacketData,data,length)
}

//Removed from interface, commented rather than removed in case needed later
/*
%extend InternalPacket
{
	STRUCT_CUSTOM_UNSIGNED_CHAR_ARRAY_TYPEMAP(dataIsCached,dataCache,unsigned char * data,SetInternalPacketData,InternalPacket_data_get,InternalPacket,dataBitLength/8)
	STRUCT_UNSIGNED_CHAR_ARRAY_EXTEND(SetInternalPacketData,data)
}*/

%define STRUCT_UNSIGNED_INT_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(IN_FUNCTION_NAME,IN_DATA_NAME)
	void IN_FUNCTION_NAME (unsigned int * inUnsignedIntArray,int numInts)
	{
	for (int i=0;i<numInts;i++)
	{
		if (i>=Packet::Priority::NUMBER_OF_PRIORITIES)
		{break;}
		self->IN_DATA_NAME[i]=inUnsignedIntArray[i];
	}
	}	
%enddef

%define STRUCT_UNSIGNED_INT64_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(IN_FUNCTION_NAME,IN_DATA_NAME)
	void IN_FUNCTION_NAME (unsigned long long int  * inUint64Array,int numUint64)
	{
	for (int i=0;i<numUint64;i++)
	{
		if (i>=RNS_PER_SECOND_METRICS_COUNT)
		{break;}
		self->IN_DATA_NAME[i]=inUint64Array[i];
	}
	}
%enddef

%define STRUCT_DOUBLE_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(IN_FUNCTION_NAME,IN_DATA_NAME)
	void IN_FUNCTION_NAME (double * inDoubleArray,int numDoubles)
	{
	for (int i=0;i<numDoubles;i++)
	{
		if (i>=Packet::Priority::NUMBER_OF_PRIORITIES)
		{break;}
		self->IN_DATA_NAME[i]=inDoubleArray[i];
	}
	}	
%enddef

%extend Jinra::NetStatistics
{
STRUCT_DOUBLE_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(SetBytesInSendBuffer,bytesInSendBuffer);
STRUCT_UNSIGNED_INT_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(SetMessageInSendBuffer,messageInSendBuffer);
STRUCT_UNSIGNED_INT64_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(SetRunningTotal,runningTotal);
STRUCT_UNSIGNED_INT64_ARRAY_EXTEND_SPECIAL_NETSTATISTICS(SetValueOverLastSecond,valueOverLastSecond);
}

%define STRUCT_CHAR_TO_BYTE_ARRAY_TYPEMAP_INSIDE_EXTEND(BOOLNAME,CACHENAME,IN_DATA_CHANGE_FUNCTION,IN_DATA_GET_FUNCTION,IN_CLASS,IN_LEN_ATTRIBUTE,IN_DATA_NAME)
	%typemap(imtype, out="IntPtr") char * IN_DATA_NAME "IntPtr"

	STRUCT_CUSTOM_GENERAL_ARRAY_TYPEMAP(BOOLNAME,CACHENAME,char * IN_DATA_NAME,byte,byte,IN_DATA_CHANGE_FUNCTION,IN_DATA_GET_FUNCTION,IN_CLASS,IN_LEN_ATTRIBUTE)

	void IN_DATA_CHANGE_FUNCTION (unsigned char * inByteArray,int numBytes)
	{
		if (self->IN_DATA_NAME!=NULL)
		{
			free(self->IN_DATA_NAME);
		}	
	
		//create new with size
		self->IN_DATA_NAME=(char *) malloc(numBytes);
		//copy
		memcpy( self->IN_DATA_NAME,inByteArray, numBytes );
		self->IN_LEN_ATTRIBUTE=numBytes;
	}
%enddef