// BitStream
%ignore Jinra::BitStream::write(unsigned char * const inTemplateVar);
%ignore Jinra::BitStream::write(const unsigned char * const inTemplateVar);
%ignore Jinra::BitStream::write( const char* inputByteArray, const unsigned int numberOfBytes );
%ignore Jinra::BitStream::read(char *varString);
%ignore Jinra::BitStream::read(unsigned char *varString);
%ignore Jinra::BitStream::read( char* output, const unsigned int numberOfBytes );
%ignore Jinra::BitStream::copyData(unsigned char** _data ) const;
%ignore Jinra::BitStream::getData;
%ignore Jinra::BitStream::read(BitStream &bitStream, BitSize_t numberOfBits);
%ignore Jinra::BitStream::read(BitStream &bitStream );
%ignore Jinra::BitStream::write(BitStream &bitStream, BitSize_t numberOfBits);
%ignore Jinra::BitStream::write(BitStream &bitStream);

//NetPeer
%ignore Jinra::NetPeer::GetOfflinePingResponse( char **data, unsigned int *length );
%ignore Jinra::NetPeer::SendOutOfBand(const char *host, unsigned short remotePort, MessageID header, const char *data, BitSize_t dataLength, unsigned connectionSocketIndex=0 );
%ignore Jinra::NetPeer::SetUserUpdateThread(void (*_userUpdateThreadPtr)(NetPeerInterface *, void *), void *_userUpdateThreadData);
%ignore Jinra::NetPeer::SendList;
%ignore Jinra::NetPeer::ReleaseSockets;
%ignore Jinra::NetPeer::GetSocket( const NetAddress target );
%ignore Jinra::NetPeer::GetSockets( Jinra::DataStructures::List<JinraSmartPtr<JinraSocket> > &sockets );
%ignore Jinra::NetPeer::ConnectWithSocket;
%ignore Jinra::NetPeer::SetRouterInterface;
%ignore Jinra::NetPeer::RemoveRouterInterface;
%ignore Jinra::NetPeer::getConnectionList( NetAddress *remoteSystems, unsigned short *numberOfSystems ) const;
%ignore Jinra::NetPeer::SetIncomingDatagramEventHandler( bool (*_incomingDatagramEventHandler)(RNS2RecvStruct *) );
%ignore Jinra::NetPeer::RemoteSystemStruct;

//Swig doesn't know how to handle friend functions, so even if it is in the protected section 
//They must be explicitly ignored
//This Specific case is somehow placed in the Jinra namespace rather than Jinra::NetPeer
//Ignore both to be safe
%ignore Jinra::NetPeer::getStatisticsList;
%ignore Jinra::NetPeer::processOfflineNetworkPacket;
%ignore Jinra::NetPeer::processNetworkPacket;
%ignore Jinra::processOfflineNetworkPacket;
%ignore Jinra::processNetworkPacket;

//NetAddress
%ignore Jinra::NetAddress::toString(bool writePort, char *dest) const;
%ignore Jinra::NetAddress::toString() const;

// NetGUID
%ignore Jinra::NetGUID::toString(char *dest) const;

// AddressOrGUID
%ignore Jinra::AddressOrGUID::toString(bool writePort, char *dest) const;
%ignore Jinra::AddressOrGUID::toString() const;

// NetSocket
%ignore Jinra::NetSocket::recvEvent;
%ignore Jinra::NetSocket::Fcntl;

//Uin24_t
%ignore Jinra::uint24_t::operator++(int);
%ignore Jinra::uint24_t::operator--(int);

//Global
%ignore REGISTER_STATIC_RPC;
%ignore CLASS_MEMBER_ID;
%ignore REGISTER_CLASS_MEMBER_RPC;
%ignore UNREGISTER_STATIC_RPC;
%ignore UNREGISTER_CLASS_MEMBER_RPC;

//Operators
//These need te be handled manually or not at all
%ignore operator const char*;
%ignore operator uint32_t;
%ignore operator &; //Not overloadable in C#
%ignore operator <<;//Doesn't work the same in C#, only usable with int
%ignore operator >>;//Doesn't work the same in C#, only usable with int

// X= is automatically handled in C# if you overload = and X, you can't specify an overload
%ignore operator +=;
%ignore operator -=;
%ignore operator /=;

// Global
%ignore statisticsToString; //Custom C# wrapper written for it