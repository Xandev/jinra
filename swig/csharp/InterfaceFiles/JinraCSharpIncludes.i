%include "Jinra.h"
%include "NetTypedefs.h"
%include "NetTime.h"
%include "uint24_t.h"
%include "NetGUID.h"
%include "NetAddress.h"
%include "AddressOrGUID.h"
%include "Packet.h"
%include "NetSocket2.h"
%include "BitStream.h"
%include "MessageIdentifiers.h"
%include "NetDefines.h"
%include "NetStatistics.h"
%include "SocketDescriptor.h"
%include "NetPeer.h"
%include "NetServer.h"
%include "NetClient.h"