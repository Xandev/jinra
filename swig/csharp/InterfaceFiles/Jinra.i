%module(directors="1") Jinra

%feature("director") NetServer;
%feature("director") NetClient;
 
%include "JinraCppIncludes.i"

// Swig's interfaces
%include "cpointer.i"
%include "arrays_csharp.i"
%include "typemaps.i"
%include "carrays.i"
%include "stdint.i"
%include "std_string.i"

%include "JinraPreprocesors.i"
%include "JinraIgnores.i"
%include "JinraTypeMaps.i"
%include "JinraMethodModifiers.i"
%include "JinraRenames.i"
%include "JinraExtends.i"

//What these two functions are for, is to get around the string in/out problem I return the string
//These helper functions are hidden from the user, the user sees the original in/out api
//This is the C++ code insert
%{
char* statisticsToStringHelper(Jinra::NetStatistics* s, char* buffer, int verbosityLevel)
{
	statisticsToString(s, buffer, verbosityLevel);
	return buffer;
}
%}

//This code is for Swig parsing
char* statisticsToStringHelper(Jinra::NetStatistics* s, char* buffer, int verbosityLevel);

%include "JinraCSharpIncludes.i"
using namespace Jinra;

%include "JinraTemplateDefines.i"
