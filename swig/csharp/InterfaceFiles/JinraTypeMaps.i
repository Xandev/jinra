//------------------------------TypeMaps--------------------------
//Swig typemaps help to either convert a C++ type into a C# type or tell it which types are the same
//Swig typemaps also can modify the postprocessed C++ file as well as the postprocessed C# files
//For example the cscode typemap is used extensively to write C# code into the postprocessed file

//In the below section INOUT is used when the array needs to be passed in and results need copied out
//INPUT is used for just a copy in
//OUTPUT is used just for a copy out
//Any pointers or references will need a typemap to be used in the original api form, otherwise it
//Will show up as SWIGTYPE_p_int

//NOTE: The typmaps are commented for the first class they are specifically made for, that doesn't mean other classes do not depend on the typemaps, once a typemap is defined it applies to all instances

//Allow array of strings
CSHARP_ARRAYS(char *,string)

//INOUT char to byte array
//This is used because RakNet uses char arrays as byte arrays, C# char arrays are unicode
%apply unsigned char INOUT[] {unsigned char* inputByteArray};
%apply unsigned char INOUT[] {unsigned char* inByteArray};
%apply unsigned char INOUT[] {unsigned char* inByteArray2};
%apply unsigned char INOUT[] {unsigned char* inOutByteArray};
%apply unsigned char INOUT[] {unsigned char* packetData};
%apply unsigned char INOUT[] {unsigned char* inOutData};

%typemap(ctype)   unsigned char *inByteArrayArray "unsigned char*"
%typemap(cstype)  unsigned char *inByteArrayArray  "byte[,]"
%typemap(imtype, inattributes="[In, MarshalAs(UnmanagedType.LPArray)]") unsigned char *inByteArrayArray  "byte[,]"
%typemap(csin)    unsigned char *inByteArrayArray  "$csinput"

%typemap(in)     unsigned char *inByteArrayArray  "$1 = $input;"
%typemap(freearg) unsigned char *inByteArrayArray  ""
%typemap(argout)  unsigned char *inByteArrayArray  ""

%apply int INOUT[] {int *lengths};

//Other inout - RakPeer
%apply unsigned int INOUT[] {unsigned int outputFrequencyTable[ 256 ]};
%apply unsigned int INOUT[] {unsigned int inputFrequencyTable[ 256 ]};
%apply unsigned short * INOUT {unsigned short *numberOfSystems };

//For the read functions, out vars
%apply long long &OUTPUT {long long &outTemplateVar};
%apply long &OUTPUT {long &outTemplateVar};
%apply short &OUTPUT {short &outTemplateVar};
%apply bool &OUTPUT {bool &outTemplateVar};
%apply float &OUTPUT {float &outTemplateVar};
%apply float &OUTPUT {float &outFloat};
%apply unsigned char &OUTPUT {unsigned char &outTemplateVar};

//RakPeer out
%apply unsigned int *OUTPUT {unsigned int *outLength};

//NetStatistics
%apply unsigned int OUTPUT[] {unsigned int *inUnsignedIntArray};
%apply unsigned long long OUTPUT[] {unsigned long long  *inUint64Array};
//%apply ulong OUTPUT[] {uint64_t *inUint64Array};
%apply double OUTPUT[] {double *inDoubleArray};

//Inouts for serialize and such
%apply long long &INOUT {long long &inOutTemplateVar};
%apply long &INOUT {long &inOutTemplateVar};
%apply short &INOUT {short &inOutTemplateVar};
%apply unsigned short &INOUT {unsigned short &inOutTemplateVar};
%apply bool &INOUT {bool &inOutTemplateVar};
%apply float &INOUT {float &inOutTemplateVar};
%apply float &INOUT {float &inOutFloat};
%apply unsigned char &INOUT {unsigned char &inOutTemplateVar};

%apply long long &INOUT {long long &inOutCurrentValue};
%apply long &INOUT {long &inOutCurrentValue};
%apply short &INOUT {short &inOutCurrentValue};
%apply unsigned short &INOUT {unsigned short &inOutCurrentValue};
%apply bool &INOUT {bool &inOutCurrentValue};
%apply float &INOUT {float &inOutCurrentValue};
%apply float &INOUT {float &inOutCurrentValue};
%apply unsigned char &INOUT {unsigned char &inOutCurrentValue};

//This is more efficient than the loop version but less compatible
/*
Vars:
BOOLNAME- The name of the variable that keeps track of if it is cached, typically varIsCached
CACHENAME- The name of the variable C# cache typically varCache
CTYPE- The type in C of the var
INCSTYPE- C# type
INTERMEDIATETYPE- Type that is worked with in the function 
IN_DATA_CHANGE_FUNCTION- The name of the set function, Typically SetVar
IN_DATA_GET_FUNCTION- The get function for that data in the pinvoke, typically Class_var_get
IN_CLASS- The class we are working with 
IN_LEN_METHOD- Something to get the length of the C/C++ array for marshalling. This is called from C#, not C.
*/
%define STRUCT_CUSTOM_GENERAL_ARRAY_TYPEMAP(BOOLNAME,CACHENAME,CTYPE,INCSTYPE,INTERMEDIATETYPE,IN_DATA_CHANGE_FUNCTION,IN_DATA_GET_FUNCTION,IN_CLASS,IN_LEN_METHOD)
%typemap(cstype, out="INCSTYPE[]") CTYPE "INCSTYPE[]"

%typemap(csvarin, excode=SWIGEXCODE2) CTYPE 
%{
	set 
	{
	    	CACHENAME=value;
		BOOLNAME = true;
		IN_DATA_CHANGE_FUNCTION (value, value.Length);    
	}
%}
%typemap(csvarout, excode=SWIGEXCODE2) CTYPE  
%{
        get
        {
            INCSTYPE[] returnArray;
            if (!BOOLNAME)
            {
                IntPtr cPtr = JinraPINVOKE.IN_DATA_GET_FUNCTION (swigCPtr);
                int len = (int) IN_LEN_METHOD;
		if (len<=0)
		{
			return null;
		}
                returnArray = new INCSTYPE[len];
                INTERMEDIATETYPE[] marshalArray = new INTERMEDIATETYPE[len];
                Marshal.Copy(cPtr, marshalArray, 0, len);
                marshalArray.CopyTo(returnArray, 0);
                CACHENAME = returnArray;
                BOOLNAME = true;
            }
            else
            {
                returnArray = CACHENAME;
            }
            return returnArray;
        }
 %}
%enddef

//More compatible version of the above macro
//One new variable CONVERSION_MODIFIER that converts the type in the marshalarray to output array
//this can be a function or a typecast if a function leave out the parentheses
%define STRUCT_CUSTOM_GENERAL_ARRAY_TYPEMAP_LOOP_COPY(BOOLNAME,CACHENAME,CTYPE,INCSTYPE,INTERMEDIATETYPE,IN_DATA_CHANGE_FUNCTION,IN_DATA_GET_FUNCTION,IN_CLASS,IN_LEN_METHOD,CONVERSION_MODIFIER)
%typemap(cstype, out="INCSTYPE[]") CTYPE "INCSTYPE[]"

%typemap(csvarin, excode=SWIGEXCODE2) CTYPE 
%{
	set 
	{
	    	CACHENAME=value;
		BOOLNAME = true;
		IN_DATA_CHANGE_FUNCTION (value, value.Length);    
	}
%}
%typemap(csvarout, excode=SWIGEXCODE2) CTYPE  
%{
        get
        {
            INCSTYPE[] returnArray;
            if (!BOOLNAME)
            {
                IntPtr cPtr = JinraPINVOKE.IN_DATA_GET_FUNCTION (swigCPtr);
                int len = (int) IN_LEN_METHOD;
		if (len<=0)
		{
			return null;
		}
                returnArray = new INCSTYPE[len];
                INTERMEDIATETYPE[] marshalArray = new INTERMEDIATETYPE[len];
                Marshal.Copy(cPtr, marshalArray, 0, len);
                for (int i=0;i<len;i++)
                {
                    returnArray[i]= CONVERSION_MODIFIER ( marshalArray[i] );
                }
                CACHENAME = returnArray;
                BOOLNAME = true;
            }
            else
            {
                returnArray = CACHENAME;
            }
            return returnArray;
        }
 %}
%enddef

namespace Jinra
{
STRUCT_CUSTOM_GENERAL_ARRAY_TYPEMAP_LOOP_COPY(runningTotalIsCached,runningTotalCache,unsigned long long runningTotal [ RNS_PER_SECOND_METRICS_COUNT ],unsigned long,long,SetRunningTotal,NetStatistics_runningTotal_get,Jinra::NetStatistics,RNSPerSecondMetrics.RNS_PER_SECOND_METRICS_COUNT,(unsigned long));
STRUCT_CUSTOM_GENERAL_ARRAY_TYPEMAP_LOOP_COPY(valueOverLastSecondIsCached,valueOverLastSecondCache,unsigned long long valueOverLastSecond [ RNS_PER_SECOND_METRICS_COUNT ],unsigned long,long,SetValueOverLastSecond,NetStatistics_valueOverLastSecond_get,Jinra::NetStatistics,RNSPerSecondMetrics.RNS_PER_SECOND_METRICS_COUNT,(unsigned long));
}

//GetpntrTypemaps
//Many of these typemaps are required to copy the
//cached or changed data back into C++ when the pointer is passed into a C++ function
//Think of it as a type of cache writeback

%typemap(csimports) Jinra::Packet
%{
using System;
using System.Runtime.InteropServices;
%}

%typemap(csbody) Jinra::Packet 
%{
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Packet(IntPtr cPtr, bool cMemoryOwn) 
  {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(Packet obj)
  {
     if (obj != null)
     {
        if (obj.dataIsCached)
        {
           obj.SetPacketData(obj.data, obj.data.Length); //If an individual index was modified we need to copy the data before passing to C++
        }
	obj.dataIsCached=false;
     }
     return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }
%}

%typemap(csimports) Jinra::NetStatistics
%{
using System;
using System.Runtime.InteropServices;
%}

//Extra C# code
//These typemaps add C# code to the postprocessed file, mostly to improve the user interface so it matches the original
//API for things Swig doesn't support

//This adds to the main RakNet class, generally used for globals
	%pragma(csharp) modulecode=
	%{ 
	    public static readonly NetAddress UNASSIGNED_SYSTEM_ADDRESS = new NetAddress();
	    public static readonly NetGUID UNASSIGNED_RAKNET_GUID = new NetGUID(ulong.MaxValue);

	    public static void statisticsToString(NetStatistics s, out string buffer, int verbosityLevel) 
 	   {
		System.String tmp = new System.String('c', 9999);
		buffer=statisticsToStringHelper(s,tmp,verbosityLevel);
 	   }
	%}

//BitStream
%typemap(csimports) Jinra::BitStream
%{
using System;
%}

%typemap(cscode) Jinra::BitStream 
%{
  //String reading using original api, but converted to c# logic
  public bool read(out string varString) 
  {
      String tmp = new String('c', (int)getNumberOfUnreadBits()/8);
      varString = CSharpStringReader(tmp);
      return varString!="";
  }

  //Byte read out
  public bool read(byte[] inOutByteArray,uint numberOfBytes) 
  {
      return CSharpByteReader(inOutByteArray,numberOfBytes);      
  }

  public bool read(out char outTemplateVar)
  {
   	byte tmp;
	bool returnVal=read(out tmp);
	outTemplateVar=(char)tmp;
	return returnVal;
  }

  public uint copyData(out byte[] outByteArray)
  {
	byte[] tmp= new byte[getNumberOfBitsAllocated()/8];
	uint byteNum = CSharpCopyDataHelper(tmp);
	outByteArray=tmp;
	return byteNum;
  }

  public byte[] getData()
  {
	byte[] tmp= new byte[getNumberOfBitsAllocated()/8];
	CSharpCopyDataHelper(tmp);
	return tmp;
  }

%}

%typemap(cscode) Jinra::NetPeer 
%{

	public void GetOfflinePingResponse( byte[] inOutByteArray, out uint length )
	{
		CSharpGetOfflinePingResponseHelper(inOutByteArray,out length);
	}

%}

%typemap(csimports) Jinra::NetAddress
%{
using System;
using System.Runtime.InteropServices;
#pragma warning disable 0660
%}

%typemap(cscode) Jinra::NetAddress
%{

	public override int GetHashCode()
	{    
		return (int) toInteger(this);
	}
	public static bool operator ==(NetAddress a, NetAddress b)
	{
 	   	// If both are null, or both are same instance, return true.
 		if (System.Object.ReferenceEquals(a, b))
 		{
 	       		return true;
 	   	}

  		// If one is null, but not both, return false.
   	 	if (((object)a == null) || ((object)b == null))
    		{
       		 	return false;
    		}

		    return a.Equals(b);//Equals should be overloaded as well
	}

	public static bool operator !=(NetAddress a, NetAddress b)
	{
   		 return a.OpNotEqual(b);
	}

	public static bool operator < (NetAddress a, NetAddress b)
	{
    		return a.OpLess(b);
	}

	public static bool operator >(NetAddress a, NetAddress b)
	{
		return a.OpGreater(b);
	}

	public static bool operator <=(NetAddress a, NetAddress b)
	{
		return (a.OpLess(b) || a==b);
	}

	public static bool operator >=(NetAddress a, NetAddress b)
	{
		return (a.OpGreater(b) || a==b);
	}

	public override string ToString()
	{
		return toString(true);
	}

	public void toString(bool writePort,out string dest)
	{
		dest=toString(writePort);
	}
%}

%typemap(csimports) Jinra::NetGUID
%{
using System;
using System.Runtime.InteropServices;
#pragma warning disable 0660
%}


%typemap(cscode) Jinra::NetGUID 
%{

	public override int GetHashCode()
	{
		 return (int) toUint32(this);
	}

	public static bool operator ==(NetGUID a, NetGUID b)
	{
 	   	// If both are null, or both are same instance, return true.
 		if (System.Object.ReferenceEquals(a, b))
 		{
 	       		return true;
 	   	}

  		// If one is null, but not both, return false.
   	 	if (((object)a == null) || ((object)b == null))
    		{
       		 	return false;
    		}

		    return a.Equals(b);//Equals should be overloaded as well
	}

	public static bool operator !=(NetGUID a, NetGUID b)
	{
   		 return a.OpNotEqual(b);
	}

	public static bool operator < (NetGUID a, NetGUID b)
	{
    		return a.OpLess(b);
	}

	public static bool operator >(NetGUID a, NetGUID b)
	{
		return a.OpGreater(b);
	}

	public static bool operator <=(NetGUID a, NetGUID b)
	{
		return (a.OpLess(b) || a==b);
	}

	public static bool operator >=(NetGUID a, NetGUID b)
	{
		return (a.OpGreater(b) || a==b);
	}

 	public void ToString(out string dest) 
	{
		dest = ToString();
	}
%}

%typemap(cscode) Jinra::AddressOrGUID
%{
	public static implicit operator AddressOrGUID(NetAddress systemAddress)
	{
		return new AddressOrGUID(systemAddress);
	} 

	public static implicit operator AddressOrGUID(NetGUID guid)
	{
		return new AddressOrGUID(guid);
	} 


%}

%typemap(csimports) Jinra::uint24_t
%{
using System;
using System.Runtime.InteropServices;
#pragma warning disable 0660
%}

%typemap(cscode) Jinra::uint24_t
%{

	public override int GetHashCode()
	{    
		return (int)this.val;
	}

	public static implicit operator uint24_t(uint inUint)
	{
		return new uint24_t(inUint);
	} 


	public static bool operator ==(uint24_t a, uint24_t b)
	{
 	   	// If both are null, or both are same instance, return true.
 		if (System.Object.ReferenceEquals(a, b))
 		{
 	       		return true;
 	   	}

  		// If one is null, but not both, return false.
   	 	if (((object)a == null) || ((object)b == null))
    		{
       		 	return false;
    		}

		    return a.Equals(b);//Equals should be overloaded as well
	}

	public static bool operator !=(uint24_t a, uint24_t b)
	{
   		 return a.OpNotEqual(b);
	}

	public static bool operator < (uint24_t a, uint24_t b)
	{
    		return a.OpLess(b);
	}

	public static bool operator >(uint24_t a, uint24_t b)
	{
		return a.OpGreater(b);
	}

	public static uint24_t operator +(uint24_t a, uint24_t b)
	{
		return a.OpPlus(b);
	}

	public static uint24_t operator ++(uint24_t a)
	{
		return a.OpPlusPlus();
	}

	public static uint24_t operator --(uint24_t a)
	{
		return a.OpMinusMinus();
	}

	public static uint24_t operator *(uint24_t a, uint24_t b)
	{
		return a.OpMultiply(b);
	}

	public static uint24_t operator /(uint24_t a, uint24_t b)
	{
		return a.OpDivide(b);
	}

	public static uint24_t operator -(uint24_t a, uint24_t b)
	{
		return a.OpMinus(b);
	}
//------------

	public static bool operator ==(uint24_t a, uint b)
	{
 	   	// If both are null, or both are same instance, return true.
 		if (System.Object.ReferenceEquals(a, b))
 		{
 	       		return true;
 	   	}

  		// If one is null, but not both, return false.
   	 	if (((object)a == null) || ((object)b == null))
    		{
       		 	return false;
    		}

		    return a.Equals(b);//Equals should be overloaded as well
	}

	public static bool operator !=(uint24_t a, uint b)
	{
   		 return a.OpNotEqual(b);
	}

	public static bool operator < (uint24_t a, uint b)
	{
    		return a.OpLess(b);
	}

	public static bool operator >(uint24_t a, uint b)
	{
		return a.OpGreater(b);
	}

	public static uint24_t operator +(uint24_t a, uint b)
	{
		return a.OpPlus(b);
	}

	public static uint24_t operator *(uint24_t a, uint b)
	{
		return a.OpMultiply(b);
	}

	public static uint24_t operator /(uint24_t a, uint b)
	{
		return a.OpDivide(b);
	}

	public static uint24_t operator -(uint24_t a, uint b)
	{
		return a.OpMinus(b);
	}

	public override string ToString()
	{
		return val.ToString();
	}

%}

%define STRUCT_UNSIGNED_CHAR_ARRAY_ONLY_CSCODE(BOOLNAME,CACHENAME)
%{
    private bool BOOLNAME = false;
    private byte[] CACHENAME;
%}
%enddef

//Packet->data related typemaps
%typemap(cscode) Jinra::Packet
STRUCT_UNSIGNED_CHAR_ARRAY_ONLY_CSCODE(dataIsCached,dataCache)

%typemap(cscode) Jinra::NetStatistics
%{

	private bool bytesInSendBufferIsCached  = false;
	private bool messageInSendBufferIsCached  = false;
	private bool runningTotalIsCached  = false;
	private bool valueOverLastSecondIsCached  = false;
	private double[] bytesInSendBufferCache;
	private uint[] messageInSendBufferCache;
	private ulong[] runningTotalCache;
	private ulong[] valueOverLastSecondCache;
%}

%define STRUCT_CUSTOM_UNSIGNED_CHAR_ARRAY_TYPEMAP(BOOLNAME,CACHENAME,CTYPE,IN_DATA_CHANGE_FUNCTION,IN_DATA_GET_FUNCTION,IN_CLASS,IN_LEN_METHOD)
%typemap(cstype, out="byte[]") CTYPE "byte[]"

%typemap(csvarin, excode=SWIGEXCODE2) CTYPE %{
	set 
	{
	    	CACHENAME=value;
		BOOLNAME = true;
		IN_DATA_CHANGE_FUNCTION (value, value.Length);
	    

	}
%}
%typemap(csvarout, excode=SWIGEXCODE2) CTYPE  %{
        get
        {
            byte[] returnBytes;
            if (!BOOLNAME)
            {
                IntPtr cPtr = JinraPINVOKE.IN_DATA_GET_FUNCTION (swigCPtr);
                int len = (int)((IN_CLASS)swigCPtr.Wrapper).IN_LEN_METHOD;
		if (len<=0)
		{
			return null;
		}
                returnBytes = new byte[len];
                Marshal.Copy(cPtr, returnBytes, 0, len);
                CACHENAME = returnBytes;
                BOOLNAME = true;
            }
            else
            {
                returnBytes = CACHENAME;
            }
            return returnBytes;
        }
 %}
%enddef