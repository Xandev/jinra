//----------------------------Method modifiers---------------------
//These modify the method types by default it uses just publix x, here you can specify ovverideds private functions
//and such. Many times this is used to hide the helper functions from the user

//BitStream
%csmethodmodifiers Jinra::BitStream::CSharpStringReader "private"
%csmethodmodifiers Jinra::BitStream::CSharpStringReaderCompressedDelta "private"
%csmethodmodifiers Jinra::BitStream::CSharpStringReaderCompressed "private"
%csmethodmodifiers Jinra::BitStream::CSharpStringReaderDelta "private"
%csmethodmodifiers Jinra::BitStream::CSharpByteReader(unsigned char* inOutByteArray,unsigned int numberOfBytes) "private"
%csmethodmodifiers Jinra::BitStream::CSharpCopyDataHelper(unsigned char* inOutByteArray) "private"

%csmethodmodifiers Jinra::NetPeer::CSharpGetOfflinePingResponseHelper(unsigned char* inOutByteArray, unsigned int* outLength) "private"
%csmethodmodifiers Jinra::NetPeer::GetBandwidth() "private"

%csmethodmodifiers Jinra::NetGUID::ToString() const "public override"

%csmethodmodifiers  Jinra::statisticsToStringHelper "private"

%csmethodmodifiers DataStructures::List <unsigned short>::GetHelper "private"
%csmethodmodifiers DataStructures::List <unsigned short>::PopHelper "private"

//Operators
%csmethodmodifiers operator > "private"
%csmethodmodifiers operator < "private"
%csmethodmodifiers operator != "private"
%csmethodmodifiers operator [] "private"
%csmethodmodifiers operator >= "private"
%csmethodmodifiers operator <= "private"
%csmethodmodifiers operator / "private"
%csmethodmodifiers operator * "private"
%csmethodmodifiers operator -- "private"
%csmethodmodifiers operator ++ "private"
%csmethodmodifiers operator - "private"
%csmethodmodifiers operator + "private"