#ifdef SWIGWIN
#define _MSC_VER 1800
#define WIN32
#define _WIN32
#endif

typedef int int32_t;
typedef unsigned int uint32_t;
typedef uint32_t DefaultIndexType;

// Global Inserts
#define SWIG_CSHARP_NO_IMCLASS_STATIC_CONSTRUCTOR 1;

%include "JinraMacros.i"