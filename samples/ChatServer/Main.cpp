#include <iostream>
#include "Jinra.h"
#include "NetServer.h"
#include "NetTime.h"

using namespace Jinra;

class ChatServerListener : public NetServerListener
{
    void onClientConnect() override;

    void onClientDisconnect() override;

    void onClientLostConnection() override;

    void handleMessage(u8 packetId, BitStream* bitStream) override;
};

void ChatServerListener::onClientConnect()
{
    std::cout << "Client connected." << std::endl;
}

void ChatServerListener::onClientDisconnect()
{
    std::cout << "Client disconnected." << std::endl;
}

void ChatServerListener::onClientLostConnection()
{
    std::cout << "Client lost connection." << std::endl;
}

void ChatServerListener::handleMessage(u8 packetId, BitStream* bitStream) 
{
    TimeMS n = Time::nowMS();

    TimeMS t;
    bitStream->read(t);
    
    String str;
    bitStream->read(str);

    std::cout << str << std::endl;
    std::cout << "Roznica w czasie: " << n - t << std::endl;
    auto packet = getServer()->getPacket();
    std::cout << "Roznica w zegarze: " << getServer()->getPeer()->getClockDifferential(packet->guid) << std::endl;
    std::cout << "Ostatni odebrany ping: " << getServer()->getPeer()->getLastPing(packet->guid) << std::endl;
}

int main()
{
    NetServer chatServer;
    chatServer.initialize(42069, 2);

    NetServerListenerPtr listener(new ChatServerListener());
    chatServer.setListener(listener);

    while (true) {
        chatServer.receive();
    }

    return 0;
}