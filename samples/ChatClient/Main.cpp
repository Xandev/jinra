#include <iostream>
#include "Jinra.h"
#include "NetTime.h"
#include "NetClient.h"

using namespace Jinra;

class ChatClientListener : public NetClientListener
{
public:
    void onConnect() override;

    void onDisconnect() override;

    void onLostConnection() override;

    void handleMessage(u8 packetId, BitStream* bitStream) override;
};

void ChatClientListener::onConnect()
{
    std::cout << "Connected." << std::endl;
}

void ChatClientListener::onDisconnect()
{
    std::cout << "Disconnected." << std::endl;
}

void ChatClientListener::onLostConnection()
{
    std::cout << "Lost connection." << std::endl;
}

void ChatClientListener::handleMessage(u8 packetId, BitStream* bitStream) 
{
    std::cout << "Message." << std::endl;
}

int main()
{
    NetClient chatClient;
    chatClient.initialize(7777);
    chatClient.connect("86.111.112.99", 42069);
    
    NetClientListenerPtr listener(new ChatClientListener());
    chatClient.setListener(listener);

    int i = 0;
    
    while (true) {
    
        chatClient.receive();
    
        if (i == 0 && chatClient.getPeer()->getConnectionState(chatClient.getPeer()->GetSystemAddressFromIndex(0)) == NetPeer::ConnectionState::IS_CONNECTED) {
            BITSTREAM(b, 200);
            TimeMS n = Time::nowMS();
            b.write(n);
            b.write("TEST");
            
            chatClient.send(&b, Packet::IMMEDIATE_PRIORITY);
            std::cout << "Ostatni odebrany ping: " << chatClient.getPeer()->getLastPing(chatClient.getPeer()->GetSystemAddressFromIndex(0)) << std::endl;
            i = 1;
        }
    }

    return 0;
}