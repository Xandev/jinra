# Jinra
  
**Jinra** is a cross-platform network library based on [RakNet](https://github.com/OculusVR/RakNet).  
  
### Why Jinra instead of RakNet?
-   Modern C++11
-   Additional helpful classes such as Client and Server (less code to write, easier to understand and manage)
-   No more obsolete modules that are never used in 99.99% situations (faster build time and smaller size)
-   Not abandoned ;)
  
### Not finished yet
Jinra is still in development stage.  
Here is a list of files that require hard refactoring:  
 
-   NetDefines.h
-   BitStream.h
-   BitStream.cpp
-   ReliabilityLayer.h
-   ReliabilityLayer.cpp
-   NetPeer.h
-   NetPeer.cpp
-   MessageIdentifiers.h
-   Jinra.h
-   Jinra.cpp
-   All DS_*.h / DS_*.cpp (they should be replaced by STL containers)

### Supported platforms
-   Windows
-   Linux
-   Android
  
### How to build
Windows:  
1)   Open Jinra.sln, it's Visual Studio 2013 solution.  
2)   Choose your configuration for compilation (Debug, Release etc.).  
3)   Build.  
4)   Go to $(JINRA_HOME)/build/$(CONFIGURATION)/ for *.lib.  
  
Linux:  
1)   cd build  
2)   cmake ..   
3)   make  
4)   If there is no error, you should see newly created libjinra.a file.  
  
Android:  
a) On Windows:  
Just run Windows-AndroidBuild.cmd. Android NDK (r10e or newer) is required.  
  
b) On Linux:  
1) ...  
  
#### What about OS X & iOS? 
Currently, there's no projects to build for those platforms.  
Just because I don't have device to build and test, but I'm sure that Jinra should support those platforms as RakNet does.
  
If there is someone who have the OS X and want to help, feel free to contact me.

### Jinra for C#/Unity3D? (SWIG)
Soon after hard refactoring of Jinra.