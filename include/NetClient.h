#pragma once

#include "NetPeer.h"
#include "NetClientListener.h"

namespace Jinra
{

class JINRA_DLL_EXPORT NetClient
{
public:
	NetClient();

	virtual ~NetClient();

	NetPeer::StartupResult initialize(u16 port = 0) const;

    void connect(const String& ip, u16 port);

	void connect(const String& ip, u16 port, const String& connectionPassword);

	void reconnect(const String& connectionPassword) const;

	void receive();

	void send(const BitStream* bitStream, Packet::Priority priority = Packet::IMMEDIATE_PRIORITY) const;

	void shutdown() const;

    NetPeer* getPeer() const {
        return _peer.get();
	}

    Packet* getPacket() const {
        return _packet;
    }

    void setListener(NetClientListenerPtr listener) {
        _listener = listener;
        _listener->setClient(this);
    }

protected:
    NetPeerPtr _peer;
	NetAddress _connectionAddress;
    NetClientListenerPtr _listener;
    Packet* _packet;
};

} // namespace Jinra