﻿#pragma once

namespace Jinra
{

class BitStream;

class JINRA_DLL_EXPORT NetServerListener
{
    friend class NetServer;

public:
    NetServerListener();

    virtual ~NetServerListener();

    virtual void onClientConnect() { }

    virtual void onClientDisconnect() { }

    virtual void onClientLostConnection() { }

    virtual void handleMessage(u8 packetId, BitStream* bitStream) { }

    NetServer* getServer() const {
        return _server;
    }

private:
    void setServer(NetServer* server) {
        _server = server;
    }

private:
    NetServer* _server;
};

} // namespace Jinra