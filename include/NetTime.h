#pragma once

namespace Jinra
{

typedef u64 TimeMS;
typedef u64 TimeNS;
typedef u64 TimeUS;

class JINRA_DLL_EXPORT Time
{
public:
    static TimeMS nowMS();

    static TimeNS nowNS();

    static TimeUS nowUS();

    static TimeMS nsToMS(TimeNS time);

    static double nsToMS(TimeNS time, byte precise);

    static TimeNS msToNS(TimeMS time);

    static void sleepMS(TimeMS time);

    template<typename Func, typename... Args>
    static TimeMS getExecutionTimeInMS(Func function, Args&&... args)
    {
        auto startTime = nowMS();
        function(std::forward<Args>(args)...);
        return nowMS() - startTime;
    }

    template<typename Func, typename... Args>
    static TimeNS getExecutionTimeInNS(Func function, Args&&... args)
    {
        auto startTime = nowNS();
        function(std::forward<Args>(args)...);
        return nowNS() - startTime;
    }
};

} // namespace Jinra