#pragma once

#include <thread>
#include <atomic>

namespace Jinra
{

enum class ThreadState
{
    RUNNING, 
    CLOSING, 
    TERMINATED
};

template<typename T>
class Thread
{
public:
    Thread()
        : _threadState(ThreadState::TERMINATED) {
    }

    void start() {
        setState(ThreadState::RUNNING);
        _thread = std::thread(&T::main, static_cast<T *>(this));
    }

    void join() {
        if (_thread.joinable()) {
            _thread.join();
        }
    }

    void stop() {
        setState(ThreadState::CLOSING);
    }

protected:
    ThreadState getState() const {
        return _threadState.load(std::memory_order_relaxed);
    }

    void setState(ThreadState newState) {
        _threadState.store(newState, std::memory_order_relaxed);
    }

private:
    std::atomic<ThreadState> _threadState;
    std::thread _thread;
};

} // namespace Jinra