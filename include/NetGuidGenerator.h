﻿#pragma once

namespace Jinra
{

class NetGuidGenerator
{
public:
    static u64 get64BitUniqueRandomNumber();
};

} // namespace Jinra