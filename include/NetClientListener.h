﻿#pragma once

namespace Jinra
{

class BitStream;

class JINRA_DLL_EXPORT NetClientListener
{
    friend class NetClient;

public:
    NetClientListener();

    virtual ~NetClientListener();

    virtual void onConnect() { }

    virtual void onAlreadyConnected() { }

    virtual void onNoFreeIncomingConnections() { }

    virtual void onDisconnect() { }

    virtual void onLostConnection() { }
    
    virtual void onBanned() { }
    
    virtual void onInvalidPassword() { }
    
    virtual void onIncompatibleProtocolVersion() { }

    virtual void handleMessage(u8 packetId, BitStream* bitStream) { }

    NetClient* getClient() const {
        return _client;
    }

private:
    void setClient(NetClient* client) {
        _client = client;
    }

private:
    NetClient* _client;
};

} // namespace Jinra