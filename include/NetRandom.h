#pragma once

#include <random>

namespace Jinra
{

class NetRandom
{
public:
    static std::mt19937& rng() {
        thread_local std::random_device rd;
        thread_local std::mt19937 generator(rd());
        return generator;
    }

    static float next(float min, float max) {
        thread_local std::uniform_real_distribution<float> uniformRand;
        if (min == max) {
            return min;
        }
        if (min > max) {
            std::swap(min, max);
        }
        return uniformRand(rng(), std::uniform_real_distribution<float>::param_type(min, max));
    }

    static s32 next(s32 min, s32 max) {
        thread_local std::uniform_int_distribution<s32> uniformRand;
        if (min == max) {
            return min;
        } 
        if (min > max) {
            std::swap(min, max);
        }
        return uniformRand(rng(), std::uniform_int_distribution<s32>::param_type(min, max));
    }
};

} // namespace Jinra