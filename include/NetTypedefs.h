﻿#pragma once

#if defined(JINRA_NAMESPACED_TYPEDEF)
namespace Jinra
{
#endif // defined(JINRA_NAMESPACED_TYPEDEF)

typedef const char cchar;

typedef int8_t s8;
typedef uint8_t u8;
typedef int16_t s16;
typedef uint16_t u16;
typedef int32_t s32;
typedef uint32_t u32;
typedef int64_t s64;
typedef uint64_t u64;

typedef s8 sbyte;
typedef u8 byte;
typedef unsigned long ulong;

typedef std::string String;

#if defined(JINRA_NAMESPACED_TYPEDEF)
} // namespace Jinra
#endif // defined(JINRA_NAMESPACED_TYPEDEF)

namespace Jinra
{

class NetPeer;
using NetPeerPtr = std::shared_ptr<NetPeer>;
using NetPeerWeakPtr = std::weak_ptr<NetPeer>;

class NetServerListener;
using NetServerListenerPtr = std::shared_ptr<NetServerListener>;

class NetServer;
using NetServerWeakPtr = std::weak_ptr<NetServer>;

class NetClient;
using NetClientWeakPtr = std::weak_ptr<NetClient>;

class NetClientListener;
using NetClientListenerPtr = std::shared_ptr<NetClientListener>;

} // namespace Jinra