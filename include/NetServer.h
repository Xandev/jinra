#pragma once

#include "NetPeer.h"
#include "NetServerListener.h"

namespace Jinra
{

class JINRA_DLL_EXPORT NetServer
{
public:
	NetServer();

	virtual ~NetServer();

	NetPeer::StartupResult initialize(u16 port, u32 maxConnections) const;

	NetPeer::StartupResult initialize(u16 port, u32 maxConnections, const String& connectionPassword) const;

	void receive();

	u32 send(const BitStream* bitStream, Packet::Priority priority, const NetGUID& guid) const;

	u32 sendToAll(const BitStream* bitStream, Packet::Priority priority) const;

	u32 sendToAllExcept(const BitStream* bitStream, Packet::Priority priority,
						const NetGUID& guid) const;

	void shutdown() const;

    NetPeer* getPeer() const {
        return _peer.get();
	}

    Packet* getPacket() const {
        return _packet;
    }

    void setListener(NetServerListenerPtr listener) {
        _listener = listener;
        _listener->setServer(this);
	}

protected:
    NetPeerPtr _peer;
    NetServerListenerPtr _listener;
    Packet* _packet;
};

} // namespace Jinra