#pragma once

namespace Jinra
{

/// The namespace DataStructures was only added to avoid compiler errors for commonly named data structures
/// As these data structures are stand-alone, you can use them outside of Jinra for your own projects if you wish.
namespace DataStructures
{
template <class weight_type, class data_type, bool isMaxHeap>
class JINRA_DLL_EXPORT Heap
{
public:
	struct HeapNode
	{
		HeapNode() {}
		HeapNode(const weight_type &w, const data_type &d) : weight(w), data(d) {}
		weight_type weight; // I'm assuming key is a native numerical type - float or int
		data_type data;
	};

	Heap() {
        optimizeNextSeriesPush = false;
    }

	~Heap() {
        //Clear(true, _FILE_AND_LINE_);
    }

	void Push(const weight_type &weight, const data_type &data);

	/// Call before calling PushSeries, for a new series of items
	void StartSeries() {
	    optimizeNextSeriesPush = false;
	}

	/// If you are going to push a list of items, where the weights of the items on the list are in order and follow the heap order, PushSeries is faster than Push()
	void PushSeries(const weight_type &weight, const data_type &data);

	data_type Pop(const u32 startingIndex);

	data_type Peek(const u32 startingIndex = 0u) const {
        return heap[startingIndex].data;
    }

	weight_type PeekWeight(const u32 startingIndex = 0u) const {
        return heap[startingIndex].weight;
    }

	void clear() {
        heap.clear();
    }

	data_type& operator[] (const u32 position) {
        return heap[position].data;
    }

    u32 size() const {
        return heap.size();
    }

protected:
    static u32 LeftChild(const u32 i) {
        return i * 2 + 1;
    }

    static u32 RightChild(const u32 i) {
        return i * 2 + 2;
    }

    static u32 Parent(const u32 i) {
#ifdef JINRA_DEBUG
        ASSERT(i != 0);
#endif
        return (i - 1) / 2;
    }

	void Swap(const u32 i, const u32 j) {
        auto temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }
	
protected:
    std::vector<HeapNode> heap;
    bool optimizeNextSeriesPush;
};

template  <class weight_type, class data_type, bool isMaxHeap>
void Heap<weight_type, data_type, isMaxHeap>::PushSeries(const weight_type &weight, const data_type &data)
{
	if (optimizeNextSeriesPush == false) {
		// If the weight of what we are inserting is greater than / less than in order of the heap of every sibling and sibling of parent, then can optimize next push
		u32 currentIndex = heap.size();
	    if (currentIndex > 0) {
			for (u32 parentIndex = Parent(currentIndex); parentIndex < currentIndex; parentIndex++) {
				if (isMaxHeap) {
					// Every child is less than its parent
					if (weight > heap[parentIndex].weight) {
						// Can't optimize
						Push(weight, data);
						return;
					}
				} else {
					// Every child is greater than than its parent
					if (weight < heap[parentIndex].weight) {
						// Can't optimize
						Push(weight, data);
						return;
					}
				}
			}
		}

		// Parent's subsequent siblings and this row's siblings all are less than / greater than inserted element. Can insert all further elements straight to the end
		heap.push_back(HeapNode(weight, data));
		optimizeNextSeriesPush = true;
	} else {
		heap.push_back(HeapNode(weight, data));
	}
}

template  <class weight_type, class data_type, bool isMaxHeap>
void Heap<weight_type, data_type, isMaxHeap>::Push(const weight_type &weight, const data_type &data)
{
	u32 currentIndex = heap.size();
    heap.push_back(HeapNode(weight, data));
	while (currentIndex != 0) {
		u32 parentIndex = Parent(currentIndex);

		if (isMaxHeap) {
			if (heap[parentIndex].weight < weight) {
				Swap(currentIndex, parentIndex);
				currentIndex = parentIndex;
			} else
				break;
		} else {
			if (heap[parentIndex].weight > weight) {
				Swap(currentIndex, parentIndex);
				currentIndex = parentIndex;
			} else
				break;
		}
	}
}

template  <class weight_type, class data_type, bool isMaxHeap>
data_type Heap<weight_type, data_type, isMaxHeap>::Pop(const u32 startingIndex)
{
	// While we have children, swap out with the larger of the two children.

	// This line will assert on an empty heap
	data_type returnValue = heap[startingIndex].data;

	// Move the last element to the head, and re-heapify
	heap[startingIndex] = heap[heap.size() - 1];

    u32 currentIndex = startingIndex;
	weight_type currentWeight = heap[startingIndex].weight;
	heap.pop_back();

	while (true) {
		u32 leftChild = LeftChild(currentIndex);
		u32 rightChild = RightChild(currentIndex);
		if (leftChild >= heap.size()) {
			// Done
			return returnValue;
		}
		if (rightChild >= heap.size()) {
			// Only left node.
			if ((isMaxHeap == true && currentWeight < heap[leftChild].weight) ||
				(isMaxHeap == false && currentWeight > heap[leftChild].weight))
				Swap(leftChild, currentIndex);

			return returnValue;
		}
	    // Swap with the bigger/smaller of the two children and continue
	    if (isMaxHeap) {
	        if (heap[leftChild].weight <= currentWeight && heap[rightChild].weight <= currentWeight)
	            return returnValue;

	        if (heap[leftChild].weight > heap[rightChild].weight) {
	            Swap(leftChild, currentIndex);
	            currentIndex = leftChild;
	        } else {
	            Swap(rightChild, currentIndex);
	            currentIndex = rightChild;
	        }
	    } else {
	        if (heap[leftChild].weight >= currentWeight && heap[rightChild].weight >= currentWeight)
	            return returnValue;

	        if (heap[leftChild].weight < heap[rightChild].weight) {
	            Swap(leftChild, currentIndex);
	            currentIndex = leftChild;
	        } else {
	            Swap(rightChild, currentIndex);
	            currentIndex = rightChild;
	        }
	    }
	}
}

}

}