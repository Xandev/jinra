#pragma once

#include "Thread.h"

namespace Jinra
{

class NetTask;

class NetTaskDispatcher : public Thread<NetTaskDispatcher>
{
public:
    static NetTaskDispatcher& getInstance() {
        static NetTaskDispatcher instance;
        return instance;
    }
    
    void main();

    void addTask(NetTask* task, bool pushFront = false);

    void shutdown();

    u64 getDispatcherCycle() const {
        return _dispatcherCycle;
    }

private:
    NetTaskDispatcher();

private:
    std::mutex _taskLock;
    std::condition_variable _taskSignal;
    std::list<NetTask*> _taskList;
    u64 _dispatcherCycle;
};


} // namespace Jinra
