#pragma once

#include "Thread.h"

namespace Jinra
{

class NetSchedulerTask;

class NetTaskScheduler : public Thread<NetTaskScheduler>
{
private:
    struct TaskComparator
    {
        bool operator()(const NetSchedulerTask* lhs, const NetSchedulerTask* rhs) const;
    };

public:
    static NetTaskScheduler& getInstance() {
        static NetTaskScheduler instance;
        return instance;
    }

    void main();

    u32 addEvent(NetSchedulerTask* task);

    bool stopEvent(u32 eventId);

    void shutdown();

private:
    NetTaskScheduler();

private:
    std::mutex _eventLock;
    std::condition_variable _eventSignal;
    u32 _lastEventId;
    std::priority_queue<NetSchedulerTask*, std::deque<NetSchedulerTask*>, TaskComparator> _eventList;
    std::unordered_set<u32> _eventIds;
};

} // namespace Jinra

