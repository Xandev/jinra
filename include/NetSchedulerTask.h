#pragma once

#include "NetTask.h"

namespace Jinra
{

class NetSchedulerTask : public NetTask
{
public:
    NetSchedulerTask(std::function<void()>&& f, u32 delay);

    u32 getEventId() const {
        return _eventId;
    }

    void setEventId(u32 id) {
        _eventId = id;
    }

    std::chrono::system_clock::time_point getCycle() const {
        return _expiration;
    }

protected:
    u32 _eventId = 0u;
};

} // namespace Jinra