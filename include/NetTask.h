#pragma once

namespace Jinra
{

class NetTask
{
public:
    explicit NetTask(std::function<void()> f);

    NetTask(std::function<void()> f, u32 delay);

    virtual ~NetTask() = default;

    void operator()() const;

    void setDontExpire();

    bool hasExpired() const;

protected:
    std::function<void()> _func;
    // Expiration has another meaning for scheduler tasks,
    // then it is the time the task should be added to the
    // dispatcher
    std::chrono::system_clock::time_point _expiration;
};

} // namespace Jinra
