LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libJinra
SRC_FILES := $(wildcard $(LOCAL_PATH)/../../src/*.cpp)
SRC_FILES := $(SRC_FILES:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES += $(SRC_FILES)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../include
LOCAL_CFLAGS := -frtti -fsigned-char

include $(BUILD_STATIC_LIBRARY)
